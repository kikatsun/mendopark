package com.mendopark.service.driver;

import javax.ejb.Singleton;
import java.util.*;

@Singleton
public class ParkingAreaDriver {

    /* Map<clientNumber, List<parkingAreaNumber> */
    private Map<Long, List<Long>> parkingAreasFrom = new HashMap<Long, List<Long>>();

    public void addParkingArea (Long clientNumber, Long parkingAreaNumber) {
        List<Long> parkingAreas;
        if (parkingAreasFrom.containsKey(clientNumber)){
            parkingAreas = parkingAreasFrom.get(clientNumber);
        }else {
            parkingAreas = new ArrayList<Long>();
        }
        parkingAreas.add(parkingAreaNumber);
        parkingAreasFrom.put(clientNumber, parkingAreas);
    }

    public Long getClientFrom (Long parkingAreaNumber) {
        Long clientNumber = null;
        Iterator<Long> iterator = parkingAreasFrom.keySet().iterator();
        while (iterator.hasNext()) {
            Long iteratorClientNumber = iterator.next();
            List<Long> parkingAreas = parkingAreasFrom.get(iteratorClientNumber);
            if (parkingAreas.contains(parkingAreaNumber)){
                clientNumber = iteratorClientNumber;
                break;
            }
        }
        return clientNumber;
    }

    public boolean isParkingAreaFromClient (Long clientNumber, Long parkingAreaNumber) {
        boolean result = false;
        if (parkingAreasFrom.containsKey(clientNumber) && parkingAreasFrom.get(clientNumber).contains(parkingAreaNumber)) {
            result = true;
        }
        return result;
    }

}
