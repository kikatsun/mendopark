package com.mendopark.service.security;

import com.mendopark.model.Role;
import com.mendopark.model.security.GrantType;
import com.mendopark.model.security.Resource;
import com.mendopark.model.security.RoleResourceAssignment;
import com.mendopark.repository.RoleResourceAssignmentRepository;
import com.mendopark.rest.role.RoleController;
import com.mendopark.service.MEndoParkLogger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.logging.Level;

@Startup
@Singleton
public class RoleResourceManager {

    @EJB
    private RoleResourceAssignmentRepository roleResourceAssignmentRepository;

    /* RoleId, List<RoleResourceAssignment> */
    private Map<Integer, List<RoleResourceAssignment>> roleResourceAssignments = new HashMap<>();

    /**
     * Agrega una asignación al role indicado, en caso ya exista una asignación para el mismo rol y recurso, es
     * reemplazada por la nueva asignación, esto último sirve para no tener asiganciones repetidas en un mismo rol.
     *
     * @param newAssignment
     * @throws InvalidParameterException
     */
    public void addAssignment (RoleResourceAssignment newAssignment) throws InvalidParameterException {
        validateAssignment (newAssignment);
        if (newAssignment.getEndDate() != null) {
            throw new InvalidParameterException(newAssignment.toString() + " está dada de baja.");
        }
        Integer roleNumber = newAssignment.getFromRole().getNumber();
        List<RoleResourceAssignment> assignments;
        if(roleResourceAssignments.containsKey(roleNumber)){
            assignments = removeRoleResourceAssignments(roleResourceAssignments.get(roleNumber), newAssignment);
        }else {
            assignments = new ArrayList<>();
        }
        MEndoParkLogger.MENDOPARK.log(Level.INFO, "Se agregó "+newAssignment.toString());
        assignments.add(newAssignment);
        roleResourceAssignments.put(roleNumber, assignments);
    }

    public void removeAssignment (RoleResourceAssignment assignment) throws InvalidParameterException {
        validateAssignment(assignment);
        Integer roleNumber = assignment.getFromRole().getNumber();
        if (roleResourceAssignments.containsKey(roleNumber)) {
            roleResourceAssignments.put(
                roleNumber,
                removeRoleResourceAssignments(roleResourceAssignments.get(roleNumber), assignment));
        }
    }

    public void removeRole (Integer roleId){
        roleResourceAssignments.remove(roleId);
    }

    private List<RoleResourceAssignment> removeRoleResourceAssignments (
            List<RoleResourceAssignment> assignments, RoleResourceAssignment assignment) {
        Integer indexToRemove = null;
        for (int i = 0; i < assignments.size(); i++) {
            RoleResourceAssignment roleResourceAssignment = assignments.get(i);
            if (assignment.getToResource().getId().equals(roleResourceAssignment.getToResource().getId())) {
                indexToRemove = i;
                break;
            }
        }
        if(indexToRemove != null){
            assignments.remove(indexToRemove.intValue());
        }
        return assignments;
    }

    private void validateAssignment (RoleResourceAssignment assignment) throws InvalidParameterException{
        if (assignment.getId() == null || assignment.getFromRole() == null || assignment.getToResource() == null
            || assignment.getFromRole().getNumber() == null || assignment.getToResource().getId() == null) {
            throw new InvalidParameterException(assignment.toString());
        }
    }

    public List<Resource> getResources (Role role) {
        List<Resource> resources = new ArrayList<>();
        if (role.getNumber() != null && roleResourceAssignments.containsKey(role.getNumber())) {
            List<RoleResourceAssignment> assignments = roleResourceAssignments.get(role.getNumber());
            if (assignments != null && assignments.size() > 0) {
                for (RoleResourceAssignment assignment: assignments ) {
                    if(assignment.getToResource() != null){
                        resources.add(assignment.getToResource());
                    }else {
                        MEndoParkLogger.MENDOPARK.log(
                            Level.WARNING, this.toString() + ": Recurso null para "+role.toString());
                    }
                }
            }
        }
        return resources;
    }

    public List<RoleResourceAssignment> getAssignments(Integer number) {
        return roleResourceAssignments.get(number);
    }

    public List<Role> getRoles (Resource resource){
        List<Role> roles = new ArrayList<>();
        String resourceId = resource.getId();
        Iterator<Integer> roleResourceIterator = roleResourceAssignments.keySet().iterator();
        while (roleResourceIterator.hasNext()){
            Integer roleId = roleResourceIterator.next();
            List<RoleResourceAssignment> assignments = roleResourceAssignments.get(roleId);
            if (assignments != null && assignments.size() > 0) {
                roles: for (RoleResourceAssignment assignment: assignments ) {
                    if (resourceId.equals(assignment.getToResource().getId())){
                        roles.add(assignment.getFromRole());
                        break roles;
                    }
                }
            }
        }
        return roles;
    }

    public List<Role> getAllowedRoles(Resource resource, List<GrantType.Type> grantTypes) {
        List<Role> result = new ArrayList<>();
        if (resource != null) {
            Iterator<Integer> roleIdItertator = roleResourceAssignments.keySet().iterator();
            while (roleIdItertator.hasNext()) {
                Integer roleId = roleIdItertator.next();
                List<RoleResourceAssignment> assignments = roleResourceAssignments.get(roleId);
                if (assignments != null && assignments.size() > 0) {
                    found:
                    for (int i = 0; i < assignments.size(); i++) {
                        RoleResourceAssignment assignment = assignments.get(i);
                        if (assignment.getToResource() != null
                            && assignment.getFromRole() != null
                            && assignment.getToResource().getId().equals(resource.getId())
                            && assignment.getGrantType() != null
                            && grantTypes.contains(GrantType.Type.getTypeById(assignment.getGrantType().getNumber()))) {
                            result.add(assignment.getFromRole());
                            break found;
                        }
                    }
                }
            }
        }
        return result;
    }

    public List<Integer> getAllRolesIds () {
        return new ArrayList<>(roleResourceAssignments.keySet());
    }

    @PostConstruct
    public void init () {
        roleResourceAssignments = new HashMap<>();
        List<RoleResourceAssignment> assignments = roleResourceAssignmentRepository.getAvailables();
        if(assignments != null && assignments.size() > 0) {
            for (RoleResourceAssignment assignment: assignments ) {
                try {
                    addAssignment(assignment);
                } catch (InvalidParameterException ex) {
                    MEndoParkLogger.MENDOPARK.log(Level.WARNING, "No se pudo agregar los permisos de asignación.", ex);
                }
            }
        }
        /* En caso no esté en la BD la asignación del recurso role al administrador del sistema se carga en memoria dicho permiso */
        boolean needRoleResourceRootAssiggnment = true;
        if (roleResourceAssignments != null && roleResourceAssignments.size() > 0) {
            List<RoleResourceAssignment> rootAssignments = roleResourceAssignments.get(Role.Type.SYSTEM_ADMIN.getRoleId());
            if (rootAssignments != null && rootAssignments.size() > 0) {
                for (RoleResourceAssignment rootAssignemt: rootAssignments ) {
                    if(rootAssignemt.getToResource() != null && rootAssignemt.getToResource().getName().equals(RoleController.RESOURCE_NAME)){
                        needRoleResourceRootAssiggnment = false;
                        break;
                    }
                }
            }
        }
        if (needRoleResourceRootAssiggnment) {
            RoleResourceAssignment rootAssignment = new RoleResourceAssignment();
            Resource roleResource = new Resource();
            roleResource.setName(RoleController.RESOURCE_NAME);
            roleResource.setId(UUID.randomUUID().toString());
            roleResource.setCreateDate(new Date());
            rootAssignment.setToResource(roleResource);
            Role rootRole = new Role();
            rootRole.setNumber(Role.Type.SYSTEM_ADMIN.getRoleId());
            rootAssignment.setFromRole(rootRole);
            GrantType persistedGrant = new GrantType();
            persistedGrant.setNumber(GrantType.Type.CREATE.getId());
            rootAssignment.setGrantType(persistedGrant);
            addAssignment(rootAssignment);
        }
    }

    public List<RoleResourceAssignment> gett(Integer roleId){
        return roleResourceAssignments.get(roleId);
    }

}
