package com.mendopark.service.security;

import com.mendopark.model.security.Resource;

import java.util.List;

public interface Resourceable {


    public abstract String getResourceName();

    public abstract Resource getResource();

    public abstract void setResource(Resource resource);

}
