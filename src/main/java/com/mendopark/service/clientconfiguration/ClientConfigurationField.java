package com.mendopark.service.clientconfiguration;

public enum ClientConfigurationField {

    MANUAL_PARKING_SPACE_LOCK_TIME,
    PARKING_SPACE_PRICE,
    RESERVATION_TIMEOUT

}
