package com.mendopark.service.clientconfiguration;

import com.mendopark.model.Client;
import com.mendopark.model.ClientConfiguration;

public class ClientConfigurationDto {

    private String id;
    private Long clientNumber;
    private String companyName;
    private Float parkingSpacePrice;
    private Long manualParkingSpaceLockTime;
    private Long reservationTimeout;
    private WeeklySchedulesWorkDto weeklySchedulesWorkDto;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Float getParkingSpacePrice() {
        return parkingSpacePrice;
    }

    public void setParkingSpacePrice(Float parkingSpacePrice) {
        this.parkingSpacePrice = parkingSpacePrice;
    }

    public Long getManualParkingSpaceLockTime() {
        return manualParkingSpaceLockTime;
    }

    public void setManualParkingSpaceLockTime(Long manualParkingSpaceLockTime) {
        this.manualParkingSpaceLockTime = manualParkingSpaceLockTime;
    }

    public Long getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Long clientNumber) {
        this.clientNumber = clientNumber;
    }

    public Long getReservationTimeout() {
        return reservationTimeout;
    }

    public void setReservationTimeout(Long reservationTimeout) {
        this.reservationTimeout = reservationTimeout;
    }

    public WeeklySchedulesWorkDto getWeeklySchedulesWorkDto() {
        return weeklySchedulesWorkDto;
    }

    public void setWeeklySchedulesWorkDto(WeeklySchedulesWorkDto weeklySchedulesWorkDto) {
        this.weeklySchedulesWorkDto = weeklySchedulesWorkDto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class Factory {
        public static ClientConfiguration getClientConfiguration (ClientConfigurationDto dto) {
            ClientConfiguration clientConfiguration = new ClientConfiguration();
            clientConfiguration.setCompanyName(dto.getCompanyName());

            if(dto.getId() != null){
                clientConfiguration.setId(dto.getId());
            }

            clientConfiguration.setManualParkingSpaceLockTime(dto.getManualParkingSpaceLockTime());
            clientConfiguration.setParkingSpacePrice(dto.getParkingSpacePrice());
            clientConfiguration.setReservationTimeout(dto.getReservationTimeout());

            if (dto.getClientNumber() != null) {
                Client client = new Client();
                client.setNumber(dto.getClientNumber());
                clientConfiguration.setFromClient(client);
            }

            return clientConfiguration;
        }

        public static ClientConfigurationDto getDto (ClientConfiguration clientConfiguration) {
            ClientConfigurationDto dto = new ClientConfigurationDto();
            dto.setId(clientConfiguration.getId());
            if (clientConfiguration.getFromClient() != null && clientConfiguration.getFromClient().getNumber() != null) {
                dto.setClientNumber(clientConfiguration.getFromClient().getNumber());
            }

            dto.setCompanyName(clientConfiguration.getCompanyName());
            if(clientConfiguration.getManualParkingSpaceLockTime() != null){
                dto.setManualParkingSpaceLockTime(clientConfiguration.getManualParkingSpaceLockTime()/60000);
            }
            if(clientConfiguration.getReservationTimeout() != null){
                dto.setReservationTimeout(clientConfiguration.getReservationTimeout()/60000);
            }
            dto.setParkingSpacePrice(clientConfiguration.getParkingSpacePrice());

            return dto;
        }
    }
}
