package com.mendopark.service.clientconfiguration;

import com.mendopark.model.ClientConfiguration;
import com.mendopark.repository.ClientConfigurationRepository;
import com.mendopark.repository.ClientRepository;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Startup
@Singleton
public class ClientConfigurationManager {

    @EJB
    private ClientConfigurationRepository clientConfigurationRepository;
    @EJB
    private ClientRepository clientRepository;

    public static final String DEFAULT_COMPANY_NAME = "MendoPark";
    public static final Long DEFAULT_CLIENT_NUMBER = 1L;
    public static final Long DEFAULT_MANUAL_PARKING_SPACE_LOCK_TIME = 15000L; // 15 segundos
    public static final Float DEFAULT_PARKING_SPACE_PRICE = 50F;
    public static final Long DEFAULT_RESERVATION_TIMEOUT = 1800000L; // 30 minutos

    private Map<Long, ClientConfiguration> configurations;

    public ClientConfigurationManager () {
        configurations = new HashMap<Long, ClientConfiguration>();
    }


    public void start(){
        configurations.put(DEFAULT_CLIENT_NUMBER, buildDefaultClientConfiguration());
    }

    public ClientConfiguration getClientConfiguration (Long clientNumber) {
        return configurations.get(clientNumber);
    }

    public void addClientConfiguration (ClientConfiguration clientConfiguration) {
        if (clientConfiguration.getFromClient() != null && clientConfiguration.getFromClient().getNumber() != null) {
            configurations.put(clientConfiguration.getFromClient().getNumber(), clientConfiguration);
        }else {
            // error grave
        }
    }

    public Long getManualParkingSpaceLockTime (Long clientNumber) {
        Long manualParkingSpaceLockTime = null;
        if (configurations.containsKey(clientNumber)){
            manualParkingSpaceLockTime = configurations.get(clientNumber).getManualParkingSpaceLockTime();
        } else {
            // se loguea una info
            if(configurations.size() == 0){
                start();
            }
            manualParkingSpaceLockTime = getManualParkingSpaceLockTime();
        }
        return manualParkingSpaceLockTime;
    }

    public Long getManualParkingSpaceLockTime () {
        return getManualParkingSpaceLockTime(DEFAULT_CLIENT_NUMBER);
    }

    public Float getParkingSPacePrice (Long clientNumber) {
        Float parkingSpacePrice = null;
        if (configurations.containsKey(clientNumber)) {
            parkingSpacePrice = configurations.get(clientNumber).getParkingSpacePrice();
        } else {
            // se loguea una info
            if(configurations.size() == 0){
                start();
            }
            parkingSpacePrice = getParkingSPacePrice();
        }
        return parkingSpacePrice;
    }

    public Float getParkingSPacePrice () {
        return getParkingSPacePrice(DEFAULT_CLIENT_NUMBER);
    }

    public Long getReservationTimeout (Long clientNumber) {
        Long reservationTimeout = null;
        if (configurations.containsKey(clientNumber)) {
            reservationTimeout = configurations.get(clientNumber).getReservationTimeout();
        }else {
            // se loguea una info
//            BUCLE INFINITO JAJAJAJAJAJ
//            if(configurations.size() == 0){
//                start();
//            }
//            reservationTimeout = getReservationTimeout();
        }
        return reservationTimeout;
    }

    public Long getReservationTimeout () {
        return getReservationTimeout(DEFAULT_CLIENT_NUMBER);
    }

    public ClientConfiguration buildDefaultClientConfiguration(){
        ClientConfiguration clientConfiguration = new ClientConfiguration();

        clientConfiguration.setFromClient(clientRepository.get(DEFAULT_CLIENT_NUMBER));
        clientConfiguration.setCompanyName(DEFAULT_COMPANY_NAME);
        clientConfiguration.setManualParkingSpaceLockTime(DEFAULT_MANUAL_PARKING_SPACE_LOCK_TIME);
        clientConfiguration.setParkingSpacePrice(DEFAULT_PARKING_SPACE_PRICE);
        clientConfiguration.setReservationTimeout(DEFAULT_RESERVATION_TIMEOUT);

        return clientConfiguration;
    }

    public String getWeeklySchedules (Long clientNumber) {
        String result = "";
        if (configurations.containsKey(clientNumber)) {
            String[] weeklySchedules = configurations.get(clientNumber).getWeeklySchedulesWork();
            if (weeklySchedules != null && weeklySchedules.length > 0) {
                for (int i = 0; i < weeklySchedules.length; i++) {
                    if (i>0) {
                        result = result + "| ";
                    }
                    result = result + weeklySchedules[i] + " ";
                }
            }
        }
        return result;
    }


    @PostConstruct
    public void init(){
        List<ClientConfiguration> cl = null;
        try {
            cl = clientConfigurationRepository.getAvailables();
        }catch (Exception ex){}
        if(cl != null && cl.size() > 0){
            for (ClientConfiguration c : cl  ) {
                addClientConfiguration(c);
            }
        }
    }

}
