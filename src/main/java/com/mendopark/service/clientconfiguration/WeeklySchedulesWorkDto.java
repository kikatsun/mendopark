package com.mendopark.service.clientconfiguration;

import java.util.Date;

public class WeeklySchedulesWorkDto {

    private String id;
    private Date endDate;
    private Date startDate;
    private String startTime;
    private String finishTime;

}
