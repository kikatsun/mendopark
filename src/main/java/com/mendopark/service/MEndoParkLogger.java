package com.mendopark.service;

import java.io.IOException;
import java.util.logging.*;

public class MEndoParkLogger {

    private static MEndoParkLogger instance = new MEndoParkLogger();

    public static Logger MENDOPARK;

    private MEndoParkLogger(){
        MENDOPARK = Logger.getLogger("MENDOPARK");
        init();
    }

    public static MEndoParkLogger getInstance(){
        if(instance == null){
            instance = new MEndoParkLogger();
        }
        return instance;
    }

    public Logger getLogger(){
        return MENDOPARK;
    }

    public void init(){

        try {
            Handler consoleHandler = new ConsoleHandler();
            Handler fileHandler = new FileHandler("/home/erick/server/log/mendopark.log", false);
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            fileHandler.setFormatter(simpleFormatter);
            MENDOPARK.addHandler(consoleHandler);
            MENDOPARK.addHandler(fileHandler);
            consoleHandler.setLevel(Level.ALL);
            fileHandler.setLevel(Level.ALL);
        }catch (IOException ioex){
            MENDOPARK.log(Level.WARNING,"Error al crear Logger",ioex);
        }

    }

}
