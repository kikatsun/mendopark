package com.mendopark.service.utilities;

import java.util.Date;

public class DateUtilities {

    public static final int HOUR = 3600000;

    public static final int calculateHoursBetween (Date start, Date end) {
        int result = 0;
        if (start != null && end != null) {
            long milis = end.getTime() - start.getTime();
            result = Long.valueOf(milis / HOUR).intValue();
            if (milis % HOUR > HOUR/2) {
                result ++;
            }
        }
        return result;
    }

}
