package com.mendopark.service.utilities;

public class StringUtilities {

    public static boolean isValid (String string) {
        boolean result = true;
        if(string == null || string.isEmpty()) {
            result = false;
        }
        return result;
    }

}
