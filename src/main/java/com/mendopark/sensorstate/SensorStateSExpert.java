package com.mendopark.sensorstate;

import com.mendopark.application.SExpert;
import com.mendopark.model.ParkingSpace;
import com.mendopark.model.SensorState;
import com.mendopark.repository.SensorRepository;
import com.mendopark.repository.SensorStateRepository;
import com.mendopark.rest.parkingarea.ParkingAreaFactory;
import com.mendopark.rest.sensorstate.SensorStateFactory;

import javax.ejb.*;
import javax.ws.rs.BadRequestException;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SensorStateSExpert extends SExpert {

    @EJB
    private SensorStateRepository repository;
    @EJB
    private SensorStateFactory sensorStateFactory;
    @EJB
    private SensorRepository sensorRepository;
    @EJB
    private ParkingAreaFactory parkingAreaFactory;

    public void persist(SensorState sensorState) {

        validateSensorState(sensorState);
        if(!sensorStateFactory.isSensorAvailableByReferenceCode(sensorState.getName())){
            String message = "No está habilitado el sensor "+sensorState.getName()+" para registrar sus estados";
            System.out.println("SensorState: "+message);
            throw new BadRequestException(message);
        }
        ParkingSpace parkingSpace = parkingAreaFactory.findParkingSpaceFromSensorReference(sensorState.getName());
        sensorState.setName(parkingSpace.getId());
        repository.persist(sensorState);
        sensorStateFactory.updateParkingSpaceFromSensor(sensorState);
    }

    public void saveSensorStateFromUser(SensorState sensorState) {
        validateSensorState(sensorState);
//        ParkingSpace parkingSpace = parkingAreaFactory.getParkingSpaceByName(sensorState.getName());
//        sensorState.setName(parkingSpace.getId());
        repository.persist(sensorState);
        sensorStateFactory.updateParkingSpaceFromUser(sensorState);
    }

    private void validateSensorState(SensorState sensorState) {
        if(sensorState == null){
            throw new BadRequestException("EL formato del estado del sensor es incorrecto");
        }
        if(sensorState.getName() == null || sensorState.getName().isEmpty()){
            throw new BadRequestException("El nombre del sensor no debe ser vacío");
        }
        if(sensorState.getState() == null || sensorState.getState().isEmpty()){
            throw new BadRequestException("El nombre del estado del sensor no debe ser vacío");
        }
    }
}
