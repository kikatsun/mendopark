
package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.model.Province;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class ProvinceRepository implements Repository<Integer, Province> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + Province.class.getName() + " pe";

	@Override
	public Province get(Integer id) {
		Province result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<Province> query = entityManager.createQuery(sql, Province.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public Province getAvailable(Integer id) {
		Province result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<Province> query = entityManager.createQuery(sql, Province.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<Province> getAll() {
		String sql = SELECT_ENTITY;
		return getProvinces(sql);
	}

	@Override
	public List<Province> getAvailables() {
		String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		//String sql = SELECT_ENTITY;
		return getProvinces(sql);
	}

	private List<Province> getProvinces(String sql) {
		TypedQuery<Province> query = entityManager.createQuery(sql, Province.class);
		List<Province> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Province>();
		}
		return result;
	}

	@Override
	public Province persist(Province entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(Province entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + Province.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getProvinceExist(String name, Integer id){

		String nameAux = name.toLowerCase();
		Province result;

		try{
			String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus ) " +
					"AND pe.endDate IS NULL ";

			if (id != null){
				sql += "AND pe.id != :idProvince ";
			}

			TypedQuery<Province> query = entityManager.createQuery(sql, Province.class);
			query.setMaxResults(1);
			query.setParameter("name", name);
			query.setParameter("nameMinus", nameAux);

			if (id != null){
				query.setParameter("idProvince", id);
			}
			result = query.getSingleResult();

			if (result != null){
				return true; // Provincia SI existe
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("No existe Provincia con nombre: " + name);
			return false;
		}
	}
}
