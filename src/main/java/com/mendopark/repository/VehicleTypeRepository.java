
package com.mendopark.repository;

import com.mendopark.model.Role;
import com.mendopark.model.VehicleType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class VehicleTypeRepository implements Repository<Integer, VehicleType> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + VehicleType.class.getName() + " pe";

	@Override
	public VehicleType get(Integer id) {
		VehicleType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<VehicleType> query = entityManager.createQuery(sql, VehicleType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public VehicleType getAvailable(Integer id) {
		VehicleType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<VehicleType> query = entityManager.createQuery(sql, VehicleType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<VehicleType> getAll() {
		String sql = SELECT_ENTITY;
		return getVehicleTypes(sql);
	}

	@Override
	public List<VehicleType> getAvailables() {
		//String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		String sql = SELECT_ENTITY;
		return getVehicleTypes(sql);
	}
	public List<VehicleType> getAvailables2() {
		String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		return getVehicleTypes(sql);
	}

	private List<VehicleType> getVehicleTypes(String sql) {
		TypedQuery<VehicleType> query = entityManager.createQuery(sql, VehicleType.class);
		List<VehicleType> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<VehicleType>();
		}
		return result;
	}

	@Override
	public VehicleType persist(VehicleType entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(VehicleType entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + VehicleType.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getVehicleTypeExist(String nameVT, String descriptionVT, Integer id){

		String nameAux = nameVT.toLowerCase();
		//String descriptionAux = descriptionVT.toLowerCase();
		VehicleType result;

		try{
			String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus ) " +
					"AND pe.endDate IS NULL ";

			if (id != null){
				sql += "AND pe.id != :idSensor ";
			}

			TypedQuery<VehicleType> query = entityManager.createQuery(sql, VehicleType.class);
			query.setMaxResults(1);
			query.setParameter("name", nameVT);
			query.setParameter("nameMinus", nameAux);

			if (id != null){
				query.setParameter("idSensor", id);
			}

			result = query.getSingleResult();

			if (result != null){
				return true;
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("No existe Role con nombre: " + nameVT + " y descripcion: " + descriptionVT);
			return false;
		}
	}
}
