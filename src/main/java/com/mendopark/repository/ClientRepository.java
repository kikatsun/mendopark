package com.mendopark.repository;

import com.mendopark.model.Client;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

@Stateless
@LocalBean
public class ClientRepository implements Repository<Long, Client> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    private AddressRepository addressRepository;

    private static final String SELECT_ENTITY = "SELECT c FROM " + Client.class.getName() + " c";

    @Override
    public Client get(Long id) {
        Client result = null;
        try {
            String sql = SELECT_ENTITY + " WHERE c.number = :number";
            TypedQuery<Client> query = entityManager.createQuery(sql, Client.class);
            query.setParameter("number", id);
            result = query.getSingleResult();
        }catch (Exception ex){
            MEndoParkLogger.MENDOPARK.log(Level.WARNING, "No se encontró el cliente: "+id,ex);
        }
        return result;
    }

    @Override
    public Client getAvailable(Long id) {
        Client result = null;

        String sql = SELECT_ENTITY + " WHERE c.number = :number AND c.endDate IS NULL";
        TypedQuery<Client> query = entityManager.createQuery(sql, Client.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<Client> getAll() {
        String sql = SELECT_ENTITY;
        return getClients(sql);
    }

    @Override
    public List<Client> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE c.endDate IS NULL";
        return getClients(sql);
    }

    private List<Client> getClients(String sql) {
        TypedQuery<Client> query = entityManager.createQuery(sql, Client.class);
        List<Client> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<Client>();
        }
        return result;
    }

    @Override
    public Client persist(Client entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Client entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public Long getLastId() {
        Long lastId = null;
        try {
            String sql = "SELECT max(number) FROM " + Client.class.getName() + " p  ";
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Long) query.getSingleResult();
        } catch (Exception e) {
            lastId = 0L;
        } // Devuelve null
        return lastId;
    }

    public void flushData(){
        this.entityManager.flush();
    }

}
