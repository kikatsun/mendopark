package com.mendopark.repository;

import java.util.List;

/**
 * Interfaz del repositorio para el acceso de datos
 *
 * Define una interfaz estándar para el acceso de datos a la BD, los métodos definidos están pensados para un ABM
 * simple.
 *
 * @param <K>   Key: es el primary key de negocio de la entidad.
 * @param <E>   Entity: es la entidad que se administrará.
 */
public interface Repository<K, E> {

    /**
     * Busca la entidad por su Id.
     * @param id identificador de la entidad.
     * @return La entidad buscada, si no se encuentra devuelve null.
     */
    E get(K id);

    /**
     * Busca la entidad por su Id y con una fecha de fin nula que representa una entidad disponible.
     * @param id identificador de la entidad.
     * @return La entidad buscada, si no se encuentra devuelve null.
     */
    E getAvailable(K id);

    /**
     * Busca todas las entidades.
     * @return Una lista de las entidades encontradas ó lista vacía en caso de no haber.
     */
    List<E> getAll();

    /**
     * Busca todas las entidades con una fecha de fin nula que representa a las entidades disponibles.
     * @return Una lista de las entidades encontradas ó lista vacía en caso de no haber.
     */
    List<E> getAvailables();

    /**
     * Guarda/actualiza una entidad, en caso que se quiera actualizar se debe modificar la misma entidad buscada.
     * Sino se tomará como una nueva entidad.
     * @param entity Entidad a guardar/actualizar.
     */
    E persist(E entity);

    /**
     * Da de baja una entidad poniendole una fecha de fin
     * @param entity Entidad a dar de baja.
     */
    void finalize(E entity);

    /**
     * Devuelve el último id de la BD de la entidad, en caso que el identificador no sea numérico se devuelve null.
     * @return ultimo id o null.
     */
    K getLastId();

}
