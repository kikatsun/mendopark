package com.mendopark.repository;

import com.mendopark.model.Vehicle;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
@Stateless
@LocalBean
public class VehicleRepository implements Repository<String, Vehicle>{

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pe FROM " + Vehicle.class.getName() + " pe";

    @Override
    public Vehicle get(String domain) {
        Vehicle result = null;
        try {
            String sql = SELECT_ENTITY + " WHERE pe.domain = :domain";
            TypedQuery<Vehicle> query = entityManager.createQuery(sql, Vehicle.class);
            query.setParameter("domain", domain);
            result = query.getSingleResult();
        }catch (Exception ex){result = null;}
        return result;
    }

    @Override
    public Vehicle getAvailable(String domain) {
        Vehicle result = null;
        try {
            String sql = SELECT_ENTITY + " WHERE pe.domain = :domain AND pe.endDate IS NULL";
            TypedQuery<Vehicle> query = entityManager.createQuery(sql, Vehicle.class);
            query.setParameter("domain", domain);
            result = query.getSingleResult();
        }catch (Exception ex){
            System.out.println("VehicleRepositoryVehicleRepositoryVehicleRepositoryVehicleRepositoryVehicleRepositoryVehicleRepository");
            System.out.println(ex.getMessage());
            result = null;
        }
        return result;
    }

    @Override
    public List<Vehicle> getAll() {
        String sql = SELECT_ENTITY;
        return getVehicles(sql);
    }

    @Override
    public List<Vehicle> getAvailables() {
        return getAll();
    }

    private List<Vehicle> getVehicles(String sql) {
        TypedQuery<Vehicle> query = entityManager.createQuery(sql, Vehicle.class);
        List<Vehicle> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<Vehicle>();
        }
        return result;
    }

    @Override
    public Vehicle persist(Vehicle entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Vehicle entity) {
        /* No tiene fecha de fin */
    }

    public void flu(){
        entityManager.flush();
    }

    @Override
    public String getLastId() {
        return null;
    }
}
