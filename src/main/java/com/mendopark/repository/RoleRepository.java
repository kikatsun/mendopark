
package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.model.Role;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class RoleRepository implements Repository<Integer, Role> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + Role.class.getName() + " pe";

	@Override
	public Role get(Integer id) {
		Role result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<Role> query = entityManager.createQuery(sql, Role.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public Role getAvailable(Integer id) {
		Role result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<Role> query = entityManager.createQuery(sql, Role.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<Role> getAll() {
		String sql = SELECT_ENTITY + " ORDER BY pe.number";
		return getRoles(sql);
	}

	@Override
	public List<Role> getAvailables() {
		String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL " +
				"AND pe.vigenciaDesde <= CURRENT_DATE " +
				"AND pe.vigenciaHasta >= CURRENT_DATE " +
				"ORDER BY pe.number";
		//String sql = SELECT_ENTITY;
		return getRoles(sql);
	}

	private List<Role> getRoles(String sql) {
		TypedQuery<Role> query = entityManager.createQuery(sql, Role.class);
		List<Role> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Role>();
		}
		return result;
	}

	@Override
	public Role persist(Role entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(Role entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + Role.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getRoleExist(String nameRole, String descriptionRole, Integer number){

		String nameAux = nameRole.toLowerCase();
		String descriptionAux = descriptionRole.toLowerCase();
		Role result;

		try{
			//String sql = SELECT_ENTITY + " WHERE LOWER(pe.name) = :name AND LOWER(pe.description) = :description";
			String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus )" +
					"AND pe.endDate IS NULL ";

			if (number != null){
				sql += "AND pe.number != :number";
			}

			TypedQuery<Role> query = entityManager.createQuery(sql, Role.class);
			query.setMaxResults(1);
			query.setParameter("name", nameRole);
			query.setParameter("nameMinus", nameAux);

			if (number != null){
				query.setParameter("number", number);
			}

			result = query.getSingleResult();

			if (result != null){
				return true;
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("No existe Role con nombre: " + nameRole + " y descripcion: " + descriptionRole);
			return false;
		}
	}
}
