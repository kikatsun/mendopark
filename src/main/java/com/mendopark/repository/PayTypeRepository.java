package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.model.PayType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class PayTypeRepository implements Repository<Long, PayType> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pe FROM "+PayType.class.getName()+ " pe";

    @Override
    public PayType get(Long id) {
        PayType result = null;

        String sql = SELECT_ENTITY + " WHERE pe.number = :number";
        TypedQuery<PayType> query = entityManager.createQuery(sql,PayType.class);
        query.setParameter("number",id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public PayType getAvailable(Long id) {
        PayType result = null;

        String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
        TypedQuery<PayType> query = entityManager.createQuery(sql,PayType.class);
        query.setParameter("number",id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<PayType> getAll() {
        String sql = SELECT_ENTITY;
        return getPayTypes(sql);
    }

    @Override
    public List<PayType> getAvailables() {
        //String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
        String sql = SELECT_ENTITY;
        return getPayTypes(sql);
    }

    public List<PayType> getAvailables2() {
        String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
//        String sql = SELECT_ENTITY;
        return getPayTypes(sql);
    }

    private List<PayType> getPayTypes(String sql) {
        TypedQuery<PayType> query = entityManager.createQuery(sql,PayType.class);
        List<PayType> result = query.getResultList();
        if(result == null){
            result = new ArrayList<PayType>();
        }
        return result;
    }

    @Override
    public PayType persist(PayType entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(PayType entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public Long getLastId() {
        Long lastId = null;
        try{
            String sql = "SELECT max(number) FROM "+PayType.class.getName()+" p  ";
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Long)query.getSingleResult();
        }
        catch(Exception e){} // Devuelve null
        return lastId;
    }

    public boolean getPayTypeExist(String namePT, String descriptionPT, Long number){
        String nameAux = namePT.toLowerCase();
        //String descriptionAux = descriptionPT.toLowerCase();
        PayType result;

        try {
            //String sql = SELECT_ENTITY + " WHERE LOWER(pe.name) = :name AND LOWER(pe.description) = :description";
            String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus ) " +
                    "AND pe.endDate IS NULL ";

            if (number != null){
                sql += "AND pe.number != :number ";
            }

            TypedQuery<PayType> query = entityManager.createQuery(sql, PayType.class);
            query.setMaxResults(1);
            query.setParameter("name", namePT);
            query.setParameter("nameMinus", nameAux);
            //query.setParameter("description", descriptionAux);

            if (number != null){
                query.setParameter("number", number);
            }

            result = query.getSingleResult();

            if (result != null){
                return true;
            } else {
                return false;
            }
        } catch (NoResultException ex){
            System.out.println("getPayTypeExist(). No existe Tipo de Pago con nombre: " + namePT + " y idProvince: " + descriptionPT);
            return false;
        }
    }
}
