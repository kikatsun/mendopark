package com.mendopark.repository;

import com.mendopark.model.SensorState;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class SensorStateRepository implements Repository<String, SensorState> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @Override
    public SensorState get(String id) {
        return null;
    }

    @Override
    public SensorState getAvailable(String id) {
        return null;
    }

    @Override
    public List<SensorState> getAll() {
        return null;
    }

    @Override
    public List<SensorState> getAvailables() {
        return null;
    }

    @Override
    public SensorState persist(SensorState entity) {
        if(entity.getId() == null){
            entity.setId(UUID.randomUUID().toString());
        }
        entity.setCreationDate(new Date());
        entityManager.persist(entity);
        return entity;
    }

    public SensorState findLastSensorStateFromParkingSpace (String parkingSpaceId){
        SensorState result = null;

        String sql = "SELECT pe FROM "+SensorState.class.getName()+ " pe WHERE pe.name = :id ORDER BY pe.creationDate DESC";
        TypedQuery<SensorState> query = entityManager.createQuery(sql,SensorState.class);
        query.setParameter("id",parkingSpaceId);
        query.setMaxResults(1);
        try {
            result = query.getSingleResult();
        }catch (Exception ex){

        }

        return result;
    }

    @Override
    public void finalize(SensorState entity) {

    }

    @Override
    public String getLastId() {
        return null;
    }
}
