
package com.mendopark.repository;

import com.mendopark.model.Day;
import com.mendopark.model.Locality;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class DayRepository implements Repository<Integer, Day> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + Day.class.getName() + " pe";

	@Override
	public Day get(Integer id) {
		Day result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<Day> query = entityManager.createQuery(sql, Day.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public Day getAvailable(Integer id) {
		Day result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<Day> query = entityManager.createQuery(sql, Day.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<Day> getAll() {
		String sql = SELECT_ENTITY;
		return getDays(sql);
	}

	@Override
	public List<Day> getAvailables() {
		String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		//String sql = SELECT_ENTITY;
		return getDays(sql);
	}

	private List<Day> getDays(String sql) {
		TypedQuery<Day> query = entityManager.createQuery(sql, Day.class);
		List<Day> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Day>();
		}
		return result;
	}

	@Override
	public Day persist(Day entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(Day entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + Day.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getDayExist(String nameDay, Long idCliente, Integer number){

		String nameAux = nameDay.toLowerCase();
		Day result;

		try {
			String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus ) " +
					"AND pe.endDate IS NULL ";

			if (idCliente != null){
				sql += "AND pe.fromClient.number = :idCliente ";
			}

			if (number != null){
				sql += "AND pe.number != :number ";
			}

			TypedQuery<Day> query = entityManager.createQuery(sql, Day.class);
			query.setMaxResults(1);
			query.setParameter("name", nameDay);
			query.setParameter("nameMinus", nameAux);

			if (idCliente != null){
				query.setParameter("idCliente", idCliente);
			}

			if (number != null){
				query.setParameter("number", number);
			}

			result = query.getSingleResult();

			if (result != null){
				return true;  //Dia SI existe
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("getDayExist(). No existe un Día con nombre: " + nameDay);
			return false;
		}
	}

    public List<Day> findByClient(Long clientNumber) {
		List<Day> result = null;

		String sql = SELECT_ENTITY + " WHERE pe.fromClient.number = :number AND pe.endDate IS NULL";
		TypedQuery<Day> query = entityManager.createQuery(sql, Day.class);
		query.setParameter("number", clientNumber);
		result = query.getResultList();

		return result;
    }
}
