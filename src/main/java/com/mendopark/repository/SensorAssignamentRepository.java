package com.mendopark.repository;

import com.mendopark.model.SensorAssignament;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class SensorAssignamentRepository implements Repository<String, SensorAssignament> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    private AddressRepository addressRepository;

    private static final String SELECT_ENTITY = "SELECT c FROM " + SensorAssignament.class.getName() + " c";

    @Override
    public SensorAssignament get(String id){
        SensorAssignament result = null;
        String sql = SELECT_ENTITY + " WHERE c.id = :id";
        TypedQuery<SensorAssignament> query = entityManager.createQuery(sql, SensorAssignament.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public SensorAssignament getAvailable(String id) {
        SensorAssignament result = null;

        String sql = SELECT_ENTITY + " WHERE c.id = :id AND c.endDate IS NULL";
        TypedQuery<SensorAssignament> query = entityManager.createQuery(sql, SensorAssignament.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<SensorAssignament> getAll() {
        return getSensorAssignaments(SELECT_ENTITY);
    }

    @Override
    public List<SensorAssignament> getAvailables(){
        String sql = SELECT_ENTITY + " WHERE c.endDate IS NULL";
        return getSensorAssignaments(sql);
    }

    private List<SensorAssignament> getSensorAssignaments(String sql) {
        TypedQuery<SensorAssignament> query = entityManager.createQuery(sql, SensorAssignament.class);
        List<SensorAssignament> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<SensorAssignament>();
        }
        return result;
    }

    @Override
    public SensorAssignament persist(SensorAssignament entity) {
        if(entity.getId() == null || entity.getId().isEmpty()){
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(SensorAssignament entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public String getLastId() {
        return UUID.randomUUID().toString();
    }
}
