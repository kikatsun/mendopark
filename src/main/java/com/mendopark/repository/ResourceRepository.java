package com.mendopark.repository;

import com.mendopark.model.security.Resource;
import com.mendopark.model.security.Resource;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

@Stateless
@LocalBean
public class ResourceRepository implements Repository<String, Resource> {
    
    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT x FROM " + Resource.class.getName() + " x";

    @Override
    public Resource get(String id) {
        Resource locality = null;
        String sql = SELECT_ENTITY + " WHERE x.id = :id";
        TypedQuery<Resource> query = entityManager.createQuery(sql, Resource.class);
        query.setParameter("id", id);
        try {
            locality = query.getSingleResult();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.INFO, "No se encontró entidad con id "+id, ex);
        }
        return locality;
    }

    public Resource findByName (String name) {
        Resource resource = null;
        String sql = SELECT_ENTITY + " WHERE x.name = :name";
        TypedQuery<Resource> query = entityManager.createQuery(sql, Resource.class);
        query.setParameter("name", name);
        try {
            resource = query.getSingleResult();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.INFO, "No se encontró entidad con nombre "+name, ex);
        }
        return resource;
    }

    public boolean getByNameAndPath (String name, String path, String id) {
        String nameAux = name.toLowerCase();
        String pathAux = path.toLowerCase();
        Resource resource = null;

        try {

            String sql = SELECT_ENTITY + " WHERE (x.name = :name OR LOWER(x.name) = :nameMinus ) " +
                    //"AND ( x.path = :path OR LOWER(x.path) = :pathMinus) " +
                    "AND x.endDate IS NULL ";

            if (id != null){
                sql += "AND x.id != :id ";
            }

            TypedQuery<Resource> query = entityManager.createQuery(sql, Resource.class);
            query.setParameter("name", name);
            query.setParameter("nameMinus", nameAux);
            //query.setParameter("path", path);
            //query.setParameter("pathMinus", pathAux);

            if (id != null){
                query.setParameter("id", id);
            }

            resource = query.getSingleResult();

            if (resource != null ){
                return true; // Menu SI existe
            } else {
                return false;
            }
        }catch (NoResultException ex){
            System.out.println("No existe Menu con nombre: " + name);
            return false;
        }
    }

    @Override
    public Resource getAvailable(String id) {
        Resource result = null;

        String sql = SELECT_ENTITY + " WHERE x.id = :id AND x.endDate IS NULL";
        TypedQuery<Resource> query = entityManager.createQuery(sql, Resource.class);
        query.setParameter("id", id);
        try {
            result = query.getSingleResult();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.INFO, "No se encontró entidad con id "+id, ex);
        }
        return result;
    }

    @Override
    public List<Resource> getAll() {
        String sql = SELECT_ENTITY;
        return getResources(sql);
    }

    @Override
    public List<Resource> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE x.endDate IS NULL";
        //String sql = SELECT_ENTITY;
        return getResources(sql);
    }

    private List<Resource> getResources(String sql){
        TypedQuery<Resource> query = entityManager.createQuery(sql, Resource.class);
        List<Resource> result = null;
        try {
            result = query.getResultList();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.WARNING, "No se pudieron encontrar asignaciones de rol a recurso.", ex);
        }
        return result;
    }

    @Override
    public Resource persist(Resource entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Resource entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    public void flush(){
        this.entityManager.flush();
    }

    @Override
    public String getLastId() {
        return UUID.randomUUID().toString();
    }
}
