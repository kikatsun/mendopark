package com.mendopark.repository;

import com.mendopark.model.Address;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class AddressRepository implements Repository<String, Address> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @Override
    public Address get(String id) {
        return null;
    }

    @Override
    public Address getAvailable(String id) {
        return null;
    }

    @Override
    public List<Address> getAll() {
        return null;
    }

    @Override
    public List<Address> getAvailables() {
        return null;
    }

    @Override
    public Address persist(Address entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Address entity) {

    }

    @Override
    public String getLastId() {
        return null;
    }
}
