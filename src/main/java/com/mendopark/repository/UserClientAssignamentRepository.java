package com.mendopark.repository;

import com.mendopark.model.User;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
@LocalBean
public class UserClientAssignamentRepository {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    private UserRepository userRepository;

    private static final String TABLE_NAME = "client_user";
    private static final String CLIENT_FIELD_NAME = "client_number";
    private static final String USER_FIELD_NAME = "users_id";

    public Long findClientIdFrom(User user){
        if (user.getFromClient() != null) {
            if (user.getFromClient().getNumber() != null)
                return user.getFromClient().getNumber();
            else
                return 0l;

        } else {
            return 0l;
        }
    }

}
