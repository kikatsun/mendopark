package com.mendopark.repository;

import com.mendopark.model.PayType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class ReporteRepository {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pe FROM "+PayType.class.getName()+ " pe";
    private static final SimpleDateFormat dataBaseDateFormatter = new SimpleDateFormat("YYYY-MM-dd");

    public List<Object[]> getCantidadVehiculosPorZonas(Date fechaDesde, Date fechaHasta, Long idCliente) {
        List<Object[]> obj = new ArrayList<>();

        String cadena = "";
        if (idCliente != null){
            cadena = "AND pa.fromClient_number = :idCliente ";
            System.out.println("idCliente: "+ idCliente);
        }

        Query q = entityManager.createNativeQuery("SELECT COUNT(1) AS cantidad, pa.name AS nombreZona " +
                                                    "FROM parkingarea AS pa, parking AS p " +
                                                    "WHERE pa.endDate IS NULL " +
                                                     cadena +
                                                    "AND parkingArea_number = pa.number " +
                                                    "AND DATE(p.start) >= :fechaDesde " +
                                                    "AND DATE(p.start) <= :fechaHasta " +
                                                    "GROUP BY pa.number");

        q.setParameter("fechaDesde", dataBaseDateFormatter.format(fechaDesde));
        q.setParameter("fechaHasta", dataBaseDateFormatter.format(fechaHasta));

        //para verificar formato de fechas
        System.out.println("fechaDesde: "+ dataBaseDateFormatter.format(fechaDesde));
        System.out.println("fechaHasta: "+ dataBaseDateFormatter.format(fechaHasta));
        //fin verificar

        if (idCliente != null){
            q.setParameter("idCliente", idCliente);
        }
        System.out.println(q.toString());
        obj = q.getResultList();
        System.out.println("RESLTTTTTTTTTTTTTTTTTTTTT: "+obj.size());
        return obj;
    }

    public List<Object[]> getCantidadVehiculosNoPagan(Date fechaDesde, Date fechaHasta, Long idCliente, Date fechaFinalizadaXProceso) {
        List<Object[]> obj = new ArrayList<>();

        String cadena = "";
        if (idCliente != null){
            cadena = "AND pa.fromClient_number = :idCliente ";
            System.out.println("Parametros getCantidadVehiculosNoPagan() ...");
            System.out.println("...idCliente: "+ idCliente);
        }

        Query q = entityManager.createNativeQuery("SELECT COUNT(1) AS cantidad, pa.name AS nombreZona " +
                                                    "FROM parkingarea AS pa, parking AS p " +
                                                    "WHERE parkingArea_number = pa.number " +
                                                    cadena +
                                                    "AND DATE(p.start) >= :fechaDesde " +
                                                    "AND DATE(p.start) <= :fechaHasta " +
                                                    "AND DATE(p.end) = :fechaFinalizadaXProceso " + //fechaFinalizadaXProceso es igual a 1970-01-01
                                                    "GROUP BY pa.number");
        q.setParameter("fechaDesde", dataBaseDateFormatter.format(fechaDesde));
        q.setParameter("fechaHasta", dataBaseDateFormatter.format(fechaHasta));
        q.setParameter("fechaFinalizadaXProceso", dataBaseDateFormatter.format(fechaFinalizadaXProceso));

        //para verificar formato de fechas
        System.out.println("...fechaDesde: "+ dataBaseDateFormatter.format(fechaDesde));
        System.out.println("...fechaHasta: "+ dataBaseDateFormatter.format(fechaHasta));
        System.out.println("...fechaFinalizadaXProceso: "+ dataBaseDateFormatter.format(fechaFinalizadaXProceso));
        //fin verificar

        if (idCliente != null){
            q.setParameter("idCliente", idCliente);
        }

        obj = q.getResultList();

        return obj;
    }

    public List<Object[]> getCantidadMediosDePagoUsado(Date fechaDesde, Date fechaHasta, Long idCliente) {
        List<Object[]> obj = new ArrayList<>();

        String cadena = "";
        if (idCliente != null){
            cadena = "AND pa.fromClient_number = :idCliente ";
        }

        Query q = entityManager.createNativeQuery("SELECT COUNT(1) AS cantidad, p.name AS nombreTipoPago " +
                "FROM reserve AS r, parkingarea AS pa, paytype AS p " +
                "WHERE r.parkingarea_number = pa.number " +
                "AND p.number = r.paytype_number " +
                "AND r.paytype_number IS NOT NULL " +
                cadena +
                "AND DATE(r.creationDate) >= :fechaDesde " +
                "AND DATE(r.creationDate) <= :fechaHasta " +
                "GROUP BY p.name " +
                "HAVING COUNT(r.id) > 0");
        q.setParameter("fechaDesde", dataBaseDateFormatter.format(fechaDesde));
        q.setParameter("fechaHasta", dataBaseDateFormatter.format(fechaHasta));

        if (idCliente != null){
            q.setParameter("idCliente", idCliente);
        }

        obj = q.getResultList();

        return obj;
    }

    public List<Object[]> getCantidadVehiculosHorarioLimite(Date fechaDesde, Date fechaHasta, Long idCliente) {
        List<Object[]> obj = new ArrayList<>();
        String cadena = "";
        if (idCliente != null){
            cadena = "AND pa.fromClient_number = :idCliente ";
            System.out.println("Parametros getCantidadVehiculosHorarioLimite() ...");
            System.out.println("...idCliente: "+ idCliente);
        }

        String sql = "SELECT COUNT(1) AS cantidad, AUX.name AS nombreZona " +
                        "FROM (SELECT * " +
                                "FROM parkingarea AS pa, parking AS p " +
                                "WHERE parkingArea_number = pa.number " +
                                cadena +
                                "AND DATE(p.start) >= :fechaDesde " +
                                "AND DATE(p.start) <= :fechaHasta " +
                                "AND DATE(p.end) = '1970-01-01' " +
                               "UNION " +
                                "SELECT * " +
                                "FROM parkingarea AS pa, parking AS p " +
                                "WHERE parkingArea_number = pa.number " +
                                cadena +
                                "AND DATE(p.start) >= :fechaDesde " +
                                "AND DATE(p.start) <= :fechaHasta " +
                                "AND TIME(p.start) = '08:00:00' " +
                        ") AS AUX " +
                        "GROUP BY AUX.name ";

        //para verificar contenido SQL
        System.out.println("...sql: "+ sql);

        Query q = entityManager.createNativeQuery(sql);

        if (idCliente != null){
            q.setParameter("idCliente", idCliente);
        }

        q.setParameter("fechaDesde", dataBaseDateFormatter.format(fechaDesde));
        q.setParameter("fechaHasta", dataBaseDateFormatter.format(fechaHasta));

        //para verificar formato de fechas
        System.out.println("...fechaDesde: "+ dataBaseDateFormatter.format(fechaDesde));
        System.out.println("...fechaHasta: "+ dataBaseDateFormatter.format(fechaHasta));
        //fin verificar

        obj = q.getResultList();

        return obj;
    }
}
