package com.mendopark.repository;

import com.mendopark.model.AnticipatedAmount;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class AnticipatedAmountRepository implements Repository<String, AnticipatedAmount> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT aa FROM " + AnticipatedAmount.class.getName() + " aa";

    @Override
    public AnticipatedAmount get(String id) {
        throw  new UnsupportedOperationException();
    }

    @Override
    public AnticipatedAmount getAvailable(String id) {
        throw  new UnsupportedOperationException();
    }

    @Override
    public List<AnticipatedAmount> getAll() {
        throw  new UnsupportedOperationException();
    }

    @Override
    public List<AnticipatedAmount> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE aa.endDate IS NULL";
        TypedQuery<AnticipatedAmount> query = entityManager.createQuery(sql, AnticipatedAmount.class);
        List<AnticipatedAmount> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<AnticipatedAmount>();
        }
        return result;
    }

    @Override
    public AnticipatedAmount persist(AnticipatedAmount entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(AnticipatedAmount entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public String getLastId() {
        throw  new UnsupportedOperationException();
    }
}
