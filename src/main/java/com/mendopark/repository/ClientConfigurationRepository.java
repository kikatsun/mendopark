package com.mendopark.repository;

import com.mendopark.model.ClientConfiguration;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class ClientConfigurationRepository implements Repository<String, ClientConfiguration> {


    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT cc FROM " + ClientConfiguration.class.getName() + " cc";

    @Override
    public ClientConfiguration get(String id) {
        ClientConfiguration result = null;

        String sql = SELECT_ENTITY + " WHERE cc.id = :id";
        TypedQuery<ClientConfiguration> query = entityManager.createQuery(sql, ClientConfiguration.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public ClientConfiguration getAvailable(String id) {
        ClientConfiguration result = null;

        String sql = SELECT_ENTITY + " WHERE cc.id = :id AND cc.endDate IS NULL";
        TypedQuery<ClientConfiguration> query = entityManager.createQuery(sql, ClientConfiguration.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<ClientConfiguration> getAll() {
        String sql = SELECT_ENTITY;
        return getClientConfigurations(sql);
    }

    @Override
    public List<ClientConfiguration> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE cc.endDate IS NULL";
        return getClientConfigurations(sql);
    }

    private List<ClientConfiguration> getClientConfigurations (String sql) {
        TypedQuery<ClientConfiguration> query = entityManager.createQuery(sql, ClientConfiguration.class);
        List<ClientConfiguration> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<ClientConfiguration>();
        }
        return result;
    }

    @Override
    public ClientConfiguration persist(ClientConfiguration entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(ClientConfiguration entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public String getLastId() {
        return null;
    }
}
