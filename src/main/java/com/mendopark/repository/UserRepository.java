package com.mendopark.repository;

import com.mendopark.model.User;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Repositorio del Usuario.
 * Se encarga de las operaciones con la Base de Datos para poder crear, actualizar, eliminar y consultar un usuario.
 *
 * @Pattern: Indirection
 */
@Stateless
@LocalBean
//public class UserRepository implements Repository<String, User> {
public class UserRepository{

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT us FROM " + User.class.getName() + " us ";

    /**
     * Obtiene un Usuario
     * @param id: id de usuario
     * @return User
     */
    //@Override
    public User get(String id) {
        User result;
        String sql = SELECT_ENTITY + " WHERE us.id = :id";
        TypedQuery<User> query = entityManager.createQuery(sql, User.class);
        query.setParameter("id", id);
        try {
            result = query.getSingleResult();
        }catch (NoResultException ex){
            result = null;
        }
        return result;
    }

    public User getByName(String userName) {
        User result;
        String sql = SELECT_ENTITY + " WHERE us.name = :name";
        TypedQuery<User> query = entityManager.createQuery(sql, User.class);
        query.setParameter("name", userName);
        try {
            result = query.getSingleResult();
        }catch (NoResultException ex){
            result = null;
        }
        return result;
    }

    //@Override
    public User getAvailable(Integer id) {
        User result = null;

        String sql = SELECT_ENTITY + " WHERE us.number = :nroUsuario AND us.endDate IS NULL";
        TypedQuery<User> query = entityManager.createQuery(sql, User.class);
        query.setParameter("nroUsuario",id);
        result = query.getSingleResult();

        return result;
    }

    //@Override
    public List<User> getAll() {
        String sql = SELECT_ENTITY;

        return this.getUsers(sql);
    }

    public List<User> getAllByClient( Long clientId) {
        String sql = SELECT_ENTITY + " WHERE fromClient_number = "+clientId+" AND endDate is NULL";

        return this.getUsers(sql);
    }

    private List<User> getUsers(String sql) {
        TypedQuery<User> query = entityManager.createQuery(sql, User.class);
        List<User> result = query.getResultList();

        if(result == null){
            result = new ArrayList<User>();
        }

        return result;
    }

    //@Override
    public List<User> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE us.endDate IS NULL";

        return getUsers(sql);
    }

    //@Override
    public void persist(User entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        entityManager.persist(entity);
    }

    //@Override
    public void finalize(User entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    //@Override
    public Integer getLastId() {
        Integer lastID = null;

        return lastID;
    }

    public boolean getUserExists(User user){
        User result = null;

        String sql = SELECT_ENTITY + "WHERE us.name = :name AND us.endDate IS NULL";
        TypedQuery<User> query = entityManager.createQuery(sql, User.class);
        query.setParameter("name",user.getName());
        result = query.getSingleResult();

        if (result != null){
            return false;
        } else {
            return true;
        }
    }

    public List<User> findRootClientUser(Long clientNumber){
        String sql = SELECT_ENTITY + " WHERE us.fromClient = "+clientNumber + " AND us.userRole = 2";
        System.out.println("VERRRRR: "+sql);
        return getUsers(sql);
    }

    public User findUserByEmailAndDocumentNumber(String email, String documentNumber){
        User result;

        try {
            String sql = SELECT_ENTITY + " WHERE us.email = :email " +
                    " AND us.fromPerson.documentNumber = :documentNumber";

            TypedQuery<User> query = entityManager.createQuery(sql, User.class);
            query.setMaxResults(1);
            query.setParameter("email",email);
            query.setParameter("documentNumber", documentNumber);
            result = query.getSingleResult();

            if (result != null){
                return result;
            } else {
                return null;
            }

        } catch (NoResultException ex) {
            System.out.println("No existe Role con email: " + email + " y documento: " + documentNumber);
            System.out.println("Error: " + ex.getMessage());
            return null;
        }
    }
}
