
package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.model.ParkingSpaceType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class ParkingSpaceTypeRepository implements Repository<Integer, ParkingSpaceType> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + ParkingSpaceType.class.getName() + " pe";

	@Override
	public ParkingSpaceType get(Integer id) {
		ParkingSpaceType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<ParkingSpaceType> query = entityManager.createQuery(sql, ParkingSpaceType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public ParkingSpaceType getAvailable(Integer id) {
		ParkingSpaceType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<ParkingSpaceType> query = entityManager.createQuery(sql, ParkingSpaceType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<ParkingSpaceType> getAll() {
		String sql = SELECT_ENTITY;
		return getParkingSpaceTypes(sql);
	}

	@Override
	public List<ParkingSpaceType> getAvailables() {
		//String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		String sql = SELECT_ENTITY;
		return getParkingSpaceTypes(sql);
	}

	public List<ParkingSpaceType> getAvailablesS() {
		String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		return getParkingSpaceTypes(sql);
	}

	private List<ParkingSpaceType> getParkingSpaceTypes(String sql) {
		TypedQuery<ParkingSpaceType> query = entityManager.createQuery(sql, ParkingSpaceType.class);
		List<ParkingSpaceType> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<ParkingSpaceType>();
		}
		return result;
	}

	@Override
	public ParkingSpaceType persist(ParkingSpaceType entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(ParkingSpaceType entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + ParkingSpaceType.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getParkingSpaceTypeExist(String namePST, String descriptionPST, Integer number){

		String nameAux = namePST.toLowerCase();
		String descriptionAux = descriptionPST.toLowerCase();
		ParkingSpaceType result;

		try {
			String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus ) " +
					"AND pe.endDate IS NULL ";

			if ( number != null){
				sql += "AND pe.number != :number ";
			}

			TypedQuery<ParkingSpaceType> query = entityManager.createQuery(sql, ParkingSpaceType.class);
			query.setMaxResults(1);
			query.setParameter("name", namePST);
			query.setParameter("nameMinus", nameAux);

			if ( number != null){
				query.setParameter("number", number);
			}

			result = query.getSingleResult();

			if (result != null){
				return true;
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("getParkingSpaceTypeExist(). No existe Tipo Plaza Estacionamiento con nombre: " + namePST + " y descripcion: " + descriptionPST);
			return false;
		}
	}
}
