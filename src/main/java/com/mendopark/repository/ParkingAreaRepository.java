package com.mendopark.repository;

import com.mendopark.model.ParkingArea;
import com.mendopark.model.ParkingAreaDetail;
import com.mendopark.model.ParkingSpace;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class ParkingAreaRepository implements Repository<Long, ParkingArea>{

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    private ParkingSpaceRepository parkingSpaceRepository;

    private static final String SELECT_ENTITY = "SELECT pa FROM " + ParkingArea.class.getName() + " pa";


    @Override
    public ParkingArea get(Long id) {
        ParkingArea result = null;

        String sql = SELECT_ENTITY + " WHERE pa.number = :number";
        TypedQuery<ParkingArea> query = entityManager.createQuery(sql, ParkingArea.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public ParkingArea getAvailable(Long id) {
        ParkingArea result = null;

        String sql = SELECT_ENTITY + " WHERE pa.number = :number AND pa.endDate IS NULL";
        TypedQuery<ParkingArea> query = entityManager.createQuery(sql, ParkingArea.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<ParkingArea> getAll() {
        String sql = SELECT_ENTITY;
        return getProvinces(sql);
    }

    @Override
    public List<ParkingArea> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE pa.endDate IS NULL";
        return getProvinces(sql);
    }


    private List<ParkingArea> getProvinces(String sql) {
        TypedQuery<ParkingArea> query = entityManager.createQuery(sql, ParkingArea.class);
        List<ParkingArea> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<ParkingArea>();
        }
        return result;
    }

    @Override
    public ParkingArea persist(ParkingArea parkingArea) {
        if (parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0) {
            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces()) {
                parkingSpaceRepository.persist(parkingSpace);
            }
        }
        if (parkingArea.getParkingAreaDetails() != null && parkingArea.getParkingAreaDetails().size() > 0) {
            for (ParkingAreaDetail parkingAreaDetail: parkingArea.getParkingAreaDetails()) {
                parkingAreaDetail.setFromParkingArea(parkingArea);
                persistParkingAreaDetail(parkingAreaDetail);
            }
        }
        if(parkingArea.getCenterPoint() != null){
            if(parkingArea.getCenterPoint().getId() == null){
                parkingArea.getCenterPoint().setId(UUID.randomUUID().toString());
            }
            entityManager.persist(parkingArea.getCenterPoint());
        }
        entityManager.persist(parkingArea);
        return parkingArea;
    }

//    public ParkingArea update(ParkingArea parkingArea) {
//        if (parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0) {
//            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces()) {
//                parkingSpaceRepository.update(parkingSpace);
//            }
//        }
//        if (parkingArea.getParkingAreaDetails() != null && parkingArea.getParkingAreaDetails().size() > 0) {
//            for (ParkingAreaDetail parkingAreaDetail: parkingArea.getParkingAreaDetails()) {
//                parkingAreaDetail.setFromParkingArea(parkingArea);
//                persistParkingAreaDetail(parkingAreaDetail);
//            }
//        }
//        if(parkingArea.getCenterPoint() != null){
//            if(parkingArea.getCenterPoint().getId() == null){
//                parkingArea.getCenterPoint().setId(UUID.randomUUID().toString());
//                entityManager.persist(parkingArea.getCenterPoint());
//            }else {
//                entityManager.refresh(parkingArea.getCenterPoint());
//            }
//        }
//        entityManager.refresh(parkingArea);
//        return parkingArea;
//    }

    private void persistParkingAreaDetail (ParkingAreaDetail parkingAreaDetail) {
        if(parkingAreaDetail.getCoordinatePoint() != null) {
            if(parkingAreaDetail.getCoordinatePoint().getId() == null) {
                parkingAreaDetail.getCoordinatePoint().setId(UUID.randomUUID().toString());
            }
            entityManager.persist(parkingAreaDetail.getCoordinatePoint());
        }
        if (parkingAreaDetail.getId() == null) {
            parkingAreaDetail.setId(UUID.randomUUID().toString());
        }
        entityManager.persist(parkingAreaDetail);
    }

    @Override
    public void finalize(ParkingArea entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public Long getLastId() {
        Long lastId = null;
        try {
            String sql = "SELECT max(number) FROM " + ParkingArea.class.getName() + " p  ";
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Long) query.getSingleResult();
        } catch (Exception e) {
        } // Devuelve null
        return lastId;
    }

    public void deleteParkingAreaDetail(ParkingAreaDetail parkingAreaDetail){
        this.entityManager.remove(parkingAreaDetail);
    }

}
