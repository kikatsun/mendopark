package com.mendopark.repository;

import com.mendopark.model.security.GrantType;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@Stateless
@LocalBean
public class GrantTypeRepository implements Repository<Integer, GrantType> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static  final String SELECT_ENTITY = "SELECT td FROM " + GrantType.class.getName() + " td";

    @Override
    public GrantType get(Integer id) {
        GrantType result;

        String sql = SELECT_ENTITY + " WHERE td.number = :number";
        TypedQuery<GrantType> query = entityManager.createQuery(sql, GrantType.class);
        query.setParameter("number", id);
        try {
            result = query.getSingleResult();
        }catch (NoResultException ex){
            result = null;
            MEndoParkLogger.MENDOPARK.log(Level.INFO, "No se encontró entidad con id "+id, ex);
        }

        return result;
    }

    @Override
    public GrantType getAvailable(Integer id) {
        /*
         * No se implementa el método debido que a que no maneja fechas de disponibilidad
         * */
        return null;
    }

    @Override
    public List getAll() {
        String sql = SELECT_ENTITY;
        return getGrantTypes(sql);
    }

    private List<GrantType> getGrantTypes(String sql){
        TypedQuery<GrantType>  query = entityManager.createQuery(sql, GrantType.class);
        List<GrantType> result = query.getResultList();
        if (result == null)
            result = new ArrayList<GrantType>();

        return result;
    }

    @Override
    public List getAvailables() {
        /*
         * No se implementa el método debido que a que no maneja fechas de disponibilidad
         * */
        return null;
    }

    @Override
    public GrantType persist(GrantType entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(GrantType entity) {
    }

    @Override
    public Integer getLastId() {
        Integer lastId = null;
        try {
            String sql = "SELECT max(number) FROM " + GrantType.class.getName() + " td" ;
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Integer) query.getSingleResult();
        }
        catch (Exception e){} //Devuelve null

        return lastId;
    }
}
