package com.mendopark.repository;

import com.mendopark.model.ParkingSpace;
import com.mendopark.model.ParkingSpaceRegistration;
import com.mendopark.model.ParkingSpaceStateRegistration;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class ParkingSpaceRepository implements Repository<String, ParkingSpace> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pa FROM " + ParkingSpace.class.getName() + " pa";

    @Override
    public ParkingSpace get(String id) {
        ParkingSpace result = null;
        try {
            String sql = SELECT_ENTITY + " WHERE pa.id = :id";
            TypedQuery<ParkingSpace> query = entityManager.createQuery(sql, ParkingSpace.class);
            query.setParameter("id", id);
            result = query.getSingleResult();
        }catch (Exception e){}
        return result;
    }

    @Override
    public ParkingSpace getAvailable(String id) {
        return null;
    }

    @Override
    public List<ParkingSpace> getAll() {
        return null;
    }

    @Override
    public List<ParkingSpace> getAvailables() {
        return null;
    }

    @Override
    public ParkingSpace persist(ParkingSpace parkingSpace) {
        if (parkingSpace.getId() == null) {
            parkingSpace.setId(UUID.randomUUID().toString());
        }
        if (parkingSpace.getParkingSpaceRegistrations() != null &&
            parkingSpace.getParkingSpaceRegistrations().size() > 0) {
            for (ParkingSpaceRegistration parkingSpaceRegistration: parkingSpace.getParkingSpaceRegistrations()) {
                if (parkingSpaceRegistration.getId() == null) {
                    parkingSpaceRegistration.setId(UUID.randomUUID().toString());
                }
                if(parkingSpaceRegistration.getFromSensor() != null) {
                    if(parkingSpaceRegistration.getFromSensor().getId() == null) {
                        parkingSpaceRegistration.getFromSensor().setId(UUID.randomUUID().toString());
                    }
//                    if(parkingSpaceRegistration.getFromSensor().getCoordinatePoint() != null){
//                        if(parkingSpaceRegistration.getFromSensor().getCoordinatePoint().getId() == null){
//                            parkingSpaceRegistration.getFromSensor().getCoordinatePoint().setId(UUID.randomUUID().toString());
//                        }
//                        entityManager.persist(parkingSpaceRegistration.getFromSensor().getCoordinatePoint());
//                    }
                    entityManager.persist(parkingSpaceRegistration.getFromSensor());
                }
                entityManager.persist(parkingSpaceRegistration);
            }
        }
        if(parkingSpace.getCoordinatePoint() != null){
            if(parkingSpace.getCoordinatePoint().getId() == null ){
                parkingSpace.getCoordinatePoint().setId(UUID.randomUUID().toString());
            }
            entityManager.persist(parkingSpace.getCoordinatePoint());
        }
        if (parkingSpace.getStates() != null && parkingSpace.getStates().size() > 0) {
            for (ParkingSpaceStateRegistration parkingSpaceStateRegistration: parkingSpace.getStates()) {
                if(parkingSpaceStateRegistration.getId() == null) {
                    parkingSpaceStateRegistration.setId(UUID.randomUUID().toString());
                }
                entityManager.persist(parkingSpaceStateRegistration);
            }
        }
        entityManager.persist(parkingSpace);
        return parkingSpace;
    }

//    public ParkingSpace update(ParkingSpace parkingSpace) {
//        if (parkingSpace.getId() == null) {
//            parkingSpace = persist(parkingSpace);
//        }else {
//            if (parkingSpace.getParkingSpaceRegistrations() != null &&
//                    parkingSpace.getParkingSpaceRegistrations().size() > 0) {
//                for (ParkingSpaceRegistration parkingSpaceRegistration : parkingSpace.getParkingSpaceRegistrations()) {
//                    if (parkingSpaceRegistration.getId() == null) {
//                        parkingSpaceRegistration.setId(UUID.randomUUID().toString());
//                        if (parkingSpaceRegistration.getFromSensor() != null) {
//                            if (parkingSpaceRegistration.getFromSensor().getId() == null) {
//                                parkingSpaceRegistration.getFromSensor().setId(UUID.randomUUID().toString());
//                                entityManager.persist(parkingSpaceRegistration.getFromSensor());
//                            }else {
//                                entityManager.refresh(parkingSpaceRegistration.getFromSensor());
//                            }
//                        }
//                        entityManager.persist(parkingSpaceRegistration);
//                    }else {
//                        if (parkingSpaceRegistration.getFromSensor() != null) {
//                            if (parkingSpaceRegistration.getFromSensor().getId() == null) {
//                                parkingSpaceRegistration.getFromSensor().setId(UUID.randomUUID().toString());
//                                entityManager.persist(parkingSpaceRegistration.getFromSensor());
//                            }else {
//                                entityManager.refresh(parkingSpaceRegistration.getFromSensor());
//                            }
//                        }
//                        entityManager.refresh(parkingSpaceRegistration);
//                    }
//                }
//            }
//            if (parkingSpace.getCoordinatePoint() != null) {
//                if (parkingSpace.getCoordinatePoint().getId() == null) {
//                    parkingSpace.getCoordinatePoint().setId(UUID.randomUUID().toString());
//                    entityManager.persist(parkingSpace.getCoordinatePoint());
//                }else {
//                    entityManager.refresh(parkingSpace.getCoordinatePoint());
//                }
//            }
//            if (parkingSpace.getStates() != null && parkingSpace.getStates().size() > 0) {
//                for (ParkingSpaceStateRegistration parkingSpaceStateRegistration : parkingSpace.getStates()) {
//                    if (parkingSpaceStateRegistration.getId() == null) {
//                        parkingSpaceStateRegistration.setId(UUID.randomUUID().toString());
//                        entityManager.persist(parkingSpaceStateRegistration);
//                    }else {
//                        entityManager.refresh(parkingSpaceStateRegistration);
//                    }
//                }
//            }
//            entityManager.refresh(parkingSpace);
//        }
//        return parkingSpace;
//    }

    @Override
    public void finalize(ParkingSpace entity) {

    }

    @Override
    public String getLastId() {
        return null;
    }
}
