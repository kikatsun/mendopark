package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

@Stateless
@LocalBean
public class LocalityReporsitory implements Repository<Integer, Locality> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT lo FROM " + Locality.class.getName() + " lo";

    @Override
    public Locality get(Integer id) {
        Locality locality = null;
        String sql = SELECT_ENTITY + " WHERE lo.number = :number";
        TypedQuery<Locality> query = entityManager.createQuery(sql, Locality.class);
        query.setParameter("number", id);
        locality = query.getSingleResult();
        return locality;
    }

    @Override
    public Locality getAvailable(Integer id) {
        Locality result = null;

        String sql = SELECT_ENTITY + " WHERE lo.number = :number AND lo.endDate IS NULL";
        TypedQuery<Locality> query = entityManager.createQuery(sql, Locality.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<Locality> getAll() {
        String sql = SELECT_ENTITY;
        return getLocalities(sql);
    }

    @Override
    public List<Locality> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE lo.endDate IS NULL";
        //String sql = SELECT_ENTITY;
        return getLocalities(sql);
    }

    private List<Locality> getLocalities(String sql){
        TypedQuery<Locality> query = entityManager.createQuery(sql, Locality.class);
        List<Locality> result = null;
        try {
            result = query.getResultList();
        }catch (Exception ex){
            MEndoParkLogger.MENDOPARK.log(Level.WARNING, "No se pudieron encontrar localidades.", ex);
        }
        return result;
    }

    @Override
    public Locality persist(Locality entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Locality entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public Integer getLastId() {
        Integer lastId = null;
        try {
            String sql = "SELECT max(number) FROM " + Locality.class.getName() + " lo  ";
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Integer) query.getSingleResult();
        } catch (Exception e) {
        } // Devuelve null
        return lastId;
    }

    public boolean getLocalityExist(String nameLocality, Integer idProvince, Integer number){

        String nameLocalityAux = nameLocality.toLowerCase();
        Locality result;

        try {
            String sql = SELECT_ENTITY + " WHERE ( lo.name = :name OR LOWER(lo.name) = :nameMinus ) AND lo.fromProvince.number = :idProvince " +
                    "AND lo.endDate IS NULL ";

            if (number != null){
                sql += "AND lo.number != :number ";
            }

            TypedQuery<Locality> query = entityManager.createQuery(sql, Locality.class);
            query.setMaxResults(1);

            query.setParameter("name", nameLocality);
            query.setParameter("nameMinus", nameLocalityAux);
            query.setParameter("idProvince", idProvince);

            if (number != null){
                query.setParameter("number", number);
            }

            result = query.getSingleResult();

            if (result != null){
                return true;    //Localidad SI existe
            } else {
                return false;
            }
        } catch (NoResultException ex){
            System.out.println("No existe Localidad con nombre: " + nameLocalityAux + " y idProvince: " + idProvince);
            return false;
        }

    }
}
