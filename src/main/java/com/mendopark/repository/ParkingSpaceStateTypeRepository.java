
package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.model.ParkingSpaceStateType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class ParkingSpaceStateTypeRepository implements Repository<Integer, ParkingSpaceStateType> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + ParkingSpaceStateType.class.getName() + " pe";

	@Override
	public ParkingSpaceStateType get(Integer id) {
		ParkingSpaceStateType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<ParkingSpaceStateType> query = entityManager.createQuery(sql, ParkingSpaceStateType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public ParkingSpaceStateType getAvailable(Integer id) {
		ParkingSpaceStateType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<ParkingSpaceStateType> query = entityManager.createQuery(sql, ParkingSpaceStateType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<ParkingSpaceStateType> getAll() {
		String sql = SELECT_ENTITY;
		return getParkingSpaceStateTypes(sql);
	}

	@Override
	public List<ParkingSpaceStateType> getAvailables() {
		//String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		String sql = SELECT_ENTITY;
		return getParkingSpaceStateTypes(sql);
	}

	private List<ParkingSpaceStateType> getParkingSpaceStateTypes(String sql) {
		TypedQuery<ParkingSpaceStateType> query = entityManager.createQuery(sql, ParkingSpaceStateType.class);
		List<ParkingSpaceStateType> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<ParkingSpaceStateType>();
		}
		return result;
	}

	@Override
	public ParkingSpaceStateType persist(ParkingSpaceStateType entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(ParkingSpaceStateType entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + ParkingSpaceStateType.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getParkingSpaceStateTypeExist(String namePSST, String descriptionPSST, Integer number){

		String nameAux = namePSST.toLowerCase();
		String descriptionAux = descriptionPSST.toLowerCase();
		ParkingSpaceStateType result;

		try {
			String sql = SELECT_ENTITY + " WHERE ( pe.name = :name OR LOWER(pe.name) = :nameMinus ) " +
					"AND pe.endDate IS NULL ";

			if (number != null){
				sql += "AND pe.number != :number ";
			}

			TypedQuery<ParkingSpaceStateType> query = entityManager.createQuery(sql, ParkingSpaceStateType.class);
			query.setMaxResults(1);
			query.setParameter("name", namePSST);
			query.setParameter("nameMinus", nameAux);

			if (number != null){
				query.setParameter("number", number);
			}

			result = query.getSingleResult();

			if (result != null){
				return true;
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("getParkingSpaceStateTypeExist(). No existe Tipo Estado Plaza Estacionamiento con nombre: " + namePSST + " y description: " + descriptionPSST);
			return false;
		}

	}
}
