package com.mendopark.repository;

import com.mendopark.model.security.RoleResourceAssignment;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

@Stateless
@LocalBean
public class RoleResourceAssignmentRepository implements Repository <String, RoleResourceAssignment> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT x FROM " + RoleResourceAssignment.class.getName() + " x";

    @Override
    public RoleResourceAssignment get(String id) {
        RoleResourceAssignment locality = null;
        String sql = SELECT_ENTITY + " WHERE x.id = :id";
        TypedQuery<RoleResourceAssignment> query = entityManager.createQuery(sql, RoleResourceAssignment.class);
        query.setParameter("id", id);
        try {
            locality = query.getSingleResult();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.INFO, "No se encontró entidad con id "+id, ex);
        }
        return locality;
    }

    @Override
    public RoleResourceAssignment getAvailable(String id) {
        RoleResourceAssignment result = null;

        String sql = SELECT_ENTITY + " WHERE x.id = :id AND x.endDate IS NULL";
        TypedQuery<RoleResourceAssignment> query = entityManager.createQuery(sql, RoleResourceAssignment.class);
        query.setParameter("id", id);
        try {
            result = query.getSingleResult();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.INFO, "No se encontró entidad con id "+id, ex);
        }
        return result;
    }

    @Override
    public List<RoleResourceAssignment> getAll() {
        String sql = SELECT_ENTITY;
        return getRoleResourceAssignments(sql);
    }

    @Override
    public List<RoleResourceAssignment> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE x.endDate IS NULL";
        //String sql = SELECT_ENTITY;
        return getRoleResourceAssignments(sql);
    }

    private List<RoleResourceAssignment> getRoleResourceAssignments(String sql){
        TypedQuery<RoleResourceAssignment> query = entityManager.createQuery(sql, RoleResourceAssignment.class);
        List<RoleResourceAssignment> result = null;
        try {
            result = query.getResultList();
        }catch (NoResultException ex){
            MEndoParkLogger.MENDOPARK.log(Level.WARNING, "No se pudieron encontrar asignaciones de rol a recurso.", ex);
        }
        return result;
    }

    @Override
    public RoleResourceAssignment persist(RoleResourceAssignment entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(RoleResourceAssignment entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public String getLastId() {
        return UUID.randomUUID().toString();
    }
}
