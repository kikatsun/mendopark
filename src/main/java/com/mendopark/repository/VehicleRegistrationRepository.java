package com.mendopark.repository;

import com.mendopark.model.VehicleRegistration;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class VehicleRegistrationRepository implements Repository<String, VehicleRegistration> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pe FROM " + VehicleRegistration.class.getName() + " pe";

    @Override
    public VehicleRegistration get(String domain) {
        VehicleRegistration result = null;
        try {
            String sql = SELECT_ENTITY + " WHERE pe.domain = :domain";
            TypedQuery<VehicleRegistration> query = entityManager.createQuery(sql, VehicleRegistration.class);
            query.setParameter("domain", domain);
            result = query.getSingleResult();
        }catch (Exception ex){
            return null;
        }
        return result;
    }

    @Override
    public VehicleRegistration getAvailable(String domain) {
        VehicleRegistration result = null;
        try {
            String sql = SELECT_ENTITY + " WHERE pe.domain = :domain AND pe.endDate IS NULL";
            TypedQuery<VehicleRegistration> query = entityManager.createQuery(sql, VehicleRegistration.class);
            query.setParameter("domain", domain);
            result = query.getSingleResult();
        }catch (Exception ex){
            return null;
        }
        return result;
    }

    @Override
    public List<VehicleRegistration> getAll() {
        String sql = SELECT_ENTITY;
        return getVehicleRegistrations(sql);
    }

    @Override
    public List<VehicleRegistration> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
        return getVehicleRegistrations(sql);
    }

    private List<VehicleRegistration> getVehicleRegistrations(String sql) {
        TypedQuery<VehicleRegistration> query = entityManager.createQuery(sql, VehicleRegistration.class);
        List<VehicleRegistration> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<VehicleRegistration>();
        }
        return result;
    }

    @Override
    public VehicleRegistration persist(VehicleRegistration entity) {
        if(entity.getId() == null){
            entity.setId(UUID.randomUUID().toString());
        }
        System.out.println("DEBUGGGGGGGGGGGGGGGGGGGGGGGGGGGG: "+entity.getId());
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(VehicleRegistration entity) {
        /* No tiene fecha de fin */
    }

    @Override
    public String getLastId() {
        return null;
    }
}
