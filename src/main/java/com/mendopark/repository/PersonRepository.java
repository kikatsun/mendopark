package com.mendopark.repository;

import com.mendopark.model.Person;
import com.mendopark.model.PersonPK;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;

@Stateless
@LocalBean
public class PersonRepository implements Repository<PersonPK, Person> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    private AddressRepository addressRepository;

    private static final String SELECT_ENTITY = "SELECT pe FROM " + Person.class.getName() + " pe";

    @Override
    public Person get(PersonPK id) {
        Person result = null;
        String documentNumber = id.getDocumentNumber();
        Long documentType = id.getDocumentType().getNumber();
        String sql = SELECT_ENTITY + " WHERE pe.documentNumber = :dn AND pe.documentType = :dt";
        TypedQuery<Person> query = entityManager.createQuery(sql, Person.class);
        query.setParameter("dn", documentNumber);
        query.setParameter("dt", documentType);
        result = query.getSingleResult();
        return result;
    }

    @Override
    public Person getAvailable(PersonPK id) {
        // ToDo no utilizado
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<Person> getAll() {
        // ToDo no utilizado
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<Person> getAvailables() {
        // ToDo no utilizado
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Person persist(Person entity) {
        try {
            this.entityManager.persist(entity);
        } catch (Exception e) {
            System.out.println("Error persist " + e.getLocalizedMessage());
        }

        return entity;
    }

    public void update (Person entity){
        try {
            this.entityManager.persist(entity);
        } catch (Exception e){
            System.out.println("PersonRepository Error update " + e.getLocalizedMessage());
        }
    }

    @Override
    public void finalize(Person entity) {
        // ToDo no utilizado
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public PersonPK getLastId() {
        // ToDo no utilizado
        throw new UnsupportedOperationException("Not implemented");
    }
}
