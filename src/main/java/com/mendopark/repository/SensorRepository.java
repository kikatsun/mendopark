package com.mendopark.repository;

import com.mendopark.model.CoordinatePoint;
import com.mendopark.model.Locality;
import com.mendopark.model.Sensor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class SensorRepository implements Repository<String, Sensor>{

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT s FROM " + Sensor.class.getName() + " s";

    @Override
    public Sensor get(String id) {
        Sensor result;
        try {
            String sql = SELECT_ENTITY + " WHERE s.id = :id";
            TypedQuery<Sensor> query = entityManager.createQuery(sql, Sensor.class);
            query.setParameter("id", id);
            result = query.getSingleResult();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            result = null;
        }
        return result;
    }

    @Override
    public Sensor getAvailable(String id) {
        Sensor result = null;

        String sql = SELECT_ENTITY + " WHERE s.id = :id AND s.endDate IS NULL";
        TypedQuery<Sensor> query = entityManager.createQuery(sql, Sensor.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<Sensor> getAll() {
        String sql = SELECT_ENTITY + " ORDER BY s.referenceCode";
        return getSensors(sql);
    }

    @Override
    public List<Sensor> getAvailables() {
        return getSensors(SELECT_ENTITY);
    }

    private List<Sensor> getSensors(String sql) {
        TypedQuery<Sensor> query = entityManager.createQuery(sql, Sensor.class);
        List<Sensor> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<Sensor>();
        }
        return result;
    }

    @Override
    public Sensor persist(Sensor entity) {
        if(entity.getId() == null || entity.getId().isEmpty()){
            entity.setId(UUID.randomUUID().toString());
        }
//        if(entity.getCoordinatePoint() != null){
//            CoordinatePoint coordinatePoint = entity.getCoordinatePoint();
//            if(coordinatePoint.getId() == null || coordinatePoint.getId().isEmpty()){
//                coordinatePoint.setId(UUID.randomUUID().toString());
//            }
//            this.entityManager.persist(coordinatePoint);
//        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Sensor entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public String getLastId() {
        return UUID.randomUUID().toString();
    }

    public boolean getSensorExist(String nombreSensor, String descripcionSensor, String id){
        String nombreAux = nombreSensor.toLowerCase();
        //String descripcionAux = descripcionSensor.toLowerCase();
        Sensor result;

        try{
            //String sql = SELECT_ENTITY + " WHERE LOWER(s.referenceCode) = :referenceCode AND LOWER(s.description) = :description " +
            String sql = SELECT_ENTITY + " WHERE LOWER(s.referenceCode) = :referenceCode " +
                    "AND s.endDate IS NULL ";

            if (id != null){
                sql += "AND s.id != :idSensor ";
            }

            TypedQuery<Sensor> query = entityManager.createQuery(sql, Sensor.class);
            query.setMaxResults(1);
            query.setParameter("referenceCode", nombreAux);
            //query.setParameter("description", descripcionAux);

            if (id != null){
                query.setParameter("idSensor", id);
            }

            result = query.getSingleResult();

            if (result != null){
                return true;    //Sensor SI existe
            } else {
                return false;
            }
        } catch (NoResultException ex){
            System.out.println("No existe Sensor para el nombre: " + nombreSensor + " descripcion: " + descripcionSensor);
            return false;
        }


    }
}
