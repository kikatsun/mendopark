
package com.mendopark.repository;

import com.mendopark.model.CommentType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class CommentTypeRepository implements Repository<Integer, CommentType> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + CommentType.class.getName() + " pe";

	@Override
	public CommentType get(Integer id) {
		CommentType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<CommentType> query = entityManager.createQuery(sql, CommentType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public CommentType getAvailable(Integer id) {
		CommentType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<CommentType> query = entityManager.createQuery(sql, CommentType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<CommentType> getAll() {
		String sql = SELECT_ENTITY;
		return getCommentTypes(sql);
	}

	@Override
	public List<CommentType> getAvailables() {
		String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL";
		return getCommentTypes(sql);
	}

	private List<CommentType> getCommentTypes(String sql) {
		TypedQuery<CommentType> query = entityManager.createQuery(sql, CommentType.class);
		List<CommentType> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<CommentType>();
		}
		return result;
	}

	@Override
	public CommentType persist(CommentType entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(CommentType entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + CommentType.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}
}
