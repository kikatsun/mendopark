package com.mendopark.repository;

import com.mendopark.model.DocumentType;
import com.mendopark.model.PayType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class DocumentTypeRepository implements Repository<Long, DocumentType> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static  final String SELECT_ENTITY = "SELECT td FROM " + DocumentType.class.getName() + " td";

    @Override
    public DocumentType get(Long id) {
        DocumentType result = null;

        String sql = SELECT_ENTITY + " WHERE td.number = :number";
        TypedQuery<DocumentType> query = entityManager.createQuery(sql, DocumentType.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public DocumentType getAvailable(Long id) {
        /*
        * JVera: no implemento el método debido que a que no maneja fechas de disponibilidad
        * */
        return null;
    }

    @Override
    public List getAll() {
        String sql = SELECT_ENTITY;
        return getDocumentTypes(sql);
    }

    private List<DocumentType> getDocumentTypes(String sql){
        TypedQuery<DocumentType>  query = entityManager.createQuery(sql, DocumentType.class);
        List<DocumentType> result = query.getResultList();
        if (result == null)
            result = new ArrayList<DocumentType>();

        return result;
    }

    @Override
    public List getAvailables() {
        /*
         * Jonas: no implemento el método debido que a que no maneja fechas de disponibilidad
         * */
        return null;
    }

    @Override
    public DocumentType persist(DocumentType entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(DocumentType entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public Long getLastId() {
        Long lastId = null;
        try {
            String sql = "SELECT max(number) FROM " + DocumentType.class.getName() + " td" ;
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Long) query.getSingleResult();
        }
        catch (Exception e){} //Devuelve null

        return lastId;
    }
}
