package com.mendopark.repository;

import com.mendopark.model.ClientType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class ClientTypeRepository implements Repository<Integer, ClientType> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT c FROM " + ClientType.class.getName() + " c";

    @Override
    public ClientType get(Integer id) {
        ClientType result = null;

        String sql = SELECT_ENTITY + " WHERE c.number = :number";
        TypedQuery<ClientType> query = entityManager.createQuery(sql, ClientType.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public ClientType getAvailable(Integer id) {
        ClientType result = null;

        String sql = SELECT_ENTITY + " WHERE c.number = :number AND c.endDate IS NULL";
        TypedQuery<ClientType> query = entityManager.createQuery(sql, ClientType.class);
        query.setParameter("number", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<ClientType> getAll() {
        String sql = SELECT_ENTITY;
        return getClients(sql);
    }

    @Override
    public List<ClientType> getAvailables() {
        String sql = SELECT_ENTITY + " WHERE c.endDate IS NULL";
        return getClients(sql);
    }

    private List<ClientType> getClients(String sql) {
        TypedQuery<ClientType> query = entityManager.createQuery(sql, ClientType.class);
        List<ClientType> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<ClientType>();
        }
        return result;
    }

    @Override
    public ClientType persist(ClientType entity) {
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(ClientType entity) {
        entity.setEndDate(new Date());
        this.entityManager.persist(entity);
    }

    @Override
    public Integer getLastId() {
        Integer lastId = null;
        try {
            String sql = "SELECT max(number) FROM " + ClientType.class.getName() + " p  ";
            Query query = entityManager.createQuery(sql);
            query.setMaxResults(1);
            lastId = (Integer) query.getSingleResult();
        } catch (Exception e) {
        } // Devuelve null
        return lastId;
    }
}
