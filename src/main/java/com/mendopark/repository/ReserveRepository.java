package com.mendopark.repository;

import com.mendopark.model.Reserve;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
public class ReserveRepository implements Repository<String, Reserve> {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pe FROM " + Reserve.class.getName() + " pe";

    @Override
    public Reserve get(String id) {
        Reserve result = null;
        String sql = SELECT_ENTITY + " WHERE pe.id = :id";
        TypedQuery<Reserve> query = entityManager.createQuery(sql, Reserve.class);
        query.setParameter("id", id);
        try {
            result = query.getSingleResult();
        }catch (Exception e){
            System.out.println("No se encontró la reserva con id : "+id);
        }
        return result;
    }

    @Override
    public Reserve getAvailable(String id) {
        Reserve result = null;

        String sql = SELECT_ENTITY + " WHERE pe.id = :id ";
        TypedQuery<Reserve> query = entityManager.createQuery(sql, Reserve.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<Reserve> getAll() {
        String sql = SELECT_ENTITY ;
        return getReserves(sql);
    }

    @Override
    public List<Reserve> getAvailables() {
        //String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL"; se comenta para que muestre los que se encuentran con fechaBaja
        String sql = SELECT_ENTITY ;
        return getReserves(sql);
    }

    private List<Reserve> getReserves(String sql) {
        TypedQuery<Reserve> query = entityManager.createQuery(sql, Reserve.class);
        List<Reserve> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<Reserve>();
        }
        return result;
    }

    @Override
    public Reserve persist(Reserve entity) {
        if(entity.getId() == null){
            entity.setId(UUID.randomUUID().toString());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Reserve entity) {
        //no se dan de baja
    }

    @Override
    public String getLastId() {
        return UUID.randomUUID().toString();
    }
}
