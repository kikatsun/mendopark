
package com.mendopark.repository;

import com.mendopark.model.Locality;
import com.mendopark.model.ReserveStateType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Repositorio del Tipo Estado de Reserva.
 * Se encarga de las operaciones con la Base de Datos para poder crear, actualizar, eliminar y consultar un usuario.
 *
 * @Pattern: Indirection
 */

@Stateless
@LocalBean
public class ReserveStateTypeRepository implements Repository<Integer, ReserveStateType> {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	private static final String SELECT_ENTITY = "SELECT pe FROM " + ReserveStateType.class.getName() + " pe";

	@Override
	public ReserveStateType get(Integer id) {
		ReserveStateType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number";
		TypedQuery<ReserveStateType> query = entityManager.createQuery(sql, ReserveStateType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public ReserveStateType getAvailable(Integer id) {
		ReserveStateType result = null;

		String sql = SELECT_ENTITY + " WHERE pe.number = :number AND pe.endDate IS NULL";
		TypedQuery<ReserveStateType> query = entityManager.createQuery(sql, ReserveStateType.class);
		query.setParameter("number", id);
		result = query.getSingleResult();

		return result;
	}

	@Override
	public List<ReserveStateType> getAll() {
		String sql = SELECT_ENTITY;
		return getReserveStateTypes(sql);
	}

	@Override
	public List<ReserveStateType> getAvailables() {
		//String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL"; se comenta para que muestre los que se encuentran con fechaBaja
		String sql = SELECT_ENTITY;
		return getReserveStateTypes(sql);
	}

	private List<ReserveStateType> getReserveStateTypes(String sql) {
		TypedQuery<ReserveStateType> query = entityManager.createQuery(sql, ReserveStateType.class);
		List<ReserveStateType> result = query.getResultList();
		if (result == null) {
			result = new ArrayList<ReserveStateType>();
		}
		return result;
	}

	@Override
	public ReserveStateType persist(ReserveStateType entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public void finalize(ReserveStateType entity) {
		entity.setEndDate(new Date());
		this.entityManager.persist(entity);
	}

	@Override
	public Integer getLastId() {
		Integer lastId = null;
		try {
			String sql = "SELECT max(number) FROM " + ReserveStateType.class.getName() + " p  ";
			Query query = entityManager.createQuery(sql);
			query.setMaxResults(1);
			lastId = (Integer) query.getSingleResult();
		} catch (Exception e) {
		} // Devuelve null
		return lastId;
	}

	public boolean getReserveStateTypeExist(String nameRST, String description, Integer number){

		String nameAux = nameRST.toLowerCase();
		//String descriptionAux = description.toLowerCase();
		ReserveStateType result;

		try {
			//String sql = SELECT_ENTITY + " WHERE LOWER(pe.name) = :name AND LOWER(pe.description) = :description";
			String sql = SELECT_ENTITY + " WHERE (pe.name = :name OR LOWER(pe.name) = :nameMinus) " +
					"AND pe.endDate IS NULL";

			if (number != null){
				sql += "AND pe.number != :number";
			}

			TypedQuery<ReserveStateType> query = entityManager.createQuery(sql, ReserveStateType.class);
			query.setMaxResults(1);
			query.setParameter("name", nameRST);
			query.setParameter("nameMinus", nameAux);

			if (number != null){
				query.setParameter("number", number);
			}

			result = query.getSingleResult();

			if (result != null){
				return true;
			} else {
				return false;
			}
		} catch (NoResultException ex){
			System.out.println("getReserveStateType(). No existe Tipo Estado Reserva con nombre: " + nameRST + " y descripcion: " + description);
			return false;
		}
	}
}
