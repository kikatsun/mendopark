package com.mendopark.repository;

import com.mendopark.model.Parking;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Stateless
@LocalBean
public class ParkingRepository implements Repository<String, Parking> {


    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    private static final String SELECT_ENTITY = "SELECT pe FROM " + Parking.class.getName() + " pe";

    @Override
    public Parking get(String id) {
        Parking result = null;
        String sql = SELECT_ENTITY + " WHERE pe.id = :id";
        TypedQuery<Parking> query = entityManager.createQuery(sql, Parking.class);
        query.setParameter("id", id);
        try {
            result = query.getSingleResult();
        }catch (Exception e){
            System.out.println("No se encontró la reserva con id : "+id);
        }
        return result;
    }

    @Override
    public Parking getAvailable(String id) {
        Parking result = null;

        String sql = SELECT_ENTITY + " WHERE pe.id = :id ";
        TypedQuery<Parking> query = entityManager.createQuery(sql, Parking.class);
        query.setParameter("id", id);
        result = query.getSingleResult();

        return result;
    }

    @Override
    public List<Parking> getAll() {
        String sql = SELECT_ENTITY + " WHERE pe.parkingSpace IS NOT NULL ORDER BY pe.end ASC";
        List<Parking> result = getParkings(sql);
        if (result != null) {
            for (Parking parking: result ) {
                parking.getParkingArea();
                parking.getParkingSpace();
            }
        }
        return result;
    }

    @Override
    public List<Parking> getAvailables() {
        //String sql = SELECT_ENTITY + " WHERE pe.endDate IS NULL"; se comenta para que muestre los que se encuentran con fechaBaja
        String sql = SELECT_ENTITY ;
        return getParkings(sql);
    }

    private List<Parking> getParkings(String sql) {
        TypedQuery<Parking> query = entityManager.createQuery(sql, Parking.class);
        List<Parking> result = query.getResultList();
        if (result == null) {
            result = new ArrayList<Parking>();
        }
        return result;
    }

    @Override
    public Parking persist(Parking entity) {
        if(entity.getId() == null){
            entity.setId(UUID.randomUUID().toString());
        }
        if(entity.getReserve() != null){
            if(entity.getReserve().getId() == null){
                entity.getReserve().setId(UUID.randomUUID().toString());
            }
            entityManager.persist(entity.getReserve());
        }
        this.entityManager.persist(entity);
        return entity;
    }

    @Override
    public void finalize(Parking entity) {
        //no se dan de baja
    }

    @Override
    public String getLastId() {
        return UUID.randomUUID().toString();
    }

}
