package com.mendopark.session;

import com.mendopark.model.Session;
import com.mendopark.model.User;
import com.mendopark.repository.UserRepository;
import com.mendopark.rest.session.MendoParkSessionDto;

import javax.ejb.*;
import javax.ws.rs.ServerErrorException;
import java.security.InvalidParameterException;
import java.util.List;

/**
 * Experto de Sesión
 * Experto que define la lógica relacionada con la Sesión
 *
 * @Pattern: Expert
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SessionExpert {

    @EJB
    SessionManager sessionManager;
    @EJB
    private UserRepository userRepository;

    public SessionExpert(){
    }

    public Session createSession(Session session){
        User sessionUser = session.getUserSession();
        User persistedUser = userRepository.getByName(sessionUser.getName());
        if (persistedUser != null) {
            if (persistedUser.getPassword().equals(sessionUser.getPassword())) {
                if(persistedUser.getEndDate() == null) {
                    session.setUserSession(persistedUser);
                    session = MendoParkSessionDto.Factory.createSession(session);
                    session = sessionManager.add(session);
                }else{
                    throw new InvalidParameterException("No puede ingresar porque su usuario: "+sessionUser.getName()+" está dado de baja.");
                }
            }else{
                throw new InvalidParameterException("Contaseña inválida para el usuario: "+sessionUser.getName());
            }
        }else{
            throw new InvalidParameterException("Usuario inválido: " + sessionUser.getName() );
        }
        if(session == null){
            throw new ServerErrorException("No se pudo crear la sesión para el usuario " + sessionUser,500);
        }
        return session;
    }

    public List<Session> findSessions() {
        return sessionManager.getSessions();
    }
}
