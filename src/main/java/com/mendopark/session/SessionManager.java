package com.mendopark.session;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.model.Session;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogManager;

/**
 * Administrador de Sesiones.
 * Se encarga de tener en memoria las sesiones válidas activas que se hayan creado, se define la lógica de cuándo una
 * sesión es válida, al igual que su tiempo de expiración.
 *
 * @Pattern: Singleton
 */
@Singleton
public class SessionManager {

    @EJB
    private CredentialManager credentialManager;

    private static final int MAX_SESSION_SIZE = 10; //tamaño máximo de sesiones en memoria.
    private static final int MINUTES_LIFE_TIME = 30; //minutos de vida de la sesión.
    private static final int LIFE_TIME = MINUTES_LIFE_TIME * 60 * 1000; //tiempo de vida de la sesión

    /* El mapa se ordena por la fecha de creación cuando se actualiza con put, los primeros son los más antiguos */
    private Map<Session, String> sessionMap = new TreeMap<Session, String>();

    /**
     * Se busca una sesión válida en el mapa de sesiones y actualiza la fecha de la sesión
     * @param sessionId
     * @return devuelve {@code null}} si no se encuentra o es una sesión inválida
     */
    public Session find(String sessionId){
        Session session = get(sessionId);
        if(session != null) {
            session = validate(session);
        }
        return session;
    }

    /**
     * Se busca la sesión a partir de un id de sesión,
     * @param sessionId
     * @return
     */
    private Session get(String sessionId){
        Session session = null;
        if(sessionMap.containsValue(sessionId)){
            Iterator<Session> sessionIterator = sessionMap.keySet().iterator();
            while(sessionIterator.hasNext()){
                Session sessionTarget = sessionIterator.next();
                if (sessionTarget.getSessionId().equals(sessionId)){
                    session = sessionTarget;
                    break;
                }
            }
        }
        return session;
    }

    /**
     * Agrega una sesión al mapa de sesiones
     * @param session
     * @return
     */
    public Session add(Session session){
        if(!sessionMap.containsValue(session.getSessionId())) {
            boolean isActive =false;
            Session sessionActived = null;
            for (Session mSession: sessionMap.keySet()) {
                if(mSession.getUserSession().getName().equals(session.getUserSession().getName())){
                    isActive = true;
                    sessionActived = mSession;
                    break;
                }
            }
            if(isActive){
                remove(sessionActived);
            }
            if (sessionMap.size() > MAX_SESSION_SIZE) {
                LogManager.getLogManager().getLogger(this.getClass().getName()).log(Level.WARNING, "Exceded sessions limit");
            }
            sessionMap.put(session, session.getSessionId());
            if (session.getUserSession().getUserRole() != null &&
                CredentialManager.credencialablesRoles.contains(session.getUserSession().getUserRole().getNumber())) {
                credentialManager.addCredential(session);
            }
        }else{
            LogManager.getLogManager().getLogger(this.getClass().getName()).log(Level.SEVERE, "Invalid sessionId, there are one with same Id {0}",session.getSessionId());
            session = null; // se devuelve null porque es inválido.
        }
        return session;
    }

    /**
     * Quita la sesión con el id de sesión del mapa de sesiones.
     * @param sessionId
     */
    public void remove(String sessionId){
        Session sessionToRemove = get(sessionId);
        if(sessionToRemove != null){
            remove(sessionToRemove);
        }
        credentialManager.removeCredential(sessionId);
    }

    /**
     * Quita la sesión en el mapa de sesiones.
     * @param session
     */
    private void remove(Session session){
        sessionMap.remove(session);
        credentialManager.removeCredential(session.getSessionId());
    }

    /**
     * Valida que el tiempo máximo de vida de la sesión sea menor a la fecha actual y actualiza la fecha de último acceso
     * con la fecha actual.
     * @param session
     * @return
     */
    private Session validate(Session session){
        long maxSessionLifeTime = session.getLastAccessedDate().getTime() + LIFE_TIME;
        long currenTime = System.currentTimeMillis();
        if(currenTime <= maxSessionLifeTime){
            session.setLastAccessedDate(new Date(currenTime));
        }else{
            session = null;
        }
        return session;
    }

    public List<Session> getSessions(){
        return new ArrayList<Session>(sessionMap.keySet());
    }

}
