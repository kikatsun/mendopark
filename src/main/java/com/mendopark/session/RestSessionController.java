package com.mendopark.session;

import com.mendopark.model.Role;
import com.mendopark.model.Session;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Esta clase es la encargada de coordinar el control de sesión entre los controladores de tipo Rest y nuestro sistema.
 */
@Stateless
@LocalBean
public class RestSessionController {

    @EJB
    SessionManager sessionManager;

    public boolean validateSession(String sessionId, List<Integer> allowedRoles){
        boolean result = false;
        Session session = sessionManager.find(sessionId);
        if( session != null &&
            session.getUserSession().getUserRole() != null &&
            allowedRoles.contains(session.getUserSession().getUserRole().getNumber())){
            result = true;
        } else {
            System.out.println("Sesion no encontrada: "+sessionId);
        }
        return result;
    }

    public Role getRoleFromSession(String sessionId){
        Role role = null;
        Session session = sessionManager.find(sessionId);
        if(session != null && session.getUserSession() != null && session.getUserSession().getUserRole() != null){
            role = session.getUserSession().getUserRole();
        }
        return role;
    }

}
