package com.mendopark.session;

import java.util.UUID;

public class MendoParkSession implements Comparable<MendoParkSession>{

    private String sessionId;
    private String userName;
    private String password;
    private long creationDate;
    private long lastAccessedDate;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getLastAccessedDate() {
        return lastAccessedDate;
    }

    public void setLastAccessedDate(long lastAccessedDate) {
        this.lastAccessedDate = lastAccessedDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int compareTo(MendoParkSession mendoParkSession) {
        return (int)(this.lastAccessedDate - mendoParkSession.getLastAccessedDate());
    }

    public static class Factory{

        public static final MendoParkSession createSession(String userName){
            MendoParkSession mendoParkSession = new MendoParkSession();
            mendoParkSession.setSessionId(UUID.randomUUID().toString());
            mendoParkSession.setUserName(userName);
            mendoParkSession.setCreationDate(System.currentTimeMillis());
            mendoParkSession.setLastAccessedDate(mendoParkSession.getCreationDate());
            return mendoParkSession;
        }

    }
}
