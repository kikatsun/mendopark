
package com.mendopark.crud.parkingspacetype;

import com.mendopark.application.SExpert;
import com.mendopark.model.ParkingSpaceStateType;
import com.mendopark.model.ParkingSpaceType;
import com.mendopark.model.Province;
import com.mendopark.repository.ParkingSpaceTypeRepository;
import com.mendopark.rest.parkingspacetype.ParkingSpaceTypeDto;

import javax.ejb.*;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ParkingSpaceTypeSExpert extends SExpert {

	@EJB
	private ParkingSpaceTypeRepository parkingSpaceTypeRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = parkingSpaceTypeRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public ParkingSpaceType find(ParkingSpaceTypeDto dto) {
		ParkingSpaceType result = null;

		result = parkingSpaceTypeRepository.get(dto.getNumber());

		return result;
	}

	public List<ParkingSpaceType> findParkingSpaceTypes() {
		return parkingSpaceTypeRepository.getAvailables();
	}

	public List<ParkingSpaceType> findParkingSpaceTypesS() {
		return parkingSpaceTypeRepository.getAvailablesS();
	}

	public boolean delete(ParkingSpaceTypeDto parkingSpaceTypeDto) {
		boolean result;
		ParkingSpaceType parkingSpaceType = parkingSpaceTypeRepository.get(parkingSpaceTypeDto.getNumber());
		parkingSpaceType.setEndDate(new Date());
		try {
			parkingSpaceTypeRepository.persist(parkingSpaceType);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public ParkingSpaceType update(ParkingSpaceTypeDto parkingSpaceTypeDto) throws Exception {

		ParkingSpaceType result = parkingSpaceTypeRepository.get(parkingSpaceTypeDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(parkingSpaceTypeDto.getName());

		result.setDescription(parkingSpaceTypeDto.getDescription());

		parkingSpaceTypeRepository.persist(result);

		return result;
	}

	public ParkingSpaceType persist(ParkingSpaceTypeDto parkingSpaceTypeDto) {
		ParkingSpaceType result = ParkingSpaceTypeDto.Factory.getParkingSpaceTypeFromDto(parkingSpaceTypeDto);
		parkingSpaceTypeRepository.persist(result);
		return result;
	}

	public boolean validateNameAndDescription(ParkingSpaceTypeDto parkingSpaceTypeDto) {

		/* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
		boolean result = false;

		if (parkingSpaceTypeDto.getNumber() != null){
			ParkingSpaceType parkingSpaceType = parkingSpaceTypeRepository.get(parkingSpaceTypeDto.getNumber());
			if (parkingSpaceType != null){
				if (!parkingSpaceTypeDto.getName().equals(parkingSpaceType.getName())){
					result = parkingSpaceTypeRepository.getParkingSpaceTypeExist(parkingSpaceTypeDto.getName(), parkingSpaceTypeDto.getDescription(), parkingSpaceTypeDto.getNumber());
				}
			}
		} else {
			result = parkingSpaceTypeRepository.getParkingSpaceTypeExist(parkingSpaceTypeDto.getName(), parkingSpaceTypeDto.getDescription(), null);
		}
		return result;

		//return parkingSpaceTypeRepository.getParkingSpaceTypeExist(parkingSpaceTypeDto.getName(), parkingSpaceTypeDto.getDescription());
	}
}
