
package com.mendopark.crud.province;

import com.mendopark.application.SExpert;
import com.mendopark.model.Province;
import com.mendopark.repository.ProvinceRepository;
import com.mendopark.rest.province.ProvinceDto;

import javax.ejb.*;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ProvinceSExpert extends SExpert {

	@EJB
	private ProvinceRepository provinceRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = provinceRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public Province find(ProvinceDto dto) {
		Province result = null;

		result = provinceRepository.get(dto.getNumber());

		return result;
	}

	public List<Province> findProvinces() {
		return provinceRepository.getAll();
	}

	public List<Province> findProvincesAvailables() {
		return provinceRepository.getAvailables();
	}

	public boolean delete(ProvinceDto provinceDto) {
		boolean result;
		Province province = provinceRepository.get(provinceDto.getNumber());
		province.setEndDate(new Date());
		try {
			provinceRepository.persist(province);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public Province update(ProvinceDto provinceDto) throws Exception {

		Province result = provinceRepository.get(provinceDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(provinceDto.getName());

		provinceRepository.persist(result);

		return result;
	}

	public Province persist(ProvinceDto provinceDto) {
		Province result = ProvinceDto.Factory.getProvinceFromDto(provinceDto);
		provinceRepository.persist(result);
		return result;
	}

	public boolean validateName(ProvinceDto provinceDto) {
		/* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
		boolean result = false;

		if (provinceDto.getNumber() != null){
			Province province = provinceRepository.get(provinceDto.getNumber());
			if (province != null){
				if (!provinceDto.getName().equals(province.getName())){
					result = provinceRepository.getProvinceExist(provinceDto.getName(), provinceDto.getNumber());
				}
			}
		} else {
			result = provinceRepository.getProvinceExist(provinceDto.getName(), null);
		}
		return result;
		//return provinceRepository.getProvinceExist(provinceDto.getName());
	}
}
