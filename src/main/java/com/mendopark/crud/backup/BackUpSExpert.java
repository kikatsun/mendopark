
package com.mendopark.crud.backup;

import com.mendopark.model.Client;
import com.mendopark.model.Day;
import com.mendopark.repository.ClientRepository;
import com.mendopark.repository.DayRepository;
import com.mendopark.rest.day.DayDto;

import javax.ejb.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BackUpSExpert {

	@EJB
	private DayRepository dayRepository;

	@EJB
	private ClientRepository clientRepository;

	public int generarBackUpBD() {
		int copiaSeguridad = 1;

		try{
			Calendar fecha = Calendar.getInstance();
			String fechaHoy = fecha.get(Calendar.DATE)+"_"+fecha.get(Calendar.MONTH)+"_"+fecha.get(Calendar.YEAR);
			String dbUserName = "root";
			String dbPassword = "123456";
			String dbName = "mendopark";

			/// *** Para Windorchooooooooooo
			//String comando = "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.1\\bin\\mysqldump.exe --opt  --password=123456  --user=root --databases mendopark -r ";
			String pathDestinoArchivo = "D:\\";

			/// *** Para Linux
			//String comando = "mysqldump -u " + dbUserName + " -p" + dbPassword + " --add-drop-database -B " + dbName + " -r ";
			//String pathDestinoArchivo = "/home/erick/";
			//String nombreArchivo = "Respaldo_Sistema_"+fechaHoy+".sql";
			//Process runtimeProcess = Runtime.getRuntime().exec(comando + pathDestinoArchivo + nombreArchivo);

			String comando = "cp /home/erick/mendopark.sql ";
			String nombreArchivo = "/home/erick/mendoparkupdate_"+fechaHoy+".sql";

			Process runtimeProcess = Runtime.getRuntime().exec(comando + nombreArchivo);

			copiaSeguridad = runtimeProcess.waitFor();

		} catch (Exception e) {
			System.out.println("Error al generarBackUpBD() " + e.getMessage());
		}

		return copiaSeguridad;

	}

}
