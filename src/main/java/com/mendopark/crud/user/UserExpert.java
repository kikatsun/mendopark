package com.mendopark.crud.user;

import com.mendopark.application.SExpert;
import com.mendopark.model.*;
import com.mendopark.repository.*;
import com.mendopark.rest.user.UserDto;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.ejb.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;

/**
 * Experto de Usuario
 * Clase experta que define la lógica para el recurso Usuario.
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UserExpert extends SExpert {

    @EJB
    PersonRepository personRepository;
    @EJB
    UserRepository userRepository;
    @EJB
    AddressRepository addressRepository;
    @EJB
    LocalityReporsitory localityReporsitory;
    @EJB
    RoleRepository roleRepository;
    @EJB
    DocumentTypeRepository documentTypeRepository;

    public static final Integer COMMOM_USER_ID = 3;

    public User update(User user) {
        User persistedUser = userRepository.get(user.getId());
        if (persistedUser != null) {

            persistedUser.setName(user.getName());
            persistedUser.setPassword(user.getPassword());
            persistedUser.setEmail(user.getEmail());

            if(user.getUserRole() != null && user.getUserRole().getNumber() != null) {
                Role persistedRole = roleRepository.get(user.getUserRole().getNumber());
                if(persistedRole != null) {
                    persistedUser.setUserRole(persistedRole);
                }
            }

            Person userPerson = user.getFromPerson();
            Person persistedPerson = persistedUser.getFromPerson();

            persistedPerson.setName(userPerson.getName());
            persistedPerson.setLastName(userPerson.getLastName());
            persistedPerson.setEmail(userPerson.getEmail());
            persistedPerson.setPhoneNumber(userPerson.getPhoneNumber());
            /* El tipo de documento no viene de la interfaz, por ahora se setea del backend y no se debe modificar */
//            persistedPerson.setDocumentType(userPerson.getDocumentType());
            persistedPerson.setDocumentNumber(userPerson.getDocumentNumber());

            personRepository.persist(persistedPerson);
            userRepository.persist(persistedUser);
        }
        return user;
    }

    public User persist (User user) {
        return persist(user, true);
    }

    public User persist(@NotNull User user, boolean verifyClient) {
        if(user.getFromPerson() != null ) {
            User persistedUser = userRepository.getByName(user.getName());
            if(persistedUser != null){
                throw new BadRequestException("El nombre de usuario ya existe!");
            }
            Role role;
            if(user.getUserRole() != null && !Role.Type.COMMON.getRoleId().equals(user.getUserRole().getNumber())){
                role = roleRepository.get(user.getUserRole().getNumber());
                if (verifyClient) {
                    if (getClient() != null) {
                        user.setFromClient(getClient());
                    } else {
                        throw new BadRequestException("Es necesario que el usuario de sesion tenga un cliente para crear otro cliente.");
                    }
                }
            }else{
                System.out.println("Un usuario normal se creó");
                role = roleRepository.get(COMMOM_USER_ID);
            }
            user.setUserRole(role);
            Person userPerson = user.getFromPerson();
            if(userPerson.getDocumentType() != null){
                userPerson.setDocumentType(documentTypeRepository.get(userPerson.getDocumentType().getNumber()));
            }
            if(userPerson.getAddress() != null) {
                Address address = userPerson.getAddress();
                Locality locality = localityReporsitory.get(address.getFromLocality().getNumber());
                address.setFromLocality(locality);
                addressRepository.persist(address);
            }
            try {
                personRepository.persist(userPerson);
            }catch (Exception ex){
                throw new BadRequestException("EL número de documento ya existe");
            }
            userRepository.persist(user);
        }
        return user;
    }

    public int getLastId(){
        Integer lastNumber = null;
        try{
            lastNumber = userRepository.getLastId();
            if (lastNumber == null){
                lastNumber = 0;
            }
        } catch (Exception ex) {
            lastNumber = 0;
        }

        return lastNumber;
    }

    public List<User> findUsers(){
        List<User> users = userRepository.getAll();
        List<User> result = new ArrayList<>();
        if(getClient() != null && Client.MENDOPARK_ID.equals(getClient().getNumber())){
            result = users;
        }else{
            if (users != null && users.size() > 0) {
                for (User user : users) {
                    if (user.getFromClient() != null && user.getFromClient().getNumber() != null
                            && getClient().getNumber() != null
                            && user.getFromClient().getNumber().equals(getClient().getNumber())) {
                        result.add(user);
                    }
                }
            }
        }

        return result;
    }

    public User findUser(UserDto dto){
        return userRepository.get(dto.getId());
    }

    public User findClientUser (Long clientNumber){
        List<User> users =  userRepository.findRootClientUser(clientNumber);
        if(users != null && users.size() > 0){
            System.out.println("VERRRRRRRR: "+users.size());
            return users.get(0);
        }else{
            System.out.println("VERRRRRRR OUCHHHH");
            return null;
        }
    }

    public boolean delete(User userToDelete) {

        if(userToDelete.getId() != null){
            User persistedUser = userRepository.get(userToDelete.getId());
            if(persistedUser != null){
                persistedUser.setEndDate(new Date());
                userRepository.persist(persistedUser);
                userToDelete = persistedUser;
            }
        }
        if(userToDelete == null) {
            throw new BadRequestException("Usuario no identificado para dar de baja");
        }
        return true;
    }

    public User findUserByUserName(UserDto dto) {
        return userRepository.getByName(dto.getName());
    }

    public boolean validateUserName(UserDto dto) {
        boolean result = false;
        if(dto.getUserName() != null && !dto.getUserName().isEmpty()){
            User user = userRepository.getByName(dto.getUserName());
            if(user != null){
                if(dto.getId() != null && dto.getId().equals(user.getId())){
                    result = true;
                }
            }else{
                result = true;
            }
        }else {
            throw new BadRequestException("El nombre de usuario es un campo obligatorio");
        }
        return result;
    }

    public int restoreUsernameAndPassword(UserDto userDto){
        User user;

        if (userRepository.findUserByEmailAndDocumentNumber(userDto.getEmail(), userDto.getDocumentNumber()) != null){ //Existe el usuario solicitado
            user = userRepository.findUserByEmailAndDocumentNumber(userDto.getEmail(), userDto.getDocumentNumber());

            try{

                Properties properties = new Properties();
                properties.setProperty("mail.smtp.host", "smtp.gmail.com");
                properties.setProperty("mail.smtp.starttls.enable", "true");
                properties.setProperty("mail.smtp.port", "587");
                properties.setProperty("mail.smtp.auth", "true");

                Session session = Session.getDefaultInstance(properties);

                String correoRemitente = "mendopark.mendoza@gmail.com";  //SOLO para PROBAAAARRRR
                String passwordRemitente = "dccovkfkcbhadtlq";         //SOLO para PROBAAAARRRR
                String correoReceptor = user.getEmail();
                String asuntoCorreo = " Mendopark - Recupero Contraseña";
                String mensaje = "Tu usuario es: " + user.getName() + " y la contaseña es: " + user.getPassword();

                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(correoRemitente));

                message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoReceptor));
                message.setSubject(asuntoCorreo);
                message.setText(mensaje);

                Transport transport = session.getTransport("smtp");
                transport.connect(correoRemitente, passwordRemitente);
                transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
                transport.close();

                return 1;
            } catch (NoResultException | AddressException ex){      //Error al enviar mail
                System.out.println(ex.getMessage());
                return -1;
            } catch (MessagingException e) {
                e.printStackTrace();
                return -1;
            }
        } else {        //No existe el usuario consultado
            return 0;
        }
    }
}
