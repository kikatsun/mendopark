package com.mendopark.crud.resource;

import com.mendopark.model.PayType;
import com.mendopark.model.Role;
import com.mendopark.model.security.Resource;
import com.mendopark.model.security.RoleResourceAssignment;
import com.mendopark.repository.PayTypeRepository;
import com.mendopark.repository.ResourceRepository;
import com.mendopark.rest.paytype.PayTypeDto;
import com.mendopark.rest.resource.ResourceDto;
import com.mendopark.service.security.RoleResourceManager;

import javax.ejb.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ResourceSExpert {

    @EJB
    private ResourceRepository resourceRepository;

    @EJB
    private RoleResourceManager roleResourceManager;

    public Resource find(ResourceDto dto) {
        Resource result = null;

        result = resourceRepository.get(dto.getId());

        return result;
    }

    public boolean validateNameAndPath(ResourceDto dto){

        boolean result = false;

        if (dto.getId() != null){
            Resource resource = resourceRepository.get(dto.getId());
            if (resource != null) {
                if (!dto.getName().equals(resource.getName())) {
                    result = resourceRepository.getByNameAndPath(dto.getName(), dto.getPath(), dto.getId());
                }
            }
        } else {
            result = resourceRepository.getByNameAndPath(dto.getName(), dto.getPath(), null);
        }
        return result;
    }

    /**
     * Busca todos los Menues, incluso los dado de baja
     * @return
     */
    public List<Resource> findAllResources() {
        return resourceRepository.getAll();
    }

    public boolean delete(ResourceDto resourceDto) {
        boolean result;
        Resource resource = resourceRepository.get(resourceDto.getId());
        resource.setEndDate(new Date());
        try{
            resourceRepository.persist(resource);
            result = true;
            resourceRepository.flush();
            updateResources();
        }catch (Exception ex){
            result = false;
        }
        return result;
    }

    public Resource update(ResourceDto resourceDto) throws Exception{
        Resource result = resourceRepository.get(resourceDto.getId());

        if(result == null){
            throw new Exception("No se encontró Menú");
        }

        result.setName(resourceDto.getName());
        result.setDescription(resourceDto.getDescription());
        result.setPath(resourceDto.getPath());

        resourceRepository.persist(result);
        resourceRepository.flush();
        updateResources();
        return result;
    }

    public Resource persist(ResourceDto resourceDto) {
        Resource result = ResourceDto.Factory.getResourceFromDto(resourceDto);
        resourceRepository.persist(result);
        return result;
    }

    public List<Resource> getResourcesFromRole(Integer id) {
        Role role = new Role();
        role.setNumber(id);
        return roleResourceManager.getResources(role);
    }

    private void updateResources(){
        roleResourceManager.init();
    }
}
