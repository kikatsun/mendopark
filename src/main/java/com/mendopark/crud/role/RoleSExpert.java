
package com.mendopark.crud.role;

import com.mendopark.application.SExpert;
import com.mendopark.model.Province;
import com.mendopark.model.Role;
import com.mendopark.model.security.GrantType;
import com.mendopark.model.security.RoleResourceAssignment;
import com.mendopark.repository.GrantTypeRepository;
import com.mendopark.repository.ResourceRepository;
import com.mendopark.repository.RoleRepository;
import com.mendopark.repository.RoleResourceAssignmentRepository;
import com.mendopark.rest.role.RoleDto;
import com.mendopark.rest.role.RoleResourceAssignmentDto;
import com.mendopark.service.security.RoleResourceManager;

import javax.ejb.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RoleSExpert extends SExpert {

	@EJB
	private RoleRepository roleRepository;

	@EJB
	private GrantTypeRepository grantTypeRepository;

	@EJB
	private ResourceRepository resourceRepository;

	@EJB
	private RoleResourceAssignmentRepository roleResourceAssignmentRepository;

	@EJB
	private RoleResourceManager roleResourceManager;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = roleRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public RoleDto find(RoleDto dto) {
		RoleDto result = null;

		result = RoleDto.Factory.getDtoFromRole(roleRepository.get(dto.getNumber()));
		if(result != null && result.getNumber() != null){
			result.setAssignments(RoleResourceAssignmentDto.Factory.getDtosFrom(roleResourceManager.getAssignments(result.getNumber())));
		}
		return result;
	}

	public List<RoleDto> findRoles() {
		List<Role> roles = roleRepository.getAll();
		List<Role> result = new ArrayList<>();
		List<RoleDto> resultDto = new ArrayList<>();
		if(getRole() != null) {
			if (!getRole().getNumber().equals(Role.Type.SYSTEM_ADMIN.getRoleId())) {
				if (roles != null && roles.size() > 0) {
					for (Role role : roles) {
						if (role.getEndDate() != null) {
							continue;
						}
						if (Role.Type.SYSTEM_ADMIN.getRoleId().equals(role.getNumber()) && !isSystemAdminUser()) {
							continue;
						}
						result.add(role);
					}
				}
			} else {
				result = roles;
			}
		}
		if(result != null && result.size() >0){
			for (Role rol: result) {
				RoleDto dto = RoleDto.Factory.getDtoFromRole(rol);
				dto.setAssignments(RoleResourceAssignmentDto.Factory.getDtosFrom(roleResourceManager.getAssignments(dto.getNumber())));
				resultDto.add(dto);
			}
		}
		return resultDto;
	}

	public List<Role> findRolesAvailables() {
		List<Role> roles = roleRepository.getAvailables();
		List<Role> result = new ArrayList<>();
		if(getRole() != null && roles != null && roles.size() > 0) {
			for (Role role : roles) {
				if (role.getEndDate() != null) {
					continue;
				}
				if (Role.Type.SYSTEM_ADMIN.getRoleId().equals(role.getNumber()) && !isSystemAdminUser()) {
					continue;
				}
				if (isActive(role)) {
					result.add(role);
				}
			}
		}
		return result;
	}

	private boolean isActive (Role role) {
		boolean result = false;
		if (role.getVigenciaDesde() != null && role.getVigenciaHasta() != null) {
			result = System.currentTimeMillis() >= role.getVigenciaDesde().getTime() && System.currentTimeMillis() <= role.getVigenciaHasta().getTime() ;
		}
		return result;
	}

	private List<Role> removeRoles(Role role, List<Role> roles){

		List<Role> result = new ArrayList<>();
		if(roles != null && roles.size() > 0){
			List<Integer> rolesToIgnore = getRolesToIgnore(role.getNumber());
			if(rolesToIgnore != null){
				for (Role resultRole: roles) {
					if(!rolesToIgnore.contains(resultRole.getNumber())){
						result.add(resultRole);
					}
				}
			}
		}
		return result;
	}

	private List<Integer> getRolesToIgnore(Integer roleId){
		List<Integer> rolesToIgnore = null;

		switch (roleId){
			case 1:
				rolesToIgnore = new ArrayList<>();
				break;
			case 2:
				rolesToIgnore = Arrays.asList(1);
				break;
			case 3:
				rolesToIgnore = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
				break;
			case 4:
				rolesToIgnore = Arrays.asList(1,2);
				break;
			case 5:
				rolesToIgnore = Arrays.asList(1,2,4);
				break;
			default:
				rolesToIgnore = null;
				break;
		}

		return rolesToIgnore;
	}

	public boolean delete(RoleDto roleDto) {
		boolean result;
		Role role = roleRepository.get(roleDto.getNumber());
		role.setEndDate(new Date());
		List<RoleResourceAssignment> assignments = roleResourceManager.getAssignments(role.getNumber());
		List<RoleResourceAssignment> assignmentsToDelete = new ArrayList<>();
		if(assignments != null && assignments.size() > 0){
			Date deletedDate = new Date();
			for (RoleResourceAssignment assignment: assignments ){
				RoleResourceAssignment persistedAssignment = roleResourceAssignmentRepository.get(assignment.getId());
				persistedAssignment.setEndDate(deletedDate);
				assignmentsToDelete.add(persistedAssignment);
			}
		}
		try {
			roleResourceManager.removeRole(role.getNumber());
			roleRepository.persist(role);
			if (assignmentsToDelete.size() > 0) {
				for (RoleResourceAssignment assignment: assignmentsToDelete ) {
					roleResourceAssignmentRepository.persist(assignment);
				}
			}
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	/* ToDo Debería de pasarse como parámetro la entidad Role, los dto los debe de manejar los controladores */
	public Role update(RoleDto roleDto) throws Exception {

		Role result = roleRepository.get(roleDto.getNumber());
		List<RoleResourceAssignment> assignments = null;
		List<RoleResourceAssignment> updatedAssignments = new ArrayList<>();
		if(roleDto.getAssignments() != null){
			assignments = RoleResourceAssignmentDto.Factory.getRoleResourceAssignmentsFrom(roleDto.getAssignments());
		}
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(roleDto.getName());

		result.setDescription(roleDto.getDescription());

		String effectiveDateFromAsString = null;
		String effectiveDateToAsString = null;
		if (roleDto.getRangoVigencia() != null){
			String[] parts = roleDto.getRangoVigencia().split("-");
			effectiveDateFromAsString = parts[0]; // fecha Desde
			effectiveDateToAsString = parts[1]; // fecha Hasta
		}

		if (effectiveDateFromAsString != null && !effectiveDateFromAsString.isEmpty()){
			result.setVigenciaDesde(parseFechaToDate(effectiveDateFromAsString));
		}

		if (effectiveDateToAsString != null && !effectiveDateToAsString.isEmpty()){
			result.setVigenciaHasta(parseFechaToDate(effectiveDateToAsString));
		}
		/* Se dan de baja las asignaciones que existen pero no vienen de la interfaz */
		List<RoleResourceAssignment> persistedAssignments = roleResourceManager.getAssignments(result.getNumber());
		if(persistedAssignments != null && persistedAssignments.size() > 0){
			for (RoleResourceAssignment persistedAssignment: persistedAssignments ){
				boolean remove = true;
				if(assignments != null && assignments.size() > 0){
					updatedFound: for (int i = 0; i < assignments.size(); i++) {
						if(assignments.get(i).getId() != null && assignments.get(i).getId().equals(persistedAssignment.getId())){
							remove =false;
							break updatedFound;
						}
					}
				}
				if(remove){
					RoleResourceAssignment assignmentToDelete = roleResourceAssignmentRepository.get(persistedAssignment.getId());
					assignmentToDelete.setEndDate(new Date());
					updatedAssignments.add(assignmentToDelete);
				}
			}
		}

		/* Agrega nuevas asignaciones o actualiza las ya existentes */
		if(assignments != null && assignments.size() > 0){
			for (RoleResourceAssignment assignment: assignments) {
				RoleResourceAssignment updatedAssigment;
				if(assignment.getId() != null && !assignment.getId().isEmpty()){
					updatedAssigment = roleResourceAssignmentRepository.get(assignment.getId());
				}else{
					updatedAssigment = assignment;
				}
				if(updatedAssigment != null) {
					if (assignment.getGrantType() != null && assignment.getGrantType().getNumber() != null) {
						updatedAssigment.setGrantType(grantTypeRepository.get(assignment.getGrantType().getNumber()));
					} else {
						updatedAssigment.setGrantType(grantTypeRepository.get(GrantType.Type.CREATE.getId()));
					}
					if (assignment.getToResource() != null && assignment.getToResource().getId() != null) {
						updatedAssigment.setToResource(resourceRepository.get(assignment.getToResource().getId()));
					}
					if (assignment.getStartDate() == null) {
						updatedAssigment.setStartDate(new Date());
					}
					updatedAssignments.add(updatedAssigment);
				}
			}
		}
		roleRepository.persist(result);
		if(updatedAssignments != null && updatedAssignments.size() > 0){
			for (RoleResourceAssignment assignment: updatedAssignments) {
				assignment.setFromRole(result);
				roleResourceAssignmentRepository.persist(assignment);
				if(assignment.getEndDate() == null) {
					roleResourceManager.addAssignment(assignment);
				}else{
					roleResourceManager.removeAssignment(assignment);
				}
			}
		}
		return result;
	}

	public Role persist(Role role, List<RoleResourceAssignment> assignments) {
		if(assignments != null && assignments.size() > 0){
			for (RoleResourceAssignment assignment: assignments) {
				if(assignment.getGrantType() != null && assignment.getGrantType().getNumber() != null){
					assignment.setGrantType(grantTypeRepository.get(assignment.getGrantType().getNumber()));
				}else {
					assignment.setGrantType(grantTypeRepository.get(GrantType.Type.CREATE.getId()));
				}
				if(assignment.getToResource() != null && assignment.getToResource().getId() != null){
					assignment.setToResource(resourceRepository.get(assignment.getToResource().getId()));
				}
				if(assignment.getStartDate() == null){
					assignment.setStartDate(new Date());
				}
			}
		}
		role = roleRepository.persist(role);

		if(assignments != null && assignments.size() > 0){
			for (RoleResourceAssignment assignment: assignments) {
				assignment.setFromRole(role);
				roleResourceAssignmentRepository.persist(assignment);
				roleResourceManager.addAssignment(assignment);
			}
		}
		return role;
	}

	/**
	 * Permite convertir un String en fecha (Date).
	 * @param fecha Cadena de fecha dd/MM/yyyy
	 * @return Objeto Date
	 */
	public Date parseFechaToDate(String fecha)
	{
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaDate = null;
		try {
			fechaDate = formato.parse(fecha);
		} catch (ParseException ex) {
			System.out.println(ex);
		}
		return fechaDate;
	}


	/**
	 * Permite convertir una fecha Date en String.
	 * @param fecha Date (dd/MM/yyyy)
	 * @return Objeto String
	 */
	public String parseFechaToString(Date fecha){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String fechaDate = null;
		fechaDate = formato.format(fecha);

		return fechaDate;
	}

	public boolean validateNameAndDescription(RoleDto roleDto) {

		boolean result = false;

		if (roleDto.getNumber() != null){
			Role role = roleRepository.get(roleDto.getNumber());
			if (role != null){
				if (!roleDto.getName().equals(role.getName())){
					result = roleRepository.getRoleExist(roleDto.getName(), roleDto.getDescription(), roleDto.getNumber());
				}
			}
		} else {
			result = roleRepository.getRoleExist(roleDto.getName(), roleDto.getDescription(), null);
		}
		return result;

		//return roleRepository.getRoleExist(roleDto.getName(), roleDto.getDescription());
	}
}
