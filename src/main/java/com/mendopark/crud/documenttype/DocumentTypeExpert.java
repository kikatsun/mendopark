package com.mendopark.crud.documenttype;

import com.mendopark.model.DocumentType;
import com.mendopark.repository.DocumentTypeRepository;
import com.mendopark.rest.documenttype.DocumentTypeDto;

import javax.ejb.*;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DocumentTypeExpert {

    @EJB
    private DocumentTypeRepository documentTypeRepository;

    public long getLastId(){

        Long lastNumber = null;
        try {
            lastNumber = documentTypeRepository.getLastId();
            if (lastNumber == null){
                lastNumber = 0L;
            }
        }catch (Exception ex){
            lastNumber = 0L;
        }

        return lastNumber;
    }

    public DocumentType find(DocumentTypeDto dto){

        DocumentType result = null;

        result = documentTypeRepository.get(dto.getNumber());

        return result;
    }

    public boolean delete (DocumentTypeDto documentTypeDto){

        boolean result;
        DocumentType documentType = documentTypeRepository.get(documentTypeDto.getNumber());
        documentType.setEndDate(new Date());

        try {
            documentTypeRepository.persist(documentType);
            result = true;
        }catch (Exception e){
            result = false;
        }

        return result;
    }

    public DocumentType update (DocumentTypeDto documentTypeDto) throws Exception{

        DocumentType result = documentTypeRepository.get(documentTypeDto.getNumber());

        if (result == null){
            throw new Exception("No se encontró el Tipo Documento");
        }
        result.setName(result.getName());
        result.setDescription(result.getDescription());

        documentTypeRepository.persist(result);

        return result;
    }

    public DocumentType persist(DocumentTypeDto documentTypeDto) {
        DocumentType result = DocumentTypeDto.Factory.getDocumentTypeFromDto(documentTypeDto);
        documentTypeRepository.persist(result);

        return result;
    }

}
