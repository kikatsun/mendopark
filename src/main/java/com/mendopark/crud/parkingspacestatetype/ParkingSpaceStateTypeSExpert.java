
package com.mendopark.crud.parkingspacestatetype;

import com.mendopark.application.SExpert;
import com.mendopark.model.ParkingSpaceStateType;
import com.mendopark.model.Province;
import com.mendopark.repository.ParkingSpaceStateTypeRepository;
import com.mendopark.rest.parkingspacestatetype.ParkingSpaceStateTypeDto;

import javax.ejb.*;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ParkingSpaceStateTypeSExpert extends SExpert {

	@EJB
	private ParkingSpaceStateTypeRepository parkingSpaceStateTypeRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = parkingSpaceStateTypeRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public ParkingSpaceStateType find(ParkingSpaceStateTypeDto dto) {
		ParkingSpaceStateType result = null;

		result = parkingSpaceStateTypeRepository.get(dto.getNumber());

		return result;
	}

	public List<ParkingSpaceStateType> findParkingSpaceStateTypes() {
		return parkingSpaceStateTypeRepository.getAvailables();
	}

	public boolean delete(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) {
		boolean result;
		ParkingSpaceStateType parkingSpaceStateType = parkingSpaceStateTypeRepository
				.get(parkingSpaceStateTypeDto.getNumber());
		parkingSpaceStateType.setEndDate(new Date());
		try {
			parkingSpaceStateTypeRepository.persist(parkingSpaceStateType);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public ParkingSpaceStateType update(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws Exception {

		ParkingSpaceStateType result = parkingSpaceStateTypeRepository.get(parkingSpaceStateTypeDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(parkingSpaceStateTypeDto.getName());

		result.setDescription(parkingSpaceStateTypeDto.getDescription());

		parkingSpaceStateTypeRepository.persist(result);

		return result;
	}

	public ParkingSpaceStateType persist(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) {
		ParkingSpaceStateType result = ParkingSpaceStateTypeDto.Factory
				.getParkingSpaceStateTypeFromDto(parkingSpaceStateTypeDto);
		parkingSpaceStateTypeRepository.persist(result);
		return result;
	}

	public boolean validateNameAndDescription(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto){

		/* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
		boolean result = false;

		if (parkingSpaceStateTypeDto.getNumber() != null){
			ParkingSpaceStateType parkingSpaceStateType = parkingSpaceStateTypeRepository.get(parkingSpaceStateTypeDto.getNumber());
			if (parkingSpaceStateType != null){
				if (!parkingSpaceStateTypeDto.getName().equals(parkingSpaceStateType.getName())){
					result = parkingSpaceStateTypeRepository.getParkingSpaceStateTypeExist(parkingSpaceStateTypeDto.getName(), parkingSpaceStateTypeDto.getDescription(), parkingSpaceStateTypeDto.getNumber());
				}
			}
		} else {
			result = parkingSpaceStateTypeRepository.getParkingSpaceStateTypeExist(parkingSpaceStateTypeDto.getName(), parkingSpaceStateTypeDto.getDescription(), null);
		}
		return result;

		//return parkingSpaceStateTypeRepository.getParkingSpaceStateTypeExist(parkingSpaceStateTypeDto.getName(), parkingSpaceStateTypeDto.getDescription());
	}
}
