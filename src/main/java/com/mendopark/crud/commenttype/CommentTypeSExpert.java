
package com.mendopark.crud.commenttype;

import com.mendopark.model.CommentType;
import com.mendopark.repository.CommentTypeRepository;
import com.mendopark.rest.commenttype.CommentTypeDto;

import javax.ejb.*;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CommentTypeSExpert {

	@EJB
	private CommentTypeRepository commentTypeRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = commentTypeRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public CommentType find(CommentTypeDto dto) {
		CommentType result = null;

		result = commentTypeRepository.get(dto.getNumber());

		return result;
	}

	public List<CommentType> findCommentTypes() {
		return commentTypeRepository.getAvailables();
	}

	public boolean delete(CommentTypeDto commentTypeDto) {
		boolean result;
		CommentType commentType = commentTypeRepository.get(commentTypeDto.getNumber());
		commentType.setEndDate(new Date());
		try {
			commentTypeRepository.persist(commentType);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public CommentType update(CommentTypeDto commentTypeDto) throws Exception {

		CommentType result = commentTypeRepository.get(commentTypeDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(commentTypeDto.getName());

		result.setDescription(commentTypeDto.getDescription());

		commentTypeRepository.persist(result);

		return result;
	}

	public CommentType persist(CommentTypeDto commentTypeDto) {
		CommentType result = CommentTypeDto.Factory.getCommentTypeFromDto(commentTypeDto);
		commentTypeRepository.persist(result);
		return result;
	}
}
