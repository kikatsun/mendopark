package com.mendopark.crud.client;

import com.mendopark.application.SExpert;
import com.mendopark.crud.day.DaySExpert;
import com.mendopark.crud.user.UserExpert;
import com.mendopark.model.*;
import com.mendopark.repository.*;
import com.mendopark.service.clientconfiguration.ClientConfigurationManager;

import javax.ejb.*;
import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ClientSExpert extends SExpert {

    @EJB
    private ClientRepository clientRepository;

    @EJB
    private ClientTypeRepository clientTypeRepository;

    @EJB
    private AddressRepository addressRepository;

    @EJB
    private LocalityReporsitory localityReporsitory;

    @EJB
    private ClientConfigurationRepository clientConfigurationRepository;

    @EJB
    private ClientConfigurationManager clientConfigurationManager;

    @EJB
    private UserExpert userExpert;

    @EJB
    private DaySExpert daySExpert;

    public static final Integer CLIENT_TYPE_NUMBER_DEFAULT = 2;

    public Long persist(Client newClient, User newUser){

        validateFields(newClient);
        validateUserFields(newUser);
        String cuit = newClient.getCuit();
        List<Client> clientList = clientRepository.getAll();
        boolean isClientDuplicated = false;
        if(clientList != null && clientList.size() > 0){
            for (int i = 0; i < clientList.size(); i++) {
                if(cuit.equals(clientList.get(i).getCuit())){
                    isClientDuplicated = true;
                    break;
                }
            }
        }
        if(isClientDuplicated){
            throw new BadRequestException("El número de CUIT del nuevo cliente ya existe");
        }
        newClient.setNumber(getLastId()+1);
        Integer clientTypeNumber = CLIENT_TYPE_NUMBER_DEFAULT;
        if(newClient.getClientType() != null && newClient.getClientType().getNumber() != null){
            clientTypeNumber = newClient.getClientType().getNumber();
        }
        ClientType clientType = clientTypeRepository.get(clientTypeNumber);
        newClient.setClientType(clientType);

        Address clientAddress = newClient.getAddress();

        if (clientAddress != null && clientAddress.getFromLocality() != null && clientAddress.getFromLocality().getNumber() >0){
            clientAddress.setFromLocality(localityReporsitory.get(clientAddress.getFromLocality().getNumber()));
            addressRepository.persist(clientAddress);
        }

        clientRepository.persist(newClient);

        Person clientPerson = new Person();

        clientPerson.setPhoneNumber(newClient.getPhoneNumber());
        clientPerson.setName(newClient.getCompanyName());
        clientPerson.setDocumentNumber(newClient.getCuit());
        clientPerson.setEmail(newUser.getEmail());
        clientPerson.setAddress(newClient.getAddress());
        clientPerson.setDocumentType(new DocumentType(DocumentType.Type.OTHER.getDocumentTypeId()));

        newUser.setFromPerson(clientPerson);
        newUser.setUserRole(new Role(Role.Type.CLIENT_ADMIN.getRoleId()));
        newUser.setFromClient(newClient);
        userExpert.persist(newUser, false);

        return newClient.getNumber();
    }

    public List<Client> findClients(){
        return clientRepository.getAvailables();
    }

    private void validateFields(Client client){

        if(client == null){
            throw new BadRequestException("Los datos del cliente no fueron recibidos");
        }
        if(client.getCompanyName() == null || client.getCompanyName().isEmpty()){
            throw new BadRequestException("Campo de cliente obligatorio: nombre vacío.");
        }
        if(client.getCuit() == null || client.getCuit().isEmpty()){
            throw new BadRequestException("Campo de cliente obligatorio: cuit vacío.");
        }
    }

    private void validateUserFields (User user){
        if(user.getName() == null || user.getName().isEmpty()){
            throw new BadRequestException("Campo de usuario obligatorio: nombre vacío.");
        }
        if(user.getPassword() == null || user.getPassword().isEmpty()){
            throw new BadRequestException("Campo de usuario obligatorio: contraseña vacía.");
        }
        if(user.getEmail() == null){
            throw new BadRequestException("Campo de usuario obligatorio: mail vacío.");
        }
    }

    public boolean updateConfiguration() {
        return updateConfiguration(clientConfigurationManager.getClientConfiguration(getClient().getNumber()));
    }

    public boolean updateConfiguration(ClientConfiguration clientConfiguration) {

        ClientConfiguration clientConfigurationToUpdate = null;

        if(clientConfiguration.getId() != null) {
            try {
                clientConfigurationToUpdate = clientConfigurationRepository.get(clientConfiguration.getId());
            }catch (Exception e){
                clientConfigurationToUpdate = null;
            }
        }
        if(clientConfiguration == null) {
            List<ClientConfiguration> clientConfigurationList;
            try {
                clientConfigurationList = clientConfigurationRepository.getAvailables();
            } catch (Exception e) {
                clientConfigurationList = null;
            }
            if (clientConfigurationList != null && clientConfigurationList.size() > 0) {
                for (int i = 0; i < clientConfigurationList.size(); i++) {
                    Client client = clientConfigurationList.get(i).getFromClient();
                    if (client != null && client.getNumber() != null && getClient() != null
                            && client.getNumber().equals(getClient().getNumber())
                    && clientConfigurationList.get(i).getEndDate() == null) {
                        clientConfigurationToUpdate = clientConfigurationList.get(i);
                        break;
                    }
                }
            }
        }
        if(clientConfigurationToUpdate == null){
            clientConfigurationToUpdate = clientConfiguration;
        }else {
            clientConfigurationToUpdate.setManualParkingSpaceLockTime(clientConfiguration.getManualParkingSpaceLockTime());
            clientConfigurationToUpdate.setParkingSpacePrice(clientConfiguration.getParkingSpacePrice());
            clientConfigurationToUpdate.setReservationTimeout(clientConfiguration.getReservationTimeout());
        }
        clientConfigurationToUpdate = updateWeeklySchedule(clientConfigurationToUpdate);
        clientConfigurationRepository.persist(clientConfigurationToUpdate);
        clientConfigurationManager.addClientConfiguration(clientConfigurationToUpdate);

        return true;
    }

    public boolean update(Client clientToUpdate) {

        validateClientNumber(clientToUpdate);
        validateFields(clientToUpdate);
        Long numberClient = clientToUpdate.getNumber();
        Client persistedClient = null;
        List<Client> clientList = clientRepository.getAll();
        if(clientList != null && clientList.size() > 0){
            for (int i = 0; i < clientList.size(); i++) {
                if(clientList.get(i).getNumber().equals(numberClient)){
                    persistedClient = clientList.get(i);
                    break;
                }
            }
        }
        if(persistedClient == null){
            throw new BadRequestException("El número del nuevo cliente ya existe");
        }
        persistedClient.setPhoneNumber(clientToUpdate.getPhoneNumber());
        persistedClient.setBankData(clientToUpdate.getBankData());
        persistedClient.setCompanyName(clientToUpdate.getCompanyName());
        persistedClient.setCuit(clientToUpdate.getCuit());
        if(clientToUpdate.getClientType() != null && clientToUpdate.getClientType().getNumber() != null){
            Integer clientTypeNumber = clientToUpdate.getClientType().getNumber();
            ClientType clientType = clientTypeRepository.get(clientTypeNumber);
            persistedClient.setClientType(clientType);
        }

        Address clientAddressToUpdate = clientToUpdate.getAddress();
        Address persistedClientAddress = persistedClient.getAddress();
        if (clientAddressToUpdate != null && clientAddressToUpdate.getFromLocality() != null && clientAddressToUpdate.getFromLocality().getNumber() >0){
            persistedClientAddress.setFromLocality(localityReporsitory.get(clientAddressToUpdate.getFromLocality().getNumber()));
        }
        if(clientAddressToUpdate != null) {
            persistedClientAddress.setStreetName(clientAddressToUpdate.getStreetName());
            persistedClientAddress.setDepartmentNumber(clientAddressToUpdate.getDepartmentNumber());
            persistedClientAddress.setStreetNumber(clientAddressToUpdate.getStreetNumber());
            persistedClient.setAddress(persistedClientAddress);
        }

        addressRepository.persist(persistedClientAddress);
        clientRepository.persist(persistedClient);

        return true;
    }

    private void validateClientNumber(Client clientToUpdate) {
        if (!getRole().getNumber().equals(Role.Type.SYSTEM_ADMIN.getRoleId())
            && getClient() != null && getClient().getNumber() != null
            && !getClient().getNumber().equals(clientToUpdate.getNumber())){
            throw new BadRequestException("Solicitud inválida, el cliente que quiere actualizar no es al que pertenece su usuario!");
        }
    }

    public List<Client> getClients() {
        if(getClient().getNumber().equals(1L)){
            List<Client> clientList = clientRepository.getAll();
            if(clientList != null && clientList.size() > 0){
                for (Client client: clientList ) {
                    if(client.getAddress() != null && client.getAddress().getFromLocality() != null ){
                        client.getAddress().getFromLocality().getFromProvince();
                    }
                    client.getClientType();
                    client.getWeeklySchedulesWork();
                }
            }
            return clientRepository.getAll();
        }else{
            List<Client> result = new ArrayList<>();
            if(getClient() != null && getClient().getNumber()!= null){
                Client myClient = clientRepository.get(getClient().getNumber());
                if(myClient != null) {
                    result.add(myClient);
                }
            }
            return result;
        }
    }

    public boolean delete(Client clientToDelete) {

        Long clientNumber = clientToDelete.getNumber();
        if(clientNumber == null){
            throw new BadRequestException("No se pudo dar de baja al cliente porque no existe. Número de cliente vacío.");
        }
        Client persistedClient = null;
        List<Client> clientList = clientRepository.getAll();
        if(clientList != null && clientList.size() > 0){
            for (int i = 0; i < clientList.size(); i++) {
                if(clientList.get(i).getNumber().equals(clientNumber)){
                    persistedClient = clientList.get(i);
                    break;
                }
            }
        }
        if(persistedClient == null){
            throw new BadRequestException("No se pudo dar de baja al cliente porque no existe. Número de cliente: "+clientNumber);
        }
        persistedClient.setEndDate(new Date());
        clientRepository.persist(persistedClient);
        return true;
    }

    public Long getLastId() {
        return clientRepository.getLastId();
    }

    public Client findClient(Long clientNumber) {
        return clientRepository.get(clientNumber);
    }

    public ClientConfiguration findCLientConfiguration(Long number) {
        ClientConfiguration result = null;

        List<ClientConfiguration> clientConfigurationList;
        try {
            clientConfigurationList = clientConfigurationRepository.getAvailables();
        }catch (Exception e){
            clientConfigurationList = null;
        }
        if(clientConfigurationList != null && clientConfigurationList.size() > 0){
            for (int i = 0; i < clientConfigurationList.size(); i++) {
                Client client = clientConfigurationList.get(i).getFromClient();
                if(client != null && client.getNumber() != null
                        && client.getNumber().equals(number)){
                    result = clientConfigurationList.get(i);
                    break;
                }
            }
        }
        return result;
    }

    public ClientConfiguration updateWeeklySchedule (ClientConfiguration clientConfiguration) {
        clientConfiguration = updateWeeklySchedule(clientConfiguration, getClient().getNumber());
        return clientConfiguration;
    }

    public ClientConfiguration updateWeeklySchedule (ClientConfiguration clientConfiguration, Long clientNumber) {
        List<Day> days = daySExpert.findByClient(getClient().getNumber());
        if (days != null && days.size() > 0) {
            String[] weeklySchedules = new String[days.size()];
            for (int i = 0; i < days.size(); i++) {
                weeklySchedules[i] = days.get(i).getName() + " " +days.get(i).getSchedule();
            }
            clientConfiguration.setWeeklySchedulesWork(weeklySchedules);
        }
        return clientConfiguration;
    }
}
