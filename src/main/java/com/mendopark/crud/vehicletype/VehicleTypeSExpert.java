
package com.mendopark.crud.vehicletype;

import com.mendopark.application.SExpert;
import com.mendopark.model.*;
import com.mendopark.repository.VehicleRegistrationRepository;
import com.mendopark.repository.VehicleRepository;
import com.mendopark.repository.VehicleTypeRepository;
import com.mendopark.rest.user.VehicleDto;
import com.mendopark.rest.vehicletype.VehicleTypeDto;

import javax.ejb.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class VehicleTypeSExpert extends SExpert {

	@EJB
	private VehicleTypeRepository vehicleTypeRepository;

	@EJB
	private VehicleRegistrationRepository vehicleRegistrationRepository;
	@EJB
	private VehicleRepository vehicleRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = vehicleTypeRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public VehicleType find(VehicleTypeDto dto) {
		VehicleType result = null;

		result = vehicleTypeRepository.get(dto.getNumber());

		return result;
	}

	public List<VehicleType> findVehicleTypes() {
		return vehicleTypeRepository.getAvailables();
	}

	public List<VehicleType> findVehicleTypes2() {
		return vehicleTypeRepository.getAvailables2();
	}

	public boolean delete(VehicleTypeDto vehicleTypeDto) {
		boolean result;
		VehicleType vehicleType = vehicleTypeRepository.get(vehicleTypeDto.getNumber());
		vehicleType.setEndDate(new Date());
		try {
			vehicleTypeRepository.persist(vehicleType);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public VehicleType update(VehicleTypeDto vehicleTypeDto) throws Exception {

		VehicleType result = vehicleTypeRepository.get(vehicleTypeDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(vehicleTypeDto.getName());

		result.setDescription(vehicleTypeDto.getDescription());

		vehicleTypeRepository.persist(result);

		return result;
	}

	public VehicleType persist(VehicleTypeDto vehicleTypeDto) {
		VehicleType result = VehicleTypeDto.Factory.getVehicleTypeFromDto(vehicleTypeDto);
		vehicleTypeRepository.persist(result);
		return result;
	}

    public List<Vehicle> findPlates(User user) {
		List<Vehicle> dtos = new ArrayList<>();
		if(user != null) {
			List<VehicleRegistration> regs = vehicleRegistrationRepository.getAvailables();
			VehicleRegistration persisted = null;
			if (regs != null && regs.size() > 0) {
				for (int i = 0; i < regs.size(); i++) {
					VehicleRegistration reg = regs.get(i);
					if (reg.getFromUser() != null && reg.getFromUser().getId() != null && reg.getFromUser().getId().equals(user.getId())) {
						dtos.add(reg.getVehicle());
					}
				}
			}
		}
		return dtos;
    }

	public Vehicle persistPlate(Vehicle vehicle, User user) {
		System.out.println("TRESSSSS");
		if(user != null && vehicle != null && vehicle.getDomain() != null && !vehicle.getDomain().isEmpty()) {

			System.out.println("CUATROOOOOOOO");
			Vehicle persV = vehicleRepository.get(vehicle.getDomain());

			if(persV == null){
				if(vehicle.getVehicleType() != null){
					VehicleType vt = vehicleTypeRepository.get(vehicle.getVehicleType().getNumber());
					vehicle.setVehicleType(vt);
				}
				vehicleRepository.persist(vehicle);
				persistNewReg(vehicle, user);
//				vehicleRepository.flu();
//				vehicleRepository.flu();
			}
			else {
				persV.setBrand(vehicle.getBrand());
				persV.setColor(vehicle.getColor());
				persV.setModel(vehicle.getModel());
				persV.setYear(vehicle.getYear());
				if(persV.getVehicleType() != null){
					VehicleType vt = vehicleTypeRepository.get(persV.getVehicleType().getNumber());
					persV.setVehicleType(vt);
				}
//				vehicleRepository.flu();
				List<VehicleRegistration> regs = vehicleRegistrationRepository.getAvailables();
				VehicleRegistration persisted = null;
				if (regs != null && regs.size() > 0) {
					for (int i = 0; i < regs.size(); i++) {
						VehicleRegistration reg = regs.get(i);
						if (reg.getVehicle() != null && reg.getVehicle().getDomain() != null && reg.getVehicle().getDomain().equals(vehicle.getDomain())) {
							persisted = reg;
							break;
						}
					}
				}
				if(persisted == null){
					persistNewReg(persV, user);
//					vehicleRepository.flu();
				}
				vehicleRepository.persist(persV);
			}
		}

		return vehicle;
	}

	private void persistNewReg (Vehicle vehicle, User user){
		VehicleRegistration newVeRe = new VehicleRegistration();
		newVeRe.setStartDate(new Date());
		newVeRe.setFromUser(user);
		newVeRe.setVehicle(vehicle);
		vehicleRegistrationRepository.persist(newVeRe);
	}

	public Vehicle deletePlate(Vehicle vehicle, User user) {
		List<VehicleRegistration> regs = vehicleRegistrationRepository.getAvailables();
		VehicleRegistration persisted = null;
		if (regs != null && regs.size() > 0) {
			for (int i = 0; i < regs.size(); i++) {
				VehicleRegistration reg = regs.get(i);
				if (reg.getVehicle() != null && reg.getVehicle().getDomain() != null && reg.getVehicle().getDomain().equals(vehicle.getDomain())) {
					persisted = reg;
					break;
				}
			}
		}
		if(persisted != null){
			persisted.setEndDate(new Date());
			vehicleRegistrationRepository.persist(persisted);
		}
		return vehicle;
	}

	public boolean validateNameAndDescription(VehicleTypeDto vehicleTypeDto){
		/* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
		boolean result = false;

		if (vehicleTypeDto.getNumber() != null){
			VehicleType vehicleType = vehicleTypeRepository.get(vehicleTypeDto.getNumber());
			if (vehicleType != null){
				if (!vehicleTypeDto.getName().equals(vehicleType.getName())){
					result = vehicleTypeRepository.getVehicleTypeExist(vehicleTypeDto.getName(), vehicleTypeDto.getDescription(), vehicleTypeDto.getNumber());
				}
			}
		} else {
			result = vehicleTypeRepository.getVehicleTypeExist(vehicleTypeDto.getName(), vehicleTypeDto.getDescription(), null);
		}
		return result;
	}
}
