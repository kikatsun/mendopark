package com.mendopark.crud.reports;

import com.mendopark.application.SExpert;
import com.mendopark.repository.ReporteRepository;
import com.mendopark.rest.reports.MedioPagoDto;
import com.mendopark.rest.reports.VehiculoPorZonaDto;

import javax.ejb.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReportSExpert extends SExpert {

    @EJB
    private ReporteRepository reporteRepository;


    public List<Object[]> getCantidadVehiculosPorZonas(VehiculoPorZonaDto dto) {

        String[] fechas = dto.getFecha().split("-");

        System.out.println("Fecha Desde >>> " + fechas[0]);
        System.out.println("Fecha Hasta >>> " + fechas[1]);

        Date fechaDesde = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[0]));
        Date fechaHasta = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[1]));

        return reporteRepository.getCantidadVehiculosPorZonas(fechaDesde, fechaHasta, dto.getIdCliente());
    }


    public List<Object[]> getCantidadVehiculosNoPagan(VehiculoPorZonaDto dto) {

        String[] fechas = dto.getFecha().split("-");

        System.out.println("Fecha Desde >>> " + fechas[0]);
        System.out.println("Fecha Hasta >>> " + fechas[1]);

        Date fechaDesde = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[0]));
        Date fechaHasta = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[1]));
        Date fechaFinalizadaXProceso = this.parseFechaToDate(this.formatearStringAformatoFecha("01011970"));

        return reporteRepository.getCantidadVehiculosNoPagan(fechaDesde, fechaHasta, dto.getIdCliente(), fechaFinalizadaXProceso);
    }


    public List<Object[]> getCantidadVehiculosHorarioLimite(VehiculoPorZonaDto dto) {

        String[] fechas = dto.getFecha().split("-");

        System.out.println("Fecha Desde >>> " + fechas[0]);
        System.out.println("Fecha Hasta >>> " + fechas[1]);

        Date fechaDesde = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[0]));
        Date fechaHasta = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[1]));

        return reporteRepository.getCantidadVehiculosHorarioLimite(fechaDesde, fechaHasta, dto.getIdCliente());
    }


    public List<Object[]> getCantidadMediosDePagoUsado(MedioPagoDto dto) {

        String[] fechas = dto.getFecha().split("-");

        System.out.println("Fecha Desde >>> " + fechas[0]);
        System.out.println("Fecha Hasta >>> " + fechas[1]);

        Date fechaDesde = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[0]));
        Date fechaHasta = this.parseFechaToDate(this.formatearStringAformatoFecha(fechas[1]));

        return reporteRepository.getCantidadMediosDePagoUsado(fechaDesde, fechaHasta, dto.getIdCliente());
    }


    public String formatearStringAformatoFecha(String fecha) {
        String d = fecha.substring(0, 2);
        String m = fecha.substring(2, 4);
        String y = fecha.substring(4);

        System.out.println("Fecha >>> " + d);
        System.out.println("Mes >>> " + m);
        System.out.println("Año >>> " + y);

        return (d + "/" + m + "/" + y);
    }


    /**
     * Permite convertir un String en fecha (Date).
     * @param fecha Cadena de fecha dd/MM/yyyy
     * @return Objeto Date
     */
    public Date parseFechaToDate(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return fechaDate;
    }


    /**
     * Permite convertir una fecha Date en String.
     * @param fecha Date (dd/MM/yyyy)
     * @return Objeto String
     */
    public String parseFechaToString(Date fecha){
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaDate = null;
        fechaDate = formato.format(fecha);

        return fechaDate;
    }
}
