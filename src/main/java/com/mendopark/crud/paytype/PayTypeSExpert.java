package com.mendopark.crud.paytype;

import com.mendopark.application.SExpert;
import com.mendopark.model.PayType;
import com.mendopark.model.Role;
import com.mendopark.repository.PayTypeRepository;
import com.mendopark.rest.paytype.PayTypeDto;

import javax.ejb.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PayTypeSExpert extends SExpert {

    @EJB
    private PayTypeRepository payTypeRepository;

    public long getLastId(){
        Long lastNumber = null;
        try {
            lastNumber = payTypeRepository.getLastId();
            if (lastNumber == null) {
                lastNumber = 0L;
            }
        }catch(Exception ex){
            lastNumber = 0L;
        }
        return lastNumber;
    }

    public PayType find(PayTypeDto dto) {
        PayType result = null;

        result = payTypeRepository.get(dto.getNumber());

        return result;
    }

    public List<PayType> findPayTypes() {
        List<PayType> result =  payTypeRepository.getAvailables();
        if (!getRole().getNumber().equals(Role.Type.SYSTEM_ADMIN.getRoleId())) {
            result = filterExpirationEntities(result);
        }
        return result;
    }

    /**
     * ToDo: se debería implementar una interfaz para las clases con fechas de vigencia, puede ser Expiryable (expirable)
     * una traducción para fechaDesde es effectiveDate y para fechaHasta es expirationDate
     *
     */
    public List<PayType> filterExpirationEntities(List<PayType> entities) {
        List<PayType> result = new ArrayList<>();
        if (entities != null) {
            Date today = new Date();
            for (PayType paytype : entities) {
                if (paytype.getEndDate() == null &&
                    paytype.getVigenciaDesde() != null &&
                    paytype.getVigenciaHasta() != null &&
                    paytype.getVigenciaDesde().before(today) &&
                    paytype.getVigenciaHasta().after(today)) {
                    result.add(paytype);
                }
            }
        }
        return result;
    }

    public boolean delete(PayTypeDto payTypeDto) {
        boolean result;
        PayType payType = payTypeRepository.get(payTypeDto.getNumber());
        payType.setEndDate(new Date());
        try{
            payTypeRepository.persist(payType);
            result = true;
        }catch (Exception ex){
            result = false;
        }
        return result;
    }

    public PayType update(PayTypeDto payTypeDto) throws Exception{

        String parte1 = null;
        String parte2 = null;

        PayType result = payTypeRepository.get(payTypeDto.getNumber());
        if(result == null){
            throw new Exception("No se encontró la entidad");
        }

        if (payTypeDto.getRangoVigencia() != null){
            String[] parts = payTypeDto.getRangoVigencia().split("-");
            parte1 = parts[0]; // fecha Desde
            parte2 = parts[1]; // fecha Hasta
        }

        if (payTypeDto.getRangoVigencia() != null){
            result.setVigenciaDesde(parseFechaToDate(parte1));
        }

        if (payTypeDto.getRangoVigencia() != null){
            result.setVigenciaHasta(parseFechaToDate(parte2));
        }

        result.setName(payTypeDto.getName());
        result.setDescription(payTypeDto.getDescription());

        payTypeRepository.persist(result);

        return result;
    }

    public PayType persist(PayTypeDto payTypeDto) {
        PayType result = PayTypeDto.Factory.getPayTypeFromDto(payTypeDto);
        payTypeRepository.persist(result);
        return result;
    }

    /**
     * Permite convertir un String en fecha (Date).
     * @param fecha Cadena de fecha dd/MM/yyyy
     * @return Objeto Date
     */
    public Date parseFechaToDate(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return fechaDate;
    }


    /**
     * Permite convertir una fecha Date en String.
     * @param fecha Date (dd/MM/yyyy)
     * @return Objeto String
     */
    public String parseFechaToString(Date fecha){
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaDate = null;
        fechaDate = formato.format(fecha);

        return fechaDate;
    }

    public boolean validateNameAndDescription(PayTypeDto payTypeDto){

        boolean result = false;

        if (payTypeDto.getNumber() != null){
            PayType payType = payTypeRepository.get(payTypeDto.getNumber());
            if (payType != null){
                if (!payTypeDto.getName().equals(payTypeDto.getName())){
                    result = payTypeRepository.getPayTypeExist(payTypeDto.getName(), payTypeDto.getDescription(), payTypeDto.getNumber());
                }
            }
        } else {
            result = payTypeRepository.getPayTypeExist(payTypeDto.getName(), payTypeDto.getDescription(), null);
        }
        return result;

        //return payTypeRepository.getPayTypeExist(payTypeDto.getName(), payTypeDto.getDescription());
    }
}
