package com.mendopark.crud.locality;

import com.mendopark.application.SExpert;
import com.mendopark.model.Locality;
import com.mendopark.model.Province;
import com.mendopark.repository.LocalityReporsitory;
import com.mendopark.repository.ProvinceRepository;
import com.mendopark.rest.locality.LocalityDto;
import com.mendopark.rest.province.ProvinceDto;

import javax.ejb.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class LocalitySExpert extends SExpert {

    @EJB
    private LocalityReporsitory localityReporsitory;
    @EJB
    private ProvinceRepository provinceRepository;

    public int getLastId() {
        Integer lastNumber = null;
        try {
            lastNumber = localityReporsitory.getLastId();
            if (lastNumber == null) {
                lastNumber = 0;
            }
        } catch (Exception ex) {
            lastNumber = 0;
        }
        return lastNumber;
    }

    public Locality find(LocalityDto dto) {
        Locality result = null;

        result = localityReporsitory.get(dto.getNumber());

        return result;
    }

    public List<Locality> findLocalities() {
        return localityReporsitory.getAll();
    }

    public boolean delete(LocalityDto localityDto) {
        boolean result;
        Locality locality = localityReporsitory.get(localityDto.getNumber());
        locality.setEndDate(new Date());
        try {
            localityReporsitory.persist(locality);
            result = true;
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }

    public Locality update(Locality locality) throws Exception {

        Locality result = localityReporsitory.get(locality.getNumber());
        if (result == null) {
            throw new Exception("No se encontró la entidad");
        }
        result.setName(locality.getName());
        if (locality.getFromProvince() != null &&( result.getFromProvince() == null || (result.getFromProvince() != null &&
            !locality.getFromProvince().getNumber().equals(result.getFromProvince().getNumber())))){
            Province province = provinceRepository.get(locality.getFromProvince().getNumber());
            result.setFromProvince(province);
        }
        localityReporsitory.persist(result);

        return result;
    }

    public Locality persist(Locality locality) {
        if(locality.getFromProvince() != null){
            Province province = provinceRepository.get(locality.getFromProvince().getNumber());
            locality.setFromProvince(province);
        }
        localityReporsitory.persist(locality);
        return locality;
    }

    public List<Locality> findFromProvince(ProvinceDto dto) {
        List<Locality> result = new ArrayList<>();
        if(dto.getNumber() != null) {
            Integer provinceNumber = dto.getNumber();
            List<Locality> persistedLocalities = localityReporsitory.getAvailables();
            if(persistedLocalities != null && persistedLocalities.size() > 0){
                for (Locality locality: persistedLocalities) {
                    if(locality.getFromProvince() != null && provinceNumber.equals(locality.getFromProvince().getNumber())
                        && locality.getEndDate() == null){
                        result.add(locality);

                    }
                }
            }
        }
        return result;
    }

    public boolean validateNameAndProvince(LocalityDto localityDto){
        boolean result = false;

        /* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
        if (localityDto.getNumber() != null){
            Locality locality = localityReporsitory.get(localityDto.getNumber());
            if (locality != null){
                if (!localityDto.getName().equals(locality.getName())){
                    result = localityReporsitory.getLocalityExist(localityDto.getName(), localityDto.getFromProvince(), localityDto.getNumber());
                }
            }
        } else {
            result = localityReporsitory.getLocalityExist(localityDto.getName(), localityDto.getFromProvince(), null);
        }

        return result;
    }
}
