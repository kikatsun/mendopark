
package com.mendopark.crud.day;

import com.mendopark.application.SExpert;
import com.mendopark.model.Client;
import com.mendopark.model.Day;
import com.mendopark.model.Role;
import com.mendopark.repository.ClientRepository;
import com.mendopark.repository.DayRepository;
import com.mendopark.rest.day.DayDto;

import javax.ejb.*;
import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DaySExpert extends SExpert {

	@EJB
	private DayRepository dayRepository;

	@EJB
	private ClientRepository clientRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = dayRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public Day find(DayDto dto) {
		Day result = null;
		Day persistedDay = dayRepository.get(dto.getNumber());
		if (getRole().getNumber().equals(Role.Type.SYSTEM_ADMIN.getRoleId())) {
			result = persistedDay;
		} else if ( persistedDay != null && persistedDay.getFromClient() != null && persistedDay.getFromClient().getNumber() != null &&
					persistedDay.getFromClient().getNumber().equals(getClient().getNumber())) {
					result = persistedDay;
		}

		return result;
	}

	public List<Day> findByClient (Long clientNumber) {
		List<Day> result = dayRepository.findByClient(clientNumber);
		if (result == null) {
			result = new ArrayList<>();
		}
		return result;
	}

	public List<Day> findAll(){
		if (getRole().getNumber().equals(Role.Type.SYSTEM_ADMIN.getRoleId())) {
			return dayRepository.getAll();
		} else {
			return findDays();
		}
	}

	public List<Day> findDays() {

		List<Day> result = new ArrayList<>();
		List<Day> days = dayRepository.getAll();

		if (days != null) {
			for (Day day : days) {
				if (day.getFromClient() != null && day.getFromClient().getNumber() != null &&
						day.getFromClient().getNumber().equals(getClient().getNumber())) {
					result.add(day);
				}
			}
		}

		return result;
	}

	public boolean delete(DayDto dayDto) {
		boolean result;
		Day day = dayRepository.get(dayDto.getNumber());
		day.setEndDate(new Date());
		try {
			dayRepository.persist(day);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public Day update(DayDto dayDto) throws Exception {

		Day result = dayRepository.get(dayDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(dayDto.getName());
		result.setSchedule(dayDto.getSchedule());

        if (getClient() != null) {
            Client persistedClient = clientRepository.get(getClient().getNumber());
            if (persistedClient != null) {
                result.setFromClient(persistedClient);
            }
        } else {
            throw new BadRequestException("La sesión no es válida");
        }

		dayRepository.persist(result);

		return result;
	}

	public Day persist(DayDto dayDto) {
		Day result = DayDto.Factory.getDayFromDto(dayDto);

		if (getClient() != null) {
			Client persistedClient = clientRepository.get(getClient().getNumber());
			if (persistedClient != null) {
				result.setFromClient(persistedClient);
			}
		} else {
			throw new BadRequestException("La sesión no es válida");
		}

		dayRepository.persist(result);
		return result;
	}

	public boolean validateName(DayDto dayDto){
		Long idCliente = getClient().getNumber();
		boolean result = false;

		/* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
		if (dayDto.getNumber() != null){
			Day day = dayRepository.get(dayDto.getNumber());
			if (day != null){
				if (!dayDto.getName().equals(day.getName())){
					result = dayRepository.getDayExist(dayDto.getName(), idCliente, dayDto.getNumber());
				}
			}
		} else {
			result = dayRepository.getDayExist(dayDto.getName(), idCliente, null);
		}
		return result;
	}
}
