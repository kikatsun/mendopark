package com.mendopark.crud.sensor;

import com.mendopark.application.SExpert;
import com.mendopark.model.*;
import com.mendopark.repository.SensorAssignamentRepository;
import com.mendopark.repository.SensorRepository;
import com.mendopark.rest.parkingarea.SensorDto;

import javax.ejb.*;
import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SensorSExpert extends SExpert {

    @EJB
    private SensorRepository sensorRepository;

    @EJB
    private SensorAssignamentRepository sensorAssignamentRepository;

    public String getRadomId(){
        return sensorRepository.getLastId();
    }

    public List<Sensor> getSensors(){
        List<Sensor> sensors = new ArrayList<>();
        List<SensorAssignament> asignaments;
        try {
            asignaments = sensorAssignamentRepository.getAvailables();
        } catch (Exception e) {
            asignaments = null;
        }
        if(asignaments != null && asignaments.size() > 0){
            for (SensorAssignament sensorAssignament: asignaments ) {
                if(getClient() != null && Client.MENDOPARK_ID.equals(getClient().getNumber())){
                    sensors.add(sensorAssignament.getFromSensor());
                }else {
                    if (sensorAssignament.getFromClient() != null && sensorAssignament.getFromClient().getNumber() != null
                            && getClient() != null && getClient().getNumber() != null
                            && sensorAssignament.getFromClient().getNumber().equals(getClient().getNumber())) {
                        sensors.add(sensorAssignament.getFromSensor());
                    }
                }
            }
        }
        return sensors;
    }

    public Sensor persist(Sensor sensor){
        validateSensor(sensor);
        List<Sensor> persistedSensors = getSensors();
        if(persistedSensors != null && persistedSensors.size() > 0){
            for (Sensor persistedSensor: persistedSensors ) {
                if (persistedSensor.getReferenceCode() != null
                    && persistedSensor.getReferenceCode().equals(sensor.getReferenceCode())){
                    throw new BadRequestException("El nombre del sensor es inválido, ya existe un sensor con el mismo nombre: "+sensor.getReferenceCode());
                }
            }
        }
        sensor.setId(UUID.randomUUID().toString());
        sensorRepository.persist(sensor);

        SensorAssignament newAssignament = new SensorAssignament();
        newAssignament.setFromSensor(sensor);
        newAssignament.setFromClient(getClient());
        newAssignament.setAssignamentDate(new Date());
        sensorAssignamentRepository.persist(newAssignament);

        return sensor;
    }

    private void validateSensor(Sensor sensor) {
        if(sensor == null){
            throw new BadRequestException("Los datos del sensor no se recibieron correctamente");
        }
        if(sensor.getReferenceCode() == null || sensor.getReferenceCode().isEmpty()){
            throw new BadRequestException("El nombre del sensor es inválido: está vacío");
        }
    }

    public Sensor update(Sensor sensor) {
        validateSensor(sensor);
        Sensor persistedSensorToUpdate = null;
        List<Sensor> persistedSensors = getSensors();
        if(persistedSensors != null && persistedSensors.size() > 0){
            for (Sensor persistedSensor: persistedSensors ) {
                if (persistedSensor.getId() != null
                        && persistedSensor.getId().equals(sensor.getId())){
                    persistedSensorToUpdate = persistedSensor;
                    break;
                }
            }
        }
        if(persistedSensorToUpdate == null){
            throw new BadRequestException("No se encontró el sensor con id: "+sensor.getId());
        }
        persistedSensorToUpdate.setReferenceCode(sensor.getReferenceCode());
        persistedSensorToUpdate.setDescription(sensor.getDescription());
//        if (sensor.getCoordinatePoint() != null && sensor.getCoordinatePoint().getLongitude() != null
//            && sensor.getCoordinatePoint().getLatitude() != null){
//            if(persistedSensorToUpdate.getCoordinatePoint() == null){
//                persistedSensorToUpdate.setCoordinatePoint(sensor.getCoordinatePoint());
//            }else{
//                persistedSensorToUpdate.getCoordinatePoint().setLatitude(sensor.getCoordinatePoint().getLatitude());
//                persistedSensorToUpdate.getCoordinatePoint().setLongitude(sensor.getCoordinatePoint().getLongitude());
//            }
//        }
        sensorRepository.persist(persistedSensorToUpdate);
        return persistedSensorToUpdate;
    }

    public Sensor findSensor(String id) {
        return sensorRepository.get(id);
    }

    public boolean delete(Sensor sensorFrom) {
        Sensor persistedSensor = sensorRepository.get(sensorFrom.getId());

        if(persistedSensor != null){
            persistedSensor.setEndDate(new Date());
            sensorRepository.persist(persistedSensor);
        }else{
            throw new BadRequestException("No se pudo identificar al sensor para darle de baja");
        }

        return true;
    }

    public boolean validateReferenceCodeAndDescription(SensorDto sensorDto){

        /* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
        boolean result = false;

        if (sensorDto.getId() != null){
            Sensor sensor = sensorRepository.get(sensorDto.getId());
            if (sensor != null){
                if (!sensorDto.getName().equals(sensor.getReferenceCode())){
                    result = sensorRepository.getSensorExist(sensorDto.getName(), sensorDto.getDescription(), sensorDto.getId());
                }
            }
        } else {
            result = sensorRepository.getSensorExist(sensorDto.getName(), sensorDto.getDescription(), null);
        }
        return result;

        //return sensorRepository.getSensorExist(sensorDto.getName(), sensorDto.getDescription());
    }
}
