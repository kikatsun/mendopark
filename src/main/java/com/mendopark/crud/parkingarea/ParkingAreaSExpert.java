package com.mendopark.crud.parkingarea;

import com.mendopark.application.SExpert;
import com.mendopark.model.*;
import com.mendopark.repository.ParkingAreaRepository;
import com.mendopark.repository.ParkingSpaceTypeRepository;
import com.mendopark.repository.SensorRepository;
import com.mendopark.rest.parkingarea.ParkingAreaDto;
import com.mendopark.rest.parkingarea.ParkingAreaFactory;
import com.mendopark.rest.parkingarea.PointDto;
import com.mendopark.rest.sensorstate.SensorStateFactory;
import com.mendopark.service.MEndoParkLogger;
import com.mendopark.session.SessionManager;

import javax.ejb.*;
import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ParkingAreaSExpert extends SExpert {

    @EJB
    private ParkingAreaRepository parkingAreaRepository;
    @EJB
    private SessionManager sessionManager;
    @EJB
    private SensorStateFactory sensorStateFactory;
    @EJB
    private SensorRepository sensorRepository;
    @EJB
    private ParkingSpaceTypeRepository parkingSpaceTypeRepository;

    private static final Double MAX_DISTANCE_TO_SEARCH = 0.125D;

    @EJB
    private ParkingAreaFactory parkingAreaFactory;

    public Long getLastId() {
        Long lastNumber = null;
        try {
            lastNumber = parkingAreaRepository.getLastId();
            if (lastNumber == null) {
                lastNumber = 0L;
            }
        } catch (Exception ex) {
            lastNumber = 0L;
        }
        return lastNumber;
    }

    public ParkingArea find(ParkingAreaDto dto) {
        ParkingArea result = null;

        result = parkingAreaFactory.get(dto.getNumber());

        return result;
    }

    public boolean delete(ParkingArea parkingArea) {
        parkingArea = parkingAreaRepository.get(parkingArea.getNumber());
        if(parkingArea == null){
            throw new BadRequestException("El área seleccionada no existe! -> "+parkingArea.toString());
        }
        Date endDate = new Date();
        parkingArea.setEndDate(endDate);
        List<Sensor> sensorsToRemove = new ArrayList<>();
        if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0){
            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces() ) {
                if(parkingSpace.getParkingSpaceRegistrations() != null  && parkingSpace.getParkingSpaceRegistrations().size() > 0){
                    for (ParkingSpaceRegistration registration: parkingSpace.getParkingSpaceRegistrations() ) {
                        if(registration.getEndDate() == null){
                            registration.setEndDate(endDate);
                        }
                        if(registration.getFromSensor() != null){
                            registration.getFromSensor().setEndDate(endDate);
                            sensorsToRemove.add(registration.getFromSensor());
                        }
                    }
                }
            }
        }
        parkingAreaRepository.persist(parkingArea);
        parkingAreaFactory.remove(parkingArea);
        sensorStateFactory.removeSensorsAvailable(sensorsToRemove);
        return true;
    }

    public ParkingArea update(ParkingArea updatedParkingArea) throws BadRequestException{

        ParkingArea persistedParkingArea = parkingAreaRepository.get(updatedParkingArea.getNumber());
        if (persistedParkingArea == null) {
            throw new BadRequestException("No se encontró el Área de estacionamiento: "+updatedParkingArea.getNumber());
        }

        validateUpdatedParkingArea(updatedParkingArea);

        persistedParkingArea.setName(updatedParkingArea.getName());
        persistedParkingArea.setColor(updatedParkingArea.getColor());

        /* Se cambia el orden de os update porque al parecer no se puede ejecutar un select después de haber ejecutado
        * un update, al parecer el flush a la BD lo hace al final */
        persistedParkingArea = updateParkingSpaces(persistedParkingArea, updatedParkingArea);
        persistedParkingArea = updateParkingAreaDetails(persistedParkingArea, updatedParkingArea);

        persistedParkingArea = parkingAreaRepository.persist(persistedParkingArea);
        parkingAreaFactory.remove(persistedParkingArea);
        parkingAreaFactory.addNew(persistedParkingArea);

        return persistedParkingArea;
    }

    private void validateUpdatedParkingArea (ParkingArea parkingArea){

        if(parkingArea.getNumber() == null){
            throw new BadRequestException("No se pudo actualizar: el número identificador del área de estacionamiento está vacío.");
        }
        if(parkingArea.getEndDate() != null){
            throw new BadRequestException("No se pudo actualizar: el área de estacionamiento está dada de baja.");
        }
        if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0){
            List<String> sensorIds = new ArrayList<>();
            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces() ) {
                String sensorId = getSensor(parkingSpace);
                if(sensorId != null && sensorIds.contains(sensorId)){
                    throw new BadRequestException("No se pudo actualizar: \nUn sensor no puede asignarse a dos plazas de estacionamiento." +
                            "\nAsignar otro sensor a "+parkingSpace.getName());
                }else {
                    sensorIds.add(sensorId);
                }
            }
        }

    }

    private ParkingArea updateParkingSpaces (ParkingArea persistedParkingArea, ParkingArea updatedParkingArea) throws BadRequestException{

        Date endDate = new Date();
        if (updatedParkingArea.getParkingSpaces() == null
            || updatedParkingArea.getParkingSpaces().size() == 0){
            if(persistedParkingArea.getParkingSpaces() != null && persistedParkingArea.getParkingSpaces().size() > 0){
                for (ParkingSpace parkingSpace: persistedParkingArea.getParkingSpaces() ) {
                    setEndDateRegistration(parkingSpace, endDate);
                }
            }
            persistedParkingArea.setParkingSpaces(updatedParkingArea.getParkingSpaces());
        } else{
            List<ParkingSpace> updatedParkingSpaces = updatedParkingArea.getParkingSpaces();
            List<ParkingSpace> parkingSpaces = new ArrayList<>();
            if(persistedParkingArea.getParkingSpaces() != null && persistedParkingArea.getParkingSpaces().size() > 0){
                for (ParkingSpace persistedParkingSpace: persistedParkingArea.getParkingSpaces() ) {
                    if(persistedParkingSpace.getId() != null) {
                        ParkingSpace updatedParkingSpace
                            = getParkingSpaceById(updatedParkingSpaces, persistedParkingSpace.getId());
                        if (updatedParkingSpace == null) {
                            MEndoParkLogger.MENDOPARK.log(Level.INFO, "Se da de baja: "+persistedParkingSpace.getName());
                            setEndDateRegistration(persistedParkingSpace, endDate);
                        } else {
                            persistedParkingSpace = updateParkingSpace(persistedParkingSpace, updatedParkingSpace);
                            parkingSpaces.add(persistedParkingSpace);
                        }
                    }else {
                        MEndoParkLogger.MENDOPARK.log(Level.WARNING, "Se está tratando de actualizar -> "
                            + persistedParkingSpace.toString()+" con id nulo de -> "+persistedParkingArea.toString());
                    }
                }
            }
            if(updatedParkingSpaces != null && updatedParkingSpaces.size() > 0){
                for (ParkingSpace updatedParkingSpace: updatedParkingSpaces ) {
                    if(updatedParkingSpace.getId() == null){
                        parkingSpaces.add(updatedParkingSpace);
                    }
                }
            }
            persistedParkingArea.setParkingSpaces(parkingSpaces);
        }
        return persistedParkingArea;
    }

    private ParkingSpace updateParkingSpace (ParkingSpace persistedParkingSpace, ParkingSpace updatedParkingSpace) throws BadRequestException{

        persistedParkingSpace.setName(updatedParkingSpace.getName());
        ParkingSpaceType updatedType = null;
        if(updatedParkingSpace.getParkingSpaceType() != null && updatedParkingSpace.getParkingSpaceType().getNumber() != null){
            updatedType = parkingSpaceTypeRepository.get(updatedParkingSpace.getParkingSpaceType().getNumber());
        }
        persistedParkingSpace.setParkingSpaceType(updatedType);
        if (updatedParkingSpace.getCoordinatePoint() == null){
            persistedParkingSpace.setCoordinatePoint(null);
        }else {
            if (persistedParkingSpace.getCoordinatePoint() != null){
                persistedParkingSpace.getCoordinatePoint().setLatitude(
                    updatedParkingSpace.getCoordinatePoint().getLatitude());
                persistedParkingSpace.getCoordinatePoint().setLongitude(
                    updatedParkingSpace.getCoordinatePoint().getLongitude());
            }else {
                persistedParkingSpace.setCoordinatePoint(updatedParkingSpace.getCoordinatePoint());
            }
        }
        String updatedSensorId = getSensor(updatedParkingSpace);
        if (updatedSensorId == null){
            setEndDateRegistration(persistedParkingSpace, new Date());
        }else {
            String persistedSensorId = getSensor(persistedParkingSpace);
            if (!updatedSensorId.equals(persistedSensorId)){
                Sensor sensor = sensorRepository.get(updatedSensorId);
                if(sensor == null){
                    throw  new BadRequestException("El sensor es inválido");
                }else {
                    setEndDateRegistration(persistedParkingSpace, new Date());
                    /* De la interfaz el registro de sensores siempre viene con solo uno */
                    updatedParkingSpace.getParkingSpaceRegistrations().get(0).setFromSensor(sensor);
                    persistedParkingSpace.getParkingSpaceRegistrations()
                            .addAll(updatedParkingSpace.getParkingSpaceRegistrations());
                }
            }
        }

        return persistedParkingSpace;
    }

    private String getSensor(ParkingSpace parkingSpace){
        String sensorId = null;
        if (parkingSpace != null && parkingSpace.getParkingSpaceRegistrations() != null
            && parkingSpace.getParkingSpaceRegistrations().size() > 0){
            for (int i = 0; i < parkingSpace.getParkingSpaceRegistrations().size(); i++) {
                ParkingSpaceRegistration registration = parkingSpace.getParkingSpaceRegistrations().get(i);
                if(registration != null && registration.getEndDate() == null && registration.getFromSensor() != null
                    && registration.getFromSensor().getId() != null){
                    sensorId = registration.getFromSensor().getId();
                    break;
                }
            }
        }
        return sensorId;
    }

    private ParkingSpace getParkingSpaceById(List<ParkingSpace> parkingSpaceList, String id){
        ParkingSpace result = null;
        if(parkingSpaceList != null && parkingSpaceList.size() > 0){
            for (int i = 0; i < parkingSpaceList.size(); i++) {
                ParkingSpace parkingSpace = parkingSpaceList.get(i);
                if(parkingSpace.getId() != null && parkingSpace.getId().equals(id)){
                    result = parkingSpace;
                    break;
                }
            }
        }
        return result;
    }

    private void setEndDateRegistration(ParkingSpace parkingSpace, Date endDate){
        if (parkingSpace.getParkingSpaceRegistrations() != null
            && parkingSpace.getParkingSpaceRegistrations().size() > 0){
            for (ParkingSpaceRegistration registration: parkingSpace.getParkingSpaceRegistrations() ) {
                if(registration.getEndDate() == null){
                    registration.setEndDate(endDate);
                }
            }
        }
    }

    private ParkingArea updateParkingAreaDetails (ParkingArea persistedParkingArea, ParkingArea updatedParkingArea){

        if (updatedParkingArea.getParkingAreaDetails() == null
            || updatedParkingArea.getParkingAreaDetails().size() == 0){
            persistedParkingArea.setParkingAreaDetails(updatedParkingArea.getParkingAreaDetails());
        }else {
            /* Es probable que no venga el id por lo tanto se toma la secuencia como base de la actualización */
            List<Integer> updatedSecuenceList = new ArrayList<>();
            List<ParkingAreaDetail> persistedParkingAreaDetails = persistedParkingArea.getParkingAreaDetails();
            List<ParkingAreaDetail> parkingAreaDetails = new ArrayList<>();
            for (ParkingAreaDetail updatedParkingAreaDetail: updatedParkingArea.getParkingAreaDetails() ) {
                if(updatedParkingAreaDetail.getSequence() != null){
                    ParkingAreaDetail parkingAreaDetail = getParkingAreaDetailBySequence(
                        persistedParkingAreaDetails, updatedParkingAreaDetail.getSequence());
                    if(parkingAreaDetail == null){
                        parkingAreaDetail = updatedParkingAreaDetail;
                    }else{
                        parkingAreaDetail.setCoordinatePoint(updatedParkingAreaDetail.getCoordinatePoint());
                    }
                    parkingAreaDetails.add(parkingAreaDetail);
                    updatedSecuenceList.add(updatedParkingAreaDetail.getSequence());
                }
            }
            // Para que no estén guardadas en la BD los puntos no referenciados se borran
            if(persistedParkingAreaDetails != null && persistedParkingAreaDetails.size() > 0){
                for (ParkingAreaDetail persistedParkingAreaDetail: persistedParkingAreaDetails ) {
                    if( persistedParkingAreaDetail.getSequence() != null
                        && !updatedSecuenceList.contains(persistedParkingAreaDetail.getSequence())){
                        parkingAreaRepository.deleteParkingAreaDetail(persistedParkingAreaDetail);
                    }
                }
            }
            persistedParkingArea.setParkingAreaDetails(parkingAreaDetails);
        }

        return  persistedParkingArea;
    }

    private ParkingAreaDetail getParkingAreaDetailBySequence(List<ParkingAreaDetail> detailList, Integer sequence){
        ParkingAreaDetail result = null;
        if(detailList != null) {
            for (int i = 0; i < detailList.size(); i++) {
                ParkingAreaDetail detail = detailList.get(i);
                if (detail.getSequence() != null && detail.getSequence().equals(sequence)) {
                    result = detail;
                    break;
                }
            }
        }
        return result;
    }

    public boolean persist(ParkingArea parkingArea) {
        parkingArea.setCenterPoint(calculateCenterPoint(parkingArea.getParkingAreaDetails()));
        parkingArea.setFromClient(sessionManager.find(getSessionId()).getUserSession().getFromClient());
        if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0){
            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces() ) {
                if(parkingSpace.getParkingSpaceType() != null && parkingSpace.getParkingSpaceType().getNumber() != null){
                    ParkingSpaceType type = parkingSpaceTypeRepository.get(parkingSpace.getParkingSpaceType().getNumber());
                    if(type != null){
                        parkingSpace.setParkingSpaceType(type);
                    }
                }
            }
        }
        parkingAreaRepository.persist(parkingArea);
        parkingAreaFactory.addNew(parkingArea);
        return true;
    }

    private CoordinatePoint calculateCenterPoint(List<ParkingAreaDetail> details){
        CoordinatePoint centerPoint = null;

        if(details != null && details.size() > 0){
            Double centerPointLatitude = 0D;
            Double centerPointLongitude = 0D;
            for (ParkingAreaDetail detail: details) {
                centerPointLatitude = centerPointLatitude + detail.getCoordinatePoint().getLatitude();
                centerPointLongitude = centerPointLongitude + detail.getCoordinatePoint().getLongitude();
            }
            centerPointLatitude = centerPointLatitude / new Double(details.size());
            centerPointLongitude = centerPointLongitude / new Double(details.size());
            centerPoint = new CoordinatePoint(centerPointLatitude, centerPointLongitude);
        }

        return centerPoint;
    }

    public List<ParkingArea> findParkingAreas() {
        List<ParkingArea> result = new ArrayList<>();
        if(getRole() != null) {
            List<ParkingArea> parkingAreasAvailables = parkingAreaFactory.getAvailables();
            if (Role.Type.SYSTEM_ADMIN.getRoleId().equals(getRole().getNumber())) {
                result = parkingAreasAvailables;
            } else {
                if (getClient() == null || getClient().getNumber() == null) {
                    MEndoParkLogger.MENDOPARK.log(Level.WARNING, "El usuario no tiene asignado cliente para filtrar las áreas de estacionamiento");
                } else {
                    if (parkingAreasAvailables != null && parkingAreasAvailables.size() > 0) {
                        Long clientNumber = getClient().getNumber();
                        for (ParkingArea parkingArea : parkingAreasAvailables) {
                            if (parkingArea.getFromClient() != null
                                    && clientNumber.equals(parkingArea.getFromClient().getNumber())) {
                                result.add(parkingArea);
                            }
                        }
                    }
                }
            }
        }else{
            MEndoParkLogger.MENDOPARK.log(Level.WARNING, "Rol null, credenciales vacias? "+getSessionId() + " "+getClient().toString()+ " " + getRole().toString());
        }
        return result;
    }

    public List<ParkingArea> findParkingAreas2() {
        return parkingAreaFactory.getAvailables();
    }

    public List<ParkingArea> findParkingAreas(PointDto pointDto, Long clientId) {
        List<ParkingArea> result = new ArrayList();
        if(pointDto != null && pointDto.getLongitude() != null && pointDto.getLatitude() != null) {
            List<ParkingArea> parkingAreasAvailables = parkingAreaFactory.getAvailables();
            if(parkingAreasAvailables != null && parkingAreasAvailables.size() > 0) {
                for (ParkingArea parkingArea : parkingAreasAvailables) {
                    CoordinatePoint centerPoint = parkingArea.getCenterPoint();
                    Double diffLatitude = centerPoint.getLatitude() - pointDto.getLatitude();
                    Double diffLongitude = centerPoint.getLongitude() - pointDto.getLongitude();
                    if (diffLatitude < 0) {
                        diffLatitude = diffLatitude * -1D;
                    }
                    if (diffLongitude < 0) {
                        diffLongitude = diffLongitude * -1D;
                    }
                    if (diffLatitude < MAX_DISTANCE_TO_SEARCH && diffLongitude < MAX_DISTANCE_TO_SEARCH) {
                        result.add(parkingArea);
                    }
                }
            }
        }
        if(result.size()>0 && clientId != null){
            List<ParkingArea> newResult = new ArrayList();
            for (ParkingArea pk: result ) {
                if(pk.getFromClient()!= null && clientId.equals(pk.getFromClient().getNumber())){
                    newResult.add(pk);
                }
            }
            result = newResult;
        }
        return result;
    }

    public void updateParkingAreasInfo() {
        parkingAreaFactory.init();
    }
}
