
package com.mendopark.crud.reservestatetype;

import com.mendopark.application.SExpert;
import com.mendopark.model.Province;
import com.mendopark.model.ReserveStateType;
import com.mendopark.repository.ReserveStateTypeRepository;
import com.mendopark.rest.reservestatetype.ReserveStateTypeDto;

import javax.ejb.*;
import java.util.Date;
import java.util.List;

/**
 * Experto de Tipo de Estado de Reserva
 * Experto que define la lógica relacionada con la Tipo de Estado de Reserva
 *
 * @Pattern: Expert
 */

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReserveStateTypeSExpert extends SExpert {

	@EJB
	private ReserveStateTypeRepository reserveStateTypeRepository;

	public int getLastId() {
		Integer lastNumber = null;
		try {
			lastNumber = reserveStateTypeRepository.getLastId();
			if (lastNumber == null) {
				lastNumber = 0;
			}
		} catch (Exception ex) {
			lastNumber = 0;
		}
		return lastNumber;
	}

	public ReserveStateType find(ReserveStateTypeDto dto) {
		ReserveStateType result = null;

		result = reserveStateTypeRepository.get(dto.getNumber());

		return result;
	}

	public List<ReserveStateType> findReserveStateTypes() {
		return reserveStateTypeRepository.getAvailables();
	}

	public boolean delete(ReserveStateTypeDto reserveStateTypeDto) {
		boolean result;
		ReserveStateType reserveStateType = reserveStateTypeRepository.get(reserveStateTypeDto.getNumber());
		reserveStateType.setEndDate(new Date());
		try {
			reserveStateTypeRepository.persist(reserveStateType);
			result = true;
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

	public ReserveStateType update(ReserveStateTypeDto reserveStateTypeDto) throws Exception {

		ReserveStateType result = reserveStateTypeRepository.get(reserveStateTypeDto.getNumber());
		if (result == null) {
			throw new Exception("No se encontró la entidad");
		}

		result.setName(reserveStateTypeDto.getName());

		result.setDescription(reserveStateTypeDto.getDescription());

		reserveStateTypeRepository.persist(result);

		return result;
	}

	public ReserveStateType persist(ReserveStateTypeDto reserveStateTypeDto) {
		ReserveStateType result = ReserveStateTypeDto.Factory.getReserveStateTypeFromDto(reserveStateTypeDto);
		reserveStateTypeRepository.persist(result);
		return result;
	}

	public boolean validateNameAndDescription(ReserveStateTypeDto reserveStateTypeDto) {

		/* La validación es correcta cuando toma el valor falso (Falso indica que NO existe una ocurrencia) */
		boolean result = false;

		if (reserveStateTypeDto.getNumber() != null){
			ReserveStateType reserveStateType = reserveStateTypeRepository.get(reserveStateTypeDto.getNumber());
			if (reserveStateType != null){
				if (!reserveStateTypeDto.getName().equals(reserveStateType.getName())){
					result = reserveStateTypeRepository.getReserveStateTypeExist(reserveStateTypeDto.getName(), reserveStateTypeDto.getDescription(), reserveStateTypeDto.getNumber());
				}
			}
		} else {
			result = reserveStateTypeRepository.getReserveStateTypeExist(reserveStateTypeDto.getName(), reserveStateTypeDto.getDescription(), null);
		}
		return result;

		//return reserveStateTypeRepository.getReserveStateTypeExist(reserveStateTypeDto.getName(), reserveStateTypeDto.getDescription());
	}
}
