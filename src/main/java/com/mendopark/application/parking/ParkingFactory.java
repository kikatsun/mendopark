package com.mendopark.application.parking;

import com.mendopark.crud.parkingarea.ParkingAreaSExpert;
import com.mendopark.model.*;
import com.mendopark.repository.*;
import com.mendopark.rest.parking.ParkingDto;
import com.mendopark.rest.parking.ReservaDto;
import com.mendopark.rest.parkingarea.ParkingAreaDto;
import com.mendopark.rest.parkingarea.ParkingAreaFactory;
import com.mendopark.rest.parkingspace.ParkingSpaceDto;
import com.mendopark.rest.sensorstate.SensorStateFactory;
import com.mendopark.service.clientconfiguration.ClientConfigurationManager;
import com.mendopark.service.utilities.DateUtilities;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.util.*;

@Startup
@Singleton
public class ParkingFactory {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    private ParkingAreaSExpert parkingAreaSExpert;
    @EJB
    private ClientConfigurationManager clientConfigurationManager;
    @EJB
    private ParkingAreaRepository parkingAreaRepository;
    @EJB
    private ParkingSpaceRepository parkingSpaceRepository;

    @EJB
    private ReserveRepository reserveRepository;
    @EJB
    private ParkingRepository parkingRepository;
    @EJB
    private PayTypeRepository payTypeRepository;

    /* Desde la app movil se identifican consultas por el uuid, en cambio internamente por el nombre de la plaza que está
    * activa */
    private Map<UUID, ParkingDto> memory = new HashMap<UUID, ParkingDto>();
    private Map<String, UUID> parkingActived = new HashMap<String, UUID>();//<SensorName, ParkingDtoId>

    /* Como no se persiste se guarda en memoria histórica */
    private Map<UUID, ParkingDto> parkingSpacesHistoric = new HashMap<UUID, ParkingDto>();
    private Map<String, ReservaDto> reservasHistoric = new HashMap<String, ReservaDto>();

    private Map<String, ReservaDto> reservas = new HashMap<String, ReservaDto>();

    @EJB
    private ParkingAreaFactory parkingAreaFactory;

    /* Map<parkingSpaceName, SensorState */
    private Map<String, SensorState> parkingSpaceReserved = new HashMap<String, SensorState>();

    public void createParking(SensorState sensorState){
        ParkingDto parkingDto = new ParkingDto();

        if (parkingActived.containsKey(sensorState.getName())) {
            // Se loguea severo
            endParking(sensorState);
        }

        ParkingAreaDto parkingArea = null;
        ParkingSpaceDto parkingSpace = null;
        List<ParkingAreaDto> parkingAreaDtos = ParkingAreaDto.Factory.getDtosFromParkingArea(parkingAreaSExpert.findParkingAreas2());
        main: for (int i=0; i<parkingAreaDtos.size();i++){
            if(parkingAreaDtos.get(i) != null && parkingAreaDtos.get(i).getParkingSpaces() != null) {
                for (int j = 0; j < parkingAreaDtos.get(i).getParkingSpaces().size(); j++) {
                    ParkingSpaceDto parkingSpaceDto = parkingAreaDtos.get(i).getParkingSpaces().get(j);
                    if (parkingSpaceDto.getId().equals(sensorState.getName())) {
                        parkingArea = parkingAreaDtos.get(i);
                        parkingSpace = parkingSpaceDto;
                        break main;
                    }
                }
            }
        }
        if(parkingArea != null && parkingSpace != null){
            System.out.println("MIRAAAAAAAAAAAAA: "+parkingArea.toString());
            System.out.println("MIRAAAAAAAAAAAAA: "+parkingSpace.toString());
            parkingDto.setParkingAreaNumber(parkingArea.getNumber());
            parkingDto.setParkingSpaceId(sensorState.getName());
            parkingDto.setParkingSpaceName(parkingSpace.getName());
            parkingDto.setStart(sensorState.getCreationDate());
            parkingDto.setId(UUID.randomUUID());
            if(parkingSpaceReserved.containsKey(sensorState.getName())){
                parkingSpaceReserved.remove(sensorState.getName());
                ReservaDto reserva;
                if(reservas.containsKey(sensorState.getName())) {
                    ReservaDto rr = reservas.get(parkingDto.getParkingSpaceId());
                    parkingDto.setReserva(rr);
                    reservas.remove(sensorState.getName());
                    Reserve persistedReserve = reserveRepository.get(rr.getId());
                    if(persistedReserve != null) {
                        persistedReserve.setStateId(ReserveStateType.State.STARTED.getStateId());
                        reserveRepository.persist(persistedReserve);
                        entityManager.flush();
                    }
                }else{
                    reserva = new ReservaDto();
                    reserva.setId(UUID.randomUUID().toString());
                    reserva.setObservations("Reserva Fantasma");
                    reserveRepository.persist(ReservaDto.Factory.getReserveFrom(reserva));
                    entityManager.flush();
                    parkingDto.setReserva(reserva);
                }
            }
            System.out.println("ALERTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA: "+parkingDto.toString());
            Parking pk = ParkingDto.Factory.getParkingFrom(parkingDto);
            System.out.println("\nANTESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS: "+pk.toString());
            if(pk.getReserve() != null && pk.getReserve().getId() != null){
                pk.setReserve(reserveRepository.get(pk.getReserve().getId()));
            }
            if(pk.getParkingSpace() != null && pk.getParkingSpace().getId() != null){
                pk.setParkingSpace(parkingSpaceRepository.get(pk.getParkingSpace().getId()));
            }
            if(pk.getParkingArea() != null && pk.getParkingArea().getNumber() != null){
                pk.setParkingArea(parkingAreaRepository.get(pk.getParkingArea().getNumber()));
            }
            System.out.println("\nDESPUESSSSSSSSSSSSSSSSSSSSSs: "+pk.toString());
            parkingRepository.persist(pk);
            entityManager.flush();
            memory.put(parkingDto.getId(),parkingDto);
            parkingActived.put(sensorState.getName(),parkingDto.getId());
        }else{
            /* Debe mostrar algún error en la interfaz */
        }
    }

    public void endParking(SensorState sensorState){
        System.out.println("Se finaliza el estacionamiento.");
        ParkingDto toFinalize = memory.get(parkingActived.get(sensorState.getName()));
        if(toFinalize != null) {
            Parking persistedParking = parkingRepository.get(toFinalize.getId().toString());
            if(persistedParking != null) {
                toFinalize.setEnd(sensorState.getCreationDate());
                toFinalize.setHours(DateUtilities.calculateHoursBetween(toFinalize.getStart(), toFinalize.getEnd()));
                toFinalize.setPrice(calculateParkingPrice(toFinalize.getHours(), clientConfigurationManager.getParkingSPacePrice()));
                persistedParking.setEnd(toFinalize.getEnd());
                persistedParking.setHours(toFinalize.getHours());
                persistedParking.setPrice(toFinalize.getPrice());
                if (toFinalize.getReserva() != null) {
                    toFinalize.getReserva().setTotal(toFinalize.getPrice());
                    toFinalize.getReserva().setTotalAPagar(
                            toFinalize.getPrice() - toFinalize.getReserva().getAnticipatedAmount());
                    if (toFinalize.getReserva() != null && toFinalize.getReserva().getId() != null) {
                        reservasHistoric.put(toFinalize.getReserva().getId(), toFinalize.getReserva());
                    } else {
                        // log de q no se pudo guardar reserva no tiene id
                    }
                    if(persistedParking.getReserve() != null){
                        persistedParking.getReserve().setFinalPrice(toFinalize.getReserva().getTotalAPagar());
                    }
                }
                System.out.println("Se finaliza correctamente: " + toFinalize.getId());
                parkingRepository.persist(persistedParking);
                entityManager.flush();
                parkingSpacesHistoric.put(toFinalize.getId(), toFinalize);
                memory.remove(toFinalize.getId());
                parkingActived.remove(toFinalize.getParkingSpaceName());
            }else{
                memory.remove(toFinalize.getId());
            }

        }

    }

    public List<ParkingDto> findAll(){
        List<ParkingDto> currentLis = new ArrayList<ParkingDto>(parkingSpacesHistoric.values());
        List<ParkingDto> result = new ArrayList<>();
        for (int i = currentLis.size()-1; i >= 0 ; i--) {
            result.add(currentLis.get(i));
        }
        System.out.println("Parkingss:::::: se devuelven "+result.size()+" registros de estacionamientos de " + currentLis.size());
        return result;
    }

    public List<ParkingDto> findAll(Long clientId){
        List<ParkingDto> currentLis = new ArrayList<ParkingDto>(parkingSpacesHistoric.values());
        List<ParkingDto> result = new ArrayList<>();
        List<Long> allowedParkingAreas = findParkingAreasId(clientId);
        for (int i = currentLis.size()-1; i >= 0 ; i--) {
            if (allowedParkingAreas.contains(currentLis.get(i).getParkingAreaNumber())) {
                result.add(currentLis.get(i));
            }
        }
        System.out.println("Parkingss:::::: se devuelven "+result.size()+" registros de estacionamientos de " + currentLis.size());
        return result;
    }

    private List<Long> findParkingAreasId (Long clientId) {
        List<Long> result = new ArrayList<>();
        List<ParkingArea> parkingAreasAvailables = parkingAreaFactory.getAvailables();
        if (parkingAreasAvailables != null) {
            for (ParkingArea pa: parkingAreasAvailables ) {
                if (pa.getFromClient() != null && pa.getFromClient().getNumber().equals(clientId)) {
                    result.add(pa.getNumber());
                }
            }
        }

        return result;
    }

    public ReservaDto getReserva(String id){
        ReservaDto reserva = null;
        if(reservasHistoric.containsKey(id)){
            reserva = reservasHistoric.get(id);
        }else if(reservas.containsKey(id)){
            reserva = reservas.get(id);
        }
        return reserva;
    }

    public List<ReservaDto> getReservas(){
        return new ArrayList<>(reservas.values());
    }

    public ReservaDto findParking(String parkingName){
        ReservaDto reserva = null;
        ParkingArea parkingAreaTarget = null;
        ParkingSpace parkingSpaceTarget = null;
        List<ParkingArea> parkingAreas = parkingAreaSExpert.findParkingAreas();
        if(parkingAreas == null){
            return null;
        }
        main:for (int i = 0; i < parkingAreas.size() ; i++) {
            ParkingArea parkingArea = parkingAreas.get(i);
            List<ParkingSpace> parkingSpaces = parkingArea.getParkingSpaces();
            if(parkingSpaces == null){
                return null;
            }
            for (int j = 0; j < parkingSpaces.size() ; j++) {
                ParkingSpace parkingSpace = parkingSpaces.get(j);
                if(parkingSpace.getName() != null && parkingSpace.getName().equals(parkingName)){
                    parkingAreaTarget = parkingArea;
                    parkingSpaceTarget = parkingSpace;
                    break main;
                }
            }
        }
        if(parkingAreaTarget != null && parkingSpaceTarget != null){
            reserva = new ReservaDto();
            reserva.setId(UUID.randomUUID().toString());
            //reserva.setIdPlaza(parkingSpaceTarget.getId().toString());
            reserva.setAnticipatedAmount(20f);
            reserva.setPriceByHour(50f);
            reserva.setParkingSpaceName(parkingSpaceTarget.getName());
            reserva.setParkingAreaName(parkingAreaTarget.getName());
            reserva.setParkingAreaNumber(parkingAreaTarget.getNumber());
        }

        return reserva;
    }

    public void crearReserva(ReservaDto reserva){
        Reserve reserve = ReservaDto.Factory.getReserveFrom(reserva);
        if (reserve.getId() == null) {
            reserve.setId(UUID.randomUUID().toString());
        }
        if(reserve.getParkingArea() != null && reserve.getParkingArea().getNumber() != null){
            reserve.setParkingArea(parkingAreaRepository.get(reserve.getParkingArea().getNumber()));
        }
        if(reserve.getParkingSpace() != null && reserve.getParkingSpace().getId() != null){
            reserve.setParkingSpace(parkingSpaceRepository.get(reserve.getParkingSpace().getId()));
        }
        reserve.setStateId(ReserveStateType.State.CREATED.getStateId());
        if (reserva.getPago() != null && reserva.getPago().getPayType() != null && reserva.getPago().getPayType() instanceof Long) {
            reserve.setPayType(payTypeRepository.get(reserva.getPago().getPayType()));
        } else {
            // Todo debe borrarse cuando esté activa en la parte movil.
            reserve.setPayType(payTypeRepository.get(1L));
        }
        reserveRepository.persist(reserve);
        entityManager.flush();
        reservas.put(reserva.getIdPlaza(),reserva);
    }

    public void cancelReserve (SensorState sensorState) {
        /* lógica de cancelar*/
        ReservaDto reservaDto = reservas.get(sensorState.getName());
        if(reservaDto != null && reservaDto.getId() != null) {
            Reserve persitedReserve = reserveRepository.get(reservaDto.getId());
            persitedReserve.setStateId(ReserveStateType.State.CANCELED.getStateId());
            reserveRepository.persist(persitedReserve);
            entityManager.flush();
        }
        parkingSpaceReserved.remove(sensorState.getName());
    }

    public void addParkingSpaceReserved (SensorState sensorState) {
        if(!this.parkingSpaceReserved.containsKey(sensorState.getName())){
            this.parkingSpaceReserved.put(sensorState.getName(), sensorState);
        }else{
            throw new BadRequestException(Response.status(404).entity("ParkingSpace "+sensorState.getName()+" is reserved").build());
        }
    }

    public void removeReserve (SensorState sensorState){
        if(this.parkingSpaceReserved.containsKey(sensorState.getName())){
            this.parkingSpaceReserved.remove(sensorState.getName());
        }
    }

    public void removeReserve (String parkingSpaceName){
        if(this.parkingSpaceReserved.containsKey(parkingSpaceName)){
            this.parkingSpaceReserved.remove(parkingSpaceName);
        }
    }

    private Float calculateParkingPrice (Integer hours, Float price) {
        Float result = 0F;
        if (hours != null && price != null) {
            if (hours == 0) {
                hours = 1;
            }
            result = hours * price;
        }
        return result;
    }

    @PostConstruct
    public void init(){
        List<Reserve> reservass = reserveRepository.getAll();
        if(reservass != null && reservass.size() > 0){
            for (Reserve reserve: reservass ) {
                System.out.println("Se agrego la reserva a historicos: "+reserve.getId());
                if(reserve.getStateId() != null && ReserveStateType.State.CREATED.getStateId().equals(reserve.getStateId())) {
                    ReservaDto rr = ReservaDto.Factory.getDtoFrom(reserve);
                    reservas.put(rr.getIdPlaza(), rr);
                }else{
                    reservasHistoric.put(reserve.getId(), ReservaDto.Factory.getDtoFrom(reserve));
                }

            }
        }
        List<Parking> parkings = parkingRepository.getAll();
        if(parkings != null && parkings.size() > 0){
            for (int i = 0; i < parkings.size(); i++) {
                Parking parking = parkings.get(i);
                System.out.println("Se agrega el estacionamiento con id " + parking.getId());
                ParkingDto dto = ParkingDto.Factory.getDtoFrom(parking);
                parkingSpacesHistoric.put(dto.getId(), dto);
            }
        }
    }

}
