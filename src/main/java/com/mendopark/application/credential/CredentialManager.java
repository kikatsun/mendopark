package com.mendopark.application.credential;

import com.mendopark.model.Role;
import com.mendopark.model.Session;
import com.mendopark.repository.UserClientAssignamentRepository;
import com.mendopark.session.SessionManager;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.*;

@Singleton
public class CredentialManager {

    @EJB
    private UserClientAssignamentRepository userClientAssignamentRepository;
    @EJB
    private SessionManager sessionManager;

    public static final List<Integer> credencialablesRoles = Arrays.asList( Role.Type.SYSTEM_ADMIN.getRoleId(),
            Role.Type.CLIENT_ADMIN.getRoleId(), Role.Type.OPERATIONAL.getRoleId(), Role.Type.SUPERVISOR.getRoleId()
    );

    /* Map<sessionId, credential> */
    private Map<String, Credential> credentials = new HashMap<String, Credential>();

    public void addCredential(String sessionId){
        Session session = sessionManager.find(sessionId);
        if (session != null && session.getUserSession() != null) {
            Long clientId = userClientAssignamentRepository.findClientIdFrom(session.getUserSession());
            if (clientId != null) {
                credentials.put(sessionId, new Credential(sessionId, session.getUserSession().getId(), clientId));
            }
            // Se loguea que el usuario no tiene cliente asignado
        }
        // Se loguea que el usuario no tiene una sesion activa
    }

    public void addCredential (Session session) {
        if (session.getUserSession() != null) {
            Long clientId = userClientAssignamentRepository.findClientIdFrom(session.getUserSession());
            if (clientId != null) {
                credentials.put(
                    session.getSessionId(),
                    new Credential(session.getSessionId(), session.getUserSession().getId(), clientId));
            }
        }
    }

    public void removeCredential (String sessionId) {
        credentials.remove(sessionId);
    }

    public Long findClientId (String sessionId) {
        Long clientId = null;
        if (credentials.containsKey(sessionId)) {
            Credential credential = credentials.get(sessionId);
            clientId = credential.getClientId();
        }
        return clientId;
    }

    class Credential {
        private String sessionId;
        private String userId;
        private Long clientId;

        public Credential (String sessionId, String userId, Long clientId) {
            this.sessionId = sessionId;
            this.userId = userId;
            this.clientId = clientId;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public Long getClientId() {
            return clientId;
        }

        public void setClientId(Long clientId) {
            this.clientId = clientId;
        }
    }
}
