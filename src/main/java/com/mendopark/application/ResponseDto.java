package com.mendopark.application;

/**
 * Entidad con la estructura genérica para devolver cualquier tipo de respuesta en formato DTO.
 *
 * @attribute message   Una cadena de caracteres que es un mensaje de la respuesta
 * @attribute code      Un entero que representa un código de respuesta interno al servidor.
 * @attribute result    Cualquier objeto que representa en sí la respuesta.
 */
public class ResponseDto {

    private String message;
    private int code;
    private Object result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public static class Factory{

        public static ResponseDto create(String messageE, int code, Object result){
            ResponseDto dto = new ResponseDto();
            dto.setMessage(messageE);
            dto.setCode(code);
            dto.setResult(result);
            return dto;
        }
        public static ResponseDto create(String message){
            ResponseDto dto = new ResponseDto();
            dto.setMessage(message);
            return dto;
        }
        public static ResponseDto create(int code){
            ResponseDto dto = new ResponseDto();
            dto.setCode(code);
            return dto;
        }
        public static ResponseDto create(Object result){
            ResponseDto dto = new ResponseDto();
            dto.setResult(result);
            return dto;
        }
        public static ResponseDto create(String message, Object result){
            ResponseDto dto = new ResponseDto();
            dto.setResult(result);
            dto.setMessage(message);
            return dto;
        }
        public static ResponseDto create(int code, Object result){
            ResponseDto dto = new ResponseDto();
            dto.setCode(code);
            dto.setResult(result);
            return dto;
        }
        public static ResponseDto create(String message, int code){
            ResponseDto dto = new ResponseDto();
            dto.setMessage(message);
            dto.setCode(code);
            return dto;
        }
    }

}
