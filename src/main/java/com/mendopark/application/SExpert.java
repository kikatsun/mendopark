package com.mendopark.application;

import com.mendopark.model.Client;
import com.mendopark.model.Role;

public class SExpert {

    private String sessionId;
    private Client client;
    private Role role;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Client getClient(){
        return client;
    }

    public void setClient(Client client){
        this.client = client;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isSystemAdminUser () {
        boolean result = false;
        if (getRole() != null &&
            getRole().getNumber() != null &&
            getRole().getNumber().equals(Role.Type.SYSTEM_ADMIN.getRoleId())) {
            result = true;
        }
        return result;
    }
}
