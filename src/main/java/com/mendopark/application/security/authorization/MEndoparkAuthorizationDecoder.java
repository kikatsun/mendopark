package com.mendopark.application.security.authorization;

import com.mendopark.model.Session;
import com.mendopark.model.User;
import com.mendopark.model.security.MEndoParkAuthorization;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.util.Base64;

import static com.mendopark.model.security.MEndoParkAuthorization.*;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MEndoparkAuthorizationDecoder {

    public Session getSessionFromAuthorizationHeader(String authorizationHeader) {
        Session session = new Session();
        authorizationHeader = authorizationHeader.trim();
        String authorizationType = authorizationHeader.substring(0, authorizationHeader.indexOf(" "));
        if (authorizationType != null && authorizationType.length() > 0) {
            User user = new User();
            MEndoParkAuthorization mEndoParkAuthorization = null;
            try {
                mEndoParkAuthorization = valueOf(authorizationType.toUpperCase());
            } catch (IllegalArgumentException iaex) {
                /* Log de error */
            }
            switch (mEndoParkAuthorization) {
                case BASIC:
                    String authorizationValue = authorizationHeader.substring(authorizationHeader.indexOf(" "));
                    authorizationValue = new String(Base64.getDecoder().decode(authorizationValue.trim()));
                    String[] basicAuthData = authorizationValue.split(":");
                    if (basicAuthData.length == 2) {
                        user.setName(basicAuthData[0]);
                        user.setPassword(basicAuthData[1]);
                    }
                    break;
                default:
                    break;
            }
            session.setUserSession(user);
        }
        return session;
    }

}
