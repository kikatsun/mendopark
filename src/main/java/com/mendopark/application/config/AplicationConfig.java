package com.mendopark.application.config;

import com.mendopark.rest.commenttype.CommentTypeController;
import com.mendopark.rest.mendopark.MendoParkController;
import com.mendopark.rest.parking.ParkingController;
import com.mendopark.rest.parkingarea.ParkingAreaController;
import com.mendopark.rest.resource.SessionResourceController;
import com.mendopark.rest.sensor.SensorController;
import com.mendopark.rest.sensorstate.SensorStateController;
import com.mendopark.rest.session.MendoParkSessionController;
import com.mendopark.rest.sessionDecorator.*;
import com.mendopark.rest.user.UserController;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ApplicationPath("/rest")
public class AplicationConfig extends Application {

    /**
     * Nota: Una vez agregado el Decorador de la clase, sacar el controlador
     * de la clase extendida
     */

    public static final List<Class<?>> resources = Arrays.asList(
            //PayTypeController.class,
            PayTypeControllerSessionDecorator.class,
            //DayController.class,
            DayControllerSessionDecorator.class,
            CommentTypeController.class,
            //ParkingSpaceStateTypeController.class,
            ParkingSpaceStateTypeControllerSessionDecorator.class,
            //ParkingSpaceTypeController.class,
            ParkingSpaceTypeControllerSessionDecorator.class,
            ProvinceControllerSessionDecorator.class,
            //ReserveStateTypeController.class,
            RoleControllerSessionDecorator.class,
            //VehicleTypeController.class,
            VehicleTypeControllerSessionDecorator.class,
            MendoParkSessionController.class,
            UserController.class,
            SensorStateController.class,
            LocalityControllerSessionDecorator.class,
            ParkingAreaController.class,
            SessionResourceController.class,
            ParkingController.class,
            MendoParkController.class,
            ReserveStateTypeControllerSessionDecorator.class,
            ClientControllerSessionDecorator.class,
            SensorController.class,
            //ReportCantVehiculosPorZonasController.class,
            VehiclesByZoneReportsControllerSessionDecorator.class,
            PaymentTypesReportsControllerDecorator.class,
            //BackUpController.class,
            BackUpControllerSessionDecorator.class,
            ResourceControllerSessionDecorator.class,
            UnpaymentVehiclesReportsControllerDecorator.class,
            UnfinishParkingsReportsControllerDecorator.class
    );

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(resources);
    }
}
