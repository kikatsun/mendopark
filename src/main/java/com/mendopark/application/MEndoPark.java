package com.mendopark.application;

import com.mendopark.model.*;
import com.mendopark.repository.*;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Singleton
public class MEndoPark {

    @EJB
    private ClientRepository clientRepository;
    @EJB
    private PersonRepository personRepository;
    @EJB
    private AddressRepository addressRepository;
    @EJB
    private ProvinceRepository provinceRepository;
    @EJB
    private LocalityReporsitory localityReporsitory;
    @EJB
    private ClientTypeRepository clientTypeRepository;
    @EJB
    private DocumentTypeRepository documentTypeRepository;
    @EJB
    private UserRepository userRepository;
    @EJB
    private RoleRepository roleRepository;

    public void startMEndoPark(String key) {
        if (key != null && key.equals("krapodnem2019")) {
            Province mendoza = new Province();
            mendoza.setNumber(1);
            mendoza.setName("Mendoza");
            provinceRepository.persist(mendoza);

            Locality godoyCruz = new Locality();
            godoyCruz.setNumber(1);
            godoyCruz.setName("Godoy Cruz");
            godoyCruz.setFromProvince(mendoza);
            localityReporsitory.persist(godoyCruz);

            ClientType publicClientType = new ClientType();
            publicClientType.setNumber(1);
            publicClientType.setName("Público");
            publicClientType.setDescription("Es un cliente con plazas de estacionamiento de carácter público");
            publicClientType.setStartDate(new Date());
            clientTypeRepository.persist(publicClientType);

            ClientType privateClientType = new ClientType();
            privateClientType.setNumber(2);
            privateClientType.setName("Privado");
            privateClientType.setDescription("Es un cliente con plazas de estacionamiento de carácter privado");
            privateClientType.setStartDate(new Date());
            clientTypeRepository.persist(privateClientType);

            Address mendoparkAddress = new Address();
            mendoparkAddress.setFromLocality(godoyCruz);
            mendoparkAddress.setStreetNumber(1448);
            mendoparkAddress.setStreetName("Reconquista");
            addressRepository.persist(mendoparkAddress);

            Client mendoPark = new Client();
            mendoPark.setNumber(1L);
            mendoPark.setCompanyName("MendoPark");
            mendoPark.setAddress(mendoparkAddress);
            clientRepository.persist(mendoPark);

            DocumentType dni = new DocumentType();
            dni.setNumber(1L);
            dni.setName("DNI");
            dni.setDescription("Documento de identidad Nacional de Argentina");
            documentTypeRepository.persist(dni);

            Person erick = new Person();
            erick.setAddress(mendoparkAddress);
            erick.setDocumentType(dni);
            erick.setEmail("erick.mza.2014@gmail.com");
            erick.setDocumentNumber("94190053");
            erick.setLastName("Mendoza");
            erick.setName("Erick");
            erick.setPhoneNumber("+5492616715769");
            personRepository.persist(erick);

            List<Role> roles = new ArrayList<Role>();

            Role admSis = new Role();
            admSis.setNumber(1);
            admSis.setName("Administrador del Sistema");
            admSis.setDescription("Administra todo el sistema");
            roles.add(admSis);

            Role adClient = new Role();
            adClient.setNumber(2);
            adClient.setName("Administrador del Cliente");
            adClient.setDescription("Administra todas las configuraciones del cliente");
            roles.add(adClient);

            Role commonUser = new Role();
            commonUser.setNumber(3);
            commonUser.setName("Usuario Común");
            commonUser.setDescription("Usuario final que realiza reserva de plazas de estacionamiento");
            roles.add(commonUser);

            Role encargado = new Role();
            encargado.setNumber(4);
            encargado.setName("Encargado");
            encargado.setDescription("Se encarga de controlar reportes y estadísticas");
            roles.add(encargado);

            Role operativeUser = new Role();
            operativeUser.setNumber(5);
            operativeUser.setName("Usuario Operativo");
            operativeUser.setDescription("Se encarga de controlar los estados de las plazas de estacionamiento");
            roles.add(operativeUser);

            for (Role role : roles) {
                roleRepository.persist(role);
            }

            User erickRoot = new User();
            erickRoot.setName("root");
            erickRoot.setPassword("mendopark");
            erickRoot.setFromPerson(erick);
            erickRoot.setEmail(erick.getEmail());
            erickRoot.setUserRole(admSis);
            erickRoot.setFromClient(mendoPark);
            userRepository.persist(erickRoot);

//
//            User erickClient = new User();
//            erickClient.setName("erick");
//            erickClient.setPassword("erick");
//            erickClient.setFromPerson(erick);
//            erickClient.setEmail(erick.getEmail());
//            erickClient.setUserRole(adClient);
//            userRepository.persist(erickClient);

//            mendoPark.addUser(erickClient);
//            clientRepository.persist(mendoPark);
        }
    }

    public void addParkingArea() {

        ParkingArea utnParkingArea = new ParkingArea();
    }

    public void addClient(){
        Locality ciudad = new Locality();
        ciudad.setNumber(2);
        ciudad.setName("Capital");
        ciudad.setFromProvince(provinceRepository.get(1));
        localityReporsitory.persist(ciudad);

        Address newAddress = new Address();
        newAddress.setFromLocality(ciudad);
        newAddress.setStreetNumber(273);
        newAddress.setStreetName("Coronel Rodríguez");
        addressRepository.persist(newAddress);

        Client utn = new Client();
        utn.setNumber(2L);
        utn.setCompanyName("UTN");
        utn.setAddress(newAddress);
        utn.setClientType(clientTypeRepository.get(2));
        clientRepository.persist(utn);
    }

    public void addUser(){

        Address newAddress = new Address();
        newAddress.setFromLocality(localityReporsitory.get(2));
        newAddress.setStreetNumber(273);
        newAddress.setStreetName("Coronel Rodríguez");
        addressRepository.persist(newAddress);

        Person utnAdminP = new Person();
        utnAdminP.setAddress(newAddress);
        utnAdminP.setDocumentType(documentTypeRepository.get(1L));
        utnAdminP.setEmail("erick.mza.2014@gmail.com");
        utnAdminP.setDocumentNumber("11222333");
        utnAdminP.setLastName("UTN");
        utnAdminP.setName("Administrador");
        utnAdminP.setPhoneNumber("+542615244500");
        personRepository.persist(utnAdminP);

        Client utn = clientRepository.get(2L);

        User utnAdmin = new User();
        utnAdmin.setName("utnAdmin");
        utnAdmin.setPassword("utnAdmin");
        utnAdmin.setFromPerson(utnAdminP);
        utnAdmin.setEmail(utnAdminP.getEmail());
        utnAdmin.setUserRole(roleRepository.get(2));
        utnAdmin.setFromClient(utn);
        userRepository.persist(utnAdmin);

        User utnEncargado = new User();
        utnEncargado.setName("utnEncargado");
        utnEncargado.setPassword("utnEncargado");
        utnEncargado.setFromPerson(utnAdminP);
        utnEncargado.setEmail(utnAdminP.getEmail());
        utnEncargado.setUserRole(roleRepository.get(4));
        utnEncargado.setFromClient(utn);
        userRepository.persist(utnEncargado);


        User utnOperativo = new User();
        utnOperativo.setName("utnOperativo");
        utnOperativo.setPassword("utnOperativo");
        utnOperativo.setFromPerson(utnAdminP);
        utnOperativo.setEmail(utnAdminP.getEmail());
        utnOperativo.setUserRole(roleRepository.get(5));
        utnOperativo.setFromClient(utn);
        userRepository.persist(utnOperativo);

    }
}
