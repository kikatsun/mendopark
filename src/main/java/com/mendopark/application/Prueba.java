package com.mendopark.application;


import com.mendopark.model.CoordinatePoint;
import com.mendopark.model.SensorState;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.Math.*;

public class Prueba {

    public static void main(String[] args){
//        List<CoordinatePoint> points = new ArrayList<CoordinatePoint>();
//        points.add(new CoordinatePoint(-32.8959722868D, -68.853502705D ));
//        points.add(new CoordinatePoint(-32.896173828D, -68.852051265D ));
//        points.add(new CoordinatePoint(-32.8962819304D, -68.851943977D ));
//        points.add(new CoordinatePoint(-32.896790910D,-68.852035172D ));
//        points.add(new CoordinatePoint(-32.896489126D, -68.853596217D ));

//        List<CoordinatePoint> points = new ArrayList<CoordinatePoint>();
//        points.add(new CoordinatePoint(3D, 1D ));
//        points.add(new CoordinatePoint(15D, 11D ));
//        points.add(new CoordinatePoint(9D,11D ));
//        points.add(new CoordinatePoint(3D, 7D ));
//
//        CoordinatePoint targetPoint = new CoordinatePoint(40D, 5D);
//        Double sumaTotal = 0D;
//        for (int i = 0; i < points.size(); i++) {
//            if(i == (points.size()-1)){
//                sumaTotal = sumaTotal + getGrades(targetPoint, points.get(i), points.get(0));
//            }else {
//                sumaTotal = sumaTotal + getGrades(targetPoint, points.get(i), points.get(i + 1));
//            }
//        }
//        System.out.println("Suma total: "+sumaTotal);

//        long uno = 126596l;
//        long dos = 132645l;
//
//        System.out.println( (dos-uno) / 60);
        char a = '\\';
//        System.out.println(Character.isAlphabetic(a));

        String aa = "ola56ty";

        System.out.println(aa.toUpperCase());

    }

    public static Double getGrades(CoordinatePoint point, CoordinatePoint v1, CoordinatePoint v2){
        Double result = null;
        Double x1 = v1.getLatitude() - point.getLatitude();
        Double y1 = v1.getLongitude() - point.getLongitude();

        Double x2 = v2.getLatitude() - point.getLatitude();
        Double y2 = v2.getLongitude() - point.getLongitude();

        Double scalarOperation = (x1*x2) + (y1*y2);

        Double module1 = sqrt(Math.pow(x1,2) + pow(y1,2));
        Double module2 = sqrt(Math.pow(x2,2) + pow(y2,2));

        Double calc1 = scalarOperation/(module1*module2);
//        if(calc1 < 0){
//            calc1 = calc1 * -1D;
//        }
        result = Math.toDegrees(acos(calc1));
        return result;
    }

    private static Double getToSum(Double x, Double y){
        Double toSum = 0D;

        if(x < 0 && y >0){
            toSum = 180D;
        }else if(x<0 && y<0){
            toSum = 180D;
        }else if(x>0 && y<0){
            toSum = 360D;
        }
        return toSum;
    }
    private static Double calculateGrades(Double x, Double y){
        Double result = null;
        if(x.equals(0D)){
            if(y.equals(0D)){
                result = 0D;
            }else if(y > 0){
                result = 90D;
            }else if(y<0){
                result = 270D;
            }
        }else if (y.equals(0D)){
            if(x.equals(0D)){
                result = 0D;
            }else if(x > 0){
                result = 0D;
            }else if(x < 0){
                result = 180D;
            }
        }else{
            result = Math.toDegrees(atan(y/x));
            result = result + getToSum(x,y);
        }
        return result;
    }


}
