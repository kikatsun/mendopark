package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity(name = "address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String streetName;

    private Integer streetNumber;

    private Integer departmentNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private Locality fromLocality;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Integer getDepartmentNumber() {
        return departmentNumber;
    }

    public void setDepartmentNumber(Integer departmentNumber) {
        this.departmentNumber = departmentNumber;
    }

    public Locality getFromLocality() {
        return fromLocality;
    }

    public void setFromLocality(Locality fromLocality) {
        this.fromLocality = fromLocality;
    }
}
