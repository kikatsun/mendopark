package com.mendopark.model;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "reserve")
public class Reserve {

    @Id
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingArea parkingArea;

    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingSpace parkingSpace;
    private Float priceByHour;
    private Float anticipatedAmount;
    private String plate;
    private String observations;
    private Float finalPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    private PayType payType;

    private Integer stateId;

    private Date creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ParkingArea getParkingArea() {
        return parkingArea;
    }

    public void setParkingArea(ParkingArea parkingArea) {
        this.parkingArea = parkingArea;
    }

    public ParkingSpace getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(ParkingSpace parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public Float getPriceByHour() {
        return priceByHour;
    }

    public void setPriceByHour(Float priceByHour) {
        this.priceByHour = priceByHour;
    }

    public Float getAnticipatedAmount() {
        return anticipatedAmount;
    }

    public void setAnticipatedAmount(Float anticipatedAmount) {
        this.anticipatedAmount = anticipatedAmount;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Float getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Float finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }
}
