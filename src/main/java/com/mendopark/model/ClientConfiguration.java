package com.mendopark.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity(name = "clientconfiguration")
public class ClientConfiguration {

    @Id
    private String id;
    private String companyName;
    private Float parkingSpacePrice;
    private Long manualParkingSpaceLockTime;
    private Long reservationTimeout;
    private Date endDate;
    private String[] weeklySchedulesWork;
    @OneToOne(fetch = FetchType.LAZY)
    private Client fromClient;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Float getParkingSpacePrice() {
        return parkingSpacePrice;
    }

    public void setParkingSpacePrice(Float parkingSpacePrice) {
        this.parkingSpacePrice = parkingSpacePrice;
    }

    public Long getManualParkingSpaceLockTime() {
        return manualParkingSpaceLockTime;
    }

    public void setManualParkingSpaceLockTime(Long manualParkingSpaceLockTime) {
        this.manualParkingSpaceLockTime = manualParkingSpaceLockTime;
    }

    public Long getReservationTimeout() {
        return reservationTimeout;
    }

    public void setReservationTimeout(Long reservationTimeout) {
        this.reservationTimeout = reservationTimeout;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Client getFromClient() {
        return fromClient;
    }

    public void setFromClient(Client fromClient) {
        this.fromClient = fromClient;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String[] getWeeklySchedulesWork() {
        return weeklySchedulesWork;
    }

    public void setWeeklySchedulesWork(String[] weeklySchedulesWork) {
        this.weeklySchedulesWork = weeklySchedulesWork;
    }
}
