package com.mendopark.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "person")
@IdClass(PersonPK.class)
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String documentNumber;

    private String name;

    private String lastName;

    private String phoneNumber;

    private String email;

    private Date bithdate;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    private DocumentType documentType;

    @ManyToOne(fetch = FetchType.EAGER)
    private Address address;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBithdate() {
        return bithdate;
    }

    public void setBithdate(Date bithdate) {
        this.bithdate = bithdate;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
