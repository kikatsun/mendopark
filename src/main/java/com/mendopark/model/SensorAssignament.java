package com.mendopark.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity(name = "sensorassignament")
public class SensorAssignament {

    @Id
    private String id;

    private Date assignamentDate;

    private Date endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private Client fromClient;

    @ManyToOne(fetch = FetchType.EAGER)
    private Sensor fromSensor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getAssignamentDate() {
        return assignamentDate;
    }

    public void setAssignamentDate(Date assignamentDate) {
        this.assignamentDate = assignamentDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Client getFromClient() {
        return fromClient;
    }

    public void setFromClient(Client fromClient) {
        this.fromClient = fromClient;
    }

    public Sensor getFromSensor() {
        return fromSensor;
    }

    public void setFromSensor(Sensor fromSensor) {
        this.fromSensor = fromSensor;
    }
}
