
package com.mendopark.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "reservestatetype")
public class ReserveStateType implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum State{
		CREATED(100),
		CANCELED(101),
		STARTED(102);

		private Integer stateId;

		private State(Integer stateId){
			this.stateId = stateId;
		}
		public Integer getStateId(){
			return this.stateId;
		}
	}

	@Id
	private Integer number;

	private String name;

	private String description;

	private Date endDate;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
