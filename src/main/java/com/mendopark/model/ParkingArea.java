package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name = "parkingarea")
public class ParkingArea implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long number;

    private String name;

    private String color;

    @OneToOne(fetch = FetchType.EAGER)
    private CoordinatePoint centerPoint;

    private Date startDate;

    private Date endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private ParkingSpaceType type;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ParkingSpace> parkingSpaces = new ArrayList<ParkingSpace>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "fromParkingArea")
    private List<ParkingAreaDetail> parkingAreaDetails;

    @ManyToOne(fetch = FetchType.LAZY)
    private Client fromClient;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ParkingSpaceType getType() {
        return type;
    }

    public void setType(ParkingSpaceType type) {
        this.type = type;
    }

    public List<ParkingSpace> getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(List<ParkingSpace> parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public List<ParkingAreaDetail> getParkingAreaDetails() {
        return parkingAreaDetails;
    }

    public void setParkingAreaDetails(List<ParkingAreaDetail> parkingAreaDetails) {
        this.parkingAreaDetails = parkingAreaDetails;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Client getFromClient() {
        return fromClient;
    }

    public void setFromClient(Client fromClient) {
        this.fromClient = fromClient;
    }

    public CoordinatePoint getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(CoordinatePoint centerPoint) {
        this.centerPoint = centerPoint;
    }

    @Override
    public String toString() {
        return "ParkingArea{" +
                "number=" + number +
                ", name='" + name + '\'' +
                '}';
    }
}
