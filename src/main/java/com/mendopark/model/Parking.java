package com.mendopark.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity(name="parking")
public class Parking {

    @Id
    private String id;

    private Date start;
    private Date end;
    private Integer hours;
    private Float price;
    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingArea parkingArea;
    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingSpace parkingSpace;
    @ManyToOne(fetch = FetchType.LAZY)
    private Reserve reserve;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public ParkingArea getParkingArea() {
        return parkingArea;
    }

    public void setParkingArea(ParkingArea parkingArea) {
        this.parkingArea = parkingArea;
    }

    public ParkingSpace getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(ParkingSpace parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public Reserve getReserve() {
        return reserve;
    }

    public void setReserve(Reserve reserve) {
        this.reserve = reserve;
    }

    @Override
    public String toString() {
        String result =  "Parking{" +
                "id='" + id + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", hours=" + hours +
                ", price=" + price +
                ", parkingArea=";
        if (parkingArea != null) {
            result = result + parkingArea.toString();
        } else {
            result = result + "null";
        }
        result = result + " " +
                ", parkingSpace=";
        if (parkingSpace != null) {
            result = result +parkingSpace.toString();
        } else {
            result = result + "null";
        }
        result = result + " " +
                ", reserve=" + reserve +
                '}';

        return result;
    }
}
