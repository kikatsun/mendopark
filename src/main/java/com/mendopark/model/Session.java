package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity(name = "session")
public class Session implements Serializable, Comparable<Session>{

    private static final long serialVersionUID = 1L;

    @Id
    private String sessionId;
    private Date creationDate;
    private Date endDate;
    private Date lastAccessedDate;
    private User userSession;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getUserSession() {
        return userSession;
    }

    public void setUserSession(User userSession) {
        this.userSession = userSession;
    }

    public Date getLastAccessedDate() {
        return lastAccessedDate;
    }

    public void setLastAccessedDate(Date lastAccessedDate) {
        this.lastAccessedDate = lastAccessedDate;
    }

    @Override
    public int compareTo(Session session) {
        return (int)(this.lastAccessedDate.getTime() - session.getLastAccessedDate().getTime());
    }
}
