
package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "day")
public class Day implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Integer number;
	private String name;
	private String schedule; //horario de atencion.

	private Date endDate;

	@ManyToOne(fetch = FetchType.LAZY)
	private Client fromClient;


	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Client getFromClient() {
		return fromClient;
	}

	public void setFromClient(Client fromClient) {
		this.fromClient = fromClient;
	}
}
