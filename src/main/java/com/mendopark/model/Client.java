package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final Long MENDOPARK_ID = 1L;

    @Id
    private Long number;

    private String companyName;

    private String cuit;

    private String phoneNumber;

    private Date endDate;

    private Date startDate;

    private String bankData;

    @ManyToOne(fetch = FetchType.LAZY)
    private ClientType clientType;

    @OneToOne(fetch = FetchType.LAZY)
    private Address address;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Tariff> tariffs = new ArrayList<Tariff>();

    @ManyToMany(fetch = FetchType.LAZY)
    private List<AnticipatedAmount> anticipatedAmount = new ArrayList<AnticipatedAmount>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fromClient")
    private List<WeeklySchedulesWork> weeklySchedulesWork = new ArrayList<WeeklySchedulesWork>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fromClient")
    private List<PayTypeRegistration> payTypeRegistration = new ArrayList<PayTypeRegistration>();

    public String getCompanyName() {
        return companyName;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getBankData() {
        return bankData;
    }

    public void setBankData(String bankData) {
        this.bankData = bankData;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    public List<AnticipatedAmount> getAnticipatedAmount() {
        return anticipatedAmount;
    }

    public void setAnticipatedAmount(List<AnticipatedAmount> anticipatedAmount) {
        this.anticipatedAmount = anticipatedAmount;
    }

    public List<WeeklySchedulesWork> getWeeklySchedulesWork() {
        return weeklySchedulesWork;
    }

    public void setWeeklySchedulesWork(List<WeeklySchedulesWork> weeklySchedulesWork) {
        this.weeklySchedulesWork = weeklySchedulesWork;
    }

    public List<PayTypeRegistration> getPayTypeRegistration() {
        return payTypeRegistration;
    }

    public void setPayTypeRegistration(List<PayTypeRegistration> payTypeRegistration) {
        this.payTypeRegistration = payTypeRegistration;
    }

    @Override
    public String toString() {
        return "Cliente #"+number+ " "+companyName;
    }
}
