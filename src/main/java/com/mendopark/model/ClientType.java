package com.mendopark.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "clienttype")
public class ClientType implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum Type{
        PUBLIC(1),
        PRIVATE(2);

        private Integer clientTypeNumber;

        private Type(Integer clientTypeNumber){
            this.clientTypeNumber = clientTypeNumber;
        }
        public Integer getClientTypeNumber(){
            return this.clientTypeNumber;
        }
    }

    @Id
    private Integer number;

    private String name;

    private String description;

    private Date endDate;

    private Date startDate;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
