package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity(name = "parkingspace")
public class ParkingSpace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ParkingSpaceType parkingSpaceType;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "fromParkingSpace")
    private List<ParkingSpaceStateRegistration> states = new ArrayList<ParkingSpaceStateRegistration>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "fromParkingSpace")
    private List<ParkingSpaceRegistration> parkingSpaceRegistrations = new ArrayList<ParkingSpaceRegistration>();

    @OneToOne(fetch = FetchType.EAGER)
    private CoordinatePoint coordinatePoint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParkingSpaceType getParkingSpaceType() {
        return parkingSpaceType;
    }

    public void setParkingSpaceType(ParkingSpaceType parkingSpaceType) {
        this.parkingSpaceType = parkingSpaceType;
    }

    public List<ParkingSpaceStateRegistration> getStates() {
        return states;
    }

    public void setStates(List<ParkingSpaceStateRegistration> states) {
        this.states = states;
    }

    public List<ParkingSpaceRegistration> getParkingSpaceRegistrations() {
        return parkingSpaceRegistrations;
    }

    public void setParkingSpaceRegistrations(List<ParkingSpaceRegistration> parkingSpaceRegistrations) {
        this.parkingSpaceRegistrations = parkingSpaceRegistrations;
    }

    public CoordinatePoint getCoordinatePoint() {
        return coordinatePoint;
    }

    public void setCoordinatePoint(CoordinatePoint coordinatePoint) {
        this.coordinatePoint = coordinatePoint;
    }

    @Override
    public String toString() {
        return "ParkingSpace{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
