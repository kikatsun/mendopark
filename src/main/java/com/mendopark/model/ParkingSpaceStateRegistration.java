package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity(name = "parkingspacestateregistration")
public class ParkingSpaceStateRegistration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private Date startDate;

    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingSpaceStateType parkingSpaceStateType;

    @ManyToOne(fetch = FetchType.EAGER)
    private ParkingSpace fromParkingSpace;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ParkingSpaceStateType getParkingSpaceStateType() {
        return parkingSpaceStateType;
    }

    public void setParkingSpaceStateType(ParkingSpaceStateType parkingSpaceStateType) {
        this.parkingSpaceStateType = parkingSpaceStateType;
    }
}
