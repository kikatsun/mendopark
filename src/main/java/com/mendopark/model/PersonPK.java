package com.mendopark.model;

import java.io.Serializable;

public class PersonPK implements Serializable {
    private static final long serialVersionUID = 1L;

    private String documentNumber;
    private DocumentType documentType;

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }
}
