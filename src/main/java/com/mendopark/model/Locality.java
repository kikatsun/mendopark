package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "locality")
public class Locality implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Integer number;

    private String name;

    private Date endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private Province fromProvince;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Province getFromProvince() {
        return fromProvince;
    }

    public void setFromProvince(Province fromProvince) {
        this.fromProvince = fromProvince;
    }
}
