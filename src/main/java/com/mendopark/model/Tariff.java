package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tariff")
public class Tariff implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long number;

    private String name;

    private Float price;

    private Date startDate;

    private Date endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private TimeUnit timeUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    private VehicleType vehicleType;

    @ManyToOne(fetch = FetchType.EAGER)
    private Day day;

    @ManyToOne(fetch = FetchType.LAZY)
    private Client fromClient;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Client getFromClient() {
        return fromClient;
    }

    public void setFromClient(Client fromClient) {
        this.fromClient = fromClient;
    }
}
