package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity(name = "parkingareadetail")
public class ParkingAreaDetail implements Serializable {

    private static final long serialVersionUID = 1L;
//1095337677
    @Id
    private String id;

    private Integer sequence;

    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingArea fromParkingArea;

    @OneToOne(fetch = FetchType.EAGER)
    private CoordinatePoint coordinatePoint;

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public ParkingArea getFromParkingArea() {
        return fromParkingArea;
    }

    public void setFromParkingArea(ParkingArea fromParkingArea) {
        this.fromParkingArea = fromParkingArea;
    }

    public CoordinatePoint getCoordinatePoint() {
        return coordinatePoint;
    }

    public void setCoordinatePoint(CoordinatePoint coordinatePoint) {
        this.coordinatePoint = coordinatePoint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
