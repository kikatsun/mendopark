
package com.mendopark.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "role")
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum Type{
		SYSTEM_ADMIN(1),
		CLIENT_ADMIN(2),
		COMMON(3),
		SUPERVISOR(4),
		OPERATIONAL(5);

		private Integer roleId;

		private Type(Integer roleId){
			this.roleId = roleId;
		}
		public Integer getRoleId(){
			return this.roleId;
		}
	}

	@Id
	private Integer number;

	private String name;

	private String description;

	private Date endDate;

	/*rango de vigencia*/
	private Date vigenciaDesde;
	private Date vigenciaHasta;

	public Role(){}

	public Role(Integer number){
		this.number = number;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getVigenciaDesde() {
		return vigenciaDesde;
	}

	public void setVigenciaDesde(Date vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}

	public Date getVigenciaHasta() {
		return vigenciaHasta;
	}

	public void setVigenciaHasta(Date vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}

	@Override
	public String toString() {
		return "Role{" +
				"number=" + number +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", endDate=" + endDate +
				", vigenciaDesde=" + vigenciaDesde +
				", vigenciaHasta=" + vigenciaHasta +
				'}';
	}
}
