package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/*
* ToDo Está mal el nombre SensorState ya que este representa el estado de la plaza de estacionamiento
* */
@Entity(name = "sensorstate")
public class SensorState implements Serializable {

    @Id
    private String id;

    private String name;

    private String state;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public enum State{
        FREE("LIBRE"),
        BUSSY("OCUPADO"),
        RESERVED("RESERVADO"),
        CREATED("CREADO"),
        ASSIGNED("ASIGNADO");

        private String name;

        private State(String name){
            this.name = name;
        }

        public String getName(){
            return this.name;
        }
    }

}
