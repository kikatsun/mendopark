package com.mendopark.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "parkingspaceregistration")
public class ParkingSpaceRegistration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private Date startDate;

    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private ParkingSpace fromParkingSpace;

    @ManyToOne(fetch = FetchType.EAGER)
    private Sensor fromSensor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ParkingSpace getFromParkingSpace() {
        return fromParkingSpace;
    }

    public void setFromParkingSpace(ParkingSpace fromParkingSpace) {
        this.fromParkingSpace = fromParkingSpace;
    }

    public Sensor getFromSensor() {
        return fromSensor;
    }

    public void setFromSensor(Sensor fromSensor) {
        this.fromSensor = fromSensor;
    }
}
