package com.mendopark.model.security;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "granttype")
public class GrantType {

    public enum Type{
        CREATE(1),
        READ(2),
        UPDATE(3),
        DELETE(4);

        private Integer id;

        Type(Integer id){
            this.id = id;
        }
        public Integer getId(){
            return this.id;
        }

        public static Type getTypeById(Integer id){
            switch (id){
                case 1 : return CREATE;
                case 2 : return READ;
                case 3 : return UPDATE;
                case 4 : return DELETE;
                default: return null;
            }
        }
    }

    @Id
    private Integer number;
    private String name;
    private String shortName;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
