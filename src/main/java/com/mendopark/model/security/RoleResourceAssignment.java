package com.mendopark.model.security;

import com.mendopark.model.Role;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "roleresourceassignment")
public class RoleResourceAssignment {

    @Id
    private String id;

    private Date startDate;

    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Role fromRole;

    @ManyToOne(fetch = FetchType.LAZY)
    private Resource toResource;

    @ManyToOne(fetch = FetchType.LAZY)
    private GrantType grantType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Role getFromRole() {
        return fromRole;
    }

    public void setFromRole(Role fromRole) {
        this.fromRole = fromRole;
    }

    public Resource getToResource() {
        return toResource;
    }

    public void setToResource(Resource toResource) {
        this.toResource = toResource;
    }

    public GrantType getGrantType() {
        return grantType;
    }

    public void setGrantType(GrantType grantType) {
        this.grantType = grantType;
    }

    @Override
    public String toString() {
        return "RoleResourceAssignment: " +
                "id='" + id + '\'' +
                ", fromRole=" + fromRole +
                ", toResource=" + toResource +
                '}';
    }
}
