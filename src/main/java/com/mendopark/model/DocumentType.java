package com.mendopark.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.print.Doc;
import java.io.Serializable;
import java.util.Date;

@Entity(name="documenttype")
public class DocumentType implements Serializable {

    public enum Type{
        DNI(1L),
        OTHER(2L);

        private Long roleId;

        private Type(Long roleId){
            this.roleId = roleId;
        }
        public Long getDocumentTypeId(){
            return this.roleId;
        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    private Long number;

    private String name;

    private String description;

    private Date endDate;

    public DocumentType(){}

    public DocumentType(Long number){
        this.number = number;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
