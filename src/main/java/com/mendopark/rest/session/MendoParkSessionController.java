package com.mendopark.rest.session;

import com.mendopark.application.ResponseDto;
import com.mendopark.application.security.authorization.MEndoparkAuthorizationDecoder;
import com.mendopark.session.SessionExpert;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.InvalidParameterException;

/**
 * Controlador de Sesión MendoPark
 * Controlador que define la api para el recurso Sesión.
 *
 * @Pattern: Controller
 */
@Stateless
@LocalBean
@Path("/session")
public class MendoParkSessionController {

    @EJB
    SessionExpert sessionExpert;
    @EJB
    MEndoparkAuthorizationDecoder mEndoparkAuthorizationDecoder;

    /**
     * Crea una nueva sesión a partir de la información recibida en el parámetro.
     * @param dto
     * @return
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/")
    public Response createSession(MendoParkSessionDto dto){
        try {
            return Response.ok().entity(
                MendoParkSessionDto.Factory.getDtoFrom(
                    sessionExpert.createSession(MendoParkSessionDto.Factory.getSessionFrom(dto))
                )
            ).build();
        }catch (Exception ex) {
            if(ex.getCause() instanceof InvalidParameterException){
                return Response.status(401).entity(ResponseDto.Factory.create(ex.getMessage())).build();
            }else{
                return Response.status(500).entity(ResponseDto.Factory.create(ex.getMessage())).build();
            }

        }
    }

    /**
     * Crea una nueva sesión a partir de la información recibida en el parámetro.
     * @param authorizationHeader
     * @return
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/")
    public Response createSession(@HeaderParam("Authorization") String authorizationHeader){
        try {
            return Response.ok().entity(
                    MendoParkSessionDto.Factory.getDtoFrom(
                            sessionExpert.createSession(mEndoparkAuthorizationDecoder.getSessionFromAuthorizationHeader(authorizationHeader))
                    )
            ).build();
        }catch (Exception ex) {
            if(ex.getCause() instanceof InvalidParameterException){
                return Response.status(401).entity(ResponseDto.Factory.create(ex.getMessage())).build();
            }else{
                return Response.status(500).entity(ResponseDto.Factory.create(ex.getMessage())).build();
            }

        }
    }

    /**
     * Devuelve todas las sesiones activas
     * @return
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/sessions")
    public Response findSessions(){
        try {
            return Response.ok().entity(
                    MendoParkSessionDto.Factory.getDtosFrom(
                            sessionExpert.findSessions()
                    )
            ).build();
        }catch (Exception ex) {
            if(ex.getCause() instanceof InvalidParameterException){
                return Response.status(401).entity(ResponseDto.Factory.create(ex.getMessage())).build();
            }else{
                return Response.status(500).entity(ResponseDto.Factory.create(ex.getMessage())).build();
            }

        }
    }
}
