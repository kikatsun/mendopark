package com.mendopark.rest.session;

import com.mendopark.model.Session;
import com.mendopark.model.User;

import java.util.*;

/**
 * DTO de Sesión de MendoPark
 * Datos requeridos para crear una sesión.
 *
 * @Pattern Data Object Transfer
 */
public class MendoParkSessionDto {

    private String userName;
    private String password;
    private String sessionId;
    private int role;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    /**
     * Fábrica para convertir, crear el DTO con la entidad {@link Session}.
     *
     * @Pattern: Factory
     */
    public static class Factory{

        public static MendoParkSessionDto getDtoFrom(Session session){
            MendoParkSessionDto dto = new MendoParkSessionDto();
            dto.setUserName(session.getUserSession().getName());
            dto.setSessionId(session.getSessionId());
            if(session.getUserSession() != null && session.getUserSession().getUserRole() != null){
                dto.setRole(session.getUserSession().getUserRole().getNumber().intValue());
            }
            return dto;
        }

        public static List<MendoParkSessionDto> getDtosFrom(List<Session> sessions){
            List<MendoParkSessionDto> result = new ArrayList<>();
            if (sessions != null && sessions.size() > 0){
                for (Session session: sessions) {
                    result.add(getDtoFrom(session));
                }
            }
            return result;
        }

        public static Session getSessionFrom(MendoParkSessionDto dto){
            Session session = new Session();
            User user = new User();
            user.setName(dto.getUserName());
            user.setPassword(dto.getPassword());
            session.setUserSession(user);
            return session;
        }

        public static Session createSession(Session session){

            if (session.getUserSession() != null && session.getUserSession().getName() != null &&
                !session.getUserSession().getName().isEmpty() && session.getUserSession().getPassword() != null &&
                !session.getUserSession().getPassword().isEmpty()){
                session.setSessionId(UUID.randomUUID().toString());
                session.setCreationDate(new Date());
                session.setLastAccessedDate(session.getCreationDate());
            }else{
                session = null;
            }

            return session;
        }
    }

}
