
package com.mendopark.rest.reservestatetype;

import com.mendopark.model.ReserveStateType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO de Tipo Estado de Reserva
 * Datos requeridos para crear un Tipo de Estado de Reserva.
 *
 * @Pattern Data Object Transfer
 */

public class ReserveStateTypeDto {

	private Integer number;

	private String name;

	private String description;

	private String endDate;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	/**
	 * Fábrica para convertir, crear el DTO con la entidad {@link ReserveStateType}.
	 *
	 * @Pattern: Factory
	 */
	public static class Factory {

		public static ReserveStateTypeDto getDtoFromReserveStateType(ReserveStateType reserveStateType) {
			ReserveStateTypeDto result = new ReserveStateTypeDto();

			result.setNumber(reserveStateType.getNumber());

			result.setName(reserveStateType.getName());

			result.setDescription(reserveStateType.getDescription());

			if (reserveStateType.getEndDate() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				result.setEndDate(sdf.format(reserveStateType.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			return result;
		}

		public static List<ReserveStateTypeDto> getDtosFromReserveStateTypes(List<ReserveStateType> reserveStateTypes) {
			List<ReserveStateTypeDto> result = new ArrayList<ReserveStateTypeDto>();

			for (ReserveStateType reserveStateType : reserveStateTypes) {
				result.add(getDtoFromReserveStateType(reserveStateType));
			}

			return result;
		}

		public static ReserveStateType getReserveStateTypeFromDto(ReserveStateTypeDto reserveStateTypeDto) {
			ReserveStateType result = new ReserveStateType();

			result.setNumber(reserveStateTypeDto.getNumber());

			result.setName(reserveStateTypeDto.getName());

			result.setDescription(reserveStateTypeDto.getDescription());

			return result;
		}
	}

}
