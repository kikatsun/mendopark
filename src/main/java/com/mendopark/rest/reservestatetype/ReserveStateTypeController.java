
package com.mendopark.rest.reservestatetype;

import com.mendopark.crud.reservestatetype.ReserveStateTypeSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/ReserveStateType")
public class ReserveStateTypeController extends SController {

	@EJB
	protected ReserveStateTypeSExpert expert;

	public static final String RESOURCE_NAME = "Tipo de Estado de Reserva";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() throws NamingException {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/reserveStateType/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getReserveStateType(@PathParam("number") Integer number) {
		try {
			ReserveStateTypeDto dto = new ReserveStateTypeDto();
			dto.setNumber(number);
			return Response.ok().entity(ReserveStateTypeDto.Factory.getDtoFromReserveStateType(expert.find(dto)))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/reserveStateTypes")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getReserveStateTypes() {
		try {
			return Response.ok()
					.entity(ReserveStateTypeDto.Factory.getDtosFromReserveStateTypes(expert.findReserveStateTypes()))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(ReserveStateTypeDto reserveStateTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.delete(reserveStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(ReserveStateTypeDto reserveStateTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.update(reserveStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(ReserveStateTypeDto reserveStateTypeDto) throws NamingException {
		try {

			return Response.ok().entity(expert.persist(reserveStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}
	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateNameAndDescription(ReserveStateTypeDto reserveStateTypeDto) {
		try {
			return Response.ok().entity(expert.validateNameAndDescription(reserveStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}
}
