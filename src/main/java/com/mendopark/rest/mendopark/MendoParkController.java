package com.mendopark.rest.mendopark;

import com.mendopark.application.MEndoPark;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("mendopark")
public class MendoParkController {

    @EJB
    private MEndoPark mEndoPark;

    @Path("/")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response start(@HeaderParam("key") String key) {
        try {
            mEndoPark.startMEndoPark(key);
            return Response.ok().entity(true).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/client")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response addClient() {
        try {
            mEndoPark.addClient();
            return Response.ok().entity(true).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/user")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response addUser() {
        try {
            mEndoPark.addUser();
            return Response.ok().entity(true).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

}
