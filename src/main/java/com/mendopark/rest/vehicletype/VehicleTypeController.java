
package com.mendopark.rest.vehicletype;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.crud.vehicletype.VehicleTypeSExpert;
import com.mendopark.model.Session;
import com.mendopark.model.User;
import com.mendopark.model.security.Resource;
import com.mendopark.repository.ClientRepository;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;
import com.mendopark.rest.user.VehicleDto;
import com.mendopark.session.RestSessionController;
import com.mendopark.session.SessionManager;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/VehicleType")
public class VehicleTypeController extends SController {

	@EJB
	protected VehicleTypeSExpert expert;

	@EJB
	RestSessionController restSessionController;

	@EJB
	private CredentialManager credentialManager;

	@EJB
	private ClientRepository clientRepository;
	@EJB
	private SessionManager sessionManager;

	public static final String RESOURCE_NAME = "Tipo de Vehículo";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() throws NamingException {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/vehicleType/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getVehicleType(@PathParam("number") Integer number) {
		try {
			VehicleTypeDto dto = new VehicleTypeDto();
			dto.setNumber(number);
			return Response.ok().entity(VehicleTypeDto.Factory.getDtoFromVehicleType(expert.find(dto))).build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/vehicleTypes")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getVehicleTypes() {
		try {
			return Response.ok().entity(VehicleTypeDto.Factory.getDtosFromVehicleTypes(expert.findVehicleTypes()))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/vehicleTypes2")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getVehicleTypes2() {
		try {
			return Response.ok().entity(VehicleTypeDto.Factory.getDtosFromVehicleTypes(expert.findVehicleTypes2()))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(VehicleTypeDto vehicleTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.delete(vehicleTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(VehicleTypeDto vehicleTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.update(vehicleTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(VehicleTypeDto vehicleTypeDto) throws NamingException {
		try {

			return Response.ok().entity(expert.persist(vehicleTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/plate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarPlate(@HeaderParam("Authorization") String sessionId, VehicleDto vehicle) throws NamingException {
		try {
			User user = null;
			if(sessionId != null){
				Session ses = sessionManager.find(sessionId);
				if(ses != null){
					user = ses.getUserSession();
				}
			}
			return Response.ok().entity(VehicleDto.Factory.getDtoFrom(expert.persistPlate(
					VehicleDto.Factory.getVehicleFrom(vehicle), user))).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/plateDelete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response eliminarPlate(@HeaderParam("Authorization") String sessionId, VehicleDto vehicle) throws NamingException {
		try {
			User user = null;
			if(sessionId != null){
				Session ses = sessionManager.find(sessionId);
				if(ses != null){
					user = ses.getUserSession();
				}
			}
			return Response.ok().entity(VehicleDto.Factory.getDtoFrom(expert.deletePlate(
					VehicleDto.Factory.getVehicleFrom(vehicle), user))).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/mys")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response findVehicles(@HeaderParam("Authorization") String sessionId) {
		try {
			User user = null;
			if(sessionId != null){
				Session ses = sessionManager.find(sessionId);
				if(ses != null){
					user = ses.getUserSession();
				}
			}
			return Response.ok().entity(VehicleDto.Factory.getDtosFrom(expert.findPlates(user)))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateNameAndDescription(VehicleTypeDto vehicleTypeDto) {
		try {
			return Response.ok().entity(expert.validateNameAndDescription(vehicleTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}

}
