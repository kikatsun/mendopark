
package com.mendopark.rest.vehicletype;

import com.mendopark.model.VehicleType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class VehicleTypeDto {

	private Integer number;

	private String name;

	private String description;

	private String endDate;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public static class Factory {

		public static VehicleTypeDto getDtoFromVehicleType(VehicleType vehicleType) {
			VehicleTypeDto result = new VehicleTypeDto();

			result.setNumber(vehicleType.getNumber());

			result.setName(vehicleType.getName());

			result.setDescription(vehicleType.getDescription());

			if (vehicleType.getEndDate() != null ) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				result.setEndDate(sdf.format(vehicleType.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			return result;
		}

		public static List<VehicleTypeDto> getDtosFromVehicleTypes(List<VehicleType> vehicleTypes) {
			List<VehicleTypeDto> result = new ArrayList<VehicleTypeDto>();

			for (VehicleType vehicleType : vehicleTypes) {
				result.add(getDtoFromVehicleType(vehicleType));
			}

			return result;
		}

		public static VehicleType getVehicleTypeFromDto(VehicleTypeDto vehicleTypeDto) {
			VehicleType result = new VehicleType();

			result.setNumber(vehicleTypeDto.getNumber());

			result.setName(vehicleTypeDto.getName());

			result.setDescription(vehicleTypeDto.getDescription());

			return result;
		}
	}

}
