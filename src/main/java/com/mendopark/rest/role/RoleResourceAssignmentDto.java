package com.mendopark.rest.role;

import com.mendopark.model.Role;
import com.mendopark.model.security.GrantType;
import com.mendopark.model.security.Resource;
import com.mendopark.model.security.RoleResourceAssignment;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RoleResourceAssignmentDto {

    private String id;

    private Date startDate;

    private Date endDate;

    private Integer fromRoleId;

    private String toResourceId;

    private String toResourceName;

    private Integer grantTypeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getFromRoleId() {
        return fromRoleId;
    }

    public void setFromRoleId(Integer fromRoleId) {
        this.fromRoleId = fromRoleId;
    }

    public String getToResourceId() {
        return toResourceId;
    }

    public void setToResourceId(String toResourceId) {
        this.toResourceId = toResourceId;
    }

    public Integer getGrantTypeId() {
        return grantTypeId;
    }

    public void setGrantTypeId(Integer grantTypeId) {
        this.grantTypeId = grantTypeId;
    }

    public static class Factory {

        public static RoleResourceAssignment getRoleResourceAssignmentFrom (RoleResourceAssignmentDto dto) {
            RoleResourceAssignment roleResourceAssignment = new RoleResourceAssignment();

            roleResourceAssignment.setId(dto.getId());
            /* Las fechas lo maneja el experto, no se deberían de enviar asignaciones dadas de baja */
//            roleResourceAssignment.setEndDate(dto.getEndDate());
//            roleResourceAssignment.setEndDate(dto.getEndDate());
            if(dto.getFromRoleId() != null){
                Role role = new Role();
                role.setNumber(dto.getFromRoleId());
                roleResourceAssignment.setFromRole(role);
            }
            if(dto.getToResourceId() != null) {
                Resource resource = new Resource();
                resource.setId(dto.getToResourceId());
                roleResourceAssignment.setToResource(resource);
            }
            if(dto.getGrantTypeId() != null) {
                GrantType grantType = new GrantType();
                grantType.setNumber(dto.getGrantTypeId());
                roleResourceAssignment.setGrantType(grantType);
            }

            return roleResourceAssignment;
        }

        public static RoleResourceAssignmentDto getDtoFrom (RoleResourceAssignment roleResourceAssignment) {
            RoleResourceAssignmentDto dto = new RoleResourceAssignmentDto();

            dto.setId(roleResourceAssignment.getId());
            /* Las fechas lo maneja el experto, no se deberían de enviar asignaciones dadas de baja */
//            dto.setEndDate(roleResourceAssignment.getEndDate());
//            dto.setStartDate(roleResourceAssignment.getStartDate());

            if (roleResourceAssignment.getFromRole() != null) {
                dto.setFromRoleId(roleResourceAssignment.getFromRole().getNumber());
            }
            if (roleResourceAssignment.getToResource() != null) {
                dto.setToResourceId(roleResourceAssignment.getToResource().getId());
            }
            if (roleResourceAssignment.getGrantType() != null) {
                dto.setGrantTypeId(roleResourceAssignment.getGrantType().getNumber());
            }

            return dto;
        }

        public static List<RoleResourceAssignmentDto> getDtosFrom (List<RoleResourceAssignment> roleResourceAssignments){
            List<RoleResourceAssignmentDto> dtos = new ArrayList<>();
            if (roleResourceAssignments != null && roleResourceAssignments.size() > 0) {
                for (RoleResourceAssignment roleResourceAssignment: roleResourceAssignments ) {
                    dtos.add(getDtoFrom(roleResourceAssignment));
                }
            }
            return dtos;
        }

        public static List<RoleResourceAssignment> getRoleResourceAssignmentsFrom (List<RoleResourceAssignmentDto> dtos) {
            List<RoleResourceAssignment> assignments = new ArrayList<>();
            if (dtos != null && dtos.size() > 0){
                for (RoleResourceAssignmentDto dto: dtos ) {
                    assignments.add(getRoleResourceAssignmentFrom(dto));
                }
            }
            return assignments;
        }
    }
}
