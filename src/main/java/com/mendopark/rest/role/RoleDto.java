
package com.mendopark.rest.role;

import com.mendopark.model.Role;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RoleDto {

	private Integer number;
	private String name;
	private String description;
	private String endDate;

	private String rangoVigencia;
	private String vigenciaDesde;
	private String vigenciaHasta;

	private List<RoleResourceAssignmentDto> assignments;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRangoVigencia() {
		return rangoVigencia;
	}

	public void setRangoVigencia(String rangoVigencia) {
		this.rangoVigencia = rangoVigencia;
	}

	public String getVigenciaDesde() {
		return vigenciaDesde;
	}

	public void setVigenciaDesde(String vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}

	public String getVigenciaHasta() {
		return vigenciaHasta;
	}

	public void setVigenciaHasta(String vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}

	public List<RoleResourceAssignmentDto> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<RoleResourceAssignmentDto> assignments) {
		this.assignments = assignments;
	}

	public static class Factory {

		public static RoleDto getDtoFromRole(Role role) {
			RoleDto result = new RoleDto();

			result.setNumber(role.getNumber());

			result.setName(role.getName());

			result.setDescription(role.getDescription());

			if (role.getEndDate() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				result.setEndDate(sdf.format(role.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			if (role.getVigenciaDesde() != null && role.getVigenciaHasta() != null){
				result.setRangoVigencia( parseFechaToString(role.getVigenciaDesde())
						+ " - " + parseFechaToString(role.getVigenciaHasta()) );
			} else {
				result.setRangoVigencia(null);
			}

			return result;
		}

		public static List<RoleDto> getDtosFromRoles(List<Role> roles) {
			List<RoleDto> result = new ArrayList<RoleDto>();

			for (Role role : roles) {
				result.add(getDtoFromRole(role));
			}

			return result;
		}

		public static Role getRoleFromDto(RoleDto roleDto) {
			Role result = new Role();
			String parte1 = null;
			String parte2 = null;

			result.setNumber(roleDto.getNumber());

			result.setName(roleDto.getName());

			result.setDescription(roleDto.getDescription());

			if (roleDto.getRangoVigencia() != null){
				String[] parts = roleDto.getRangoVigencia().split("-");
				parte1 = parts[0]; // fecha Desde
				parte2 = parts[1]; // fecha Hasta
			}

			if (roleDto.getRangoVigencia() != null){
				result.setVigenciaDesde(parseFechaToDate(parte1));
			}

			if (roleDto.getRangoVigencia() != null){
				result.setVigenciaHasta(parseFechaToDate(parte2));
			}

			return result;
		}

		/**
		 * Permite convertir un String en fecha (Date).
		 * @param fecha Cadena de fecha dd/MM/yyyy
		 * @return Objeto Date
		 */
		public static Date parseFechaToDate(String fecha)
		{
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaDate = null;
			try {
				fechaDate = formato.parse(fecha);
			} catch (ParseException ex) {
				System.out.println(ex);
			}
			return fechaDate;
		}


		/**
		 * Permite convertir una fecha Date en String.
		 * @param fecha Date (dd/MM/yyyy)
		 * @return Objeto String
		 */
		public static String parseFechaToString(Date fecha){
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			String fechaDate = null;
			fechaDate = formato.format(fecha);

			return fechaDate;
		}
	}

}
