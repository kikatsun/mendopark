
package com.mendopark.rest.role;

import com.mendopark.crud.role.RoleSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/Role")
public class RoleController extends SController {

	@EJB
	protected RoleSExpert expert;

	public static final String RESOURCE_NAME = "Cliente";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId(){
		try {
			return Response.ok().entity(expert.getLastId()).build();

		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/role/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRole(@PathParam("number") Integer number) {
		try {
			RoleDto dto = new RoleDto();
			dto.setNumber(number);
			return Response.ok().entity(expert.find(dto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/roles")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRoles() {
		try {
			return Response.ok().entity(expert.findRoles()).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/rolesAvailables")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getRolesAvailables() {
		try {
			return Response.ok().entity(RoleDto.Factory.getDtosFromRoles(expert.findRolesAvailables())).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(RoleDto roleDto) throws NamingException {
		try {
			return Response.ok().entity(expert.delete(roleDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(RoleDto roleDto) {
		try {
			return Response.ok().entity(expert.update(roleDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(RoleDto roleDto) {
		try {

			return Response.ok().entity(expert.persist(
					RoleDto.Factory.getRoleFromDto(roleDto),
					RoleResourceAssignmentDto.Factory.getRoleResourceAssignmentsFrom(roleDto.getAssignments())
			)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}
	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateNameAndDescription(RoleDto roleDto) {
		try {
			return Response.ok().entity(expert.validateNameAndDescription(roleDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}
}
