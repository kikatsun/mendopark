package com.mendopark.rest.sensor;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.crud.sensor.SensorSExpert;
import com.mendopark.model.Role;
import com.mendopark.model.security.Resource;
import com.mendopark.repository.ClientRepository;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;
import com.mendopark.rest.parkingarea.SensorDto;
import com.mendopark.service.exception.LoggerException;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

@Stateless
@LocalBean
@Path("/Sensor")
public class SensorController extends SController {

    @EJB
    private SensorSExpert sensorSExpert;

    @EJB
    RestSessionController restSessionController;

    @EJB
    private CredentialManager credentialManager;

    @EJB
    private ClientRepository clientRepository;

    public static final String RESOURCE_NAME = "Sensor";

    private Resource resource = null;

    @Path("/")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getClientFromSession(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId,sensorSExpert);
                return Response.ok().entity(SensorDto.Factory.getDtosFrom(sensorSExpert.getSensors())).build();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                LoggerException.log(ex);
                throw ex;
            }
        }
    }

    @Path("/sensor/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findSensor(@HeaderParam("Authorization") String sessionId, @PathParam("id") String id) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId,sensorSExpert);
                return Response.ok().entity(SensorDto.Factory.getDtoFrom(sensorSExpert.findSensor(id))).build();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                LoggerException.log(ex);
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response persist(@HeaderParam("Authorization") String sessionId, SensorDto sensorDto) throws ParseException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId,sensorSExpert);
                return Response.ok().entity(sensorSExpert.persist(SensorDto.Factory.getSensorFrom(sensorDto))).build();
            } else {
                throw new NotAuthorizedException(
                        Response.status(Response.Status.UNAUTHORIZED).entity("Rol no autorizado").build());
            }
        }catch (Exception ex){
            LoggerException.log(ex);
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response update(@HeaderParam("Authorization") String sessionId, SensorDto sensorDto) throws ParseException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId,sensorSExpert);
                return Response.ok().entity(sensorSExpert.update(SensorDto.Factory.getSensorFrom(sensorDto))).build();
            } else {
                throw new NotAuthorizedException(
                        Response.status(Response.Status.UNAUTHORIZED).entity("Rol no autorizado").build());
            }
        }catch (Exception ex){
            LoggerException.log(ex);
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response delete(@HeaderParam("Authorization") String sessionId, SensorDto sensorDto) throws ParseException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId,sensorSExpert);
                return Response.ok().entity(sensorSExpert.delete(SensorDto.Factory.getSensorFrom(sensorDto))).build();
            } else {
                throw new NotAuthorizedException(
                        Response.status(Response.Status.UNAUTHORIZED).entity("Rol no autorizado").build());
            }
        }catch (Exception ex){
            LoggerException.log(ex);
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateReferenceCodeAndDescription(SensorDto sensorDto) {
        try {
            return Response.ok().entity(sensorSExpert.validateReferenceCodeAndDescription(sensorDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
}
