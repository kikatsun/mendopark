package com.mendopark.rest.locality;

import com.mendopark.model.Locality;
import com.mendopark.model.Province;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class LocalityDto {

    private Integer number;
    private String name;
    private Integer fromProvince;
    private String fromProvinceName;
    private String endDate;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFromProvince() {
        return fromProvince;
    }

    public void setFromProvince(Integer fromProvince) {
        this.fromProvince = fromProvince;
    }

    public String getFromProvinceName() {
        return fromProvinceName;
    }

    public void setFromProvinceName(String fromProvinceName) {
        this.fromProvinceName = fromProvinceName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static class Factory {

        public static LocalityDto getDtoFromLocality(Locality locality) {
            LocalityDto result = new LocalityDto();

            result.setNumber(locality.getNumber());

            result.setName(locality.getName());

            if (locality.getEndDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                result.setEndDate(sdf.format(locality.getEndDate()));
            } else {
                result.setEndDate(null);
            }

            if(locality.getFromProvince() != null){
                result.setFromProvince(locality.getFromProvince().getNumber());
                result.setFromProvinceName(locality.getFromProvince().getName());
            }

            return result;
        }

        public static List<LocalityDto> getDtosFromLocalities(List<Locality> provinces) {
            List<LocalityDto> result = new ArrayList<LocalityDto>();

            for (Locality province : provinces) {
                result.add(getDtoFromLocality(province));
            }

            return result;
        }

        public static Locality getLocalityFromDto(LocalityDto localityDto) {
            Locality result = new Locality();

            result.setNumber(localityDto.getNumber());

            result.setName(localityDto.getName());

            if(localityDto.getFromProvince() != null){
                Province province = new Province();
                province.setNumber(localityDto.getFromProvince());
                province.setName(localityDto.getFromProvinceName());
                result.setFromProvince(province);
            }

            return result;
        }
    }
}
