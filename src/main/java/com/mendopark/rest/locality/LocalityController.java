package com.mendopark.rest.locality;

import com.mendopark.crud.locality.LocalitySExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.province.ProvinceDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/Locality")
public class LocalityController extends SController {

    @EJB
    protected LocalitySExpert expert;

    public static final String RESOURCE_NAME = "Localidad";

    private Resource resource = null;


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId() {
        try {
            return Response.ok().entity(expert.getLastId()).build();
        } catch (Exception ex) {
            return Response.status(500).entity("Error").build();
        }
    }

    @Path("/locality/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocality(@PathParam("number") Integer number) {
        try {
            LocalityDto dto = new LocalityDto();
            dto.setNumber(number);
            return Response.ok().entity(LocalityDto.Factory.getDtoFromLocality(expert.find(dto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(null).build();
        }
    }
    @Path("/localities/{provinceNumber}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocalities(@PathParam("provinceNumber") Integer provinceNumber) {
        try {
            ProvinceDto dto = new ProvinceDto();
            dto.setNumber(provinceNumber);
            return Response.ok().entity(LocalityDto.Factory.getDtosFromLocalities(expert.findFromProvince(dto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(null).build();
        }
    }

    @Path("/localities")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocalities() {
        try {
            return Response.ok().entity(LocalityDto.Factory.getDtosFromLocalities(expert.findLocalities())).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaLocalidad(LocalityDto localityDto) {
        try {
            return Response.ok().entity(expert.delete(localityDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarLocalidadModificado(LocalityDto localityDto) {
        try {
            return Response.ok().entity(expert.update(LocalityDto.Factory.getLocalityFromDto(localityDto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(e.getLocalizedMessage()).build();
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarLocalidad(LocalityDto localityDto) {
        try {
            return Response.ok().entity(expert.persist(LocalityDto.Factory.getLocalityFromDto(localityDto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndProvince(LocalityDto localityDto) {
        try {
            return Response.ok().entity(expert.validateNameAndProvince(localityDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }

}
