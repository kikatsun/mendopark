package com.mendopark.rest.parking;

public class PagoDto {

    private String id;
    private Float monto;
    private Long fechaPago;
    private String concepto;
    private Long payType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getMonto() {
        return monto;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public Long getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Long fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Long getPayType() {
        return payType;
    }

    public void setPayType(Long payType) {
        this.payType = payType;
    }
}
