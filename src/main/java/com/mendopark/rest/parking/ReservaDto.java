package com.mendopark.rest.parking;

import com.mendopark.model.ParkingArea;
import com.mendopark.model.ParkingSpace;
import com.mendopark.model.Reserve;

import java.util.Date;

public class ReservaDto {

    private String id;
    private Long parkingAreaNumber;
    private String parkingAreaName;
    private String idPlaza;
    private String parkingSpaceName;
    private float priceByHour;
    private float anticipatedAmount;
    private String plate;
    private String observations;
    private Long creationDate;

    private PagoDto pago;

    //Todo Agregar e tipo de pago seleccioando.
    private Long payType;

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getTotalAPagar() {
        return totalAPagar;
    }

    public void setTotalAPagar(float totalAPagar) {
        this.totalAPagar = totalAPagar;
    }

    private float total = 50f;
    private float totalAPagar = 30f;

    private String eventoAccion;

    public String getEventoAccion() {
        return eventoAccion;
    }

    public void setEventoAccion(String eventoAccion) {
        this.eventoAccion = eventoAccion;
    }

    public String getParkingSpaceName() {
        return parkingSpaceName;
    }

    public void setParkingSpaceName(String parkingSpaceName) {
        this.parkingSpaceName = parkingSpaceName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getParkingAreaNumber() {
        return parkingAreaNumber;
    }

    public void setParkingAreaNumber(Long parkingAreaNumber) {
        this.parkingAreaNumber = parkingAreaNumber;
    }

    public String getIdPlaza() {
        return idPlaza;
    }

    public void setIdPlaza(String idPlaza) {
        this.idPlaza = idPlaza;
    }

    public String getParkingAreaName() {
        return parkingAreaName;
    }

    public void setParkingAreaName(String parkingAreaName) {
        this.parkingAreaName = parkingAreaName;
    }

    public float getPriceByHour() {
        return priceByHour;
    }

    public void setPriceByHour(float priceByHour) {
        this.priceByHour = priceByHour;
    }

    public float getAnticipatedAmount() {
        return anticipatedAmount;
    }

    public void setAnticipatedAmount(float anticipatedAmount) {
        this.anticipatedAmount = anticipatedAmount;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getPayType() {
        return payType;
    }

    public void setPayType(Long payType) {
        this.payType = payType;
    }

    public PagoDto getPago() {
        return pago;
    }

    public void setPago(PagoDto pago) {
        this.pago = pago;
    }

    public static class Factory {

        public static Reserve getReserveFrom (ReservaDto dto){
            Reserve reserve = new Reserve();

            reserve.setAnticipatedAmount(dto.getAnticipatedAmount());
            reserve.setCreationDate(new Date(dto.getCreationDate()));
            reserve.setId(dto.getId());
            reserve.setObservations(dto.getObservations());
            reserve.setPlate(dto.getPlate());
            reserve.setPriceByHour(dto.getPriceByHour());
            if(dto.getIdPlaza() != null){
                System.out.println("PLAZA IDDDDDDDDDD "+dto.getIdPlaza());
                ParkingSpace parkingSpace = new ParkingSpace();
                parkingSpace.setId(dto.getIdPlaza());
                parkingSpace.setName(dto.getParkingSpaceName());
                reserve.setParkingSpace(parkingSpace);
            }
            if(dto.getParkingAreaNumber() != null){
                System.out.println("AREA IDDDDDDDDDD "+dto.getParkingAreaNumber());
                ParkingArea parkingArea = new ParkingArea();
                parkingArea.setNumber(dto.getParkingAreaNumber());
                parkingArea.setName(dto.getParkingAreaName());
                reserve.setParkingArea(parkingArea);
            }
            return reserve;
        }

        public static ReservaDto getDtoFrom (Reserve reserve){
            ReservaDto dto = new ReservaDto();

            dto.setAnticipatedAmount(reserve.getAnticipatedAmount());
            dto.setCreationDate(reserve.getCreationDate().getTime());
            dto.setId(reserve.getId());
            dto.setObservations(reserve.getObservations());
            dto.setPlate(reserve.getPlate());
            dto.setPriceByHour(reserve.getPriceByHour());
            if(reserve.getParkingSpace() != null){
                dto.setIdPlaza(reserve.getParkingSpace().getId());
                dto.setParkingSpaceName(reserve.getParkingSpace().getName());
            }
            if(reserve.getParkingArea() != null){
                dto.setParkingAreaNumber(reserve.getParkingArea().getNumber());
                dto.setParkingAreaName(reserve.getParkingArea().getName());
            }
            return dto;
        }

    }
}
