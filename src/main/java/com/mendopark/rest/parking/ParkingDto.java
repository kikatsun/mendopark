package com.mendopark.rest.parking;

import com.mendopark.model.Parking;
import com.mendopark.model.ParkingArea;
import com.mendopark.model.ParkingSpace;
import com.mendopark.rest.parkingarea.ParkingAreaDto;
import com.mendopark.rest.parkingspace.ParkingSpaceDto;

import java.util.Date;
import java.util.UUID;

public class ParkingDto {

    private UUID id;

    private Date start;
    private Date end;
    private Integer hours;
    private Float price;
    private String parkingSpaceName;
    private String parkingSpaceId;
    private Long parkingAreaNumber;

    private ReservaDto reserva;

    public ReservaDto getReserva() {
        return reserva;
    }

    public void setReserva(ReservaDto reserva) {
        this.reserva = reserva;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getParkingSpaceName() {
        return parkingSpaceName;
    }

    public void setParkingSpaceName(String parkingSpaceName) {
        this.parkingSpaceName = parkingSpaceName;
    }

    public Long getParkingAreaNumber() {
        return parkingAreaNumber;
    }

    public void setParkingAreaNumber(Long parkingAreaNumber) {
        this.parkingAreaNumber = parkingAreaNumber;
    }

    public String getParkingSpaceId() {
        return parkingSpaceId;
    }

    public void setParkingSpaceId(String parkingSpaceId) {
        this.parkingSpaceId = parkingSpaceId;
    }

    @Override
    public String toString() {
        return "ParkingDto{" +
                "id=" + id +
                ", start=" + start +
                ", end=" + end +
                ", hours=" + hours +
                ", price=" + price +
                ", parkingSpaceName='" + parkingSpaceName + '\'' +
                ", parkingSpaceId='" + parkingSpaceId + '\'' +
                ", parkingAreaNumber=" + parkingAreaNumber +
                ", reserva=" + reserva +
                '}';
    }

    public static class Factory {
        public static Parking getParkingFrom (ParkingDto dto){
            Parking parking = new Parking();
            parking.setEnd(dto.getEnd());
            parking.setHours(dto.getHours());
            if (dto.getId() != null) {
                parking.setId(dto.getId().toString());
            }
            parking.setPrice(dto.getPrice());
            parking.setStart(dto.getStart());

            if(dto.getParkingAreaNumber() != null){
                ParkingArea parkingArea = new ParkingArea();
                parkingArea.setNumber(dto.getParkingAreaNumber());
                parking.setParkingArea(parkingArea);
            }
            if(dto.getParkingSpaceId() != null){
                ParkingSpace parkingSpace = new ParkingSpace();
                parkingSpace.setId(dto.getParkingSpaceId());
                parking.setParkingSpace(parkingSpace);
            }
            if(dto.getReserva() != null){
                parking.setReserve(ReservaDto.Factory.getReserveFrom(dto.getReserva()));
            }

            return parking;
        }

        public static ParkingDto getDtoFrom (Parking parking){
            ParkingDto dto = new ParkingDto();
            dto.setEnd(parking.getEnd());
            dto.setHours(parking.getHours());
            dto.setId(UUID.fromString(parking.getId()));
            dto.setPrice(parking.getPrice());
            dto.setStart(parking.getStart());

            if(parking.getParkingArea() != null){
                dto.setParkingAreaNumber(parking.getParkingArea().getNumber());
            }
            if(parking.getParkingSpace() != null){
                dto.setParkingSpaceId(parking.getParkingSpace().getId());
                dto.setParkingSpaceName(parking.getParkingSpace().getName());
            }
            if(parking.getReserve() != null){
                dto.setReserva(ReservaDto.Factory.getDtoFrom(parking.getReserve()));
            }
            return dto;
        }
    }
}
