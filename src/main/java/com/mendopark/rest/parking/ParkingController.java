package com.mendopark.rest.parking;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.application.parking.ParkingFactory;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.sensorstate.SensorStateController;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/* ToDo por ahora no se restringirá los permisos a esta API */
@Stateless
@LocalBean
@Path("/parking")
public class ParkingController extends SController {


    @EJB
    RestSessionController restSessionController;

    public static final List<Integer> allowedRoles = Arrays.asList(1,2,3,4,5,6);

    public static final String RESOURCE_NAME = "Estacionamiento";

    private Resource resource = null;

    @EJB
    private ParkingFactory parkingFactory;

    @EJB
    private SensorStateController sensorStateController;

    @EJB
    private CredentialManager credentialManager;

    @Path("/parkings")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSensors(@HeaderParam("Authorization") String sessionId){
        try {
            restSessionController.validateSession(sessionId, getAllowedRoles());
            Long clientId = null;
            if(sessionId != null && !sessionId.isEmpty()){
                clientId = credentialManager.findClientId(sessionId);
            }
            return Response.ok().entity(parkingFactory.findAll(clientId)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/{parkingName}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParking(@PathParam("parkingName") String parkingName){
        try {
            ReservaDto reservaDto = parkingFactory.findParking(parkingName);
            if(reservaDto != null) {
                return Response.ok().entity(reservaDto).build();
            }else{
                return Response.ok().entity("[]").build();
            }
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/reserva")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response saveReserva(ReservaDto reservaDto){
        try {
            parkingFactory.crearReserva(reservaDto);
            sensorStateController.saveSensorStateFromUser(reservaDto.getIdPlaza()+"|RESERVADO");
            return Response.ok().entity(reservaDto).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/reserva/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response geReserva(@PathParam("id") String plaza) {
        try {
            return Response.ok().entity(parkingFactory.getReserva(plaza)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/reserva/reservas")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response geReserva() {
        try {
            return Response.ok().entity(parkingFactory.getReservas()).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Override
    public String getResourceName() {
        return null;
    }

    @Override
    public Resource getResource() {
        return null;
    }

    @Override
    public void setResource(Resource resource) {

    }
}
