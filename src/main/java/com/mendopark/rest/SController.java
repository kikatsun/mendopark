package com.mendopark.rest;

import com.mendopark.application.SExpert;
import com.mendopark.application.credential.CredentialManager;
import com.mendopark.model.Role;
import com.mendopark.model.security.GrantType;
import com.mendopark.model.security.Resource;
import com.mendopark.repository.ClientRepository;
import com.mendopark.repository.ResourceRepository;
import com.mendopark.service.security.Resourceable;
import com.mendopark.service.security.RoleResourceManager;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.NotAuthorizedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Stateless
@LocalBean
public abstract class SController implements Resourceable {

    @EJB
    RestSessionController restSessionController;

    @EJB
    private CredentialManager credentialManager;

    @EJB
    private ClientRepository clientRepository;

    @EJB
    private RoleResourceManager roleResourceManager;

    @EJB
    private ResourceRepository resourceRepository;

    protected void updateCredentials(String sessionId, SExpert expert){
        expert.setSessionId(sessionId);
        Long clientId = credentialManager.findClientId(sessionId);
        if(clientId != null) {
            expert.setClient(clientRepository.get(clientId));
        }
        expert.setRole(restSessionController.getRoleFromSession(sessionId));
    }

    protected void validateCredentials(String sessionId, List<Integer> allowedRolesToRetrieve, SExpert expert){
        if (restSessionController.validateSession(sessionId, allowedRolesToRetrieve)) {
            updateCredentials(sessionId, expert);
        } else {
            throw new NotAuthorizedException("Rol No Autorizado");
        }

    }

    protected List<Integer> getAllowedRoles () {
        List<Role> allowedRoles = roleResourceManager.getAllowedRoles(
            getResource(),
            Arrays.asList(GrantType.Type.CREATE, GrantType.Type.READ, GrantType.Type.UPDATE, GrantType.Type.DELETE)
        );
        return getRoleIds(allowedRoles);
    }

    protected List<Integer> getAllRoles () {
        return roleResourceManager.getAllRolesIds();
    }

    protected List<Integer> getAllowedRolesToCreate () {
        List<Role> allowedRolesToCreate = roleResourceManager.getAllowedRoles(
                getResource(),
                Arrays.asList(GrantType.Type.CREATE)
        );
        return getRoleIds(allowedRolesToCreate);
    }

    protected List<Integer> getAllowedRolesToRetrieve () {
        List<Role> allowedRolesToRetrieve = roleResourceManager.getAllowedRoles(
                getResource(),
                Arrays.asList(GrantType.Type.READ)
        );
        return getRoleIds(allowedRolesToRetrieve);
    }

    protected List<Integer> getAllowedRolesToUpdate () {
        List<Role> allowedRolesToUpdate = roleResourceManager.getAllowedRoles(
                getResource(),
                Arrays.asList(GrantType.Type.UPDATE)
        );
        return getRoleIds(allowedRolesToUpdate);
    }

    protected List<Integer> getAllowedRolesToDelete () {
        List<Role> allowedRolesToDelete = roleResourceManager.getAllowedRoles(
                getResource(),
                Arrays.asList(GrantType.Type.DELETE)
        );
        return getRoleIds(allowedRolesToDelete);
    }

    protected Resource findResource (String resourceName) {
        return resourceRepository.findByName(resourceName);
    }

    private List<Integer> getRoleIds (List<Role> roles) {
        List<Integer> result = new ArrayList<>();
        if (roles != null && roles.size() > 0) {
            for (Role role : roles ) {
                Integer roleId = role.getNumber();
                if(!result.contains(roleId)){
                    result.add(roleId);
                }
            }
        }
        return result;
    }
}
