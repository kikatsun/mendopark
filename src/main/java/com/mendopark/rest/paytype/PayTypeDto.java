package com.mendopark.rest.paytype;

import com.mendopark.model.PayType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PayTypeDto {

    private Long number;
    private String name;
    private String description;
    private String endDate;

    private String rangoVigencia;
    private String vigenciaDesde;
    private String vigenciaHasta;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(String vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getRangoVigencia() {
        return rangoVigencia;
    }

    public void setRangoVigencia(String rangoVigencia) {
        this.rangoVigencia = rangoVigencia;
    }

    public static class Factory{

        public static PayTypeDto getDtoFromPayType(PayType payType){
            PayTypeDto result = new PayTypeDto();

            result.setNumber(payType.getNumber());
            result.setName(payType.getName());
            result.setDescription(payType.getDescription());

            if (payType.getEndDate() != null) {
                result.setEndDate(parseFechaToString(payType.getEndDate()));
            } else {
                result.setEndDate(null);
            }

            if (payType.getVigenciaDesde() != null && payType.getVigenciaHasta() != null) {
                result.setRangoVigencia( parseFechaToString(payType.getVigenciaDesde())
                        + " - " + parseFechaToString(payType.getVigenciaHasta()) );
            } else {
                result.setRangoVigencia(null);
            }

            return result;
        }

        public static List<PayTypeDto> getDtosFromPayTypes(List<PayType> payTypes){
            List<PayTypeDto> result = new ArrayList<PayTypeDto>();

            for (PayType payType: payTypes ) {
                result.add(getDtoFromPayType(payType));
            }

            return result;
        }

        public static PayType getPayTypeFromDto(PayTypeDto payTypeDto) {
            PayType result = new PayType();
            String parte1 = null;
            String parte2 = null;

            result.setNumber(payTypeDto.getNumber());
            result.setName(payTypeDto.getName());
            result.setDescription(payTypeDto.getDescription());

            if (payTypeDto.getRangoVigencia() != null){
                String[] parts = payTypeDto.getRangoVigencia().split("-");
                parte1 = parts[0]; // fecha Desde
                parte2 = parts[1]; // fecha Hasta
            }

            if (payTypeDto.getRangoVigencia() != null){
                result.setVigenciaDesde(parseFechaToDate(parte1));
            }

            if (payTypeDto.getRangoVigencia() != null){
                result.setVigenciaHasta(parseFechaToDate(parte2));
            }

            return result;
        }

        /**
         * Permite convertir un String en fecha (Date).
         * @param fecha Cadena de fecha dd/MM/yyyy
         * @return Objeto Date
         */
        public static Date parseFechaToDate(String fecha)
        {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaDate = null;
            try {
                fechaDate = formato.parse(fecha);
            } catch (ParseException ex) {
                System.out.println(ex);
            }
            return fechaDate;
        }


        /**
         * Permite convertir una fecha Date en String.
         * @param fecha Date (dd/MM/yyyy)
         * @return Objeto String
         */
        public static String parseFechaToString(Date fecha){
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            String fechaDate = null;
            fechaDate = formato.format(fecha);

            return fechaDate;
        }
    }

}
