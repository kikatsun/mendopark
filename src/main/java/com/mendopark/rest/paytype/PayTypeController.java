package com.mendopark.rest.paytype;

import com.mendopark.crud.paytype.PayTypeSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/PayType")
public class PayTypeController extends SController {

    @EJB
    protected PayTypeSExpert expert;

    public static final String RESOURCE_NAME = "Tipo de Pago";

    private Resource resource = null;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId() throws NamingException {
        try {
            return Response.ok().entity(expert.getLastId()).build();
        }catch(Exception ex){
            return Response.status(500).entity("Error").build();
        }
    }

    @Path("/payType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPayType(@PathParam("number") Long number){
        try {
            PayTypeDto dto = new PayTypeDto();
            dto.setNumber(number);
            return Response.ok().entity(PayTypeDto.Factory.getDtoFromPayType(expert.find(dto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(null).build();
        }
    }

    @Path("/payTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPayTypes() {
        try {
            return Response.ok().entity(PayTypeDto.Factory.getDtosFromPayTypes(expert.findPayTypes())).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(PayTypeDto payTypeDto) throws NamingException{
        try {
            return Response.ok().entity(expert.delete(payTypeDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(PayTypeDto payTypeDto) throws NamingException{
        try{
            return Response.ok().entity(expert.update(payTypeDto)).build();
        }
        catch(Exception e){
            return Response.status(500).entity(e.getLocalizedMessage()).build();
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(PayTypeDto payTypeDto) throws NamingException{
        try{

            return Response.ok().entity(expert.persist(payTypeDto)).build();
        }
        catch(Exception e){
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndDescription(PayTypeDto payTypeDto) {
        try {
            return Response.ok().entity(expert.validateNameAndDescription(payTypeDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }

}
