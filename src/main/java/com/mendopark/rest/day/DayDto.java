
package com.mendopark.rest.day;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.model.Client;
import com.mendopark.model.ClientConfiguration;
import com.mendopark.model.Day;

import javax.ejb.EJB;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DayDto {

	private Integer number;
	private String name;
	private String schedule; //horario de atención
	private Long clientNumber;
	private String endDate;


	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public Long getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(Long clientNumber) {
		this.clientNumber = clientNumber;
	}

	public static class Factory {

		public static DayDto getDtoFromDay(Day day) {
			DayDto result = new DayDto();

			result.setNumber(day.getNumber());
			result.setName(day.getName());
			result.setSchedule(day.getSchedule());

			if(day.getFromClient() != null){
				result.setClientNumber(day.getFromClient().getNumber());
			}

			if (day.getEndDate() != null) {
				result.setEndDate(parseFechaToString(day.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			return result;
		}

		public static List<DayDto> getDtosFromDays(List<Day> days) {
			List<DayDto> result = new ArrayList<DayDto>();

			for (Day day : days) {
				result.add(getDtoFromDay(day));
			}

			return result;
		}

		public static Day getDayFromDto(DayDto dayDto) {
			Day result = new Day();

			result.setNumber(dayDto.getNumber());

			result.setName(dayDto.getName());

			result.setSchedule(dayDto.getSchedule());

			if (dayDto.getClientNumber() != null){
				Client client = new Client();
				client.setNumber(dayDto.getClientNumber());
				result.setFromClient(client);
			}

			return result;
		}

		/**
		 * Permite convertir una fecha Date en String.
		 * @param fecha Date (dd/MM/yyyy)
		 * @return Objeto String
		 */
		public static String parseFechaToString(Date fecha){
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			String fechaDate = null;
			fechaDate = formato.format(fecha);

			return fechaDate;
		}
	}

}
