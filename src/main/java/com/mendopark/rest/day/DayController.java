
package com.mendopark.rest.day;

import com.mendopark.crud.client.ClientSExpert;
import com.mendopark.crud.day.DaySExpert;
import com.mendopark.model.Day;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;
import com.mendopark.service.clientconfiguration.ClientConfigurationDto;
import com.mendopark.service.clientconfiguration.ClientConfigurationManager;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/Day")
public class DayController extends SController {

	@PersistenceContext(unitName = "mendopark")
	private EntityManager entityManager;

	@EJB
	protected DaySExpert expert;

	@EJB
	protected ClientConfigurationManager clientConfigurationManager;

	@EJB
	protected ClientSExpert clientSExpert;

	public static final String RESOURCE_NAME = "Día";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() throws NamingException {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/day/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getDay(@PathParam("number") Integer number) {
		try {
			DayDto dto = new DayDto();
			dto.setNumber(number);
			return Response.ok().entity(DayDto.Factory.getDtoFromDay(expert.find(dto))).build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/days")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getDays() {
		try {
			return Response.ok().entity(DayDto.Factory.getDtosFromDays(expert.findAll())).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(DayDto dayDto) throws NamingException {
		try {
			boolean result = expert.delete(dayDto);
			entityManager.flush();
			clientSExpert.updateConfiguration(
					clientConfigurationManager.getClientConfiguration(expert.getClient().getNumber()));
			return Response.ok().entity(result).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(DayDto dayDto) throws NamingException {
		try {
			Day day = expert.update(dayDto);
			if (day != null && day.getNumber() != null) {
				entityManager.flush();
				clientSExpert.updateConfiguration(
						);
				entityManager.flush();
				entityManager.close();
			}
			return Response.ok().entity(dayDto).build();
		} catch (Throwable e) {
			System.out.println("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR en DayController: "+e.getMessage());
			System.out.println("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR en DayController: "+e.getCause());
			return Response.ok().entity(dayDto).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(DayDto dayDto) throws NamingException {
		try {
			Day day = expert.persist(dayDto);
			if (day != null && day.getNumber() != null) {
				entityManager.flush();
				clientSExpert.updateConfiguration(
						clientConfigurationManager.getClientConfiguration(expert.getClient().getNumber()));
				entityManager.flush();
				entityManager.close();
			}
			return Response.ok().entity(day).build();
		} catch (Throwable e) {
			System.out.println("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR en DayController: "+e.getMessage());
			System.out.println("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR en DayController: "+e.getCause());
			return Response.ok().entity(dayDto).build();
		}
	}

	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateName(DayDto dayDto) {
		try {
			return Response.ok().entity(expert.validateName(dayDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}
}
