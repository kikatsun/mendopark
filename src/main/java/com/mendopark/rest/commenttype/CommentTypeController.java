
package com.mendopark.rest.commenttype;

import com.mendopark.crud.commenttype.CommentTypeSExpert;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/CommentType")
public class CommentTypeController {

	@EJB
	private CommentTypeSExpert expert;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() throws NamingException {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/CommentType/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getCommentType(@PathParam("number") Integer number) {
		try {
			CommentTypeDto dto = new CommentTypeDto();
			dto.setNumber(number);
			return Response.ok().entity(CommentTypeDto.Factory.getDtoFromCommentType(expert.find(dto))).build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/commentTypes")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getCommentTypes() {
		try {
			return Response.ok().entity(CommentTypeDto.Factory.getDtosFromCommentTypes(expert.findCommentTypes()))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(CommentTypeDto commentTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.delete(commentTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(CommentTypeDto commentTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.update(commentTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(CommentTypeDto commentTypeDto) throws NamingException {
		try {

			return Response.ok().entity(expert.persist(commentTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

}
