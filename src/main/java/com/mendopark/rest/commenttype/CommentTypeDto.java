
package com.mendopark.rest.commenttype;

import com.mendopark.model.CommentType;

import java.util.ArrayList;
import java.util.List;

public class CommentTypeDto {

	private Integer number;

	private String name;

	private String description;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static class Factory {

		public static CommentTypeDto getDtoFromCommentType(CommentType commentType) {
			CommentTypeDto result = new CommentTypeDto();

			result.setNumber(commentType.getNumber());

			result.setName(commentType.getName());

			result.setDescription(commentType.getDescription());

			return result;
		}

		public static List<CommentTypeDto> getDtosFromCommentTypes(List<CommentType> commentTypes) {
			List<CommentTypeDto> result = new ArrayList<CommentTypeDto>();

			for (CommentType commentType : commentTypes) {
				result.add(getDtoFromCommentType(commentType));
			}

			return result;
		}

		public static CommentType getCommentTypeFromDto(CommentTypeDto commentTypeDto) {
			CommentType result = new CommentType();

			result.setNumber(commentTypeDto.getNumber());

			result.setName(commentTypeDto.getName());

			result.setDescription(commentTypeDto.getDescription());

			return result;
		}
	}

}
