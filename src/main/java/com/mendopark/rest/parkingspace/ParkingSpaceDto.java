package com.mendopark.rest.parkingspace;

import com.mendopark.rest.parkingarea.SensorDto;

public class ParkingSpaceDto {

    private String id;
    private String name;
    private Long fromParkingArea;
    private SensorDto sensor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFromParkingArea() {
        return fromParkingArea;
    }

    public void setFromParkingArea(Long fromParkingArea) {
        this.fromParkingArea = fromParkingArea;
    }

    public SensorDto getSensor() {
        return sensor;
    }

    public void setSensor(SensorDto sensor) {
        this.sensor = sensor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ParkingSpaceDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", fromParkingArea=" + fromParkingArea +
                ", sensor=" + sensor +
                '}';
    }
}
