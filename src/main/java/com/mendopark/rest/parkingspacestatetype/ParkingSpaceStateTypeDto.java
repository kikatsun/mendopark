
package com.mendopark.rest.parkingspacestatetype;

import com.mendopark.model.ParkingSpaceStateType;
import com.mendopark.model.ReserveStateType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO de Tipo Estado de Plaza de Estacionamiento
 * Datos requeridos para crear un Tipo Estado de Plaza de Estacionamiento.
 *
 * @Pattern Data Object Transfer
 */

public class ParkingSpaceStateTypeDto {

	private Integer number;

	private String name;

	private String description;

	private String endDate;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * Fábrica para convertir, crear el DTO con la entidad {@link ParkingSpaceStateType}.
	 *
	 * @Pattern: Factory
	 */
	public static class Factory {

		public static ParkingSpaceStateTypeDto getDtoFromParkingSpaceStateType(ParkingSpaceStateType parkingSpaceStateType) {
			ParkingSpaceStateTypeDto result = new ParkingSpaceStateTypeDto();

			result.setNumber(parkingSpaceStateType.getNumber());

			result.setName(parkingSpaceStateType.getName());

			result.setDescription(parkingSpaceStateType.getDescription());

			if (parkingSpaceStateType.getEndDate() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				result.setEndDate(sdf.format(parkingSpaceStateType.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			return result;
		}

		public static List<ParkingSpaceStateTypeDto> getDtosFromParkingSpaceStateTypes(List<ParkingSpaceStateType> parkingSpaceStateTypes) {
			List<ParkingSpaceStateTypeDto> result = new ArrayList<ParkingSpaceStateTypeDto>();

			for (ParkingSpaceStateType parkingSpaceStateType : parkingSpaceStateTypes) {
				result.add(getDtoFromParkingSpaceStateType(parkingSpaceStateType));
			}

			return result;
		}

		public static ParkingSpaceStateType getParkingSpaceStateTypeFromDto(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) {
			ParkingSpaceStateType result = new ParkingSpaceStateType();

			result.setNumber(parkingSpaceStateTypeDto.getNumber());

			result.setName(parkingSpaceStateTypeDto.getName());

			result.setDescription(parkingSpaceStateTypeDto.getDescription());

			return result;
		}
	}

}
