
package com.mendopark.rest.parkingspacestatetype;

import com.mendopark.crud.parkingspacestatetype.ParkingSpaceStateTypeSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/ParkingSpaceStateType")
public class ParkingSpaceStateTypeController extends SController {

	@EJB
	protected ParkingSpaceStateTypeSExpert expert;

	public static final String RESOURCE_NAME = "Tipo Estado P. Estacionamiento";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() throws NamingException {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/parkingSpaceStateType/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getParkingSpaceStateType(@PathParam("number") Integer number) {
		try {
			ParkingSpaceStateTypeDto dto = new ParkingSpaceStateTypeDto();
			dto.setNumber(number);
			return Response.ok()
					.entity(ParkingSpaceStateTypeDto.Factory.getDtoFromParkingSpaceStateType(expert.find(dto))).build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/parkingSpaceStateTypes")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getParkingSpaceStateTypes() {
		try {
			return Response.ok().entity(ParkingSpaceStateTypeDto.Factory
					.getDtosFromParkingSpaceStateTypes(expert.findParkingSpaceStateTypes())).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.delete(parkingSpaceStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto)
			throws NamingException {
		try {
			return Response.ok().entity(expert.update(parkingSpaceStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws NamingException {
		try {

			return Response.ok().entity(expert.persist(parkingSpaceStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateNameAndDescription(ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) {
		try {
			return Response.ok().entity(expert.validateNameAndDescription(parkingSpaceStateTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}
}
