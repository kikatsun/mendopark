package com.mendopark.rest.client;

import com.mendopark.crud.client.ClientSExpert;
import com.mendopark.crud.user.UserExpert;
import com.mendopark.model.Client;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.service.clientconfiguration.ClientConfigurationDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Stateless
@LocalBean
@Path("/Client")
public class ClientController extends SController {

    @PersistenceContext(unitName = "mendopark")
    private EntityManager entityManager;

    @EJB
    protected ClientSExpert clientSExpert;

    @EJB
    protected UserExpert userExpert;

    public static final String RESOURCE_NAME = "Cliente";

    private Resource resource = null;

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response persist(ClientDto clientDto) {
        Long clientId = clientSExpert.persist(
                ClientDto.Factory.getClientFromDto(clientDto),
                ClientDto.Factory.getUserFrom(clientDto));
        if(clientId != null){
            entityManager.flush();
            clientDto.getClientConfiguration().setClientNumber(clientId);
            clientSExpert.updateConfiguration(
                    ClientConfigurationDto.Factory.getClientConfiguration(clientDto.getClientConfiguration()));
        }
        return Response.ok().entity(true).build();
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response update(ClientDto clientDto) {
        boolean isUpdatedClient = clientSExpert.update(ClientDto.Factory.getClientFromDto(clientDto));
        if (isUpdatedClient && clientDto.getClientConfiguration() != null) {
            entityManager.flush();
            clientDto.getClientConfiguration().setClientNumber(clientDto.getNumber());
            clientSExpert.updateConfiguration(
                    ClientConfigurationDto.Factory.getClientConfiguration(clientDto.getClientConfiguration()));
        }
        return Response.ok().entity(isUpdatedClient).build();
    }

    @Path("/delete/{clientNumber}")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    public Response delete(@PathParam("clientNumber") Long clientNUmber) {
        Client dto = new Client();
        dto.setNumber(clientNUmber);
        boolean isUpdatedClient = clientSExpert.delete(dto);
        return Response.ok().entity(isUpdatedClient).build();
    }

    @Path("/configuration")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateClientConfiguration(ClientConfigurationDto clientConfigurationDto) {
        return Response.ok().entity(clientSExpert.updateConfiguration(
                ClientConfigurationDto.Factory.getClientConfiguration(clientConfigurationDto))
        ).build();
    }

    @Path("/")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getClientFromSession() {
        return Response.ok().entity(ClientDto.Factory.getDtoFrom(clientSExpert.getClient())).build();
    }


    @Path("/clients")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getClients() {
        List<ClientDto> clientDtoList = ClientDto.Factory.getDtosFrom(clientSExpert.getClients());
        List<ClientDto> result = new ArrayList<>();
        if(clientDtoList != null && clientDtoList.size() > 0){
            for (ClientDto dto : clientDtoList ) {
                result.add(ClientDto.Factory.complete(
                        dto,
                        userExpert.findClientUser(dto.getNumber()),
                        clientSExpert.findCLientConfiguration(dto.getNumber())
                ));
            }
        }
        return Response.ok().entity(clientDtoList).build();
    }

    @Path("/lastId")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLastId() {
        return Response.ok().entity(clientSExpert.getLastId().longValue()).build();
    }

    @Path("/client/{clientNumber}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findClient(@PathParam("clientNumber") Long clientNumber) {
        ClientDto result = ClientDto.Factory.getDtoFrom(clientSExpert.findClient(clientNumber));
        return Response.ok().entity(ClientDto.Factory.complete(
                result,
                userExpert.findClientUser(clientNumber),
                clientSExpert.findCLientConfiguration(clientNumber)
        )).build();
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
