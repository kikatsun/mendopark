package com.mendopark.rest.client;

import com.mendopark.model.Client;
import com.mendopark.model.ClientConfiguration;
import com.mendopark.model.ClientType;
import com.mendopark.model.User;
import com.mendopark.rest.user.AddressDto;
import com.mendopark.service.clientconfiguration.ClientConfigurationDto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientDto {

    private Long number;

    private String companyName;

    private String cuit;

    private String phoneNumber;

    private String endDate;

    private Date startDate;

    private String bankData;

    private Integer clientTypeNumber;

    private String clientTypeName;

    private AddressDto address;

    private ClientConfigurationDto clientConfiguration;

    private String userName;

    private String password;

    private String email;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getBankData() {
        return bankData;
    }

    public void setBankData(String bankData) {
        this.bankData = bankData;
    }

    public Integer getClientTypeNumber() {
        return clientTypeNumber;
    }

    public void setClientTypeNumber(Integer clientTypeNumber) {
        this.clientTypeNumber = clientTypeNumber;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public String getClientTypeName() {
        return clientTypeName;
    }

    public void setClientTypeName(String clientTypeName) {
        this.clientTypeName = clientTypeName;
    }

    public ClientConfigurationDto getClientConfiguration() {
        return clientConfiguration;
    }

    public void setClientConfiguration(ClientConfigurationDto clientConfiguration) {
        this.clientConfiguration = clientConfiguration;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static class Factory {
        public static Client getClientFromDto (ClientDto dto){
            Client client = new Client();

            client.setNumber(dto.getNumber());
            client.setCompanyName(dto.getCompanyName());
            client.setCuit(dto.getCuit());
            client.setBankData(dto.getBankData());
            client.setPhoneNumber(dto.getPhoneNumber());
            if(dto.getAddress() != null){
                client.setAddress(AddressDto.Factory.getAddressFromDto(dto.getAddress()));
            }
            if(dto.getClientTypeNumber() != null){
                ClientType clientType = new ClientType();
                clientType.setNumber(dto.getClientTypeNumber());
                clientType.setName(dto.getClientTypeName());
                client.setClientType(clientType);
            }

            return client;
        }

        public static ClientDto getDtoFrom(Client client){
            ClientDto dto = new ClientDto();

            dto.setNumber(client.getNumber());
            dto.setCompanyName(client.getCompanyName());
            dto.setBankData(client.getBankData());
            dto.setCuit(client.getCuit());
            dto.setPhoneNumber(client.getPhoneNumber());
            //dto.setEndDate(client.getEndDate());

            if (client.getEndDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                dto.setEndDate(sdf.format(client.getEndDate()));
            } else {
                dto.setEndDate(null);
            }

            if(client.getAddress() != null){
                dto.setAddress(AddressDto.Factory.getDtoFromAddress(client.getAddress()));
            }
            if(client.getClientType() != null){
                if(client.getClientType().getNumber() != null) {
                    dto.setClientTypeNumber(client.getClientType().getNumber());
                }
                if(client.getClientType().getName() != null) {
                    dto.setClientTypeName(client.getClientType().getName());
                }
            }

            return dto;
        }

        public static List<ClientDto> getDtosFrom(List<Client> clients){
            List<ClientDto> result = new ArrayList<>();
            if(clients != null && clients.size()>0) {
                for (Client client: clients ) {
                    result.add(getDtoFrom(client));
                }
            }
            return result;
        }

        public static User getUserFrom(ClientDto clientDto){
            User user = new User();
            user.setName(clientDto.getUserName());
            user.setPassword(clientDto.getPassword());
            user.setEmail(clientDto.getEmail());
            return user;
        }

        public static ClientDto complete (ClientDto dto, User user, ClientConfiguration clientConfiguration){

            if(user != null) {
                dto.setUserName(user.getName());
                dto.setPassword(user.getPassword());
                dto.setEmail(user.getEmail());
            }
            if(clientConfiguration != null){
                dto.setClientConfiguration(ClientConfigurationDto.Factory.getDto(clientConfiguration));
            }
            return dto;
        }
    }
}
