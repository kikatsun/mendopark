package com.mendopark.rest.user;

import com.mendopark.model.Vehicle;
import com.mendopark.model.VehicleType;

import java.util.ArrayList;
import java.util.List;

public class VehicleDto {

    private String domain;

    private String model;

    private Integer year;

    private String brand; // marca

    private String color;

    private Integer type;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static class Factory {

        public static Vehicle getVehicleFrom( VehicleDto dto) {
            Vehicle vehicle = new Vehicle();

            vehicle.setDomain(dto.getDomain().toUpperCase());
            vehicle.setBrand(dto.getBrand());
            vehicle.setColor(dto.getColor());
            vehicle.setModel(dto.getModel());
            vehicle.setYear(dto.getYear());

            if(dto.getType() != null){
                VehicleType vt = new VehicleType();
                vt.setNumber(dto.getType());
                vehicle.setVehicleType(vt);
            }

            return vehicle;
        }

        public static VehicleDto getDtoFrom (Vehicle vehicle) {
            VehicleDto dto = new VehicleDto();

            dto.setDomain(vehicle.getDomain());
            dto.setBrand(vehicle.getBrand());
            dto.setColor(vehicle.getColor());
            dto.setModel(vehicle.getModel());
            dto.setYear(vehicle.getYear());

            if(vehicle.getVehicleType() != null){
                dto.setType(vehicle.getVehicleType().getNumber());
            }

            return dto;
        }

        public static List<VehicleDto> getDtosFrom (List<Vehicle> vehicles) {
            List<VehicleDto> dtos = new ArrayList<>();
            if(vehicles != null && vehicles.size() > 0) {
                for (Vehicle vehicle: vehicles ) {
                    dtos.add(getDtoFrom(vehicle));
                }
            }
            return dtos;
        }

    }

}
