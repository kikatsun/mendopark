package com.mendopark.rest.user;

import com.mendopark.model.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserDto {

    private String id;
    private String userName;
    private String password;
    private String email;
    private String documentNumber;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String bithdate;
    private long documentType;
    private Integer roleId;
    private String uuid;
    private String endDate;
    private String clientName;

    private AddressDto address;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBithdate() {
        return bithdate;
    }

    public void setBithdate(String bithdate) {
        this.bithdate = bithdate;
    }

    public long getDocumentType() {
        return documentType;
    }

    public void setDocumentType(long documentType) {
        this.documentType = documentType;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public static class Factory{

        /**
         * Genera un UserDto a partir de un User.
         * @param user
         * @return
         */
        public static final UserDto getUserDtoFrom(User user){
            UserDto dto = new UserDto();

            dto.setId(user.getId());
            dto.setUserName(user.getName());
            dto.setEmail(user.getEmail());
            dto.setPassword(user.getPassword());

            if (user.getEndDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                dto.setEndDate(sdf.format(user.getEndDate()));
            } else {
                dto.setEndDate(null);
            }

            if(user.getFromPerson() != null){
                Person userPerson = user.getFromPerson();
                dto.setName(userPerson.getName());
                dto.setLastName(userPerson.getLastName());
                dto.setDocumentNumber(userPerson.getDocumentNumber());
                dto.setPhoneNumber(userPerson.getPhoneNumber());

                if(user.getFromPerson().getAddress() != null){
                    dto.setAddress(
                            AddressDto.Factory.getDtoFromAddress(user.getFromPerson().getAddress())
                    );
                }
            }

            if(user.getUserRole() != null){
                dto.setRoleId(user.getUserRole().getNumber());
            }

            return dto;
        }

        /**
         * Genera una lista de UserDto a partir de una lista de User
         * @param users : lista de usuarios
         * @return : lista de DTO's Usuario
         */
        public static List<UserDto> getDtosFromUsers(List<User> users) {
            List<UserDto> result = new ArrayList<UserDto>();

            for (User user : users) {
                result.add(getUserDtoFrom(user));
            }

            return result;
        }

        public static final User getUserFrom(UserDto dto){
            User user = new User();

            user.setId(dto.getId());
            user.setName(dto.getUserName());
            user.setPassword(dto.getPassword());
            user.setEmail(dto.getEmail());
            //user.setEndDate(dto.getEndDate());

            Person userPerson = new Person();
            userPerson.setName(dto.getName());
            userPerson.setLastName(dto.getLastName());
            userPerson.setEmail(dto.getEmail());
            userPerson.setPhoneNumber(dto.getPhoneNumber());
            // ToDo Por definir fechas del front end
//            userPerson.setBithdate(new Date(dto.getBithdate()));
            userPerson.setDocumentNumber(dto.getDocumentNumber());

            DocumentType documentType = new DocumentType();
            if(dto.getDocumentType() > 0L){
                documentType.setNumber(dto.getDocumentType());
            }else {
                documentType.setNumber(1L);
            }
            userPerson.setDocumentType(documentType);

            if(dto.getAddress() != null) {
                userPerson.setAddress(
                        AddressDto.Factory.getAddressFromDto(dto.getAddress())
                );
            }
            user.setFromPerson(userPerson);

            if(dto.getRoleId() != null){
                Role role = new Role();
                role.setNumber(dto.getRoleId());
                user.setUserRole(role);
            }

            return user;
        }

    }
}
