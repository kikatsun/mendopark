package com.mendopark.rest.user;

import com.mendopark.model.Address;
import com.mendopark.model.Locality;

import java.util.ArrayList;
import java.util.List;

public class AddressDto {

    private String streetName;

    private Integer streetNumber;

    private Integer departmentNumber;

    private Integer fromLocality;

    private Integer fromProvince;

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Integer getDepartmentNumber() {
        return departmentNumber;
    }

    public void setDepartmentNumber(Integer departmentNumber) {
        this.departmentNumber = departmentNumber;
    }

    public Integer getFromLocality() {
        return fromLocality;
    }

    public void setFromLocality(Integer fromLocality) {
        this.fromLocality = fromLocality;
    }

    public Integer getFromProvince() {
        return fromProvince;
    }

    public void setFromProvince(Integer fromProvince) {
        this.fromProvince = fromProvince;
    }

    public static class Factory{

        public static AddressDto getDtoFromAddress(Address address){
            AddressDto result = new AddressDto();

            result.setStreetName(address.getStreetName());
            result.setDepartmentNumber(address.getDepartmentNumber());
            result.setStreetNumber(address.getStreetNumber());
            if(address.getFromLocality() != null){
                result.setFromLocality(address.getFromLocality().getNumber());
                if(address.getFromLocality().getFromProvince() != null){
                    result.setFromProvince(address.getFromLocality().getFromProvince().getNumber());
                }
            }

            return result;
        }

        public static Address getAddressFromDto(AddressDto dto){
            Address result = new Address();

            result.setStreetName(dto.getStreetName());
            result.setDepartmentNumber(dto.getDepartmentNumber());
            result.setStreetNumber(dto.getStreetNumber());

            if(dto.getFromLocality() != null) {
                Locality locality = new Locality();
                locality.setNumber(dto.getFromLocality());
                result.setFromLocality(locality);
            }

            return result;
        }

        public static List<Address> getAddressesFromDtos(List<AddressDto> dtos){
            List<Address> addresses = new ArrayList<Address>();

            for (AddressDto dto: dtos ) {
                addresses.add(getAddressFromDto(dto));
            }

            return addresses;
        }

    }

}
