package com.mendopark.rest.user;

import com.mendopark.application.ResponseDto;
import com.mendopark.application.credential.CredentialManager;
import com.mendopark.crud.user.UserExpert;
import com.mendopark.model.User;
import com.mendopark.model.security.Resource;
import com.mendopark.repository.ClientRepository;
import com.mendopark.rest.SController;
import com.mendopark.service.MEndoParkLogger;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Controlador que define la api para el recurso Usuario, se da soporte para crear, actualizar un usuario
 */
@Stateless
@LocalBean
@Path("/User")
public class UserController extends SController {

    @EJB
    private UserExpert expert;

    @EJB
    RestSessionController restSessionController;

    @EJB
    private CredentialManager credentialManager;

    @EJB
    private ClientRepository clientRepository;

    public static final String RESOURCE_NAME = "Usuario";

    private Resource resource = null;

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response update(@HeaderParam("Authorization") String sessionId, UserDto userDTO) {
        try {
            validateCredentials(sessionId, getAllowedRoles(),expert);
            return Response.ok().entity(expert.update(UserDto.Factory.getUserFrom(userDTO))).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ResponseDto.Factory.create(ex.getCause().getMessage())).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response persist(@HeaderParam("Authorization") String sessionId, UserDto userDTO){
        try {
            if(sessionId != null) {
                validateCredentials(sessionId, getAllowedRoles(), expert);
            }
            return Response.ok().entity(UserDto.Factory.getUserDtoFrom(expert.persist(UserDto.Factory.getUserFrom(userDTO)))).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ResponseDto.Factory.create(ex.getCause().getMessage())).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/delete")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    public Response delete(@HeaderParam("Authorization") String sessionId, UserDto userDTO){
        try {
            validateCredentials(sessionId, getAllowedRoles(),expert);
            return Response.ok().entity(expert.delete(UserDto.Factory.getUserFrom(userDTO))).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/users")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUsers(@HeaderParam("Authorization") String sessionId) {
        try {
            validateCredentials(sessionId, getAllRoles(),expert);
            List<User> users = expert.findUsers();
            List<UserDto> dtos = new ArrayList<>();
            if(users != null && users.size() > 0) {
                for (User user: users ) {
                    UserDto dto = UserDto.Factory.getUserDtoFrom(user);
                    if(user.getFromClient() != null){
                        dto.setClientName(user.getFromClient().getCompanyName());
                    }
                    dtos.add(dto);
                }
            }
            return Response.ok().entity(dtos).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/user/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUser(@HeaderParam("Authorization") String sessionId, @PathParam("id") String id) {
        try {
            validateCredentials(sessionId, getAllRoles(),expert);
            UserDto dto = new UserDto();
            dto.setId(id);
            return Response.ok().entity(UserDto.Factory.getUserDtoFrom(expert.findUser(dto))).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateUserName(@HeaderParam("Authorization") String sessionId, UserDto userDto) {
        try {
            validateCredentials(sessionId, getAllRoles(),expert);
            return Response.ok().entity(expert.validateUserName(userDto)).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/restore")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response restoreUsernameAndPassword(UserDto userDto) {
        try {
            return Response.ok().entity(expert.restoreUsernameAndPassword(userDto)).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
