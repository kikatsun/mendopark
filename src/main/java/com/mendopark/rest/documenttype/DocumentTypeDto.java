package com.mendopark.rest.documenttype;

import com.mendopark.model.DocumentType;

import java.util.ArrayList;
import java.util.List;

public class DocumentTypeDto {

    private Long number;
    private String name;
    private String description;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Factory{

        public static DocumentTypeDto getDtoFromDocumentType(DocumentType documentType){

            DocumentTypeDto result = new DocumentTypeDto();

            result.setNumber(documentType.getNumber());
            result.setName(documentType.getName());
            result.setDescription(documentType.getDescription());

            return result;
        }

        public static List<DocumentTypeDto> getDtosFromDocumentTypes(List<DocumentType> documentTypes){

            List<DocumentTypeDto> result = new ArrayList<DocumentTypeDto>();

            for (DocumentType documentType : documentTypes){
                result.add(getDtoFromDocumentType(documentType));
            }

            return result;
        }

        public static DocumentType getDocumentTypeFromDto(DocumentTypeDto documentTypeDto){

            DocumentType result = new DocumentType();

            result.setNumber(documentTypeDto.getNumber());
            result.setName(documentTypeDto.getName());
            result.setDescription(documentTypeDto.getDescription());

            return result;
        }

    }


}
