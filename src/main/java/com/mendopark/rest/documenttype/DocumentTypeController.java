package com.mendopark.rest.documenttype;

import com.mendopark.crud.documenttype.DocumentTypeExpert;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/DocumentType")
public class DocumentTypeController {

    @EJB
    private DocumentTypeExpert expert;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId() throws NamingException {
        try {
            return Response.ok().entity(expert.getLastId()).build();
        }catch(Exception ex){
            return Response.status(500).entity("Error").build();
        }
    }

    @Path("/DocumentType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPayType(@PathParam("number") Long number){
        try {
            DocumentTypeDto dto = new DocumentTypeDto();
            dto.setNumber(number);
            return Response.ok().entity(DocumentTypeDto.Factory.getDtoFromDocumentType(expert.find(dto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(null).build();
        }
    }
    /*
    @Path("/documentTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPayTypes() {
        try {
            return Response.ok().entity(DocumentTypeDto.Factory.getDtosFromDocumentTypes(expert.find)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }*/

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaTipoDocumento(DocumentTypeDto documentTypeDto) throws NamingException{
        try {
            return Response.ok().entity(expert.delete(documentTypeDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarTipoDocumentoModificado(DocumentTypeDto documentTypeDto) throws NamingException{
        try{
            return Response.ok().entity(expert.update(documentTypeDto)).build();
        }
        catch(Exception e){
            return Response.status(500).entity(e.getLocalizedMessage()).build();
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(DocumentTypeDto documentTypeDto) throws NamingException{
        try{

            return Response.ok().entity(expert.persist(documentTypeDto)).build();
        }
        catch(Exception e){
            return Response.status(500).entity(false).build();
        }
    }
}
