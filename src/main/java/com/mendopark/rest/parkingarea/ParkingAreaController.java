package com.mendopark.rest.parkingarea;

import com.mendopark.application.ResponseDto;
import com.mendopark.application.credential.CredentialManager;
import com.mendopark.crud.parkingarea.ParkingAreaSExpert;
import com.mendopark.model.ParkingArea;
import com.mendopark.model.security.Resource;
import com.mendopark.repository.ClientRepository;
import com.mendopark.rest.SController;
import com.mendopark.service.MEndoParkLogger;
import com.mendopark.service.clientconfiguration.ClientConfigurationManager;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@Stateless
@LocalBean
@Path("/ParkingArea")
public class ParkingAreaController extends SController {

    @EJB
    protected ParkingAreaSExpert expert;

    @EJB
    RestSessionController restSessionController;

    @EJB
    private CredentialManager credentialManager;

    @EJB
    private ClientRepository clientRepository;

    @EJB
    private ClientConfigurationManager clientConfigurationManager;

    public static final String RESOURCE_NAME = "Área de Estacionamiento";

    private Resource resource = null;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId() throws NamingException {
        try {
            return Response.ok().entity(expert.getLastId()).build();
        } catch (Exception ex) {
            return Response.status(500).entity("Error").build();
        }
    }

    @Path("/parkingAreas")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getDays() {
        try {
            return Response.ok().entity(ParkingAreaDto.Factory.getDtosFromParkingArea(expert.findParkingAreas2())).build();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            if(e.getCause() != null){
                System.out.println(e.getCause().getMessage());
            }
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "ups", e);
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/getFromDataBaseParkingSpace")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getFromDataBaseParkingSpace() {
        try {
            expert.updateParkingAreasInfo();
            return Response.ok().build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/webParkingAreas")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getWebParkingAreas(@HeaderParam("Authorization") String sessionId) {
        try {
            validateCredentials(sessionId, getAllowedRoles(),expert);
            return Response.ok().entity(WebParkingAreaDto.Factory.getDtoFrom(expert.findParkingAreas())).build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "Error registrado", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/parkingArea")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response getDays(@HeaderParam("Authorization") String sessionId, PointDto pointDto) {
        try {
            Long clientId = null;
            if(sessionId != null && !sessionId.isEmpty()){
                clientId = credentialManager.findClientId(sessionId);
            }
            List<ParkingArea> pas = expert.findParkingAreas(pointDto, clientId);
            List<ParkingAreaDto> dtos = new ArrayList<>();
            if(pas != null && pas.size()>0){
                for (ParkingArea pa : pas  ) {
                    if(pa.getFromClient() != null && pa.getFromClient().getNumber() != null){
                        Float price = clientConfigurationManager.getParkingSPacePrice(pa.getFromClient().getNumber());
                        String disponibilidad = clientConfigurationManager.getWeeklySchedules(pa.getFromClient().getNumber());
                        ParkingAreaDto dto = ParkingAreaDto.Factory.getDtoFromParkingArea(pa);
                        dto.setPrecio(price);
                        dto.setDisponibilidad(disponibilidad);
                        dtos.add(dto);
                    }
                }
            }
            return Response.ok().entity(dtos).build();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(@HeaderParam("Authorization") String sessionId, WebParkingAreaDto parkingAreaDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return Response.ok().entity(expert.delete(
                    WebParkingAreaDto.Factory.getParkingAreaFrom(parkingAreaDto))).build();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE,"Error registrado",ex);
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(@HeaderParam("Authorization") String sessionId, WebParkingAreaDto parkingAreaDto){
        try {
            validateCredentials(sessionId, getAllowedRoles(),expert);
            ParkingArea result = expert.update(
                    WebParkingAreaDto.Factory.getParkingAreaFrom(parkingAreaDto));
            return Response.ok().entity(WebParkingAreaDto.Factory.getDtoFrom(result)).build();
        } catch (Exception ex){
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE,"Error registrado",ex);
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(
                        ResponseDto.Factory.create(ex.getMessage(),400)
                ).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(@HeaderParam("Authorization") String sessionId, WebParkingAreaDto parkingAreaDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return Response.ok().entity(expert.persist(
                    WebParkingAreaDto.Factory.getParkingAreaFrom(parkingAreaDto))).build();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }

}

