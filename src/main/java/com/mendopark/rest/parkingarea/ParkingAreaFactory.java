package com.mendopark.rest.parkingarea;

import com.mendopark.model.*;
import com.mendopark.repository.ParkingAreaRepository;
import com.mendopark.repository.SensorStateRepository;
import com.mendopark.rest.sensorstate.SensorStateFactory;
import com.mendopark.service.MEndoParkLogger;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.util.*;
import java.util.logging.Level;

@Startup
@Singleton
public class ParkingAreaFactory {

    @EJB
    private ParkingAreaRepository parkingAreaRepository;

    @EJB
    private SensorStateFactory sensorStateFactory;

    @EJB
    private SensorStateRepository sensorStateRepository;
    
    private Map<Long, ParkingArea> memory = new HashMap<Long, ParkingArea>();
    private Map<String, Long> parkingSpaces = new HashMap<String, Long>(); // parkingSpaceName, ClientId
    private Map<String, ParkingSpace> allParkingSpaces = new HashMap<String, ParkingSpace>(); // parkingSpaceName, ParkingSapce
    private Map<String, ParkingSpace> sensorReferenceCodeParkingSpace = new HashMap<String, ParkingSpace>(); // referenceCode, ParkingSpace
    private Map<String, String> parkingSpaceNameReferenceCode = new HashMap<String, String>(); // ParkingSpaceName, referenceCode
    private List<String> sensorsNames = new ArrayList<String>();
    private Long lastNumber;
    
    public Long getLastId() {
        return parkingAreaRepository.getLastId();
    }
    
    public ParkingArea get(Long number){
        return memory.get(number);
    }

    public List<ParkingArea> getAvailables() {
        return new ArrayList<ParkingArea>(memory.values());
    }

    public void addNew(ParkingArea parkingArea) {
        if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() >0){
            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces()) {
                String referenceCode = null;
                if(parkingSpace.getParkingSpaceRegistrations() != null && parkingSpace.getParkingSpaceRegistrations().size() >0){
                    /* Se requiere tener en memoria sólo los registros de plazas válidos, es decir los que no se han
                    dado de baja */
                    List<ParkingSpaceRegistration> parkingSpaceRegistrations = new ArrayList<>();
                    for (ParkingSpaceRegistration resgistration: parkingSpace.getParkingSpaceRegistrations() ) {
                        MEndoParkLogger.MENDOPARK.log(Level.INFO, parkingSpace.toString() + " -> "+resgistration.getEndDate());
                        if(resgistration.getEndDate() == null){
                            if(resgistration.getFromSensor() != null) {
                                sensorsNames.add(resgistration.getFromSensor().getReferenceCode());
                                sensorStateFactory.addAvailableSensor(resgistration.getFromSensor());
                            }
                            parkingSpaceRegistrations.add(resgistration);
                        }
                    }
                    if(parkingSpaceRegistrations != null && parkingSpaceRegistrations.size() > 0){
                        parkingSpace.setParkingSpaceRegistrations(parkingSpaceRegistrations);
                        parkingSpaces.put(parkingSpace.getName(),parkingArea.getFromClient().getNumber());
                        referenceCode = parkingSpaceRegistrations.get(0).getFromSensor().getReferenceCode();
                        sensorReferenceCodeParkingSpace.put(referenceCode, parkingSpace);
                        allParkingSpaces.put(parkingSpace.getName(), parkingSpace);
                    }
                }
                if(referenceCode != null) {
                    parkingSpaceNameReferenceCode.put(parkingSpace.getName(), referenceCode);
                }else {
                    parkingSpaceNameReferenceCode.put(parkingSpace.getName(), parkingSpace.getId());
                }
                SensorState sensorState = sensorStateRepository.findLastSensorStateFromParkingSpace(parkingSpace.getId());
                if(sensorState != null){
                    sensorStateFactory.addState(sensorState);
                } else {
                    SensorState newSensorState = new SensorState();
                    newSensorState.setState(SensorState.State.FREE.getName());
                    newSensorState.setName(parkingSpace.getId());
                    newSensorState.setCreationDate(new Date());
                    sensorStateFactory.addParkingSpaceState(newSensorState);
                }
            }
        }
        memory.put(parkingArea.getNumber(),parkingArea);
        lastNumber = parkingArea.getNumber();
    }

    public void remove (ParkingArea parkingArea){
        parkingArea = memory.remove(parkingArea.getNumber());
        if(parkingArea != null && parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() >0){
            for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces()) {
                if(parkingSpace.getParkingSpaceRegistrations() != null && parkingSpace.getParkingSpaceRegistrations().size() >0){
                    for (ParkingSpaceRegistration resgistration: parkingSpace.getParkingSpaceRegistrations() ) {
                        if(resgistration.getEndDate() != null && resgistration.getFromSensor() != null){
                            sensorsNames.remove(resgistration.getFromSensor().getReferenceCode());
                            sensorStateFactory.removeSensorAvailable(resgistration.getFromSensor());
                            parkingSpaces.remove(parkingSpace.getName());
                        }
                    }
                }
            }
        }
    }

    public Long getClientNumber (String parkingSpaceName) {
        Long clientNumber = null;
        if (parkingSpaces.containsKey(parkingSpaceName)) {
            clientNumber = parkingSpaces.get(parkingSpaceName);
        }
        return clientNumber;
    }

    public Long getClientNumberFromReferenceCode (String referenceCode) {
        Long clientNumber = null;
        if(sensorReferenceCodeParkingSpace.containsKey(referenceCode)){
            ParkingSpace parkingSpace = sensorReferenceCodeParkingSpace.get(referenceCode);
            if(parkingSpace != null && parkingSpace.getName() != null){
                clientNumber = getClientNumber(parkingSpace.getName());
            }
        }
        return clientNumber;
    }

    @PostConstruct
    public void init(){
        List<ParkingArea> parkingAreasList = parkingAreaRepository.getAvailables();
        if(parkingAreasList != null && parkingAreasList.size() > 0){
            for (ParkingArea parkingArea: parkingAreasList ) {
                MEndoParkLogger.MENDOPARK.log(Level.INFO,"Se cargó de la BD -> " + parkingArea.getNumber()
                    + " : " + parkingArea.getName()+ " con "+parkingArea.getParkingSpaces().size() +" plazas de estacionamiento");
                parkingArea.getFromClient();
                addNew(parkingArea);
            }
        }
    }

    public ParkingSpace findParkingSpaceFromSensorReference(String referenceCode){
        ParkingSpace result = null;
        if(sensorReferenceCodeParkingSpace.containsKey(referenceCode)){
            result = sensorReferenceCodeParkingSpace.get(referenceCode);
        }
        return result;
    }

    public String findReferenceCodeFromParkingSpaceName (String parkingSapceName) {
        String result = null;
        if(parkingSpaceNameReferenceCode.containsKey(parkingSapceName)){
            return parkingSpaceNameReferenceCode.get(parkingSapceName);
        }
        return result;
    }

    public ParkingSpace getParkingSpaceByName(String parkingSpaceName){
        return allParkingSpaces.get(parkingSpaceName);
    }

}
