package com.mendopark.rest.parkingarea;

import com.mendopark.model.CoordinatePoint;
import com.mendopark.model.Sensor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SensorDto {

    private String id;
    private String name;
    private String description;
    private Double longitude;
    private Double latitude;
    private Long startDate;
    private String endDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static class Factory {

        public static Sensor getSensorFrom(SensorDto dto) throws ParseException {
            Sensor sensor = new Sensor();
            sensor.setId(dto.getId());
            sensor.setReferenceCode(dto.getName());
            sensor.setDescription(dto.getDescription());
            if(dto.getStartDate() != null){
                sensor.setStartDate(new Date(dto.getStartDate()));
            }
            if(dto.getEndDate() != null){
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                sensor.setEndDate(formatter.parse(dto.getEndDate()));
            }
//            if(dto.getLatitude() != null && dto.getLongitude() != null){
//                CoordinatePoint coordinatePoint = new CoordinatePoint(
//                        dto.getLatitude(), dto.getLongitude()
//                );
//                sensor.setCoordinatePoint(coordinatePoint);
//            }
            return sensor;
        }

        public static SensorDto getDtoFrom (Sensor sensor){
            SensorDto sensorDto = new SensorDto();
            if(sensor != null) {
                sensorDto.setId(sensor.getId());

                sensorDto.setName(sensor.getReferenceCode());
                sensorDto.setDescription(sensor.getDescription());
                if (sensor.getStartDate() != null) {
                    sensorDto.setStartDate(sensor.getStartDate().getTime());
                }
                if (sensor.getEndDate() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    sensorDto.setEndDate(formatter.format(sensor.getEndDate()));
                }
//            if(sensor.getCoordinatePoint() != null){
//                sensorDto.setLatitude(sensor.getCoordinatePoint().getLatitude());
//                sensorDto.setLongitude(sensor.getCoordinatePoint().getLongitude());
//            }
            }
            return sensorDto;
        }

        public static List<SensorDto> getDtosFrom (List<Sensor> sensors){
            List<SensorDto> dtos = new ArrayList<>();
            if(sensors != null && sensors.size() > 0){
                for (Sensor sensor: sensors) {
                    dtos.add(getDtoFrom(sensor));
                }
            }
            return dtos;
        }

    }

}
