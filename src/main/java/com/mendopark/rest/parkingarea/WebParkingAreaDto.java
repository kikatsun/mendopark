package com.mendopark.rest.parkingarea;

import com.mendopark.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WebParkingAreaDto {

    private Long number;
    private String name;
    private String color;
    private List<PolygonPoint> polygonPoints;
    private List<WebParkingSpace> parkingSpaces;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<PolygonPoint> getPolygonPoints() {
        return polygonPoints;
    }

    public void setPolygonPoints(List<PolygonPoint> polygonPoints) {
        this.polygonPoints = polygonPoints;
    }

    public List<WebParkingSpace> getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(List<WebParkingSpace> parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public static class Factory{

        public static ParkingArea getParkingAreaFrom(WebParkingAreaDto webParkingAreaDto){
            ParkingArea parkingArea = new ParkingArea();
            parkingArea.setName(webParkingAreaDto.getName());
            parkingArea.setColor(webParkingAreaDto.getColor());
            parkingArea.setNumber(webParkingAreaDto.getNumber());
            List<ParkingAreaDetail> parkingAreaDetailList = new ArrayList<>();
            if(webParkingAreaDto.getPolygonPoints() != null && webParkingAreaDto.getPolygonPoints().size() > 0){
                for (PolygonPoint webPolygonPoint: webParkingAreaDto.getPolygonPoints() ) {
                    ParkingAreaDetail parkingAreaDetail = new ParkingAreaDetail();
                    parkingAreaDetail.setSequence(webPolygonPoint.getSequence());
                    CoordinatePoint coordinatePoint = new CoordinatePoint(
                            webPolygonPoint.getLatitude(), webPolygonPoint.getLongitude()
                    );
                    parkingAreaDetail.setCoordinatePoint(coordinatePoint);
                    parkingAreaDetailList.add(parkingAreaDetail);
                }
            }
            parkingArea.setParkingAreaDetails(parkingAreaDetailList);
            List<ParkingSpace> parkingSpaceList = new ArrayList<>();
            if(webParkingAreaDto.getParkingSpaces() != null && webParkingAreaDto.getParkingSpaces().size() > 0){
                for (WebParkingSpace webParkingSpace: webParkingAreaDto.getParkingSpaces()) {
                    ParkingSpace parkingSpace = new ParkingSpace();
                    parkingSpace.setId(webParkingSpace.getId());
                    parkingSpace.setName(webParkingSpace.getName());
                    if (webParkingSpace.getSensorId() != null){
                        ParkingSpaceRegistration parkingSpaceRegistration = new ParkingSpaceRegistration();
                        Sensor sensor = new Sensor();
                        sensor.setId(webParkingSpace.getSensorId());
                        parkingSpaceRegistration.setFromSensor(sensor);
                        parkingSpaceRegistration.setFromParkingSpace(parkingSpace);
                        parkingSpace.setParkingSpaceRegistrations(Arrays.asList(parkingSpaceRegistration));
                    }
                    if(webParkingSpace.getPosition() != null){
                        CoordinatePoint coordinatePoint = new CoordinatePoint(
                            webParkingSpace.getPosition().getLatitude(), webParkingSpace.getPosition().getLongitude()
                        );
                        parkingSpace.setCoordinatePoint(coordinatePoint);
                    }
                    if(webParkingSpace.getType() != null){
                        ParkingSpaceType type = new ParkingSpaceType();
                        type.setNumber(webParkingSpace.getType());
                        parkingSpace.setParkingSpaceType(type);
                    }
                    parkingSpaceList.add(parkingSpace);
                }
            }
            parkingArea.setParkingSpaces(parkingSpaceList);

            return parkingArea;
        }

        public static WebParkingAreaDto getDtoFrom(ParkingArea parkingArea) {
            WebParkingAreaDto webParkingAreaDto = new WebParkingAreaDto();

            webParkingAreaDto.setNumber(parkingArea.getNumber());
            webParkingAreaDto.setName(parkingArea.getName());
            webParkingAreaDto.setColor(parkingArea.getColor());
            if(parkingArea.getParkingAreaDetails() != null && parkingArea.getParkingAreaDetails().size() > 0){
                List<PolygonPoint> polygonPointList = new ArrayList<>();
                for (ParkingAreaDetail parkingAreaDetail: parkingArea.getParkingAreaDetails() ) {
                    PolygonPoint polygonPoint = new PolygonPoint();
                    polygonPoint.setSequence(parkingAreaDetail.getSequence());
                    if(parkingAreaDetail.getCoordinatePoint() != null){
                        polygonPoint.setLatitude(parkingAreaDetail.getCoordinatePoint().getLatitude());
                        polygonPoint.setLongitude(parkingAreaDetail.getCoordinatePoint().getLongitude());
                    }
                    polygonPointList.add(polygonPoint);
                }
                webParkingAreaDto.setPolygonPoints(polygonPointList);
            }
            if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0){
                List<WebParkingSpace> webParkingSpacesList = new ArrayList<>();
                for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces() ) {
                    WebParkingSpace webParkingSpace = new WebParkingSpace();
                    webParkingSpace.setId(parkingSpace.getId());
                    webParkingSpace.setName(parkingSpace.getName());
                    if(parkingSpace.getParkingSpaceRegistrations() != null && parkingSpace.getParkingSpaceRegistrations().size() > 0){
                        for (int i = 0; i < parkingSpace.getParkingSpaceRegistrations().size(); i++) {
                            ParkingSpaceRegistration parkingSpaceRegistration = parkingSpace.getParkingSpaceRegistrations().get(i);
                            if(parkingSpaceRegistration.getEndDate() == null){
                                if(parkingSpaceRegistration.getFromSensor() != null) {
                                    Sensor sensor = parkingSpaceRegistration.getFromSensor();
                                    if(sensor.getId() != null && !sensor.getId().isEmpty()) {
                                        webParkingSpace.setSensorId(sensor.getId());
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if(parkingSpace.getCoordinatePoint() != null
                        && parkingSpace.getCoordinatePoint().getLatitude() != null
                        && parkingSpace.getCoordinatePoint().getLongitude() != null){
                        PositionDto positionDto = new PositionDto();
                        positionDto.setLatitude(parkingSpace.getCoordinatePoint().getLatitude());
                        positionDto.setLongitude(parkingSpace.getCoordinatePoint().getLongitude());
                        webParkingSpace.setPosition(positionDto);
                    }
                    if(parkingSpace.getParkingSpaceType() != null){
                        webParkingSpace.setType(parkingSpace.getParkingSpaceType().getNumber());
                    }
                    webParkingSpacesList.add(webParkingSpace);
                }
                webParkingAreaDto.setParkingSpaces(webParkingSpacesList);
            }

            return webParkingAreaDto;
        }

        public static List<WebParkingAreaDto> getDtoFrom(List<ParkingArea> parkingAreas) {
            List<WebParkingAreaDto> result = new ArrayList<>();
            if(parkingAreas != null && parkingAreas.size() > 0) {
                for (ParkingArea parkingArea: parkingAreas ) {
                    result.add(getDtoFrom(parkingArea));
                }
            }
            return result;
        }
    }

}
