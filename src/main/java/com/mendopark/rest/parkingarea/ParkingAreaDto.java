package com.mendopark.rest.parkingarea;

import com.mendopark.model.*;
import com.mendopark.rest.parkingspace.ParkingSpaceDto;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParkingAreaDto {

    private Long number;
    private String name;
    private String color;
    private List<PolygonPointsDto> polygonPoints = new ArrayList<PolygonPointsDto>();
    private List<SensorDto> sensors = new ArrayList<SensorDto>();
    private PolygonPointsDto centerPoint;
    private List<ParkingSpaceDto> parkingSpaces;

    private String disponibilidad;
    private Float precio;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<PolygonPointsDto> getPolygonPoints() {
        return polygonPoints;
    }

    public void setPolygonPoints(List<PolygonPointsDto> polygonPoints) {
        this.polygonPoints = polygonPoints;
    }

    public List<SensorDto> getSensors() {
        return sensors;
    }

    public void setSensors(List<SensorDto> sensors) {
        this.sensors = sensors;
    }

    public PolygonPointsDto getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(PolygonPointsDto centerPoint) {
        this.centerPoint = centerPoint;
    }

    public List<ParkingSpaceDto> getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(List<ParkingSpaceDto> parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public String getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(String disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "ParkingAreaDto{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", polygonPoints=" + polygonPoints +
                ", sensors=" + sensors +
                ", centerPoint=" + centerPoint +
                ", parkingSpaces=" + parkingSpaces +
                ", disponibilidad='" + disponibilidad + '\'' +
                ", precio=" + precio +
                '}';
    }

    public static class Factory{

        public static ParkingArea getParkingAreaFromDto(ParkingAreaDto dto) throws ParseException {
            ParkingArea parkingArea = new ParkingArea();
            parkingArea.setNumber(dto.getNumber());
            parkingArea.setName(dto.getName());
            parkingArea.setColor(dto.getColor());
            List<ParkingAreaDetail> details = new ArrayList<ParkingAreaDetail>();
            if(dto.getPolygonPoints() != null && dto.getPolygonPoints().size() > 0){
                for (PolygonPointsDto pointDto: dto.getPolygonPoints()) {
                    ParkingAreaDetail detail = new ParkingAreaDetail();
                    CoordinatePoint point = new CoordinatePoint(pointDto.getLatitude(), pointDto.getLongitude());
                    detail.setSequence(pointDto.getSequence());
                    detail.setCoordinatePoint(point);
                    detail.setFromParkingArea(parkingArea);
                    details.add(detail);
                }
            }
            parkingArea.setParkingAreaDetails(details);
            if(dto.getParkingSpaces() != null && dto.getParkingSpaces().size() > 0){
                List<ParkingSpace> parkingSpaces = new ArrayList<ParkingSpace>();
                for (ParkingSpaceDto parkingSpaceDto: dto.getParkingSpaces()) {
                    ParkingSpace parkingSpace = new ParkingSpace();
                    parkingSpace.setName(parkingSpaceDto.getName());
                    if(parkingSpaceDto.getSensor() != null){
                        ParkingSpaceRegistration registration = new ParkingSpaceRegistration();
                        registration.setFromSensor(SensorDto.Factory.getSensorFrom(parkingSpaceDto.getSensor()));
                        parkingSpace.setParkingSpaceRegistrations(Arrays.asList(registration));
                    }
                    parkingSpaces.add(parkingSpace);
                }
                parkingArea.setParkingSpaces(parkingSpaces);
            }
            return parkingArea;
        }

        public static List<ParkingAreaDto> getDtosFromParkingArea(List<ParkingArea> parkingAreas){
            List<ParkingAreaDto> dtos = new ArrayList<ParkingAreaDto>();
            if(parkingAreas != null && parkingAreas.size() > 0){
                for (ParkingArea parkingArea: parkingAreas) {
                    dtos.add(getDtoFromParkingArea(parkingArea));
                }
            }
            return dtos;
        }

        public static ParkingAreaDto getDtoFromParkingArea(ParkingArea parkingArea){
            ParkingAreaDto dto = new ParkingAreaDto();
            List<SensorDto> sensorDtos = new ArrayList<SensorDto>();
            dto.setName(parkingArea.getName());
            dto.setNumber(parkingArea.getNumber());
            dto.setColor(parkingArea.getColor());

            if(parkingArea.getCenterPoint() != null){
                CoordinatePoint center = parkingArea.getCenterPoint();
                PolygonPointsDto centerPoint = new PolygonPointsDto();
                centerPoint.setLatitude(center.getLatitude());
                centerPoint.setLongitude(center.getLongitude());
                centerPoint.setSequence(-1);
                dto.setCenterPoint(centerPoint);
            }

            List<PolygonPointsDto> polygonPointsDtos = new ArrayList<PolygonPointsDto>();

            if(parkingArea.getParkingAreaDetails() != null && parkingArea.getParkingAreaDetails().size() >0){
                System.out.println("R");
                for (int i = 0; i < parkingArea.getParkingAreaDetails().size() ; i++) {
                    ParkingAreaDetail detail = parkingArea.getParkingAreaDetails().get(i);
                    PolygonPointsDto pointDto = new PolygonPointsDto();
                    pointDto.setSequence(detail.getSequence());
                    pointDto.setLatitude(detail.getCoordinatePoint().getLatitude());
                    pointDto.setLongitude(detail.getCoordinatePoint().getLongitude());
                    polygonPointsDtos.add(pointDto);
                }
            }
            dto.setPolygonPoints(polygonPointsDtos);
            if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() >0){
                List<ParkingSpaceDto> parkingSpaceDtos = new ArrayList<ParkingSpaceDto>();
                for (ParkingSpace parkingSpace: parkingArea.getParkingSpaces()) {
                    ParkingSpaceDto parkingSpaceDto = new ParkingSpaceDto();
                    parkingSpaceDto.setId(parkingSpace.getId());
                    parkingSpaceDto.setName(parkingSpace.getName());
                    ParkingSpaceRegistration parkingSpaceRegistration = null;
                    if(parkingSpace.getParkingSpaceRegistrations() != null && parkingSpace.getParkingSpaceRegistrations().size() > 0){
                        for (ParkingSpaceRegistration psr: parkingSpace.getParkingSpaceRegistrations() ) {
                            if (psr.getEndDate() == null) {
                                parkingSpaceRegistration = psr;
                                break;
                            }
                        }
                    }
                    SensorDto sensorDto;
                    if (parkingSpaceRegistration != null) {
                        sensorDto = SensorDto.Factory.getDtoFrom(parkingSpaceRegistration.getFromSensor());
                    } else {
                        sensorDto = new SensorDto();
                        sensorDto.setId(parkingSpace.getId());
                        sensorDto.setName(parkingSpace.getName());
                        sensorDto.setDescription(parkingSpace.getName());
                    }
                    if  (parkingSpace.getCoordinatePoint() != null
                        && parkingSpace.getCoordinatePoint().getLatitude() != null
                        && parkingSpace.getCoordinatePoint().getLongitude() != null) {
                        sensorDto.setLatitude(parkingSpace.getCoordinatePoint().getLatitude());
                        sensorDto.setLongitude(parkingSpace.getCoordinatePoint().getLongitude());
                    }
                    parkingSpaceDto.setSensor(sensorDto);
                    sensorDtos.add(sensorDto);
                    parkingSpaceDtos.add(parkingSpaceDto);
                }
                dto.setParkingSpaces(parkingSpaceDtos);
            }
            dto.setSensors(sensorDtos);
            return dto;
        }
    }
}
