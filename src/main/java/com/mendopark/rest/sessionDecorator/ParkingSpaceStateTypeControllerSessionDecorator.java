package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.parkingspacestatetype.ParkingSpaceStateTypeController;
import com.mendopark.rest.parkingspacestatetype.ParkingSpaceStateTypeDto;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/ParkingSpaceStateType")
public class ParkingSpaceStateTypeControllerSessionDecorator extends ParkingSpaceStateTypeController {

    @EJB
    RestSessionController restSessionController;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/parkingSpaceStateType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParkingSpaceStateType(@HeaderParam("Authorization") String sessionId, @PathParam("number") Integer number) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getParkingSpaceStateType(number);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/parkingSpaceStateTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParkingSpaceStateTypes(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getParkingSpaceStateTypes();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(@HeaderParam("Authorization") String sessionId, ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.darDeBajaCategoria(parkingSpaceStateTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(@HeaderParam("Authorization") String sessionId, ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.guardarCategoriaModificado(parkingSpaceStateTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(@HeaderParam("Authorization") String sessionId, ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.guardarCategoria(parkingSpaceStateTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndDescription(@HeaderParam("Authorization") String sessionId, ParkingSpaceStateTypeDto parkingSpaceStateTypeDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.validateNameAndDescription(parkingSpaceStateTypeDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
