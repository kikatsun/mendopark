package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.backup.BackUpController;
import com.mendopark.service.security.Resourceable;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/BackUp")
public class BackUpControllerSessionDecorator extends BackUpController {

    @EJB
    RestSessionController restSessionController;

    public static final List<Integer> allowedRoles = Arrays.asList(1);

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/backup")
    public Response realizarBackUp(@HeaderParam("Authorization") String sessionId) throws NamingException {
        if(restSessionController.validateSession(sessionId, allowedRoles)) {
            return super.realizarBackUp();
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
