package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.locality.LocalityDto;
import com.mendopark.rest.parkingspacestatetype.ParkingSpaceStateTypeController;
import com.mendopark.rest.parkingspacestatetype.ParkingSpaceStateTypeDto;
import com.mendopark.rest.vehicletype.VehicleTypeController;
import com.mendopark.rest.vehicletype.VehicleTypeDto;
import com.mendopark.service.security.Resourceable;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/VehicleType")
public class VehicleTypeControllerSessionDecorator extends VehicleTypeController {

    @EJB
    RestSessionController restSessionController;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/vehicleType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getVehicleType(@HeaderParam("Authorization") String sessionId, @PathParam("number") Integer number) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getVehicleType(number);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/vehicleTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParkingSpaceStateTypes(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getVehicleTypes();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(@HeaderParam("Authorization") String sessionId, VehicleTypeDto vehicleTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.darDeBajaCategoria(vehicleTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(@HeaderParam("Authorization") String sessionId, VehicleTypeDto vehicleTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.guardarCategoriaModificado(vehicleTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
        }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(@HeaderParam("Authorization") String sessionId, VehicleTypeDto vehicleTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.guardarCategoria(vehicleTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndPath(@HeaderParam("Authorization") String sessionId, VehicleTypeDto vehicleTypeDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.validateNameAndDescription(vehicleTypeDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
