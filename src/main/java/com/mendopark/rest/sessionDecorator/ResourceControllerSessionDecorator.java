package com.mendopark.rest.sessionDecorator;

import com.mendopark.model.security.Resource;
import com.mendopark.rest.paytype.PayTypeController;
import com.mendopark.rest.paytype.PayTypeDto;
import com.mendopark.rest.resource.ResourceController;
import com.mendopark.rest.resource.ResourceDto;
import com.mendopark.service.security.Resourceable;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/Resource")
public class ResourceControllerSessionDecorator extends ResourceController {

    @EJB
    RestSessionController restSessionController;

    public static final List<Integer> allowedRoles = Arrays.asList(1);
    public static final List<Integer> allowedRolesToRetrieve = Arrays.asList(1);

    /*@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) throws NamingException {
        if(restSessionController.validateSession(sessionId, allowedRoles)) {
            return super.getLastId();
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }*/

    @Path("/resource/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getResource(@HeaderParam("Authorization") String sessionId, @PathParam("id") String id) {
        if(restSessionController.validateSession(sessionId, getAllRoles())) {
            return super.getResource(id);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Path("/resources")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getResources(@HeaderParam("Authorization") String sessionId) {
        if(restSessionController.validateSession(sessionId, getAllRoles())) {
            return super.getResources();
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Path("/resourcesFromRole/{roleId}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getResourceFromRole(@HeaderParam("Authorization") String sessionId, @PathParam("roleId") Integer id){
        if(restSessionController.validateSession(sessionId, getAllRoles())) {
            return super.getResourceFromRole(id);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndPath(@HeaderParam("Authorization") String sessionId, ResourceDto resourceDto) {
        if(restSessionController.validateSession(sessionId, getAllRoles())) {
            return super.validateNameAndPath(resourceDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(@HeaderParam("Authorization") String sessionId, ResourceDto resourceDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.darDeBajaCategoria(resourceDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(@HeaderParam("Authorization") String sessionId, ResourceDto resourceDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.guardarCategoriaModificado(resourceDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(@HeaderParam("Authorization") String sessionId, ResourceDto resourceDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.guardarCategoria(resourceDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
