package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.paytype.PayTypeController;
import com.mendopark.rest.paytype.PayTypeDto;
import com.mendopark.service.security.Resourceable;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/PayType")
public class PayTypeControllerSessionDecorator extends PayTypeController {

    @EJB
    RestSessionController restSessionController;

    public static final List<Integer> allowedRoles = Arrays.asList(1);
    public static final List<Integer> allowedRolesToRetrieve = Arrays.asList(1,2,3,4,5,6);

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) throws NamingException {
        try{
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/payType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPayType(@HeaderParam("Authorization") String sessionId, @PathParam("number") Long number) {
        try{
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.getPayType(number);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/payTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPayTypes(@HeaderParam("Authorization") String sessionId) {
        try{
            if (restSessionController.validateSession(sessionId, getAllowedRolesToRetrieve())) {
                updateCredentials(sessionId, expert);
                return super.getPayTypes();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(@HeaderParam("Authorization") String sessionId, PayTypeDto payTypeDto) throws NamingException {
        try{
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.darDeBajaCategoria(payTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(@HeaderParam("Authorization") String sessionId, PayTypeDto payTypeDto) throws NamingException {
        try{
            if (restSessionController.validateSession(sessionId, getAllowedRolesToRetrieve())) {
                updateCredentials(sessionId, expert);
                return super.guardarCategoriaModificado(payTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(@HeaderParam("Authorization") String sessionId, PayTypeDto payTypeDto) throws NamingException {
        try{
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                return super.guardarCategoria(payTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndDescription(@HeaderParam("Authorization") String sessionId, PayTypeDto payTypeDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, allowedRoles)) {
            return super.validateNameAndDescription(payTypeDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

    @Override
    public List<Integer> getAllowedRoles() {
        return allowedRoles;
    }

    @Override
    protected List<Integer> getAllowedRolesToRetrieve() {
        return allowedRolesToRetrieve;
    }
}
