package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.parkingspacetype.ParkingSpaceTypeController;
import com.mendopark.rest.parkingspacetype.ParkingSpaceTypeDto;
import com.mendopark.service.security.Resourceable;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
 * */
@Stateless
@LocalBean
@Path("/ParkingSpaceType")
public class ParkingSpaceTypeControllerSessionDecorator extends ParkingSpaceTypeController {

    @EJB
    RestSessionController restSessionController;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/parkingSpaceType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParkingSpaceType(@HeaderParam("Authorization") String sessionId, @PathParam("number") Integer number) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, expert);
            return super.getParkingSpaceType(number);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/parkingSpaceTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParkingSpaceTypes(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, expert);
            return super.getParkingSpaceTypes();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/availables")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getParkingSTs(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, expert);
            return super.getParkingSTs();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(@HeaderParam("Authorization") String sessionId, ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.darDeBajaCategoria(parkingSpaceTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(@HeaderParam("Authorization") String sessionId, ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.guardarCategoriaModificado(parkingSpaceTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(@HeaderParam("Authorization") String sessionId, ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.guardarCategoria(parkingSpaceTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndDescription(@HeaderParam("Authorization") String sessionId, ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.validateNameAndDescription(parkingSpaceTypeDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
