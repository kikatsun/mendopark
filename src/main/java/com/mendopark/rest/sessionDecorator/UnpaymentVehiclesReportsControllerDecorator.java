package com.mendopark.rest.sessionDecorator;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.rest.reports.UnpaymentVehiclesReportsController;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
 * */

@Stateless
@LocalBean
@Path("/UnpaymentVehiclesReports")
public class UnpaymentVehiclesReportsControllerDecorator extends UnpaymentVehiclesReportsController {

    @EJB
    RestSessionController restSessionController;

    @EJB
    CredentialManager credentialManager;


    @Path("/{rangoFecha}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCantidadVehiculosNoPagan(@HeaderParam("Authorization") String sessionId, @PathParam("rangoFecha") String rangoFecha) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                Long idCliente = credentialManager.findClientId(sessionId);
                return super.getCantidadVehiculosNoPagan(rangoFecha, idCliente);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        } catch (Exception ex){
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else{
                throw ex;
            }
        }
    }

}
