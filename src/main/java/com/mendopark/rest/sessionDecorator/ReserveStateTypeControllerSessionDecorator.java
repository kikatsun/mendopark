package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.reservestatetype.ReserveStateTypeController;
import com.mendopark.rest.reservestatetype.ReserveStateTypeDto;
import com.mendopark.service.security.Resourceable;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/ReserveStateType")
public class ReserveStateTypeControllerSessionDecorator extends ReserveStateTypeController {

    @EJB
    RestSessionController restSessionController;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/reserveStateType/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocality(@HeaderParam("Authorization") String sessionId, @PathParam("number") Integer number) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.getReserveStateType(number);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/reserveStateTypes")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocalities(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.getReserveStateTypes();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaLocalidad(@HeaderParam("Authorization") String sessionId, ReserveStateTypeDto reserveStateTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.darDeBajaCategoria(reserveStateTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarLocalidadModificado(@HeaderParam("Authorization") String sessionId, ReserveStateTypeDto reserveStateTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.guardarCategoriaModificado(reserveStateTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarLocalidad(@HeaderParam("Authorization") String sessionId, ReserveStateTypeDto reserveStateTypeDto) throws NamingException {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.guardarCategoria(reserveStateTypeDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndDescription(@HeaderParam("Authorization") String sessionId, ReserveStateTypeDto reserveStateTypeDto) {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.validateNameAndDescription(reserveStateTypeDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
