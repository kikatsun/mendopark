package com.mendopark.rest.sessionDecorator;

import com.mendopark.application.credential.CredentialManager;
import com.mendopark.rest.reports.MedioPagoDto;
import com.mendopark.rest.reports.PaymentTypesReportsController;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/PaymentTypesReports")
public class PaymentTypesReportsControllerDecorator extends PaymentTypesReportsController {

    @EJB
    RestSessionController restSessionController;

    @EJB
    CredentialManager credentialManager;

    @Path("/{rangoFecha}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCantidadMediosDePagoUsado(@HeaderParam("Authorization") String sessionId, @PathParam("rangoFecha") String rangoFecha) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
                Long idCliente = credentialManager.findClientId(sessionId);
                return super.getCantidadMediosDePagoUsado(rangoFecha, idCliente);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

}
