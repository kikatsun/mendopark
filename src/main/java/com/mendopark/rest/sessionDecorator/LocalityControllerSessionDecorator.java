package com.mendopark.rest.sessionDecorator;

import com.mendopark.rest.locality.LocalityController;
import com.mendopark.rest.locality.LocalityDto;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/*
 * -> Administrador del Sistema: 1
 * -> Administrador de Cliente: 2
 * -> Usuario Común: 3
 * -> Encargado: 4
 * -> Usuario Operativo: 5
* */
@Stateless
@LocalBean
@Path("/Locality")
public class LocalityControllerSessionDecorator extends LocalityController{

    @EJB
    RestSessionController restSessionController;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/locality/{number}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocality(@HeaderParam("Authorization") String sessionId, @PathParam("number") Integer number) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLocality(number);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/localities")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocalities(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLocalities();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/localities/{provinceNumber}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLocalities(@HeaderParam("Authorization") String sessionId, @PathParam("provinceNumber") Integer provinceNumber) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, expert);
            return super.getLocalities(provinceNumber);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaLocalidad(@HeaderParam("Authorization") String sessionId, LocalityDto localityDto) {
    try {
        if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
            updateCredentials(sessionId, expert);
            return super.darDeBajaLocalidad(localityDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarLocalidadModificado(@HeaderParam("Authorization") String sessionId, LocalityDto localityDto) {
    try {
        if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
            updateCredentials(sessionId, expert);
            return super.guardarLocalidadModificado(localityDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarLocalidad(@HeaderParam("Authorization") String sessionId, LocalityDto localityDto) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, expert);
            return super.guardarLocalidad(localityDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndPath(@HeaderParam("Authorization") String sessionId, LocalityDto localityDto) {
        if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
            return super.validateNameAndProvince(localityDto);
        }else{
            throw new NotAuthorizedException("Rol No Autorizado");
        }
    }

}
