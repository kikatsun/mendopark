package com.mendopark.rest.sessionDecorator;

import com.mendopark.application.ResponseDto;
import com.mendopark.application.credential.CredentialManager;
import com.mendopark.model.security.Resource;
import com.mendopark.repository.ClientRepository;
import com.mendopark.rest.client.ClientController;
import com.mendopark.rest.client.ClientDto;
import com.mendopark.service.clientconfiguration.ClientConfigurationDto;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/Client")
public class ClientControllerSessionDecorator extends ClientController {

    @EJB
    RestSessionController restSessionController;

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response persist(@HeaderParam("Authorization") String sessionId, ClientDto clientDto) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.persist(clientDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ResponseDto.Factory.create(ex.getCause().getMessage())).build());
            }else if(ex instanceof NotAuthorizedException){
                throw ex;
            }else{
                throw new ServerErrorException(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
                        ex.getMessage()
                ).build());
            }
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response update(@HeaderParam("Authorization") String sessionId,ClientDto clientDto) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.update(clientDto);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ResponseDto.Factory.create(ex.getCause().getMessage())).build());
            }else{
                throw new ServerErrorException(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
                        ex.getCause().getMessage()
                ).build());
            }
        }
    }

    @Path("/delete/{clientNumber}")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    public Response delete(@HeaderParam("Authorization") String sessionId, @PathParam("clientNumber") Long clientNUmber) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.delete(clientNUmber);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else if(ex instanceof NotAuthorizedException){
                throw ex;
            }else{
                throw new ServerErrorException(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
                        ex.getMessage()
                ).build());
            }
        }
    }

    @Path("/configuration")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateClientConfiguration(@HeaderParam("Authorization") String sessionId,ClientConfigurationDto clientConfigurationDto) {
        try{
            if(restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.updateClientConfiguration(clientConfigurationDto);
            }else{
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getClientFromSession(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.getClientFromSession();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }


    @Path("/clients")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getClients(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.getClients();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/lastId")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getLastId(@HeaderParam("Authorization") String sessionId) {
        try {
            if (restSessionController.validateSession(sessionId, getAllowedRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.getLastId();
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

    @Path("/client/{clientNumber}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findClient(@HeaderParam("Authorization") String sessionId, @PathParam("clientNumber") Long clientNumber) {
        try {
            if (restSessionController.validateSession(sessionId, getAllRoles())) {
                updateCredentials(sessionId, clientSExpert);
                updateCredentials(sessionId, userExpert);
                return super.findClient(clientNumber);
            } else {
                throw new NotAuthorizedException("Rol No Autorizado");
            }
        }catch (Exception ex){
            if(ex.getCause() instanceof BadRequestException){
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            }else{
                throw ex;
            }
        }
    }

}
