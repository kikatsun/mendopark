
package com.mendopark.rest.parkingspacetype;

import com.mendopark.model.ParkingSpaceType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ParkingSpaceTypeDto {

	private Integer number;

	private String name;

	private String description;

	private String endDate;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public static class Factory {

		public static ParkingSpaceTypeDto getDtoFromParkingSpaceType(ParkingSpaceType parkingSpaceType) {
			ParkingSpaceTypeDto result = new ParkingSpaceTypeDto();

			result.setNumber(parkingSpaceType.getNumber());

			result.setName(parkingSpaceType.getName());

			result.setDescription(parkingSpaceType.getDescription());

			if (parkingSpaceType.getEndDate() != null){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				//String fechaComoCadena = sdf.format(new Date());
				result.setEndDate( sdf.format(parkingSpaceType.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			return result;
		}

		public static List<ParkingSpaceTypeDto> getDtosFromParkingSpaceTypes(List<ParkingSpaceType> parkingSpaceTypes) {
			List<ParkingSpaceTypeDto> result = new ArrayList<ParkingSpaceTypeDto>();

			for (ParkingSpaceType parkingSpaceType : parkingSpaceTypes) {
				result.add(getDtoFromParkingSpaceType(parkingSpaceType));
			}

			return result;
		}

		public static ParkingSpaceType getParkingSpaceTypeFromDto(ParkingSpaceTypeDto parkingSpaceTypeDto) {
			ParkingSpaceType result = new ParkingSpaceType();

			result.setNumber(parkingSpaceTypeDto.getNumber());

			result.setName(parkingSpaceTypeDto.getName());

			result.setDescription(parkingSpaceTypeDto.getDescription());

			return result;
		}
	}

}
