
package com.mendopark.rest.parkingspacetype;

import com.mendopark.crud.parkingspacetype.ParkingSpaceTypeSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/ParkingSpaceType")
public class ParkingSpaceTypeController extends SController {

	@EJB
	protected ParkingSpaceTypeSExpert expert;

	public static final String RESOURCE_NAME = "Tipo Plaza de Estacionamiento";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() throws NamingException {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/parkingSpaceType/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getParkingSpaceType(@PathParam("number") Integer number) {
		try {
			ParkingSpaceTypeDto dto = new ParkingSpaceTypeDto();
			dto.setNumber(number);
			return Response.ok().entity(ParkingSpaceTypeDto.Factory.getDtoFromParkingSpaceType(expert.find(dto)))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/parkingSpaceTypes")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getParkingSpaceTypes() {
		try {
			return Response.ok()
					.entity(ParkingSpaceTypeDto.Factory.getDtosFromParkingSpaceTypes(expert.findParkingSpaceTypes()))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/availables")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getParkingSTs() {
		try {
			return Response.ok()
					.entity(ParkingSpaceTypeDto.Factory.getDtosFromParkingSpaceTypes(expert.findParkingSpaceTypesS()))
					.build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.delete(parkingSpaceTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
		try {
			return Response.ok().entity(expert.update(parkingSpaceTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(ParkingSpaceTypeDto parkingSpaceTypeDto) throws NamingException {
		try {

			return Response.ok().entity(expert.persist(parkingSpaceTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateNameAndDescription(ParkingSpaceTypeDto parkingSpaceTypeDto) {
		try {
			return Response.ok().entity(expert.validateNameAndDescription(parkingSpaceTypeDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}
	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}

}
