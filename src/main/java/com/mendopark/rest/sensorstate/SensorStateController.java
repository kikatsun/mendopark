package com.mendopark.rest.sensorstate;

import com.mendopark.model.SensorState;
import com.mendopark.sensorstate.SensorStateSExpert;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;

@Stateless
@LocalBean
@Path("/sensorstate")
public class SensorStateController {

    @EJB
    private SensorStateSExpert sensorStateSExpert;
    @EJB
    private SensorStateFactory sensorStateFactory;

    @Path("/")
    @POST
    @Produces({MediaType.TEXT_PLAIN})
    public Response saveSensorState(String data) {
        try {
            SensorState sensorState = SensorStateDto.Factory.getSensorStateFromString(data);
            sensorStateSExpert.persist(sensorState);
            return Response.ok().build();
        } catch (Exception ex) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "No se pudo registar usuario", ex);
            if (ex.getCause() instanceof BadRequestException) {
                throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(ex.getCause().getMessage()).build());
            } else {
                throw ex;
            }
        }
    }

    @Path("/fromUser")
    @POST
    @Produces({MediaType.TEXT_PLAIN})
    public Response saveSensorStateFromUser(String data) {
        try {
            SensorState sensorState = SensorStateDto.Factory.getSensorStateFromAndroidRetrofitString(data);
            sensorStateSExpert.saveSensorStateFromUser(sensorState);
            return Response.ok().build();
        } catch (Exception e) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "No se pudo ingresar "+data,e);
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/{parkingSpaceId}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getState(@PathParam("parkingSpaceId") String parkingSpaceId){
        try {
            return Response.ok().entity(SensorStateDto.Factory.getDtoFromSensorState(sensorStateFactory.getStateFromParkingSpaceId(parkingSpaceId))).build();
        } catch (Exception e) {
            MEndoParkLogger.MENDOPARK.log(Level.SEVERE, "No se pudo ingresar ",e);
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/sensors")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSensors(){
        try {
            return Response.ok().entity(SensorStateDto.Factory.getDtosFromSensorStates(sensorStateFactory.getSensors())).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }


    @Path("/sensorReferencesCodeAvailable")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getsensorReferencesCodeAvailable(){
        try {
            return Response.ok().entity(sensorStateFactory.sensorReferencesCodeAvailable).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
    @Path("/sensorsAvailable")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getsensorsAvailable(){
        try {
            return Response.ok().entity(sensorStateFactory.sensorsAvailable).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
    @Path("/sensorIdsAvailable")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getsensorIdsAvailable(){
        try {
            return Response.ok().entity(sensorStateFactory.sensorIdsAvailable).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
    @Path("/currentSensorStates")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getcurrentSensorStates(){
        try {
            return Response.ok().entity(sensorStateFactory.currentSensorStates).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
    @Path("/sensorReserved")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getsensorReserved(){
        try {
            return Response.ok().entity(sensorStateFactory.sensorReserved).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
    @Path("/manuallyBusySensor")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getmanuallyBusySensor(){
        try {
            return Response.ok().entity(sensorStateFactory.manuallyBusySensor).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
}
