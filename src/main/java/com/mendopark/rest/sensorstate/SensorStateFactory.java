package com.mendopark.rest.sensorstate;

import com.mendopark.model.ParkingSpace;
import com.mendopark.model.Sensor;
import com.mendopark.model.SensorState;
import com.mendopark.application.parking.ParkingFactory;
import com.mendopark.repository.SensorRepository;
import com.mendopark.rest.parkingarea.ParkingAreaFactory;
import com.mendopark.service.clientconfiguration.ClientConfigurationManager;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.*;

import static com.mendopark.model.SensorState.State;

@Singleton
@Startup
public class SensorStateFactory {

    @EJB
    private ParkingFactory parkingFactory;
    @EJB
    private ParkingAreaFactory parkingAreaFactory;
    @EJB
    private ClientConfigurationManager clientConfigurationManager;

    @EJB
    private SensorRepository sensorRepository;

    public List<String> sensorReferencesCodeAvailable = new ArrayList<>();
    public Map<String, Sensor> sensorsAvailable = new HashMap<>();
    public List<String> sensorIdsAvailable = new ArrayList<>();

    public Map<String,SensorState> currentSensorStates = new HashMap<String, SensorState>(); //acA SE DEBE AGREGAR LOS ESTADOS
    public List<String> sensorReserved = new ArrayList<String>();

    public Map<String, Long> manuallyBusySensor = new HashMap<String, Long>();

    public void addState(SensorState sensorState){
        String sensorName = sensorState.getName();
        if(sensorReserved.contains(sensorName) &&
            getState(sensorState.getName()).getState().equals(SensorState.State.FREE.name()) &&
            sensorState.getState().equals(SensorState.State.BUSSY.name())){
            /* Iniciar Estacionamiento */
            parkingFactory.createParking(sensorState);
            currentSensorStates.put(sensorName, sensorState);
        }else if(sensorReserved.contains(sensorName) &&
                getState(sensorState.getName()).getState().equals(SensorState.State.BUSSY.name()) &&
                sensorState.getState().equals(SensorState.State.FREE.name())){
            /* Finaliza estacionamiento */
            parkingFactory.endParking(sensorState);
            sensorReserved.remove(sensorName);
        }else if(!manuallyBusySensor.containsKey(sensorName)){
            currentSensorStates.put(sensorName, sensorState);
            if(sensorState.getState().equals(SensorState.State.BUSSY.name())){
                parkingFactory.createParking(sensorState);
            }else if(sensorState.getState().equals(SensorState.State.FREE.name())){
                parkingFactory.endParking(sensorState);
            }
        }
    }
    
    public void addParkingSpaceState (SensorState sensorState) {

        if (getState(sensorState.getName()) == null) {
            updateParkingSpace(sensorState);
        }
        
    }

    /**
    * @Deprecated preferir updateParkingSpace
    * */
    public void addManuallyState(SensorState sensorState){
        if(sensorState.getState().equals(SensorState.State.BUSSY.name())){
            manuallyBusySensor.put(sensorState.getName(), sensorState.getCreationDate().getTime());
            parkingFactory.createParking(sensorState);
        }else if(manuallyBusySensor.containsKey(sensorState.getName()) && sensorState.getState().equals(SensorState.State.FREE.name())){
            manuallyBusySensor.remove(sensorState.getName());
        }
        addState(sensorState);
    }

    public SensorState getState(String name){
        return currentSensorStates.get(name);
    }

    public List<SensorState> getSensors() {
        return new ArrayList<SensorState>(currentSensorStates.values());
    }

    public void updateParkingSpaceFromSensor(SensorState newSensorState) {
        if (evaluateManualBlockParkingSpace(newSensorState)) {
            updateParkingSpace(newSensorState);
        }
    }

    public void updateParkingSpace(SensorState newSensorState){
        State newState = findState(newSensorState.getState());
        if(newState != null){
            SensorState currentSensorState = currentSensorStates.get(newSensorState.getName());
            if (currentSensorState != null) {
                /* No se valida el currentState porque siempre se hace cuando se actualiza */
                State currentState = findState(currentSensorState.getState());
                if (currentState.equals(State.FREE) && newState.equals(State.RESERVED)) {
                    parkingFactory.addParkingSpaceReserved(newSensorState);
                } else if ((currentState.equals(State.RESERVED) ||currentState.equals(State.FREE))
                    && newState.equals(State.BUSSY)) {
                    parkingFactory.createParking(newSensorState);
                } else if (currentState.equals(State.RESERVED) && newState.equals(State.FREE)) {
                    parkingFactory.cancelReserve(newSensorState);
                } else if (currentState.equals(State.BUSSY) && newState.equals(State.RESERVED)) {
                    // Se loguea grave error no debería reservar si está ocupado
                } else if (currentState.equals(State.BUSSY) && newState.equals(State.FREE)) {
                    parkingFactory.endParking(newSensorState);
                } else {
                    //Se loguea que no se pudo hacer update.
                }
            }else{
                if(newState.equals(State.RESERVED)){
                    parkingFactory.addParkingSpaceReserved(newSensorState);
                }
                if(newState.equals(State.BUSSY)){
                    parkingFactory.createParking(newSensorState);
                }
            }
            System.out.println("ERICKKKKKKKKKKKKKKKKKKKKKKKK Se agregó el sensor con nombre "+newSensorState.getName()+ "con estado: "+newSensorState.getState());
            currentSensorStates.put(newSensorState.getName(), newSensorState);
        }else{
            // Se loguea q hubo un estado no válido.
        }
    }

    public void updateParkingSpaceFromUser (SensorState newSensorState) {
        State newState = findState(newSensorState.getState());
        if (State.BUSSY.equals(newState)) {
            manuallyBusySensor.put(newSensorState.getName(), System.currentTimeMillis());
        } else if (State.FREE.equals(newState) && manuallyBusySensor.containsKey(newSensorState.getName())) {
            manuallyBusySensor.remove(newSensorState.getName());
        }
        updateParkingSpace(newSensorState);
    }

    private State findState(String stateAsString) {
        State targetState = null;
        State[] states = State.values();
        if(states != null && states.length > 0){
            for (int i=0; i<states.length; i++ ) {
                if(states[i].getName().equals(stateAsString)){
                    targetState = states[i];
                    break;
                }
            }
        }
        return targetState;
    }

    /**
     * Evalúa si la plaza de estacionamiento ha sido bloqueado manualmente por el usuario, en caso que esté bloqueado
     * se evalúa el tiempo de bloqueo manual definido en la configuración del cliente para poder desbloquearlo en caso
     * sea necesario.
     *
     * @param newSensorState
     * @return true cuando no está bloqueado, false cuando está bloqueado
     */
    private boolean evaluateManualBlockParkingSpace (SensorState newSensorState) {
        boolean result = true;
        if (manuallyBusySensor.containsKey(newSensorState.getName())) {
            Long clientNumber = parkingAreaFactory.getClientNumberFromReferenceCode(newSensorState.getName());
            Long parkingSpaceBloquedTime = manuallyBusySensor.get(newSensorState.getName());
            Long parkingSpaceCurrentTime = newSensorState.getCreationDate().getTime();
            Long timeToEvaluate;
            if (clientNumber != null) {
                timeToEvaluate = clientConfigurationManager.getManualParkingSpaceLockTime(clientNumber);
                if (parkingSpaceCurrentTime - parkingSpaceBloquedTime > timeToEvaluate) {
                    manuallyBusySensor.remove(newSensorState.getName());
                }else {
                    result = false;
                }
            }else {
                // se loguea error
            }
        }
        return result;
    }

    public void addAvailableSensor(Sensor sensor){
        if(sensor.getEndDate() == null && sensor.getId() != null) {
            sensorReferencesCodeAvailable.add(sensor.getReferenceCode());
            sensorIdsAvailable.add(sensor.getId());
            sensorsAvailable.put(sensor.getReferenceCode(), sensor);
        }else{
            System.out.println("NO SE PUEDE AGREGAR SENSOR DADO DE BAJA: "+sensor.getReferenceCode());
        }
    }

    public boolean isSensorAvailable (Sensor sensor){
        return sensorReferencesCodeAvailable.contains(sensor.getReferenceCode());
    }

    public boolean isSensorAvailableById (String sensorId){
        return sensorIdsAvailable.contains(sensorId);
    }

    public boolean isSensorAvailableByReferenceCode(String referenceCode){
        return sensorReferencesCodeAvailable.contains(referenceCode);
    }

    public void removeSensorAvailable(Sensor sensorToRemove) {
        if(sensorToRemove.getEndDate() != null && sensorToRemove.getId() != null){
            sensorReferencesCodeAvailable.remove(sensorToRemove.getReferenceCode());
            sensorIdsAvailable.remove(sensorToRemove.getId());
        }else{
            System.out.println("NO SE PUEDE ELIMINAR SENSOR NO TIENE FECHA DE BAJA: "+sensorToRemove.getReferenceCode());
        }
    }

    public void updateSensor(String oldName, Sensor updatedSensor){
        if(sensorReferencesCodeAvailable.contains(oldName)){
            sensorReferencesCodeAvailable.remove(oldName);
        }
        addAvailableSensor(updatedSensor);
    }

    public void removeSensorsAvailable(List<Sensor> sensorsToRemove) {
        if(sensorsToRemove.size() > 0){
            for (Sensor sensorToRemove: sensorsToRemove ) {
                removeSensorAvailable(sensorToRemove);
            }
        }
    }

    @PostConstruct
    public void init(){
        List<Sensor> persistedSensors = sensorRepository.getAvailables();
        if(persistedSensors != null && persistedSensors.size() > 0){
            for (Sensor persistedSensor: persistedSensors ) {
                addAvailableSensor(persistedSensor);
            }
        }
    }

    public SensorState getStateFromParkingSpaceId(String parkingSpaceId) {
//        SensorState result = null;
//        String referenceCode = parkingAreaFactory.findReferenceCodeFromParkingSpaceName(parkingSpaceName);
//        if(referenceCode != null){
//            result = getState(referenceCode);
//            if(result != null) {
//                result.setName(parkingSpaceName);
//            }
//        }
        return currentSensorStates.get(parkingSpaceId);
    }
}
