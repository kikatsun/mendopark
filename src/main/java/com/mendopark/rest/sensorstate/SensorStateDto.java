package com.mendopark.rest.sensorstate;

import com.mendopark.model.SensorState;

import java.util.ArrayList;
import java.util.List;

public class SensorStateDto {

    public static class State{
        public static final String FREE = "LIBRE";
        public static final String BUSSY = "OCUPADO";
    }

    private String name;
    private String state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public static class Factory{
        public static SensorState getSensorStateFromDto(SensorStateDto dto){
            SensorState sensorState = new SensorState();
            sensorState.setName(dto.getName());
            sensorState.setState(dto.getState());
            return sensorState;
        }

        public static SensorState getSensorStateFromString(String data){
            SensorState result = null;
            if(data.contains("|")){
                result = new SensorState();
                result.setName(data.substring(0,data.indexOf("|")).trim());
                result.setState(data.substring(data.indexOf("|")+1).trim());
            }
            return result;
        }

        public static SensorState getSensorStateFromAndroidRetrofitString(String data){
            data = data.replaceAll("\"","");
            return getSensorStateFromString(data);
        }

        public static SensorStateDto getDtoFromSensorState(SensorState state) {
            SensorStateDto result = null;
            if(state != null) {
                result = new SensorStateDto();
                result.setName(state.getName());
                result.setState(state.getState());
            }
            return result;
        }

        public static List<SensorStateDto> getDtosFromSensorStates(List<SensorState> states){
            List<SensorStateDto> result = new ArrayList<SensorStateDto>();
            for(int i=0; i<states.size();i++){
                result.add(getDtoFromSensorState(states.get(i)));
            }
            return result;
        }
    }
}
