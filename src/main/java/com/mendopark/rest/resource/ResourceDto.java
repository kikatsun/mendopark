package com.mendopark.rest.resource;

import com.mendopark.model.security.Resource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResourceDto {

    private String id;
    private String name;
    private String description;
    private String path;
    private String endDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static class Factory{

        public static Resource getResourceFromDto(ResourceDto resourceDto) {
            Resource result = new Resource();

            result.setId(resourceDto.getId());
            result.setName(resourceDto.getName());
            result.setDescription(resourceDto.getDescription());
            result.setPath(resourceDto.getPath());

            return result;
        }

        public static ResourceDto getDtofromResource(Resource resource){
            ResourceDto result = new ResourceDto();

            result.setId(resource.getId());
            result.setName(resource.getName());
            result.setDescription(resource.getDescription());
            result.setPath(resource.getPath());

            if (resource.getEndDate() != null) {
                result.setEndDate(parseFechaToString(resource.getEndDate()));
            } else {
                result.setEndDate(null);
            }


            return result;
        }

        public static List<ResourceDto> getDtosFromResources(List<Resource> resources){
            List<ResourceDto> result = new ArrayList<ResourceDto>();

            for (Resource resource: resources) {
                result.add(getDtofromResource(resource));
            }

            return result;
        }

        /**
         * Permite convertir un String en fecha (Date).
         * @param fecha Cadena de fecha dd/MM/yyyy
         * @return Objeto Date
         */
        public static Date parseFechaToDate(String fecha)
        {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaDate = null;
            try {
                fechaDate = formato.parse(fecha);
            } catch (ParseException ex) {
                System.out.println(ex);
            }
            return fechaDate;
        }


        /**
         * Permite convertir una fecha Date en String.
         * @param fecha Date (dd/MM/yyyy)
         * @return Objeto String
         */
        public static String parseFechaToString(Date fecha){
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            String fechaDate = null;
            fechaDate = formato.format(fecha);

            return fechaDate;
        }
    }
}
