package com.mendopark.rest.resource;

import com.mendopark.crud.paytype.PayTypeSExpert;
import com.mendopark.crud.reports.ReportSExpert;
import com.mendopark.crud.resource.ResourceSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.paytype.PayTypeDto;
import com.mendopark.rest.user.UserDto;
import com.mendopark.service.MEndoParkLogger;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;

@Stateless
@LocalBean
@Path("/Resource")
public class ResourceController extends SController {

    @EJB
    private ResourceSExpert expert;

    public static final String RESOURCE_NAME = "Menú";

    private Resource resource = null;

    /*@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/lastId")
    public Response getLastId() throws NamingException {
        try {
            return Response.ok().entity(expert.getLastId()).build();
        }catch(Exception ex){
            return Response.status(500).entity("Error").build();
        }
    }*/

    @Path("/resource/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getResource(@PathParam("id") String id){
        try {
            ResourceDto dto = new ResourceDto();
            dto.setId(id);
            return Response.ok().entity(ResourceDto.Factory.getDtofromResource(expert.find(dto))).build();
        } catch (Exception e) {
            return Response.status(500).entity(null).build();
        }
    }

    @Path("/resourcesFromRole/{roleId}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getResourceFromRole(@PathParam("roleId") Integer id){
        try {
            return Response.ok().entity(ResourceDto.Factory.getDtosFromResources(expert.getResourcesFromRole(id))).build();
        } catch (Exception e) {
            return Response.status(500).entity(null).build();
        }
    }

    @Path("/resources")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getResources() {
        try {
            return Response.ok().entity(ResourceDto.Factory.getDtosFromResources(expert.findAllResources())).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/validate")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateNameAndPath(ResourceDto resourceDto) {
        try {
            return Response.ok().entity(expert.validateNameAndPath(resourceDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/delete")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response darDeBajaCategoria(ResourceDto resourceDto) throws NamingException{
        try {
            return Response.ok().entity(expert.delete(resourceDto)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }

    @Path("/update")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoriaModificado(ResourceDto resourceDto) throws NamingException{
        try{
            return Response.ok().entity(expert.update(resourceDto)).build();
        }
        catch(Exception e){
            return Response.status(500).entity(e.getLocalizedMessage()).build();
        }
    }

    @Path("/persist")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response guardarCategoria(ResourceDto resourceDto) throws NamingException{
        try{

            return Response.ok().entity(expert.persist(resourceDto)).build();
        }
        catch(Exception e){
            return Response.status(500).entity(false).build();
        }
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }

}
