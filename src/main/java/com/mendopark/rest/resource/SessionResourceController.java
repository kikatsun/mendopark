package com.mendopark.rest.resource;

import com.mendopark.application.config.AplicationConfig;
import com.mendopark.model.Role;
import com.mendopark.service.security.Resourceable;
import com.mendopark.service.security.RoleResourceManager;
import com.mendopark.session.RestSessionController;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Stateless
@LocalBean
@Path("/SessionResource")
public class SessionResourceController {

    private Map<Integer, ResourceDto> resources = null;

    @EJB
    private RestSessionController restSessionController;


    @EJB
    private RoleResourceManager roleResourceManager;

    @Path("/{roleId}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response get(@PathParam("roleId") Integer roleId) {
        try {
            return Response.ok().entity(roleResourceManager.gett(roleId)).build();
        } catch (Exception e) {
            return Response.status(500).entity(false).build();
        }
    }
//
//    @GET
//    @Produces({MediaType.APPLICATION_JSON})
//    @Path("/{roleId}")
//    public Response get(@PathParam("roleId") int roleId) {
//
//        try {
//            if(resources == null){
//                updateResources();
//            }
//            return Response.ok().entity(this.resources.get(roleId)).build();//).build();
//        } catch (Exception ex) {
//            return Response.status(500).entity("Error").build();
//        }
//    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/session/{sessionId}")
    public Response getFromSession(@PathParam("sessionId") String sessionId){
        try{
            if(resources == null){
                //updateResources();
            }
            Role sessionRole = restSessionController.getRoleFromSession(sessionId);
            if(sessionRole != null) {
                return get(sessionRole.getNumber().intValue());
            }else{
                return Response.status(200).entity(null).build();
            }
        }catch (Exception ex){
            return Response.status(500).entity("Error").build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/role/{sessionId}")
    public Response getRoleFromSession(@PathParam("sessionId") String sessionId){
        try{
            Role sessionRole = restSessionController.getRoleFromSession(sessionId);
            if(sessionRole != null) {
                return Response.status(200).entity(sessionRole.getNumber()).build();
            }else{
                return Response.status(200).entity(null).build();
            }
        }catch (Exception ex){
            return Response.status(500).entity("Error").build();
        }
    }

    /*
    private void updateResources() throws IllegalAccessException, InstantiationException{
        List<ResourceDto.Resource> resourceList = new ArrayList<ResourceDto.Resource>();
        List<Class<?>> classes = AplicationConfig.resources;
        for (Class<?> clazz: classes ) {
            if(clazz.getInterfaces().length > 0){
                for(int i=0; i<clazz.getInterfaces().length; i++){
                    Class<?> otro = clazz.getInterfaces()[i];
                    if(otro.equals(Resourceable.class)){
                        Resourceable resourceable = (Resourceable) clazz.newInstance();
                        List<Integer> allowedRoles = resourceable.getAllowedRoles();
                        if(allowedRoles != null && allowedRoles.size() >0){
                            for (int j = 0; j < allowedRoles.size(); j++) {
                                ResourceDto.Resource resource = new ResourceDto.Resource<Integer, String>(
                                        allowedRoles.get(j),
                                        clazz.getAnnotation(Path.class).value()
                                );
                                resourceList.add(resource);
                            }
                        }
                    }
                }
            }
        }
        this.resources = ResourceDto.Factory.getDto(resourceList);
    }*/
}
