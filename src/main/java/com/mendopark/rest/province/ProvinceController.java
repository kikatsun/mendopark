
package com.mendopark.rest.province;

import com.mendopark.crud.province.ProvinceSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.locality.LocalityDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/Province")
public class ProvinceController extends SController {

	@EJB
	protected ProvinceSExpert expert;

	public static final String RESOURCE_NAME = "Provincia";

	private Resource resource = null;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/lastId")
	public Response getLastId() {
		try {
			return Response.ok().entity(expert.getLastId()).build();
		} catch (Exception ex) {
			return Response.status(500).entity("Error").build();
		}
	}

	@Path("/province/{number}")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getProvince(@PathParam("number") Integer number) {
		try {
			ProvinceDto dto = new ProvinceDto();
			dto.setNumber(number);
			return Response.ok().entity(ProvinceDto.Factory.getDtoFromProvince(expert.find(dto))).build();
		} catch (Exception e) {
			return Response.status(500).entity(null).build();
		}
	}

	@Path("/provinces")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getProvinces() {
		try {
			return Response.ok().entity(ProvinceDto.Factory.getDtosFromProvinces(expert.findProvinces())).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/provincesAvailables")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getProvincesAvailables() {
		try {
			return Response.ok().entity(ProvinceDto.Factory.getDtosFromProvinces(expert.findProvincesAvailables())).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/delete")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response darDeBajaCategoria(ProvinceDto provinceDto) {
		try {
			return Response.ok().entity(expert.delete(provinceDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/update")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoriaModificado(ProvinceDto provinceDto) {
		try {
			return Response.ok().entity(expert.update(provinceDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}

	@Path("/persist")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response guardarCategoria(ProvinceDto provinceDto) {
		try {

			return Response.ok().entity(expert.persist(provinceDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Path("/validate")
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response validateName(ProvinceDto provinceDto) {
		try {
			return Response.ok().entity(expert.validateName(provinceDto)).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}
	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}

}
