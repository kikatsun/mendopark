
package com.mendopark.rest.province;

import com.mendopark.model.Province;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ProvinceDto {

	private Integer number;
	private String name;
	private String endDate;


	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public static class Factory {

		public static ProvinceDto getDtoFromProvince(Province province) {
			ProvinceDto result = new ProvinceDto();

			result.setNumber(province.getNumber());

			result.setName(province.getName());

			if (province.getEndDate() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				result.setEndDate(sdf.format(province.getEndDate()));
			} else {
				result.setEndDate(null);
			}

			return result;
		}

		public static List<ProvinceDto> getDtosFromProvinces(List<Province> provinces) {
			List<ProvinceDto> result = new ArrayList<ProvinceDto>();

			for (Province province : provinces) {
				result.add(getDtoFromProvince(province));
			}

			return result;
		}

		public static Province getProvinceFromDto(ProvinceDto provinceDto) {
			Province result = new Province();

			result.setNumber(provinceDto.getNumber());

			result.setName(provinceDto.getName());

			return result;
		}
	}

}
