package com.mendopark.rest.reports;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MedioPagoDto {

    private String fecha;
    private String nombreMedioPago;
    private int cantidad;
    private Long idCliente;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreMedioPago() {
        return nombreMedioPago;
    }

    public void setNombreMedioPago(String nombreMedioPago) {
        this.nombreMedioPago = nombreMedioPago;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }


    public static class Factory{

        public static List<MedioPagoDto> getDtosFromMediosPago(List<Object[]> mediosPago) {
            List<MedioPagoDto> result = new ArrayList<MedioPagoDto>();

            for (Object[] zona: mediosPago ) {
                MedioPagoDto medioPagoDto = new MedioPagoDto();

                medioPagoDto.setCantidad( ((BigInteger)zona[0]).intValue());
                medioPagoDto.setNombreMedioPago((String)zona[1]);
                result.add(medioPagoDto);
            }

            return result;
        }


        /**
         * Permite convertir un String en fecha (Date).
         * @param fecha Cadena de fecha dd/MM/yyyy
         * @return Objeto Date
         */
        public static Date parseFechaToDate(String fecha)
        {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaDate = null;
            try {
                fechaDate = formato.parse(fecha);
            } catch (ParseException ex) {
                System.out.println(ex);
            }
            return fechaDate;
        }


        /**
         * Permite convertir una fecha Date en String.
         * @param fecha Date (dd/MM/yyyy)
         * @return Objeto String
         */
        public static String parseFechaToString(Date fecha){
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            String fechaDate = null;
            fechaDate = formato.format(fecha);

            return fechaDate;
        }
    }

}
