package com.mendopark.rest.reports;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class VehiculoPorZonaDto {

    private String fecha;
    private String nombreZona;
    private int cantidad;
    private Long idCliente;

    public String getNombreZona() {
        return nombreZona;
    }

    public void setNombreZona(String nombreZona) {
        this.nombreZona = nombreZona;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }


    public static class Factory{

        /*public static List<VehiculoPorZonaDto> getDtosFromVehiculo(VehiculoPorZonaDto dto){
            List<VehiculoPorZonaDto> result = new ArrayList<VehiculoPorZonaDto>();

            String rangoFecha = dto.getFecha();

            String fechaDesde = null;
            String fechaHasta = null;
            String[] parts = rangoFecha.split("-");

            fechaDesde = parts[0].substring(0, 2) + "/" + parts[0].substring(2, 4) + "/" + parts[0].substring(4); // fecha Desde
            fechaHasta = parts[1].substring(0, 2) + "/" + parts[1].substring(2, 4) + "/" + parts[1].substring(4); // fecha Hasta

            VehiculoPorZonaDto dto1 = new VehiculoPorZonaDto();
            dto1.setFecha("16/12/2019");
            dto1.setZona("Zona A");
            dto1.setCantidad(20);

            VehiculoPorZonaDto dto2 = new VehiculoPorZonaDto();
            dto2.setFecha("16/12/2019");
            dto2.setZona("Zona B");
            dto2.setCantidad(100);

            VehiculoPorZonaDto dto3 = new VehiculoPorZonaDto();
            dto3.setFecha("14/12/2019");
            dto3.setZona("Zona C");
            dto3.setCantidad(40);

            VehiculoPorZonaDto dto4 = new VehiculoPorZonaDto();
            dto4.setFecha("14/12/2019");
            dto4.setZona("Zona D");
            dto4.setCantidad(10);

            VehiculoPorZonaDto dto5 = new VehiculoPorZonaDto();
            dto5.setFecha("14/12/2019");
            dto5.setZona("Zona E");
            dto5.setCantidad(80);

            Calendar cal1 = new GregorianCalendar();
            Calendar rangoDesde = new GregorianCalendar();
            Calendar rangoHasta = new GregorianCalendar();

            rangoDesde.setTime(parseFechaToDate(fechaDesde));
            rangoHasta.setTime(parseFechaToDate(fechaHasta));

            cal1.setTime(parseFechaToDate(dto1.getFecha()));
            if (cal1.after(rangoDesde) && cal1.before(rangoHasta)) {
                result.add(dto1);
            }

            cal1.setTime(parseFechaToDate(dto2.getFecha()));
            if (cal1.after(rangoDesde) && cal1.before(rangoHasta)){
                result.add(dto2);
            }

            cal1.setTime(parseFechaToDate(dto3.getFecha()));
            if (cal1.after(rangoDesde) && cal1.before(rangoHasta)){
                result.add(dto3);
            }

            cal1.setTime(parseFechaToDate(dto4.getFecha()));
            if (cal1.after(rangoDesde) && cal1.before(rangoHasta)){
                result.add(dto4);
            }

            cal1.setTime(parseFechaToDate(dto5.getFecha()));
            if (cal1.after(rangoDesde) && cal1.before(rangoHasta)){
                result.add(dto5);
            }

            return result;
        }*/

        public static List<VehiculoPorZonaDto> getDtosFromVehiculo(List<Object[]> vehiculosPorZona) {
            List<VehiculoPorZonaDto> result = new ArrayList<VehiculoPorZonaDto>();

            for (Object[] zona: vehiculosPorZona ) {
                VehiculoPorZonaDto zonaDto = new VehiculoPorZonaDto();

                zonaDto.setCantidad( ((BigInteger)zona[0]).intValue());
                zonaDto.setNombreZona((String)zona[1]);
                result.add(zonaDto);
            }

            return result;
        }


        /**
         * Permite convertir un String en fecha (Date).
         * @param fecha Cadena de fecha dd/MM/yyyy
         * @return Objeto Date
         */
        public static Date parseFechaToDate(String fecha)
        {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date fechaDate = null;
            try {
                fechaDate = formato.parse(fecha);
            } catch (ParseException ex) {
                System.out.println(ex);
            }
            return fechaDate;
        }


        /**
         * Permite convertir una fecha Date en String.
         * @param fecha Date (dd/MM/yyyy)
         * @return Objeto String
         */
        public static String parseFechaToString(Date fecha){
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            String fechaDate = null;
            fechaDate = formato.format(fecha);

            return fechaDate;
        }
    }

}
