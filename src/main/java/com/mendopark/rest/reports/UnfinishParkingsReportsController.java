package com.mendopark.rest.reports;


import com.mendopark.crud.reports.ReportSExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.reports.VehiculoPorZonaDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/UnfinishParkingsReports")
public class UnfinishParkingsReportsController extends SController {

    @EJB
    protected ReportSExpert expert;

    public static final String RESOURCE_NAME = "Vehículos en Horario Límite";

    private Resource resource = null;


    @Path("/{rangoFecha}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCantidadVehiculosHorarioLimite(@PathParam("rangoFecha") String rangoFecha, Long idCliente) {
        try {
            VehiculoPorZonaDto dto = new VehiculoPorZonaDto();
            dto.setFecha(rangoFecha);
            dto.setIdCliente(idCliente);
            System.out.println("rangoFecha " + rangoFecha);

            return Response.ok().entity(VehiculoPorZonaDto.Factory.getDtosFromVehiculo(expert.getCantidadVehiculosHorarioLimite(dto))).build();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Response.status(500).entity(false).build();
        }
    }

    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    @Override
    public Resource getResource() {
        if(resource == null){
            setResource(findResource(RESOURCE_NAME));
        }
        return resource;
    }

    @Override
    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
