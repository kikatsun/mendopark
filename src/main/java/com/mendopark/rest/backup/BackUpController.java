
package com.mendopark.rest.backup;

import com.mendopark.crud.backup.BackUpSExpert;
import com.mendopark.crud.day.DaySExpert;
import com.mendopark.model.security.Resource;
import com.mendopark.rest.SController;
import com.mendopark.rest.day.DayDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
@Path("/BackUp")
public class BackUpController extends SController {

	@EJB
	private BackUpSExpert expert;

	public static final String RESOURCE_NAME = "Backup de la Base de Datos";

	private Resource resource = null;

	@Path("/backup")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response realizarBackUp() {
		try {
			return Response.ok().entity(BackUpDto.Factory.getDtoFromBackUp(expert.generarBackUpBD())).build();
		} catch (Exception e) {
			return Response.status(500).entity(false).build();
		}
	}

	@Override
	public String getResourceName() {
		return RESOURCE_NAME;
	}

	@Override
	public Resource getResource() {
		if(resource == null){
			setResource(findResource(RESOURCE_NAME));
		}
		return resource;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}
}
