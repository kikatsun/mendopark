
package com.mendopark.rest.backup;

import com.mendopark.model.Client;
import com.mendopark.model.Day;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BackUpDto {

	private Integer copiaSeguridad;

	public Integer getCopiaSeguridad() {
		return copiaSeguridad;
	}

	public void setCopiaSeguridad(Integer copiaSeguridad) {
		this.copiaSeguridad = copiaSeguridad;
	}


	public static class Factory {

		public static BackUpDto getDtoFromBackUp(Integer copiaResguardo) {
			BackUpDto result = new BackUpDto();

			result.setCopiaSeguridad(copiaResguardo);

			return result;
		}


	}

}
