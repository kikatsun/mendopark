package com.mendopark.application.security.authorization;

import com.mendopark.model.Session;
import com.mendopark.model.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MEndoparkAuthorizationDecoderTest {

    private static MEndoparkAuthorizationDecoder mEndoparkAuthorizationDecoder;
    private final String expectedUserName = "erick";
    private final String expectedUserPassword = "qwerty";

    @BeforeClass
    public static void init() {
        mEndoparkAuthorizationDecoder = new MEndoparkAuthorizationDecoder();
    }

    @Test
    public void getSessionFromAuthorizationHeaderTest () {
        getSessionFromAuthorizationHeaderTest("Basic ZXJpY2s6cXdlcnR5");
        getSessionFromAuthorizationHeaderTest("basic ZXJpY2s6cXdlcnR5");
        getSessionFromAuthorizationHeaderTest("BASIC ZXJpY2s6cXdlcnR5");
        getSessionFromAuthorizationHeaderTest("BaSIc ZXJpY2s6cXdlcnR5");
        getSessionFromAuthorizationHeaderTest(" basic ZXJpY2s6cXdlcnR5 ");
        getSessionFromAuthorizationHeaderTest("basic     ZXJpY2s6cXdlcnR5  ");
    }

    private void getSessionFromAuthorizationHeaderTest (String authorizationHeader) {
        Session session = mEndoparkAuthorizationDecoder.getSessionFromAuthorizationHeader(authorizationHeader);
        evaluateNullPointerOnSession(session);
        evaluateUser(session.getUserSession());
    }

    private void evaluateNullPointerOnSession (Session session) {
        Assert.assertTrue(session != null);
        Assert.assertTrue(session.getUserSession() != null);
    }

    private void evaluateUser (User user) {
        Assert.assertEquals(expectedUserName, user.getName());
        Assert.assertEquals(expectedUserPassword, user.getPassword());
    }

}
